# Spatial scheme properties
1			  # Index of the scheme
1 B        	  # Scheme properties (see doc for more details)
Limiter_1	  # Additives list Index of limiter (0=None)

# Fluid selector properties
3	          		  # Type of fluid selector
1 B	          		  # Fluid spotter properties
0					  # Type of redistancing and its properties (0=None, refer to the documentation of the selected redistancing routine for the wished properties)

# Quadrature properties (0: Quadpy (write the index of the quadrature scheme and its properties after: 1 1 or 1 2 etc)    1: Custom)
1 | 1			# Spatial scheme quadrature choice (if not required by the scheme, the value will be ignored but the line cannot be blank)
1 | 1	        # Level set scheme quadrature choice

# Temporal scheme properties
1			# Time scheme
2 2			# Properties (RK order, Dec order, as many integers as needed, in the order required by the scheme (see docs))
0.2 	  	# CFL
1			# Tmax, in seconds

# Export properties
m			# Verbose mode (n=none, m=minimal, d=debug)
0.05		# Time export intervals
1		  	# 1: MeshFree light export, 0: Full export with mesh structure
