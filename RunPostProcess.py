#***************************************************************************
# Copyright (C) 2020 Élise Le Mélédo
# This file is part of LESARD.
#
# LESARD is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LESARD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LESARD.  If not, see <https://www.gnu.org/licenses/>.
#****************************************************************************

"""

The file :code:`RunPostProcess.py` provides a simple interface to post-process a previously exported solution. To access the solution, one needs

    - the problem's name
    - the mesh's resolution that has been used to compute the solution
    - the solver settings that has been used to compute the solution

|

The solution will be looked for in the folder :code:`Solutions/$ProblemDefinition_$SettingsChoice/Res$MeshResolution/RawResults`, where the $ indicates that
the associated variables names are used. Adapt the following lines of the script to your case.
If no corresponding solution folder is found, the script is aborting with an explicit error message.

.. code::

    # Parameters to load the solution to an advection-translation problem
    # on a coarse mesh from a First order Galerkin solver
    ProblemDefinition = "Advection_Translation_1Fluid"
    SettingsChoice    = "FirstOrderGalerkin"
    MeshResolution    = 20

|

Select then the type of postprocessing routine you would like to execute by commenting or uncommenting the
following lines.

.. code::

    # Plotting the solution's evolution in plotly considering all the exported timestamps
    PostProcess.Plots_AnimationsPlotly(ProblemDefinition, SettingsChoice, MeshResolution)

    # Plotting graphics at given time steps. Furnish either the list of time step to export the plots
    # at (should match the exported times, look within the folder before hand as only 7 digits are considered in the export file name)
    # or the keyword "all" for which each exported result will lead to a plot.
    PostProcess.Plots_StaticMatplotlib(ProblemDefinition, SettingsChoice, "all", MeshResolution)
    PostProcess.Plots_StaticPlotly(ProblemDefinition, SettingsChoice, [0.0, 1.179924], MeshResolution)

All those routine are gathering predefined sets of plotting functions whose list is available under the
routine documentation in the :ref:`TechnicalManual` under the rubric "Automatic post-processing routines".
Feel free to add other pre-defined plotting-routine wrap-up in the file :code:`SourceCode/PostProcess.py` at your wish.

|

Running the postprocessing tasks is then done as simply by the two lines

.. code::

    $ python RunPostProcess.py

|
|

.. note::

    - | A debugging routine is also available, plotting raw information graphically

      | :code:`PostProcess.Plot_DebugMatplotlib(ProblemDefinition, SettingsChoice, ExplicitTimeStampList, MeshResolution)`

    - Make sure that you are using python3, otherwise some module-depency may fail (in particular)

"""

# ====================================================
#  Preliminary imports                               #
# ====================================================
import SourceCode.PostProcess as PostProcess


# ====================================================
#  PostProcessing wrapper                            #
# ====================================================
if __name__ == '__main__':
    # Point to the problem you would like to solve, and define how you would like to tackle it
    ProblemDefinition = "Advection_Translation_1Fluid_SmallDomain"
    SettingsChoice    = "SecondOrderGalerkin"
    MeshResolution    = 20

    # Conversion tasks
    PostProcess.Results_ConvertToVTK(ProblemDefinition, SettingsChoice, "all", MeshResolution)

    # Error analysis tasks
    #PostProcess.Compute_Errors(ProblemDefinition, SettingsChoice, [1], ["L2norm", "L1norm", "LInfnorm"])

    # Post - processing tasks
    #PostProcess.Plot_DebugMatplotlib(ProblemDefinition, SettingsChoice, [0.0], MeshResolution)
    #PostProcess.Plots_AnimationsPlotly(ProblemDefinition, SettingsChoice, MeshResolution)
    #PostProcess.Plots_StaticMatplotlib(ProblemDefinition, SettingsChoice, [0], MeshResolution)
    #PostProcess.Plots_StaticSchlieren(ProblemDefinition, SettingsChoice, "all", MeshResolution)
    #PostProcess.Plots_StaticPlotly(ProblemDefinition, SettingsChoice, [0.0], MeshResolution)
