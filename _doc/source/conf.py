# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
sys.path.insert(0, os.path.abspath('../../..'))
sys.path.insert(0, os.path.abspath('../../../3_LESARD'))
sys.path.insert(0, os.path.abspath('../../../3_LESARD'))
sys.path.insert(0, os.path.abspath('../../../3_LESARD/SourceCode'))
sys.path.insert(0, os.path.abspath('../../../3_LESARD/LocalLib'))
sys.path.insert(0, os.path.abspath('../../../3_LESARD/LocalLib/PythonicHacks'))
sys.path.insert(0, os.path.abspath('../../../3_LESARD/LocalLib/Quadrature'))
sys.path.insert(0, os.path.abspath('../../../3_LESARD/PredefinedMeshes'))
sys.path.insert(0, os.path.abspath('../../../3_LESARD/PredefinedProblems'))
sys.path.insert(0, os.path.abspath('../../../3_LESARD/PredefinedSettings'))
sys.path.insert(0, os.path.abspath('../../../3_LESARD/Solutions'))
sys.path.insert(0, os.path.abspath('../../../3_LESARD/SourceCode/Meshing'))
sys.path.insert(0, os.path.abspath('../../../3_LESARD/SourceCode/PostProcessing'))

sys.path.insert(0, os.path.abspath('../../../3_LESARD/SourceCode/ProblemDefinition'))
sys.path.insert(0, os.path.abspath('../../../3_LESARD/SourceCode/ProblemDefinition/GoverningEquations'))
sys.path.insert(0, os.path.abspath('../../../3_LESARD/SourceCode/ProblemDefinition/BoundaryConditions'))

sys.path.insert(0, os.path.abspath('../../../3_LESARD/SourceCode/TemporalSchemes/TimeStepping'))
sys.path.insert(0, os.path.abspath('../../../3_LESARD/SourceCode/SpatialSchemes'))
sys.path.insert(0, os.path.abspath('../../../3_LESARD/SourceCode/SpatialSchemes/Limiters'))
sys.path.insert(0, os.path.abspath('../../../3_LESARD/SourceCode/_Solver/SpatialSchemes/QuadratureRules'))
sys.path.insert(0, os.path.abspath('../../../3_LESARD/SourceCode/_Solver/SpatialSchemes'))
sys.path.insert(0, os.path.abspath('../../../3_LESARD/SourceCode/_Solver'))
sys.path.insert(0, os.path.abspath('../../../3_LESARD/Templates'))
sys.path.insert(0, os.path.abspath('../3_LESARD'))

# -- Project information -----------------------------------------------------

project = 'LESARD'
copyright = '2020 Élise Le Mélédo'
author = 'Élise Le Mlélédo'
license = "GNU GPLv3"
# The full version, including alpha/beta/rc tags
release = 'v3'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = ['sphinx.ext.autodoc', 'sphinx.ext.autosummary', 'sphinx.ext.napoleon','sphinx.ext.inheritance_diagram']

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []

autoclass_content = "both"  # include both class docstring and __init__
autodoc_default_flags = [
        # Make sure that any autodoc declarations show the right members
       "members","inherited-members","private-members","show-inheritance"]

autosummary_generate = True  # Make _autosummary files and include them
napoleon_numpy_docstring = False  # Force consistency, leave only Google
napoleon_use_rtype = True  # More legible

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'nervproject'#'sphinx_materialdesign_theme'nervproject karma_sphinx_theme
#html_theme_path = ["/home/b/elemel/.local/lib/python3.6/site-packages/sphinx_materialdesign_theme"]
# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']
master_doc = 'index'
#def setup(app)#app.add_stylesheet( "css/functions.css" )
