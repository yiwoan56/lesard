.. _RunFirstProblem:

Step 1: Run a predefined problem
================================

|
|

This tutorial shows you step by step how to run and post-process a problem.
Please follow it line by line. The covered topics are

.. contents::
    :local:
    :depth: 1

|


Discover the files to modify at your ease
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Enter the "LESARD" environment

.. code::

    cd LESARD

You should see the following folders and documents

.. code::

    CleanCache.sh  LocalLib   PredefinedTests    RunProblem.py  SourceCode
    Pathes.py  RunPostProcess.py  Solutions      Templates

For running a predefined problem, the only files to consider are the ".py" and optionally the "CleanCache.sh" files.

    - The :code:`Pathes.py` is a file that allows you to edit the solution export folder and the problems location, should you like to store your own defined problems elwhere than the folder of the initial code's release
    - The :code:`RunProblem.py` is a script that allows you to automatically run a solver on a problem
    - The :code:`RunPostProcess.py` is a script that allows you to postprocess a previously exported solution semi-automatically
    - The :code:`CleanCache.sh` is a bash script that helps to remove the pythonic spurious files ".pyc" and folder "_pycache_" created at runtime

|
|


Set up the working environment
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To solve a problem, the code looks by default for predefined problems in the folder :code:`PredefinedTests/PredefinedProblems`,
for predefined meshes (automatically selected upon the choice of a problem choice) in the folder :code:`PredefinedTests/PredefinedMeshes`,
and the settings files (specifying which solver and properties to use for tackling the problem) in :code:`PredefinedTests/PredefinedSettings`.
The solution and postprocessing plots will be exported periodically in the folder :code:`Solution/$TestName_$SettingsName`.

|

Should you like to have such files defined elsewhere than in the root folder of the code, copy paste them in the location of your choice and
adapt accordingly the pathes in the file :code:`Pathes.py`. More precisely, edit the following lines.


.. code::

    $ vi Pathes.py

    # Edit the following lines
    MeshPath     = os.path.join(Dir, "PredefinedTests", "PredefinedMeshes")
    SettingPath  = os.path.join(Dir, "PredefinedTests", "PredefinedSettings")
    ProblemPath  = os.path.join(Dir, "PredefinedTests", "PredefinedProblems")
    SolutionPath = os.path.join(Dir, "Solutions")

|
|
|

Running a problem:
^^^^^^^^^^^^^^^^^^

Tackling numerically a problem is seen as a triplet (problem definition, settings to apply, mesh resolution).
To list the problems that are available, query the folder :code:`PredefinedTests/PredefinedProblems`, and
analogously for knowing the settings that are available.

.. code::

    $ ls PredefinedTests/PredefinedProblems
    Advection_Rotation_1Fluid.py Advection_Translation_1Fluid.py Advection_Translation_Bump_2Fluids.py
    Advection_Translation_Cste_2Fluids.py Advection_Translation_Obstacle_2Fluids.py Euler_SODCste_2Fluid.py
    Euler_SOD_1Fluid.py Euler_SOD_2Fluid.py Euler_StationaryVortex_1Fluid.py Test_Advection_Translation_2Fluids_FalseSystem.py

    $ ls PredefinedTests/PredefinedSettings
    FirstOrderGalerkin.txt  SecondOrderGalerkin.txt

To know the available mesh resolutions, we have to know the mesh associated to the problem before quering
the folder :code:`PredefinedTests/PredefinedMeshes`.
For this tutorial, let us consider the problem "Advection_Translation_Bump_2Fluids" with the settings "SecondOrderGalerkin".

.. code::

    # Run in Bash the following to find the mesh associated to the problem
    $ Problem=PredefinedTests.PredefinedProblems.Advection_Translation_Bump_2Fluids
    $ MeshName=$(python -c "import $Problem as Problem;print(Problem.ProblemData().MeshName)")

    # The name of the associated mesh is then
    $ echo $MeshName
    BigSimpleRectangle

    # Then list the available resolutions with
    $ ls PredefinedTests/PredefinedMeshes/$MeshName
    BigSimpleRectangle_o1_CG_res_20.pyMsh  BigSimpleRectangle_o1_CG_res_50.pyMsh
    BigSimpleRectangle_o1_CG_res_40.pyMsh  BigSimpleRectangle_o2_CG_res_20.pyMsh

As in our case we asked for a second order solver, we have to pay attention to the files containing the "o2"
substring. We can then only use either "20" for the mesh resolution (when there is choice the higher the finer). For this
tutorial, we work with the resolution "20".

|

To run the problem "Advection_Translation_Bump_2Fluids" with the solver "SecondOrderGalerkin" on a mesh of
resolution  "30", adapt the following lines of the running file :code:`RunProblem.py` (see :ref:`Run` for more details).

.. code::

    $ vi RunProblem.py

    # Modify the following lines to
    ProblemDefinition = "Advection_Translation_Bump_2Fluids"
    SettingsChoice    = "SecondOrderGalerkin"
    MeshResolution    = 20

|

The problem may then be run as simply as

.. code::

    $ python RunProblem.py

    -------------------------------------
    Performing the pre-computations tasks
    -------------------------------------
    Reading the problem specifications
    --> Done.
    Cleaning the export environment
    --> Done.
    Loading the associated mesh
    --> Done.
    Initialising the problem
    --> Setting the problem up
    --> Setting the solver up
    --> Initialising the solution
    --> Done.
    Generating the mass lumping coefficients
    --> Done.
    Initialising the Fluid selector
    --> Done.

    -------------------------------------
    Starting time dependent computations
    -------------------------------------
    Computing for the time 0
    Variable 0(h):    min:0.0  	max: 1.0931224807703024

    .
    .
    .
    .
    .

|
|

The results will be automatically exported periodically in the folder :code:`Solutions/Advection_Translation_Bump_2Fluids_SecondOrderGalerkin/Res20`.

.. code::

    $ ls Solutions/Advection_Translation_Bump_2Fluids_SecondOrderGalerkin/Res20/RawResults
    Solution_t0_000000.sol  Solution_t0_035871.sol

|

.. warning::

    - Running a solver over a problem and a mesh will erase any previous result stored within :code:`Solution/$TestName_$SettingsName`. If you want to keep the old results and start again from the last previously exported timestamp, use :code:`python RunProblem.py restart` instead.
    - Make sure to run with :code:`python3`.

|

.. note::

    - Aborting a running code does not erase the exported solution.
    - To convert a .sol exported solution into .vtk, use the furnished postprocessing routine (see below).

|
|
|

PostProcess a problem:
^^^^^^^^^^^^^^^^^^^^^^

Exporting the plots associated to the solution is done in a similar way through the helper file :code:`RunProblem.py`.
Adapt the following lines to match the previously exported solution.

.. code::

    $ vi RunProblem.py

    # Modify the following lines to
    ProblemDefinition = "Advection_Translation_Bump_2Fluids"
    SettingsChoice    = "SecondOrderGalerkin"
    MeshResolution    = 20

|

Edit then the following lines depending on the type of plots/postprocessing you like to export and the
available time points that got exported in the solution folder (see :ref:`Run` for more details on those function
formats).

.. code::

    PostProcess.Plot_DebugMatplotlib(ProblemDefinition, SettingsChoice, [0.0], MeshResolution)
    PostProcess.Plots_AnimationsPlotly(ProblemDefinition, SettingsChoice, MeshResolution)
    PostProcess.Plots_StaticMatplotlib(ProblemDefinition, SettingsChoice, "all", MeshResolution)
    PostProcess.Plots_StaticPlotly(ProblemDefinition, SettingsChoice, [0.0], MeshResolution)

|
|
|

Convert a solution to vtk
^^^^^^^^^^^^^^^^^^^^^^^^^

Converting the solution can also be done through the helper file :code:`RunProblem.py`.
Adapt the following lines to match the previously exported solution.

.. code::

    $ vi RunProblem.py

    # Modify the following lines to
    ProblemDefinition = "Advection_Translation_Bump_2Fluids"
    SettingsChoice    = "SecondOrderGalerkin"
    MeshResolution    = 20

|

Uncomment then the following line and adapt the "all" parameter to the available time points of your choice from those
that have been previously exported in the solution folder (see :ref:`Run` for more details on those function
formats).

.. code::

  # Conversion tasks
  PostProcess.Results_ConvertToVTK(ProblemDefinition, SettingsChoice, "all", MeshResolution)

|

.. note::

  The batched vtk solution files can then be imported in most of visualisation softwares as Paraview or Visit.
