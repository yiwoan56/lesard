.. _LinearAdvectionRotation:



Linear advection (Rotation)
==========================================

|
|

.. automodule:: 3_LESARD.SourceCode.ProblemDefinition.GoverningEquations.LinearAdvectionRotation
   :member-order: bysource

|

.. contents::
    :local:

|

----------------------------

Governing equations
----------------------------

|

.. autoclass:: 3_LESARD.SourceCode.ProblemDefinition.GoverningEquations.LinearAdvectionRotation.Equations
    :members:
    :member-order: bysource

|
|
|

----------------------------

Equations of state
----------------------------

|

.. autoclass:: 3_LESARD.SourceCode.ProblemDefinition.GoverningEquations.LinearAdvectionRotation.EOS
    :members:
    :member-order: bysource

|
|
|

----------------------------

Initial conditions
----------------------------

|

.. autoclass:: 3_LESARD.SourceCode.ProblemDefinition.GoverningEquations.LinearAdvectionRotation.InitialConditions
    :members:
    :member-order: bysource

|
