.. _Tools:

LESARD: Tools
===========

|

The local library :code:`LocalLib` contains pythonic hacks that are gathered as simple functions externally accessible.

|

.. rubric:: Page navigation

.. contents::
    :local:
    :depth: 1

|
|

--------------------------------------------

Simple pythonic hacks
--------------------------------------------

|

.. automodule:: 3_LESARD.LocalLib.ReversedFunctools
   :members:

|
|

--------------------------------------------

Tools handling quadrature points (external library)
--------------------------------------------------------

|

.. automodule:: 3_LESARD.LocalLib.LobattoQuadratureNodes
   :members:

|
|

--------------------------------------------

Tools apparented to Barycentric coordinates
--------------------------------------------
  
|

.. automodule:: 3_LESARD.LocalLib.BarycentricTools
   :members:

|
|

--------------------------------------------

Tools apparented to Barycentric basis functions
-----------------------------------------------

|

.. automodule:: 3_LESARD.LocalLib.BarycentricBasisFunctions
   :members:
