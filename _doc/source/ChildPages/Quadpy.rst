.. _QuadPy:

The QuadPy module
==================

|
|

The module :code:`_SourceCode/QuadratureRules/QuadPy.py` interfaces the routines of quadpy that can be used in
the LESARD's code framework so that a quadrature scheme can be selected by its index and given properties.
It contains one class for each quadrature type, each mapping on instance initialisation to the scheme of the
Quadpy library.

|
|


----------------------------

Module description
---------------------------

.. automodule::  3_LESARD.SourceCode._Solver.QuadratureRules.QuadPy

|
|

------------------------------

Inner quadrature interfacing
-----------------------------

|

.. autoclass::  3_LESARD.SourceCode._Solver.QuadratureRules.QuadPy.InnerQuadrature
   :members:


|
|

---------------------------------

Boundary quadrature interfacing
---------------------------------

|

.. autoclass::  3_LESARD.SourceCode._Solver.QuadratureRules.QuadPy.BoundaryQuadrature
   :members:

|
