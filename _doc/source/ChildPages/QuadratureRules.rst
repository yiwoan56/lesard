.. _QuadratureRules:

Quadrature Rules
================

|
|

The library :code:`_SourceCode/QuadratureRules` contains a list of modules defining each inner and
boundary quadrature rules to use when computing the new residuals within the spatial schemes.
It also contains the module :code:`_SourceCode/Quadratures` that interfaces the technical modules
with the user wishes and defines a wrapper class containing an inner and inner quadrature rule of the
user choice.

|

.. note::

  Using an artificial wrapper class allows to select quadrature rules issued from different modules or libraries
  for the boundary and inner quadrature within a same spatial scheme.

|

.. rubric:: Available quadrature modules (each module may contain several qudrature schemes)

1. :ref:`QuadPy`
2. :ref:`HandBased`

|
|


----------------------------------

Quadratures wrapper class
----------------------------------

|

.. automodule::  3_LESARD.SourceCode._Solver.QuadratureRules.Quadratures
   :members:
   :imported-members:

|
|
