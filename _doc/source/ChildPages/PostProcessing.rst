LESARD: Postprocessing
====================


|
|
|

.. rubric:: Page navigation

.. contents::
    :local:
    :depth: 1

|
|
    
-----------------------------------

Automatic post-processing routines
-----------------------------------

|

.. automodule:: 3_LESARD.SourceCode.PostProcess
   :members:
   :member-order: bysource


|
|

------------------------------
   
User information
------------------------------

|

.. automodule:: 3_LESARD.SourceCode.PostProcessing.Printouts
   :members:
   :member-order: bysource
   
|
|

------------------------------
   
Plotly plots
------------------------------

|
  
.. automodule:: 3_LESARD.SourceCode.PostProcessing.Plots_Plotly
   :members:
   :member-order: bysource
   
|
|


------------------------------

Matplotlib plots
------------------------------

|

.. automodule:: 3_LESARD.SourceCode.PostProcessing.Plots_Matplotlib
   :members:
   :member-order: bysource
   
|
|

------------------------------

Exports
------------------------------

|

.. automodule:: 3_LESARD.SourceCode.PostProcessing.Exports
   :members:
   :member-order: bysource
