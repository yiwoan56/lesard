.. _LinearAdvectionTranslation:

Linear advection (Translation)
==========================================

|
|

.. automodule:: 3_LESARD.SourceCode.ProblemDefinition.GoverningEquations.LinearAdvectionTranslation
   :member-order: bysource

|

.. contents::
    :local:

|

----------------------------

Governing equations
----------------------------

|

.. autoclass:: 3_LESARD.SourceCode.ProblemDefinition.GoverningEquations.LinearAdvectionTranslation.Equations
    :members:
    :member-order: bysource

|
|
|

----------------------------

Equations of state
----------------------------

|

.. autoclass:: 3_LESARD.SourceCode.ProblemDefinition.GoverningEquations.LinearAdvectionTranslation.EOS
    :members:
    :member-order: bysource

|
|
|

----------------------------

Initial conditions
----------------------------

|

.. autoclass:: 3_LESARD.SourceCode.ProblemDefinition.GoverningEquations.LinearAdvectionTranslation.InitialConditions
    :members:
    :member-order: bysource

|
