.. _Settings:

LESARD: Settings
--------------

|
|

All the settings files (.txt) should be stored in the folder :code:`PredefinedTests/PredefinedSettings` (or equivalent
if the path has been changed in the :code:`Pathes.py`) file. A typical setting file reads as follows, and the
associated template file can be found in the folder :code:`Templates/Settings.txt` .

.. code::

      # Spatial scheme properties
      1			                              # Index of the scheme (see Mappers.py to know the indices meaning)
      1 B	                                # Scheme properties (see the scheme's doc for more details, if any, the order should be first)
      Filtering_1<#1_1##1_1#>+Limiter_1 	# Additives list Index of limiter (0=None, see the docs for the string explanation)
      2	                                  # Type of fluid spotter (1=LS)
      1 B	                                # Fluid spotter properties

      # Quadrature properties (0: Quadpy (write the index of the quadrature scheme and its properties after: 1 1 or 1 2 etc)    1: Custom)
      2 4 9  | 1			    # Spatial scheme quadrature choice (if not required by the scheme, the value will be ignored but the line cannot be blank)
      1      | 2 6 3		  # Level set scheme quadrature choice

      # Temporal scheme properties
      1		  	# Time scheme (see Mappers.py to know the indices meaning)
      2 2 		# Properties (RK order, Dec order, as many integers as needed, in the order required by the scheme (see the chosen time scheme's docs))
      0.4 	  # CFL
      3		    # Tmax, in seconds

      # Export properties
      m		   	# Verbose mode (n=none, m=minimal, d=debug) (yet inactive)
      0.05		# Time export intervals
      1			  # 1: MeshFree light export, 0: Full export with mesh structure


See the :ref:`UserManual` for a step-by-step explanation of the format.
