.. _SpatialModifiers:

Spatial Modifiers
==================

|



Modifications applied to the result of a classical scheme such as limiter or filters have to be
implemented in :code:`_Solver/SpatialModifiers` as a module. The modifier has to be implemented within
a class whose name should match its type (e.g "Limiter" or "Filtering"), and the main method that
is intended for modifying the residuals should be named :code:`Mollify` in any case.

|

From a user perspective, it is possible to apply several mollifiers to the original residual by specifying
the wished sequence as a litteral string. The input will then be interpreted by the class Amendements upon
the instance creation. In practice, the input is parsed by the method  :code:`ExtractSequence` and
converted to the actual modifyer by :code:`StringToResu`.

|
|

.. rubric:: Page navigation

.. contents::
    :local:
    :depth: 2


|
|


----------------------------------

Usage
----------------------------------

|

Assuming one has computed a residual :code:`resu` at some time setp :code:`n`, and likes to modify it by the means
of some limiter or filter/streamlines dissipator, the wish of the amendement sequence to use and the related parameter
has to be given in the settings file.

|

.. rubric:: Specifying the sequence of wished modification of the residual

In the settings file (see the :ref:`UserManual`) indicate for the amendments the
composition of modifications in the natural way showed below, where :code:`$Limit` and :code:`$Filter` are the indices of the desired mollifiers in the mapping file :code:`Mappers.py`,
the values between the symbols :code:`<>` are the arguments to give to the desired mollifiers (see below). Check each mollifier's definition
to know the required ones, if any. If no parameter is required, writing :code:`<>` is not neccessary.


.. code::

    # No limiter
    0

    # A single modification
    Limiter_$Limit

    # A single modification that should be added to the original residual
    Original+Filtering_$Filter

    # A single modification with argument to give to the mollifier
    Limiter_$Limit<listof,parameters>

    # Or a combination
    Limiter_$Limit+Filtering_$Filter<some,parameters,topass>

|

More complicated composition can be performed in a similar way. In those cases, only the most nested mollifiers will be applied
to the original residuals (those are the terms not followed by parenthesis).

.. code::

    Limiter_3<2>(Limiter_1<1.2>(Original+Filtering_2)+Filtering_1<p>(Limiter_1<2.2>))*Limiter_2

|

.. warning::

    - There should be NO spurious spaces in the definition of the modification strings.

|
|

.. rubric:: Specifying arguments

The list of arguments to pass to the mollifier should be given between the symbols :code:`<>`, in the order required
by the selected mollifier (see below for their definition). The types of the arguments (float, integer, string, etc) may be different, but will be
passed to the related function as strings.

|

By example, if one limiter takes a float and a string as arguments, one would write in the parameter file

.. code:: html

  Limiter_1<14.2,thestring>

|

.. note::

  When the required argument is of type :code:`Quadrature`, a specific writing format should be used. The mapping
  from the argument to the asked quadrature instance is made automatically during the argument parsing in the :code:`StringToResu`
  routine, (converting the string to quadrature there rather than in the modifier class avoids an import loop at the technical level).

|

To allow this automatic mapping, a specific format for the quadrature argument has to be kept:
it should be

.. code:: html

  #InnerQuadratureSettings##BoundaryQuadratureSettings#

where the parameters :code:`InnerQuadratureSettings` and :code:`BoundaryQuadratureSettings` have to be given as in :ref:`DesignSettings`,
but with underscores separating the given properties instead of spaces. By example, if one would like to select
the 5th scheme of Williams Shunn Jameson from the Quadpy library within the elements and a hand base quadrature on the boundary,
the respective quadrature settings for the filtering method would read

.. code::

    # InnerQuadratureSettings
    2_15_5

    # BoundaryQuadratureSettings
    1

and one would write in the parameter file

.. code:: html

  Filtering_1<#2_15_5##1#,other,parameters>

|

.. note ::

  The full list of the available quadrature is given at :ref:`QuadratureRules`.

|
|
|

----------------------------------

Modifiers sequence generation
----------------------------------

|

.. automodule::  3_LESARD.SourceCode._Solver.SpatialModifiers.SpatialAmendements
   :members:
   :member-order: bysource

|
|

----------------------------------

Available limiters
----------------------------------

|

.. automodule::  3_LESARD.SourceCode._Solver.SpatialModifiers.Limiter_Psi
   :members:
   :member-order: bysource


|
|

---------------------

Available filters
---------------------


|

.. automodule::  3_LESARD.SourceCode._Solver.SpatialModifiers.Filtering_Streamline
   :members:
   :member-order: bysource


|
|
