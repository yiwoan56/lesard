.. _GoverningEquations:

LESARD: Fluid models and Governing equations
==========================================

|
|


The modules :code:`FluidModels` and those stored in the :code:`ProblemDefinition/GoverningEquations` folder take care of the fluids' motion.
Depending on the fluids' nature (and thus parameters) and their selected equation of state (defined in the Problem’s module),
they associate the right fluid's properties with the selected governing equation to determine the fluid, time and space-dependent system's flux.

|

.. rubric:: Page navigation

.. contents::
    :local:
    :depth: 1

|

-------------------------------------

Pre-defined equations and models
-------------------------------------

|

Pre-defined governing equations and fluid models can be found in :code:`ProblemDefinition/GoverningEquations/*.py` and :code:`ProblemDefinition/FluidModels.py`, respectively.
Their respective documentation can be found according to the following table. Generic information for creating new
ones are given below.

.. list-table::

    *
        - *Predefined governing equations*
        - *Predefined fluid models*

    *
        -
            - :ref:`EulerEquations`
            - :ref:`LinearAdvectionRotation`
            - :ref:`LinearAdvectionTranslation`

        -
            - :ref:`FluidModels`

|

.. note::

    Templates for creating new governing equations and fluid models can be found in :code:`Templates` folder.

|
|
|

-------------------

Governing equations
-------------------

|

All the fluids present in the computational domain are governed by a same equation selected by the user
through an index mapping to the module :code:`ProblemDefinition/GoverningEquations/$TheEquationSetName.py`,
named accordingly the considered equation's set.

|

Any governing equation is defined as a module itself containing the classes:

    - **Equations**,         specifying the fluid's dynamic
    - **EOS**,               giving the equation of state to use for each fluid, each selected by an index at the instance creation selecting the one to use according to the user's wishes, among possibly several choices
    - **InitialConditions**, giving the initial conditions to use on each subdomain, each selected by an index at the instance creation selecting the one to use according to the user's wishes, among possibly several choices

It also requires a specific fluid model to interface with the fluid's properties (i.e. which properties of the fluid are involved in the equation).
The fluid model to be used is specified directly within the governing equation's module, by specifying the name of the model to use in the field :code:`FluidModel`.
At runtime, the fluid's properties (selected from the evolving fluid's location in the computational domain) will then be injected in the relevant fluid model and transfered to the governing equations.

|

A :code:`Equations` class having the minimal required method and fields to be able to run all the numerical schemes
has the following structure. Further routines and fields can be added depending on the internal needs.

|

.. autoclass:: 3_LESARD.Templates.Template_Equations.Equations
   :members:
   :member-order: bysource

|
|

An :code:`EOS` class having the minimal required fields to be able to ensure the compatibility with the fluid's model and the governing equation has the following structure.

|

.. autoclass:: 3_LESARD.Templates.Template_Equations.EOS
   :members:
   :member-order: bysource

|
|

An :code:`InitialConditions` class having the minimal required fields to be able to initialise the solution on every subdomain has the following structure .

|

.. autoclass:: 3_LESARD.Templates.Template_Equations.InitialConditions
   :members:
   :member-order: bysource


|
|
|

--------------

Fluid's models
--------------

|

The fluid models are defined as classes in a single module :code:`ProblemDefinition.FluidModels`.
Their fields extracts the relevant properties of given fluids to format them accordingly to the
fluid model's purpose (i.e. its use in one or several governing equations).  Furthermore,
each class should at least contain the field :code:`color`, for identifying the fluid when plotting.

|

A typical fluid model's class has the following structure (other can be added depending on the governing equations' needs).

|

.. autoclass:: 3_LESARD.Templates.Template_FluidModels.CustomModel
   :members:
   :member-order: bysource

|

.. note::

    - When designing a model that requires more properties than the other models already predefined, think to update the information given in the  file :code:`SourceCode/Mappers.py` accordingly, as it sets the actual fluid's properties' values for commonly used ones (oil, water, etc).

    - In the same vein, mind that the order of the arguments should match the ones prescribed in :code:`SourceCode/Mappers.py`. Should you need different properties that the ones already given for the other models, you still have to specify them as arguments, up to dropping them later for the consistency of the parameters definition when creating a fluid in the file :code:`SourceCode/Mappers.py`.


|
