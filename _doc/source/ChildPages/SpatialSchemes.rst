.. _SpatialSchemes:

Spatial Schemes
===============

|

The modules defining spatial schemes are located in the folder :code:`_Solver/SpatialSchemes`.

    - Each module defining a spatial scheme should comport a class :code:`Scheme` within which at least the methods :code:`ComputeFlux`, :code:`Iteration`, :code:`ReconstructSolutionAtVertices` are implemented.

    - Each module should also comport a class :code:`AssociatedMeshType` comporting the fields :code:`MeshType`, :code:`MeshOrder` and :code:`ControlVolumes` that will be used to select the relevant mesh and compute properly the mass lumping.

|

.. rubric:: *Already implemented schemes*

.. contents::
    :local:

|
|

-------------------------------------

Continuous galerkin + Lax-Friedrichs
-------------------------------------

|

.. automodule::  3_LESARD.SourceCode._Solver.SpatialSchemes.CG_LFX
   :members:
   :member-order: bysource

|
|

.. automodule::  3_LESARD.SourceCode._Solver.SpatialSchemes.CG
   :members:
   :member-order: bysource


|
|

.. automodule::  3_LESARD.SourceCode._Solver.SpatialSchemes.CG_Primary
   :members:
   :member-order: bysource
