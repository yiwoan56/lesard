.. _BoundaryConditions:



LESARD: Boundary conditions
=========================

|
|

The modules related to the boundary conditions are stored in :code:`SourceCode/ProblemDefinition/BoundaryConditions`. 
Each of the modules define a specific treatment to apply at the given boundary segment. 
The indexing of the boundary types is done in the :code:`Mappers.py` file, while the application of the boundary treatment
themselves is done through the routine :code:`ApplyBCSState` defined in the module :code:`SourceCode/_Solver/Solver_ApplyBCs`
(see :ref:`Solve`).

|

.. note::

    - The interface is always considered as passive, therefore no condition is applied within the computational domain.
    - The boundary conditions are applied only to tagged boundaries according to the user wishes given in the problem's definition. The degrees of freedom lying on the boundary that did not get tagged during the mesh generation process will be automatically treated as outflow.

|

A general description of the minimal structure of a boundary condition file is described below, the respective description
of each of the implemented routines is given at the bottom of the page.

|

.. rubric:: Page navigation

.. contents::
    :local:
    :depth: 1
    
|
|

-----------------------------

Boundary condition framework
-----------------------------

|

All the boundary conditions present in the computational domain are applied by a the solver routine :code:`ApplyBCSState`
after having been selected by the user through an index mapping to all the used module of the form :code:`ProblemDefinition/BoundaryConditions/$TheConditionName.py`,
named accordingly the boundary conditions that are in use on the boundary segments.

|

Any boundary condition is defined as a module itself containing the classes, whose structure should be given as follows.
    
    **TheConditionName**,         specifying the nature and the application routine of the associated condition

|

.. automodule:: 3_LESARD.Templates.Template_BoundaryConditions
   :members:
   :member-order: bysource

|
|

--------------------------------

Pre-defined boundary conditions
--------------------------------

|

.. note::

    Templates for creating new boundary condition routines can be found in :code:`Templates` folder.

|
|

.. automodule:: 3_LESARD.SourceCode.ProblemDefinition.BoundaryConditions.Wall
   :members:
   :member-order: bysource
   
|

.. automodule:: 3_LESARD.SourceCode.ProblemDefinition.BoundaryConditions.Outflow
   :members:
   :member-order: bysource
   
|

.. automodule:: 3_LESARD.SourceCode.ProblemDefinition.BoundaryConditions.Inflow
   :members:
   :member-order: bysource
   
|

.. automodule:: 3_LESARD.SourceCode.ProblemDefinition.BoundaryConditions.Dirichlet
   :members:
   :member-order: bysource
