.. _DesignYourProblem:


Step 2: Design your problem
============================


|
|

We saw in the Step 1 that a numerical problem is seen as a triplet (problem definition, settings to apply, mesh resolution),
and saw how to run such a tripplet. We explain here how to create a "problem definition" based on predefined equations'set.
For the sake of the example, we will create a two-fluids Euler equation SOD problem, each of the fluids's initial values matching
one of the two initial constant states. We will name this problem :code:`Euler_SOD_2Fluid`.

|

.. rubric:: Steps

.. contents::
    :local:
    :depth: 1

|

------------------------------

1. Create a new problem file
----------------------------

Copy and paste the template inside the folder where the predefined problems are stored.

.. code::

    cp Templates/Template_Problem.py PredefinedTests/PredefinedProblems/Euler_SOD_2Fluid.py

|

Open the file :code:`Euler_SOD_2Fluid.py` with your preferred editor.
The definition of the problem is made only by modifying the fields of the structure :code:`ProblemData`
to fit your needs. To start with, change the field :code:`ProblemID` to :code:`Euler_SOD_2Fluid`.

.. code::

    self.ProblemID  = "Euler_SOD_2Fluid"

|
|
|

------------------------------

2. Choose the computational domain
----------------------------------

|

Choose the computational domain by specifying a mesh geometry on which the problem will be solved.
This mesh geometry should match the root name of the a range of meshes (having various refinement and elements' type, etc.)
that were built from a same geometry (square, holy circle, etc). List the available geometries by

.. code::

    $ ls PredefinedTests/PredefinedMeshes
    AnnularCylinder_o2_CG_res_20.pyMsh     BigSimpleSquare_o1_CG_res_50.pyMsh               SimpleRectangle_o1_CG_res_50.pyMsh
    AnnularCylinder_o3_CG_res_20.pyMsh     BigSimpleSquare_o2_CG_res_20.pyMsh               SimpleRectangle_o2_CG_res_50.pyMsh
    BigSimpleRectangle_o1_CG_res_20.pyMsh  BigSimpleSquare_o2_CG_res_30.pyMsh               SimpleSquare_o1_CG_res_20.pyMsh
    BigSimpleRectangle_o1_CG_res_40.pyMsh  BigSimpleSquare_o2_CG_res_50.pyMsh               SimpleSquare_o1_CG_res_50.pyMsh
    BigSimpleRectangle_o1_CG_res_50.pyMsh  LShapeWithHoleAndRoundCorner_o1_CG_res_50.pyMsh  SimpleSquare_o2_CG_res_20.pyMsh
    BigSimpleRectangle_o2_CG_res_20.pyMsh  LShapeWithHoleAndRoundCorner_o2_CG_res_50.pyMsh  SimpleSquare_o2_CG_res_50.pyMsh
    BigSimpleSquare_o1_CG_res_20.pyMsh     LShapeWithHoleAndRoundCorner_o2_DG_res_10.pyMsh
    BigSimpleSquare_o1_CG_res_30.pyMsh     LShapeWithHoleAndRoundCorner_o5_DG_res_10.pyMsh

The available geometries are here "AnnularCylinder", "BigSimpleRectange", "BigSimpleSquare", "SimpleRectangle", "SimpleSquare" and
"LShapeWithHoleAndRoundCorner". Note that the available mesh types, mesh order and mesh resolutions are not homogeneous
accross the mesh geometries. Keep it in mind when writing your setting file (that will be done in the third step).

|

If the available meshes do not appeal to you, you can always create a new one with the companion code
:code:`GeneralMeshing` available on request (using mshr of fenics). In this tutorial, we will use the
geometry "BigSimpleRectangle". Edit it accordingly in the template file.

.. code::

    self.MeshName = "BigSimpleRectangle"

|
|
|

------------------------------

3. Set the governing equations
------------------------------


This code only allows a single governing equation within the hole computational domain.
To select the one to use in the problem, get the list of implemented equations and their
associated index either in the file "Mappers.py" in the :code:`SourceCode`, or by using the
technical documentation of the class  :code:`Governing_Equations` on the page :ref:`Mapping`.

|

In our case, we can use one of the following governing equations.

.. code::

    $ python -c "import SourceCode.Mappers as MP; print(MP.Governing_Equations.__doc__)"

    # Find the lines similar to the following
    *Implemented equations given the id*
    1. | Euler equations
    2. | Linear advection (rotation)
    3. | Linear advection (translation)

Select the wished equations (in our case "Euler equations") and its index (here 1).
Copy the index in the template file in the field

.. code::

    self.GoverningEquationsIndex = 1

|
|
|

------------------------------


4. Determine the wished fluids and their properties
------------------------------------------------------

Now that we have defined the computational domain and the governing equations, we can specify the fluids that will cohabitate
within the domain. The number of fluids is implicit by the length of the specification vectors that will be defined below.
In this example, we will consider :code:`two` fluids.
The properties to define for each fluid are: its equation of state (stiff, ideal, etc), its type (oil, water, etc.), its initial
location within the computational domain (later refered as initial subdomain).

.. note::

    The order of the fluids is important: keep the consistency of the order all along the specification vectors' definition

|

.. rubric:: Equation of sate

The available equations of state are dependent on the cosen governing equations. Get the available list
and their corresponding indices by querying the documentation of the equation in the technical
handbook (in the rubric "Pre-defined equations and models" of :ref:`GoverningEquations`)
or the equations' module itself. In our case, the available equations of state read as follows.

.. code::

    $ python -c "import SourceCode.ProblemDefinition.GoverningEquations.EulerEquations as PB; print(PB.EOS.__doc__)"

    # Find the lines
    1. | Stiff fluid
    2. | Ideal

For the sake of the example, we will consider both fluids as stiff and register them as such in the field :code:`EOS`
in the given template.

.. code::

    self.EOSEquationsIndex = [1, 1]

|
|

.. rubric:: Fluids' type

We have now to specify each type of fluid to use. To do so, the list of predefined fluid types
and their associated index is accessible either in the file "Mappers.py" in the :code:`SourceCode`,
or by using the technical documentation of the class  :code:`Fluid_Properties` on the page :ref:`Mapping`.

There, we can choose between the following.

.. code::

    $ python -c "import SourceCode.Mappers as MP; print(MP.Fluid_Properties.__doc__)"

    # Find the lines similar to the following
    *Implemented fluid types given the id*
    0. | Fluid with properties :math:`\gamma=1.5` and :math:`p_\infty=0`,   with a blue representation color
    1. | Fluid with properties :math:`\gamma=1.2` and :math:`p_\infty=0.5`, with a red representation color
    2. | Fluid with properties :math:`\gamma=0.5` and :math:`p_\infty=0.1`, with a green representation color

|

In this example, we select the fluids 0 and 2, respectively and fill accordingly the field in the given
template.

.. code::

    self.FluidIndex = [0, 2]

|

.. note::

    - You can also add freely new fluids properties in the class  :code:`Fluid_Properties`, respecting the given format.
    - There only the fluid's type is selected, that is its generic properties. The fluid's model is automatically selected according to the governing equations.

|
|

.. rubric:: Fluids' subdomain

Defining the fluid's subdomain is made with patches of geometrical shapes. Each fluid will be associated
to a patch (the maximal area covered by the fluid), and the superposition of all the patches
(the top level corresponding to the last fluid given in the lists) defines each of subdomain.
For a precise tutorial of how to define those patches, we refer to the didactic example
given in :ref:`Subdomains`. Here, we choose to define the second fluid on a circle centered at (0,0) and having a radius of 0.5.
The first fluid will be defined everywhere else in the given domain.

.. code::

    self.FluidInitialSubdomain = [(4,0,0,0.5,0.5,0.5,4,4)]

.. note::

    - The length of the field  "FluidInitialSubdomain" is the number of considered fluids minus one. The fluid defined in the first position in the given vectors will be set on the remaining area.
    - Always specify the coexisting fluids in the order that is the most suitable for the wished patching. In general, the subdomain's layout has thus to be thought first.
    - If the patching process yields to an empty subomain, the code aborts.

|
|
|

------------------------------

5. Specifying the initial conditions
------------------------------------


The available initial conditions are dependent on the cosen governing equations. Get the available list
and their corresponding indices by querying the documentation of the equation in the technical
handbook (in the rubric "Pre-defined equations and models" of :ref:`GoverningEquations`)
or the equations' module itself. In our case, the available initial conditions of state read as follows.

.. code::

    $ python -c "import SourceCode.ProblemDefinition.GoverningEquations.EulerEquations as PB; print(PB.InitialConditions.__doc__)"

    # Find the lines
    0. | ConstantState
    1. | ConstantState2
    2. | StationaryVortex
    3. | SOD
    4. | SOD_Fluid1
    5. | SOD_Fluid2
    7. | PlanarShock


In this example, we will match the two initial constant states of the SOD test-case per fluid. We then select
the initial states "SOD_Fluid1" for the fluid located outside from the centered circle and "SOD_Fluid2" for
the fluid located within the centred circle. Each tuple corresponds to the index selected above and the possible
parameters that one may want to pass to the routine. Edit the following field accordingly.

.. code::

    # Without parameters
    self.InitialConditions = [(4,), (5,)]

    # With prescribed parameters
    self.InitialConditions = [(4,), (6,1.22)]

|

.. note::

    Most of the routines pay attention to the subdomain's shape when setting the initial conditions.
    To know exactly the definition of the initialisation routine and its possible parameters, check the related documentation
    of those exact functions, that can be found in the :code:`InitialConditions` class located in the
    considered governing equations. Reach it from the rubric "Pre-defined equations and models" of :ref:`GoverningEquations`.

|
|

------------------------------

6. Specifying the boundary conditions
-------------------------------------

The boundary conditions are set by defining the field :code:`self.BoundaryConditions` of the given template as a numpy array.
Each row corresponds to the boundaryTag as defined in the mesh's geometry and each column to the index of the defined fluid (not its type!).
The given values should be tuples where the first given value (integer) corresponds to the index of the boundary condition to apply if the considered fluid touches the given boundary segment,
followed by the wished properties to give to the boundary function (see the documentation of each of the implented boundary cases in :ref:`BoundaryConditions`).

|

Get the available list of boundary conditions and their respective indices by querying the documentation of the Mappers in the technical
handbook by using the technical documentation of the class  :code:`Fluid_Properties` on the page :ref:`BoundaryConditions`)
or check the equations' module itself in the file "Mappers.py" in the :code:`SourceCode`. In our case, the available boundary conditions
read as follows.

.. code::

    $ python -c "import SourceCode.Mappers as MP; print(MP.Boundary_Conditions.__doc__)"

    # Find the lines
    0. | Outflow
    1. | Inflow (see the inflow doc to know which inflow function and parameters are available)
    2. | Wall
    3. | Dirichlet

|

To know the parameters to give to each of the selected boundary conditions, check their definition in
the technical manual at :ref:`BoundaryConditions`.
By example, prescribing the boundary conditions for a domain with four tagged boundary segments (resp. 1 2 3 4) would read

.. code::

    self.BoundaryConditions = np.array(\
                                    [[(0,), (0,)],\
                                    [(1,1,0.5), (3,4.2)],\
                                    [(2,), (2,)],\
                                    [(2,), (2,)]])

There, the boundary segment tagged by :math:`1` would be associated with outflow (:code:`(0,)`) regardless the type of fluid in contact with the boundary.
The boundary segment tagged by :math:`2` would be associated with inflow condition inflowing the constant value :math:`0.5` when in contact with the first given fluid (:code:`(1,1,0.5)`),
while it is associated to Dirichlet condition when in contact with the second given fluid (:code:`(3,4.2)`) (in the order of the field :code:`FluidIndex`).
The two other segments are associated to walls regardless the fluid in contact.
