.. _Subdomains:

LESARD: Subdomain's definition module
===================================

|
|

The module :code:`FluidSubdomains` takes care of the initial subdomains' definition. 
Depending on the parameters given in the :code:`Problem`'s module, it creates the spatial 
patches over the computational domain (aka. the mesh).

|

.. rubric:: Page navigation

.. contents::
    :local:
    :depth: 1

|


.. automodule:: 3_LESARD.SourceCode.ProblemDefinition.FluidSubdomains

|
|
|

-----------------------------------------------------

Routines taking care of the domain splitting and the subdomains definition
---------------------------------------------------------------------------

|

.. autofunction:: 3_LESARD.SourceCode.ProblemDefinition.FluidSubdomains.GetSubdomains

.. autofunction:: 3_LESARD.SourceCode.ProblemDefinition.FluidSubdomains.Mapper

|
|

-----------------------------------------------------

Routines that are practically splitting the given plane
--------------------------------------------------------

|

.. autofunction:: 3_LESARD.SourceCode.ProblemDefinition.FluidSubdomains.DroppletsInBox

.. autofunction:: 3_LESARD.SourceCode.ProblemDefinition.FluidSubdomains.CutThroughBezierLine

.. autofunction:: 3_LESARD.SourceCode.ProblemDefinition.FluidSubdomains.CutThroughAbsoluteLine

.. autofunction:: 3_LESARD.SourceCode.ProblemDefinition.FluidSubdomains.CutThroughRelativeLine

|
|

------------------------------------------------------------

Routines that perform post-fluid subdomain definition tasks  
------------------------------------------------------------

|

.. autofunction:: 3_LESARD.SourceCode.ProblemDefinition.FluidSubdomains.ExportFluidFlags
