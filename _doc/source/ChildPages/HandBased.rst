.. _HandBased:


The HandBased module
=====================

|
|

The module :code:`_SourceCode/QuadratureRules/HandBased.py` procures two very simple quadrature
rules, on the boundary and within the element by furnishing the quadrature points and weights.
It contains one class for each quadrature dimension, within the element and on the boundary.

|


----------------------------

Module description
---------------------------

.. automodule::  3_LESARD.SourceCode._Solver.QuadratureRules.HandBased

|
|

------------------------------

Inner quadrature interfacing
-----------------------------

|

.. autoclass::  3_LESARD.SourceCode._Solver.QuadratureRules.HandBased.InnerQuadrature
   :members:


|
|

---------------------------------

Boundary quadrature interfacing
---------------------------------

|

.. autoclass::  3_LESARD.SourceCode._Solver.QuadratureRules.HandBased.BoundaryQuadrature
   :members:

|
