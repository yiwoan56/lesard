LESARD: Adapting to the user's environment
========================================

.. |bs|   unicode:: U+00A0 .. NO-BREAK SPACE

|

Two files allow you to map the module's location to any floder layout or local configuration you wish to use.

    -  The file :code:`Pathes` is dedicated to the user and maps to the folder containing the ressources he requires  at the runtime (settings, meshes, solution folder etc).

    -  The file :code:`IncludePathes` under the :code:`_Solver` directory is dedicated to the developper that wants to  change the location of a local library, such as the Problem definitions and PostProcessing. Its role is to bridge     the pathes of the contents in the folder :code:`_Solver` with the ones of the folder :code:`LESARD` from which the     code may be run in a batch or external running files.

|
|

Editing the local pathes for external input/output
**************************************************

.. automodule:: 3_LESARD.Pathes
   :members:
   :imported-members:
   
|
|

Editing the python include targets
**********************************

.. automodule:: 3_LESARD.SourceCode.IncludePathes
   :members:
   :imported-members:


   
