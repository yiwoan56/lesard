.. _FluidSelectors:


    
Fluid Selectors
===============

|

The modules defining the fluid selectors are located in the folder :code:`_Solver/FluidSelectors`.

    - Each module defines a fluid selector and should comport a class :code:`FS`

    - The spatial scheme and motion equation associated to the fluid selector should be implemented within the class as well.

    - The class should at least contain the methods :code:`Initialiser`, :code:`Iteration`, :code:`UpdateFSVAttributes` (see below for an example of a format).
    
|


.. rubric:: *Already implemented fluid selectors*   

.. contents::
    :local:

|
|

--------------------------------------------------------
    
Naive Level Set + Continuous galerkin + Lax-Friedrichs
--------------------------------------------------------

|

.. automodule::  3_LESARD.SourceCode._Solver.FluidSelectors.NaiveLS_CG_LFX
   :members:
   :member-order: bysource 
