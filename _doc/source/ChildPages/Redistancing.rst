.. _Redistancing:

Redistancing
============

|

The redistancing algorithms to apply on a Level Set after a couple of time steps are implemented in :code:`_Solver/Redistancers` as a module.
The redistancing technique has to be implemented within a class, and the main method to redistance (given within the class)
should be named :code:`Redistance` in any case.

|

The various spatial application routine of the redistancing as well as the parsing of user wishes for the resitancing is performed in the wrapper class contained in the
:code:`_Solver/Redistancers/WrapRedistancer.py` file.

|
|

.. rubric:: Page navigation

.. contents::
    :local:
    :depth: 2


|
|


--------------------------------------------------------

Redistancing choice and application subdomain selection
--------------------------------------------------------

|

.. automodule::  3_LESARD.SourceCode._Solver.Redistancers.WrapRedistancer
   :members:
   :member-order: bysource

|



-----------------------------------------------------------

Available redistancers and their parameter specifications
-----------------------------------------------------------

|
|

--------------------------------------------------------


HopfLax Descent
^^^^^^^^^^^^^^^

|

.. automodule::  3_LESARD.SourceCode._Solver.Redistancers.HopfLax_Descent
   :members:
   :member-order: bysource

|
|

--------------------------------------------------------

HopfLax Bregman
^^^^^^^^^^^^^^^

|
.. automodule::  3_LESARD.SourceCode._Solver.Redistancers.HopfLax_Bregman
   :members:
   :member-order: bysource

|
|

--------------------------------------------------------

HopfLax Global Bregman
^^^^^^^^^^^^^^^^^^^^^^

|

.. automodule::  3_LESARD.SourceCode._Solver.Redistancers.HopfLax_GlobalBregman
   :members:
   :member-order: bysource

|
|

--------------------------------------------------------

HopfLax Nested Bregman
^^^^^^^^^^^^^^^^^^^^^^
|

.. automodule::  3_LESARD.SourceCode._Solver.Redistancers.HopfLax_NestedBregman
   :members:
   :member-order: bysource

|
