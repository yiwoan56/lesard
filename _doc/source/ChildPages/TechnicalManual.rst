.. _TechnicalManual:

Technical Manual
================
|
|

The technical manual details the code's structure the routines that are implemented,
regardless their importance or level in the code. It is aimed for the user that would like
to develop further features. Therefore, please pay attention to the :ref:`UserManual` first to
understand the practical use of the code before entering the technicities.

|

.. rubric:: Page navigation

.. contents::
    :local:
    :depth: 1

|

------------------------

Topic-wise documentation
------------------------

|

.. list-table::

    * - *Setup and run scripts*
      - *Problem definition and mapping modules*
      - *Technical and solver modules*

    * -
        .. toctree::
            :maxdepth: 1

            WorkingEnvironment
            ProblemDefinition
            Settings
            Run

      -
        .. toctree::
            :maxdepth: 1

            Subdomains
            GoverningEquations
            BoundaryConditions
            Mapping

      -
        .. toctree::
            :maxdepth: 1


            Tools
            Meshing
            Solve
            PostProcessing

*As some occurences of classes and variables may appear cross-topic, use the search tool if you have a precise need through the documentation.*

|
|

------------------

Code organisation
-----------------

|

Spirit of the code
******************

    - The problems are predefined under :code:`PredefinedTests/PredefinedProblems`, in a user-friendly definition to be parsed. The mix-up in their definition at run-time is thus avoided.
    - The setting files are predefined under :code:`PredefinedTests/PredefinedSettings` in order to avoid the overwriting of results and to keep a memory of what exactly were ran. At each run the used settings are copied to the results folder to prevent confusion in case of a later change in the general settings definition.

    - The code is higly modular: one can add or remove a component (i.e. a scheme or a problem) without making the program collapse.

        - The modularity at destination to the user is located at the root :code:`LESARD` directory and allows  to add and remove problems, meshes and settings. No modification of the code is required.
        - The modularity at destination to the developper is located in the :code:`_Solver` subdirectories. An adaptation of the file :code:`_Solver/Mappers.py` is required when adding or removing a component.

    - The problems and numerical schemes selected by the user in those files are shortcuted during the initialisation process to generic fields in problem, solver and solution instances.

    - Running scripts are furnished as examples of practical use.

        - The :code:`RunProblem.py` allows the solver to act on the given problem, settings, and the mesh resolution
        - The :code:`RunPostProcess.py` allows the semi-automatic generation of post-processing plots
        - The main solver itself is given in the file :code:`_Solver/Solve.py`

|

Overview of the initialisation process
**************************************

    1. Using the running script :code:`RunProblem.py`, fetching the investigated problem's module from its name from the folder :code:`PredefinedTests/PredefinedProblems`.
    2. Cleaning the export environment: erasing all the previously computed solution files that may still exist under the folder :code:`Solutions/ProblemName_SettingsName`.
    3. Load the text file gathering the information on the wished solver and its properties located under :code:`PredefinedTests/PredefinedSettings` and initialising the :code:`Parameters` class.
    4. Targetting the suitable mesh upon the spatial scheme's type and the problem's associated geometry.
    5. Initialise the :code:`Problem` instance by interpreting the variables contained in the module (see the :ref:`UserManual` for a practical overview).

        a. Retrieving the number of fluids (matching the number of subdomains) to consider.
        b. Generating the initial subdomains calling the :code:`GenerateSubdomains` routine from the :code:`FluidSubdomains.py` module, according to the problem's module variable giving the parameters to generate the subdomains.
        c. For each fluid, loading the governing equations module driving its motion (located under :code:`SourceCode/ProblemDefinition/GoverningEquations`).
        d. For each fluid, selecting the wished initialisation routine, equation of state class and defining the fluid model's instance upon the fluid's properties.

    6. Parsing the solver's specifications and parameters (selected upon choices from the libraries :code:`_Solver/SpatialSchemes`,  :code:`_Solver/TemporalSchemes` and  :code:`_Solver/FluidSelectors`).
        a. Creating a :code:`Solver` instance
        b. Retrieving the quadrature rules to be used in the spatial scheme and fluid selector scheme. If none, initialising the respective instance with None values for compatibility issues.
        c. Retrieving the fluid's selector class in :code:`Solver.FluidSelector`
        d. Retrieving the temporal scheme iteration routine in :code:`Solver.TimeStep`
        e. Retrieving the spatial scheme iteration routine in :code:`Solver.SpatialScheme`
        f. Retrieving possibly the spatial scheme's amendement in :code:`Solver.SPAmendements`, whose sequence is determined at the mollifyier's instance creation :code:`_Solver.SpatialModifiers.SpatialAmendements` (possibly involving several unitary amendements and further quadrature rule instance creations).
        g. Retrieving the solution's reconstructor at physical vertices in :code:`Solver.Reconstructor`.

    7. Initialising the :code:`Solution` instance

        a. Creating the :code:`Solution` instance by using the constructor of the class :code:`Solution` located in the module :code:`_Solver/Solver_Initialisations.py`.
        b. Filling the initial values of the fluid's selector by calling the method :code:`Initialiser` of the selected :code:`FluidSelector` class.
        c. Initialising the reconstructed solution at the vertices :code:`Solution.RSol` by calling :code:`Solver.Reconstructor`, and respectively for the fluid's selector's values.

|

.. note::

    - During the initialisation process, we consider one fluid per subdomain. However, it is possible to define two different subdomains occupied per fluids that are on the same nature and driven by the same set of equations. That way one can initialise the solution from different initialisation routines. This artificial separation has no effect in practice when computing the solution.
    - In the steps 5. and 6., the selection of the right modules is done by an indexing of the routines and classes that can be found and is amenable  directly in the file :code:`Mappers.py` located under the :code:`_Solver`'s folder.
    - The postprocessing is not automatic.

|
|

Overview of the solving process for one time-step
**************************************************

    1. Get the time step upon the mesh, the CFL value and the spectral radius from the temporal routine located in the module :code:`_Solver/Solver_Temporal.py`
    2. Enter the time step routine that got shortcut as :code:`Solver.TimeStep` in the previous step 6.

        a. Process (if needed by the time-scheme) the governing equations' fluxes
        b. Enter the RD framework whose routine :code:`RD` is available in the :code:`_Solver/Solver_Spatial.py` module.

            a. For each element

                - Apply the spatial scheme to the solution through :code:`SpatialScheme.Iteration`
                - Update the fluid's selector through :code:`FluidSelector.Iteration`
                - Possibly edit the residuals according to the wished amendement through :code:`SpatialScheme.SPAdmendements.Mollify`

            b. Lump the mass matrix

        c. Update the residuals

    3. Periodically redistance the level set values (if, where, when and how as wished by user)
    4. Reconstruct the solution at the vertices for both the solution and the fluid's selector's value
    5. Periodically export the solution in binary and vtk format, after a time interval specified by the user.


|
|
|

------------

Folder layout
-------------

|

For reference, the folder layout has been reported here. Use the search text tool from your browser
to find a specific module you may be looking for.

*Note: Only showing the source code and technical files.*

|

.. code-block::

	├── CleanCache.sh
	├── LocalLib
	│   ├── BarycentricBasisFunctions.py
	│   ├── BarycentricTools.py
	│   ├── GlobalMinimisation.py
	│   ├── LobattoQuadratureNodes.py
	│   ├── ReversedFunctools.py
	│   └── Solve_WENO.py
	├── Pathes.py
	├── PredefinedTests
	│   ├── PredefinedMeshes
	│   ├── PredefinedProblems
	│   └── PredefinedSettings
	├── RunPostProcess.py
	├── RunProblem.py
	├── Solutions
	├── SourceCode
	│   ├── IncludePathes.py
	│   ├── Mappers.py
	│   ├── Meshing
	│   │   ├── MeshStructure.py
	│   │   ├── Meshing_DualMesh.py
	│   │   ├── Meshing_PrimalMesh.py
	│   │   └── __init__.py
	│   ├── PostProcess.py
	│   ├── PostProcessing
	│   │   ├── Errors.py
	│   │   ├── Exports.py
	│   │   ├── Plots_Matplotlib.py
	│   │   ├── Plots_Plotly.py
	│   │   ├── Plots_Schlieren.py
	│   │   ├── Printouts.py
	│   │   └── __init__.py
	│   ├── ProblemDefinition
	│   │   ├── BoundaryConditions
	│   │   │   ├── Dirichlet.py
	│   │   │   ├── Inflow.py
	│   │   │   ├── Outflow.py
	│   │   │   ├── Wall.py
	│   │   │   └── __init__.py
	│   │   ├── FluidModels.py
	│   │   ├── FluidSubdomains.py
	│   │   ├── GoverningEquations
	│   │   │   ├── EulerEquations.py
	│   │   │   ├── LinearAdvectionRotation.py
	│   │   │   ├── LinearAdvectionTranslation.py
	│   │   │   └── __init__.py
	│   │   └── __init__.py
	│   ├── Solve.py
	│   ├── _Solver
	│   │   ├── FluidSelectors
	│   │   │   ├── NaiveLS_CG.py
	│   │   │   ├── NaiveLS_CG_LFX.py
	│   │   │   ├── NaiveLS_CG_LFX_Limited.py
	│   │   │   └── __init__.py
	│   │   ├── QuadratureRules
	│   │   │   ├── HandBased.py
	│   │   │   ├── QuadPy.py
	│   │   │   ├── Quadratures.py
	│   │   │   └── __init__.py
	│   │   ├── Redistancers
	│   │   │   ├── HopfLax_Bregman.py
	│   │   │   ├── HopfLax_Descent.py
	│   │   │   ├── HopfLax_GlobalBregman.py
	│   │   │   ├── HopfLax_NestedBregman.py
	│   │   │   ├── WrapRedistancer.py
	│   │   │   └── __init__.py
	│   │   ├── Solver_ApplyBCs.py
	│   │   ├── Solver_Initialisations.py
	│   │   ├── Solver_Spatial.py
	│   │   ├── Solver_Temporal.py
	│   │   ├── SpatialModifiers
	│   │   │   ├── Filtering_Streamline.py
	│   │   │   ├── Limiter_Psi.py
	│   │   │   ├── SpatialAmendements.py
	│   │   │   └── __init__.py
	│   │   ├── SpatialSchemes
	│   │   │   ├── CG.py
	│   │   │   ├── CG_LFX.py
	│   │   │   ├── CG_Primary.py
	│   │   │   └── __init__.py
	│   │   ├── TemporalSchemes
	│   │   │   ├── DeC.py
	│   │   │   └── __init__.py
	│   │   └── __init__.py
	│   └── __init__.py
	├── Templates
	│   ├── Template_BoundaryConditions.py
	│   ├── Template_Equations.py
	│   ├── Template_FluidModels.py
	│   ├── Template_FluidSelector.py
	│   ├── Template_Mollifier.py
	│   ├── Template_Problem.py
	│   ├── Template_Redistancing.py
	│   ├── Template_Settings.txt
	│   ├── Template_SpatialScheme.py
	│   └── Template_TemporalScheme.py
	├── TestSheet.odt
