.. LESARD documentation master file, created by
   sphinx-quickstart on Thu Apr 23 18:19:05 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.
.. |br| raw:: html

   <br />




LESARD: LEvel Set Algorithm for Residual Distribution's documentation
======================================================================
|

|

This code has for vocation to furnish a range of numerical schemes for multiphase-flows, applied to user-friendly defined problems. See below for a presentation at a glance.

|

**Contact:** All remarks, suggestions, comments, bugshooting and complains goes to :code:`elise(point)lemeledo(at)math(dot)uzh(dot)ch`, thank you!

|

.. rubric:: Documentation

+------------------------------+--------------------------------------------+
| :ref:`UserManual`            |    :ref:`TechnicalManual`                  |
+------------------------------+--------------------------------------------+

|

.. rubric:: Page navigation

.. contents::
    :local:
    :depth: 1

|

----------

Code's features
---------------
|

**1. Define custom problems in two spatial dimension**

Any user can define a two dimensional problem of his choice by completing the furnished templates (see the :ref:`UserManual` for more information).
A problem should always be defined by:

    *  the model, number and properties of the considered fluid(s)
    *  the sets of equations governing the fluids, each of the type :math:`\frac{\partial u}{\partial t} + \nabla\cdot \mathcal{F}(u) = 0`
    *  the computational domain and its associated mesh
    *  the initial spatial distribution of the fluids coexisting in the computational domain
    *  the type of boundary conditions according to their label in the associated mesh

|

**2. Compute a numerical solution for the designed problem**

Once a problem is designed, the code allows to approximate a solution by choosing a solver and its properties (see the :ref:`UserManual` for more information).
The solver is seen as a combination of the spatial, temporal schemes and the choice of the fluid's selector.
The type of its components and their parameter choices are mostly independent (see below for the compatibility tables).
Directly accessible from the solver settings's file, the user can:

    -  select the time iteration scheme and its order
    -  select the spatial scheme, its order and its amendements (streamline dissipation, limiters, etc.)
    -  select of the type of the fluid's spotter and its associated solver (the fluid spotter is defined by its nature (e.g. level set), a spatial iterator (e.g. CG with psi-limiter), a redistancing process (e.g. HJB in pseudo-time), and a fluid's selection routine at degrees of freedom and quadrature points.)

|

**3. Post-process the solution**

Once a solution has been exported, the user can run through a semi-automated script post-processing tasks.
In particular, one can:

    -  plot the subdomains's evolution, either as static snapshots or as an html animation, in 2D or 3D
    -  plot the solution for each variable, either as static snapshots or as an html animation, in 2D or 3D
    -  plot the solution for each fluid's spotter, either as static snapshots or as an html animation, in 2D or 3D
    -  plot numerical schlieren images as static snapshots in 2D
    -  export the solution as a binary file (with pickles), raw in a text-file or in a vtk format

|

.. note::
     - All the spatial schemes are recast into the Residual Distribution framework.

     - The mass matrix is lumped

     - Single-phase problems can also be studied in this framework.

|
|
|

----------

Implemented schemes and cross compatibility
-------------------------------------------

|

Temporal schemes:
^^^^^^^^^^^^^^^^^

+----------------------------+-----------------+-----------------------------------------------+
|       *Scheme*             |     *Order*     | *Compatible with spatial schemes*             |
+----------------------------+-----------------+-----------------------------------------------+
| Deffered correction (DeC)  |     Arbitrary   |   All                                         |
+----------------------------+-----------------+-----------------------------------------------+

|

Spatial schemes:
^^^^^^^^^^^^^^^^

+----------------------------------+-----------------+--------------------+---------------------------------------------+
|       *Scheme*                   |     *Order*     |   *Acing on Grid*  | *Compatible with amendements*               |
+----------------------------------+-----------------+--------------------+---------------------------------------------+
| Continuous Galerkin (GC)         |    Arbitrary    |      Dual          | Limiter_Psi                                 |
|                                  |                 |                    |                                             |
|                                  |                 |                    |                                             |
+----------------------------------+-----------------+--------------------+---------------------------------------------+
| Continuous Galerkin on           |    Arbitrary    |      Dual          | None (does not fail but do not preserve     |
| Primary variables (CG_Primary)   |                 |                    | the contacts on primary variable as wished  |
|                                  |                 |                    | when used)                                  |
+----------------------------------+-----------------+--------------------+---------------------------------------------+
| GC + Lax-Friedrichs flux (LFX)   |  Arbitrary      |      Dual          | Limiter_Psi                                 |
|                                  |  (reduces to 1) |                    |                                             |
|                                  |                 |                    |                                             |
+----------------------------------+-----------------+--------------------+---------------------------------------------+

|

Spatial ammendements:
^^^^^^^^^^^^^^^^^^^^^

+----------------------------------+----------------------------------------------+
|  *Amendement type and name*      |   *Compatible with schemes*                  |
+----------------------------------+----------------------------------------------+
| Limiter_Psi                      | All schemes                                  |
+----------------------------------+----------------------------------------------+
| Filtering_Streamline             | All schemes -- Only sclar problems           |
+----------------------------------+----------------------------------------------+

|

Fluid's spotters
^^^^^^^^^^^^^^^^

+----------------------------------+-----------------+---------------+-------------------+-------------------+------------------------------------------+-------------------------------------------+
|       *Name*                     |     *Type*      |   *Scheme*    | *Order*           |    *Redistancing* |    *Fluid's selection algorithm*         |  *Compatibility with spatial schemes*     |
+----------------------------------+-----------------+---------------+-------------------+-------------------+------------------------------------------+-------------------------------------------+
| LS_CG_LFX                        |  Level Set      |   CG + LFX    | Arbitrary         |    None           |  Index of the Level sets' maximum        | Any dual grid based scheme                |
|                                  |                 |               |                   |                   |  absolute value                          |                                           |
|                                  |                 |               |                   |                   |                                          | Order must match the spatial scheme's     |
+----------------------------------+-----------------+---------------+-------------------+-------------------+------------------------------------------+-------------------------------------------+
| LS_CG                            |  Level Set      |   CG          | Arbitrary         |    None           |  Index of the Level sets' maximum        | Any dual grid based scheme                |
|                                  |                 |               |                   |                   |  absolute value                          |                                           |
|                                  |                 |               |                   |                   |                                          | Order must match the spatial scheme's     |
+----------------------------------+-----------------+---------------+-------------------+-------------------+------------------------------------------+-------------------------------------------+
| LS_CG_Limited                    |  Level Set      |   CG + LFX    | Arbitrary         |    None           |  Index of the Level sets' maximum        | Any dual grid based scheme                |
|                                  |                 |   + PSI       |                   |                   |  absolute value                          |                                           |
|                                  |                 |               |                   |                   |                                          | Order must match the spatial scheme's     |
+----------------------------------+-----------------+---------------+-------------------+-------------------+------------------------------------------+-------------------------------------------+

|

Level Set redistancers and spatial application range
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

+----------------------------------+-----------------+---------------+--------------------------------------------------+----------------------------------------------------+
|       *Redistancer name*         |     *Type*      |   *Scheme*    |    *Algorithm description*                       |  *Compatibility with spatial schemes*              |
+----------------------------------+-----------------+---------------+--------------------------------------------------+----------------------------------------------------+
| HopfLax_Bregman                  |  Optimiser      |   HopfLax     |   Solving the optimisation via Bregman split.    | All                                                |
|                                  |                 |   based       |                                                  |                                                    |
|                                  |                 |               |                                                  |                                                    |
+----------------------------------+-----------------+---------------+--------------------------------------------------+----------------------------------------------------+
| HopfLax_Descent                  |  Optimiser      |   HopfLax     |   Solving the optimisation via Gradient Descent. | All                                                |
|                                  |                 |   based       |                                                  |                                                    |
+----------------------------------+-----------------+---------------+--------------------------------------------------+----------------------------------------------------+
| HopfLax_GlobalBregman            |  Optimiser      |   HopfLax     |   Solving the optimisation via Inertial Bregman  | All                                                |
|                                  |                 |   based       |   Proximal Gradient method                       |                                                    |
+----------------------------------+-----------------+---------------+--------------------------------------------------+----------------------------------------------------+
| HopfLax_NestedBregman            |  Optimiser      |   HopfLax     |   Solving the optimisation via Bregman split and | All                                                |
|                                  |                 |   based       |   Inertial Bregman Proximal Gradient method      |                                                    |
+----------------------------------+-----------------+---------------+--------------------------------------------------+----------------------------------------------------+

|

+----------------------------------+--------------------------------------------------------------+-------------------------------------------+---------------------------------------------------+
|   *Spatial applicator name*      |    *Application range *                                      |  *Compatibility with redistancer type*    |  Compatibility with spatial scheme                |
+----------------------------------+--------------------------------------------------------------+-------------------------------------------+---------------------------------------------------+
| NarrowBandRedistancing           | Applies the redistancing within an interface's narrow band   | All                                       | All                                               |
+----------------------------------+--------------------------------------------------------------+-------------------------------------------+---------------------------------------------------+
| GlobalRedistancing               | Applies the redistancing on the whole computational domain   | All                                       | All                                               |
+----------------------------------+--------------------------------------------------------------+-------------------------------------------+---------------------------------------------------+

|


Quadrature rules:
^^^^^^^^^^^^^^^^^^^^^

+----------------------------------+-------------------------------+--------------------------------------+--------------------------------+
|  *Quadrature type*               |   *Compatible with schemes*   |    *Compatible with Elements*        |     *Degree of exactness*      |
+----------------------------------+-------------------------------+--------------------------------------+--------------------------------+
| Hand Based                       | All schemes                   |     Simplicial                       |          5                     |
+----------------------------------+-------------------------------+--------------------------------------+--------------------------------+
| Quadpy based                     | All schemes                   |     Simplicial and                   |  Depending on the selected     |
|                                  |                               |     Quads (restricted scheme choice) |  scheme, up to arbitrary.      |
+----------------------------------+-------------------------------+--------------------------------------+--------------------------------+

|
|
|
|

----------

Code Requirements
-----------------

|

Basic requirement
^^^^^^^^^^^^^^^^^

An installation of :code:`python3 > 3.6` and enough free space in the running folder to accept the export of the binary files of the solution.
(The export can be reduced/minimized/disabled manually in the file SourceCode/Solve.py).
A-priori it should be runable on any OS, though it has not been tested on W$ and Mac systems.

No installation script is furnished; this code is not aimed to be a library but a solver on a user-defined problem.
Just add/define your problem following the user manual and run the main file it from where it is stored.

|

Mandatory libraries furnished with the python package's distribution
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

+--------------------+---------------------+-----------------------+
| *Library  name*    |  *Tested version*   |  *Involved in*        |
+--------------------+---------------------+-----------------------+
| os                 | 3.7                 | Everywhere            |
+--------------------+---------------------+-----------------------+
| sys                | 3.7                 | Everywhere            |
+--------------------+---------------------+-----------------------+
| glob               | 3.7                 | IO (PostProcessing)   |
+--------------------+---------------------+-----------------------+
| pickle             | 3.7                 | IO (Mesh+PostProcess) |
+--------------------+---------------------+-----------------------+
| copy               | 3.7                 | Everywhere            |
+--------------------+---------------------+-----------------------+
| itertools (▲)      | 3.7                 | Meshing               |
+--------------------+---------------------+-----------------------+
| subprocess (▲)     | 3.7                 | Exports (Clean up)    |
+--------------------+---------------------+-----------------------+

*(▲)  Dependency can be dropped upon slight easy modification of the code*

|

Mandatory external libraries
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

+--------------------+-------------------+-----------------------+
| *Library  name*    |  *Tested version* |  *Involved in*        |
+--------------------+-------------------+-----------------------+
| re                 | 2.2.1             | Solver initialisation,|
|                    |                   | Meshing, PostProcess  |
|                    |                   | Redistancing          |
+--------------------+-------------------+-----------------------+
| numpy              | 1.18.5            | Everywhere            |
+--------------------+-------------------+-----------------------+
| shapely            | 1.7.0             | Initial subdomains    |
|                    |                   | Meshing               |
+--------------------+-------------------+-----------------------+

|

Optional external libraries
^^^^^^^^^^^^^^^^^^^^^^^^^^^

+----------------+-------------------+------------------------------------------------+
| *Library name* |  *Tested version* |   *Features to disable if no such libary*      |
+----------------+-------------------+------------------------------------------------+
| matplotlib     | 3.2.1             | Matlplotlib plots,                             |
|                |                   | possibly redistancing in LS,                   |
|                |                   | plotmesh in primal mesh                        |
+----------------+-------------------+------------------------------------------------+
| plotly         | 0+untagged.       | Plotly plots                                   |
|                | 25.g3b07c4e       |                                                |
+----------------+-------------------+------------------------------------------------+
| scipy (▲▲)     | 1.4.1             | Matplotlib plots,                              |
|                |                   | Plotly plots                                   |
|                |                   | Some Bregman split-based redistancing routines |
+----------------+-------------------+------------------------------------------------+
| meshio         | 4.0.15            | Export of the solution                         |
|                |                   | in a VTK format                                |
+----------------+-------------------+------------------------------------------------+
| quadpy         | 0.16.2            | QuadyPy quadrature rules                       |
+----------------+-------------------+------------------------------------------------+

*(▲▲) If no wish to disable the associated feature, the dependency can be dropped upon significant modification of the code or code's specific routine removal*

|
|

---------------

Known drawbacks
---------------

Few minor bugs have been spot and not (yet) corrected as non-prioritary. They may include:

    - The plotting routine with Plotly applied to a constant Level Set (1Fluid case) returns an empty figure
    - The export of the solution file is formatted to accept timestamps of 6 decimals. If asking more frequent timestamped solution exports, overwriting will occur.
    - It is slow: depending on the problem to solve, plan from a long coffee break to a hike vacation
