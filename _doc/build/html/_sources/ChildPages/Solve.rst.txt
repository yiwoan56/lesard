.. _solve:

LESARD: Solving modules
=====================

|

The articulation of the solver is made around three modules: :code:`Solver_Initialisations`, :code:`Solver_Spatial`, and :code:`Solver_Temporal`.
The :code:`Solver_Initialisations` takes care about all the initialisation, from the subdomain's splitting to the fluid's spotter values initialisation
through shortcutting the problem and solver routines that have been selecting by the user. The module :code:`Solver_Spatial` acts as an interface from
the Residual Distribution framework to the actual spatial scheme. The temporal solver being actually defined within a shortcut routine and not needing any
further interface, the module :code:`Solver_Temporal` acts on the time step retrieval and defines the mass lumping routines.
The documentation below explicits their role. For the practical documentation of the actual solvers (that enjoy
modularity), please see the corresponding pages.

|



.. rubric:: Page navigation

.. contents::
    :local:
    :depth: 1

|
|
|

----------------------------------------

Modular solvers
*****************************

|


.. list-table::

    * -
        .. toctree::
            :maxdepth: 1

            TemporalSchemes

      -
        .. toctree::
             :maxdepth: 1

             TemporalSchemes
             SpatialSchemes
             SpatialModifiers
             QuadratureRules

      -
        .. toctree::
             :maxdepth: 1

             FluidSelectors
             Redistancing

|
|

----------------------------------------

Solver initialisations module
*****************************


.. automodule::  3_LESARD.SourceCode._Solver.Solver_Initialisations
   :member-order: bysource

|



.. autoclass::  3_LESARD.SourceCode._Solver.Solver_Initialisations.Parameters
   :members:
   :member-order: bysource

|

.. autoclass::  3_LESARD.SourceCode._Solver.Solver_Initialisations.Solver
   :members:
   :member-order: bysource

|

.. autoclass::  3_LESARD.SourceCode._Solver.Solver_Initialisations.Solution
   :members:
   :member-order: bysource

|

.. autoclass::  3_LESARD.SourceCode._Solver.Solver_Initialisations.Problem
   :members:
   :member-order: bysource

|
|

----------------------------------------

Spatial Residual distribution interface
***************************************

|

.. automodule::  3_LESARD.SourceCode._Solver.Solver_Spatial
   :members:
   :member-order: bysource

|
|

----------------------------------------

Application of boundary conditions
***************************************

.. automodule::  3_LESARD.SourceCode._Solver.Solver_ApplyBCs
   :members:
   :member-order: bysource

|
|

----------------------------------------

Tools required for the time steppings
***************************************

|

.. automodule::  3_LESARD.SourceCode._Solver.Solver_Temporal
   :members:
   :member-order: bysource
