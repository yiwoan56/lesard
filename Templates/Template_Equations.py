#***************************************************************************
# Copyright (C) 2020 Élise Le Mélédo
# This file is part of LESARD.
#
# LESARD is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LESARD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LESARD.  If not, see <https://www.gnu.org/licenses/>.
#****************************************************************************

# 1. Add the file to the folder SourceCode/ProblemDefinition/GoverningEquations
# 2. Mofidy the file SourceCode/Mappers.py to add the governing equation under an associated index


# ==============================================================================
#	Preliminary imports
# ==============================================================================
import numpy as np


# ==============================================================================
#	Parameter that should be accessible from outside the class
# ==============================================================================
FluidModel   = "The Name of the fluid model class to use, that should preexist in the FluidModels.py module"
IntegratedLS = False

# ==============================================================================
#	Equation class giving the flux, jacobian and all tools required by schemes
# ==============================================================================
class Equations():
	""" Class furnishing all the methods that are required to define numerical
	schemes and evolve the solution according to the Euler Equation

	|

	.. rubric:: Fields

	.. |bs|   unicode:: U+00A0 .. NO-BREAK SPACE

	It contains  the following initialisation parameters (later available as well as attributes)

		- |bs| **FluidProp**   *(list of FluidModel)*  --  the list of fluid property instances (NbFluids)
		- |bs| **EOS**         *(list of callbacks)*   --  the list of Equation of state functions (NbFluids)

	It further contains the following attributes, filled upon initialisation

		- |bs| **NbVariables**          *(integer)*          --   number of variables of the model
		- |bs| **VariablesNames**       *(list of strings)*  --   ordered name of the variables
		- |bs| **VariablesUnits**       *(list of strings)*  --   symbols of the units of the variables
		- |bs| **VariablesLatexNames**  *(list of strings)*  --   ordered name of the variables, latex encoding
		- |bs| **VariablesLatexUnits**  *(list of strings)*  --   symbols of the units of the variables, latex encoding
		- |bs| **XYLatexUnits**         *(list of strings)*  --   units of the x-y coordinates in latex encoding
    """

	#### Automatic initialisation routine ####
	def __init__(self, FluidProp, EOs):
		"""
		Args:
			FluidProp   (list of FluidModel):   the list of fluid property instances (NbFluids)
			EOS         (list of callbacks):    the list of Equation of state functions (NbFluids)

		|

		.. rubric:: Methods

		"""

		# Defines the (conservative) variables number and names, in a latex typography
		self.NbVariables    = 0
		self.VariablesNames = ["The variables name in a string list"]
		self.VariablesUnits = ["The variables units in a string list"]

		self.VariablesLatexNames = [r'$ The list of the variables name in a latex format $']
		self.VariablesLatexUnits = [r'$ The list of the variables units in a latex format $']
		self.XYLatexUnits = [r'$m$', r'$m$']

		# Register the shortcut of the fluid's properties
		self.FluidProp = FluidProp
		self.EOs       = EOs

	#### Retrieve the primary variables from the conservative ones ####
	def ConservativeToPrimary(self, Var, FluidIndex, *args):
		"""Converts the conservative variables to the primary ones

		Args:
			Var         (2D numpy array):         the variables of the problem (NbVars x NbGivenPoints)
			FluidIndex  (integer numpy array):    the fluid present at each given point (NbGivenPoints)

		Returns:
			Var (numpy array): (NbVars x NbGivenPoints) the corresponding primary variables

		.. note::

			- | *args is there only for compatibility reason at call time
		"""

		# ------- Constructing the primary variables ----------------------------------------------------
		# COMPUTE HERE THE PRIMARY VARIABLES

		# Returning the Result
		return(PrimVars)

	#### Retrieve the conservative variables from the primary ones ####
	def PrimaryToConservative(self, PrimVar, FluidIndex, *args):
		"""Converts the primary variables to the conservative ones

		Args:
			Var         (2D numpy array):         the primary variables of the problem (NbVars x NbGivenPoints)
			FluidIndex  (integer numpy array):    the fluid present at each given point (NbGivenPoints)

		Returns:
			Var (numpy array): (NbVars x NbGivenPoints) the corresponding conservative variables

		.. note::

			- | *args is there only for compatibility reason at call time
		"""

		# ------- Constructing the conservative variables ----------------------------------------------------
		# COMPUTE HERE THE CONSERVATIVE VARIABLES

		# Returning the Result
		return(Vars)

	#### Flux function of the equations set ####
	def Flux(self, Var, *args):
		"""Flux function of the  (conservative) EulerEquations

		.. |bs|   unicode:: U+00A0 .. NO-BREAK SPACE

		Args:
			Var  (float numpy array):   the value of the variables (NbVariables x NbGivenPoints)

		.. rubric:: Optional argument

		- |bs| **x**           *(float numpy array)*,   --  (optional, not impacting here) x-y coordinates of the given points (NbGivenPoints x 2)
		- |bs| **FluidIndex**  *(integer numpy array)*  --  the fluid present at each given point (NbGivenPoints)

		Returns:
			Flux (3D numpy array):   the obtained flux (spatialdim x NbVars x NbPoints)

		|

		.. note::

			- | This function is Vectorised
			- | Fluid index is the fluid index in the initially given list, not the fluid type
			- | FluidProp is the list of all the fluid properties for each fluid (given the list, not the type)
			- | *args is there only for compatibility reason at call time
		"""

		# DEFINE THE FLUX FX (NbVars x NbPoints) AND FY (NbVars x NbPoints) OF
		# THE SPATIALLY 2D GOVERNING EQUATION THERE

		# Returning the flux
		Flux = np.array([Fx, Fy])
		return(Flux)

	#### Function that extracts the propagation speed from the variables to interface for the LevelSet ####
	def GetUV(self, Var, *args):
		""" Function giving back the x-y velocity at the considered point, given the variational
		values given for the the conservative Euler equation's variables.

		.. |bs|   unicode:: U+00A0 .. NO-BREAK SPACE

		Args:
			Var  (float numpy array):   the value of the variables (NbVariables x NbGivenPoints)
			x    (float numpy array):   (generally optional, required for this problem), x-y coordinates of the given points (NbGivenPoints x 2)

		.. rubric:: Optional argument

		- |bs| **FluidIndex**  *(integer numpy array)*  --  the fluid present at each given point (NbGivenPoints)

		Returns:
			UV   (float numpy array) -- the velocities values at the points (2 x NbGivenPoints)

		.. note::

			- | This function is Vectorised
			- | *args is there only for compatibility reason at call time
		"""

		# COMPUTE THE FLUID VELOCITY VALUES VX AND VY FROM THE SET OF VARIABLES THERE

		# Returning the value
		return(UV)

	#### Jacobian of the equations set ####
	def Jacobian(self, Var, x, *args):
		"""Computes the Jacobian of the flux for the  Euler equations

		.. |bs|   unicode:: U+00A0 .. NO-BREAK SPACE

		Args:
			Var        (float numpy array):   the value of the variables (NbVariables x NbGivenPoints)
			x          (float numpy array):   (generally optional, required for this problem), x-y coordinates of the given points (NbGivenPoints x 2)

		.. rubric:: Optional argument

		-- |bs| **FluidIndex**  *(integer numpy array)*  --  the fluid present at each given point (NbGivenPoints)

		Returns:
			J    (3D numpy array):   J[:,:,i] gives the jacobian of the flux taking care of the dynamic of the ith spatial coordinate.

		|

		.. note::

			- | For each flux fi = (fi1,..., fin), the returned Jacobian reads:

			  | |bs| J[:,:] = [dfi1/dx1, ....., dfi1/dxn
			  | |bs| |bs| |bs|	|bs| |bs| |bs|	 ....
			  | |bs| |bs| |bs|	|bs| |bs| |bs|	df in/dx1, ....., dfin/dxn]
			- | *args is there only for compatibility reason at call time

		"""

		# ------ Initialising the Jacobian structure ---------------------------
		# Getting the number of furnished points
		NbPoints = np.shape(Var)[1]

		# EDIT THE INITIALISATION OF J BY THE NUMBER OF VARIABLES OF YOUR EQUATION SET
		J = np.zeros((self.NbVariables, self.NbVariables, 2, NbPoints))

		# FILL THE JACOBIAN IN X THERE (EXAMPLE OF FORMAT FOR FOUR EQUATIONS)
		#J[0,:,0,:] = np.array([np.zeros(NbPoints),         np.zeros(NbPoints),    np.zeros(NbPoints),   np.zeros(NbPoints)])
		#J[1,:,0,:] = np.array([np.zeros(NbPoints),         np.zeros(NbPoints),    np.zeros(NbPoints),   np.zeros(NbPoints)])
		#J[2,:,0,:] = np.array([np.zeros(NbPoints),         np.zeros(NbPoints),    np.zeros(NbPoints),   np.zeros(NbPoints)])
		#J[3,:,0,:] = np.array([np.zeros(NbPoints),         np.zeros(NbPoints),    np.zeros(NbPoints),   np.zeros(NbPoints)])

		# FILL THE JACOBIAN IN Y THERE
		#J[0,:,1,:] = np.array([np.zeros(NbPoints),        np.zeros(NbPoints),    np.zeros(NbPoints),    np.zeros(NbPoints)])
		#J[1,:,1,:] = np.array([np.zeros(NbPoints),        np.zeros(NbPoints),    np.zeros(NbPoints),    np.zeros(NbPoints)])
		#J[2,:,1,:] = np.array([np.zeros(NbPoints),        np.zeros(NbPoints),    np.zeros(NbPoints),    np.zeros(NbPoints)])
		#J[3,:,1,:] = np.array([np.zeros(NbPoints),        np.zeros(NbPoints),    np.zeros(NbPoints),    np.zeros(NbPoints)])

		# Returning the Jacobian
		return(J)

	#### Spectral radius of the equations set ####
	def SpectralRadius(self, Var, FluidIndex, n, x, *args):
		"""Computes the spectral radius associated to the flux.

		Args:
			Var         (2D numpy array):         the variables of the problem (NbVars x NbGivenPoints)
			FluidIndex  (integer numpy array):    the fluid present at each given point (NbGivenPoints)
			n           (2D numpy array):         the x-y values of the normal at each given point  (NbGivenPoints x 2)
			x           (2D numpy array):         (generally optional, required for this problem) the x-y locations at which the variables are given  (NbGivenPoints x 2)

		Returns:
			Lmb (numpy array): the spectral radius computed at each given point

		.. note::

			- | *args is there only for compatibility reason at call time
		"""

		# ---------- Cheap way that may fail in case of zero speed ------------------------------
		"""
		# Retrieving the Jacobian
		J = self.Jacobian(Var, x, FluidIndex)

		# Computing the spectral radius at each given point, shortcuted since here we are scalar
		Lmb = np.max(np.abs([np.sum(J[i,i,:,:]*n.T, axis = 0) for i in range(4)]), axis=0)
		"""

		# DEFINE THE SPECTRAL RADIUS OF THE SYSTEM THERE

		# Returning the spectral radius at each given point
		return(Lmb)

	#### Eigen values ####
	def EigenValues(self, Var, FluidIndex, n, x, *args):
		"""Computes the eigenvalues associated to the flux.

		Args:
			Var         (2D numpy array):         the variables of the problem (NbVars x NbGivenPoints)
			FluidIndex  (integer numpy array):    the fluid present at each given point (NbGivenPoints)
			n           (2D numpy array):         the x-y values of the normal at each given point  (NbGivenPoints x 2)
			x           (2D numpy array):         (generally optional, required for this problem) the x-y locations at which the variables are given  (NbGivenPoints x 2)

		Returns:
			lbd (numpy array): (NbGivenPoints x NbEigs) the eigenvalues at each given point

		.. note::

			- | *args is there only for compatibility reason at call time
		"""

		# ---------- Ugly always working way ------------------------------

		# --- Locating and extracting the right fluid's properties for each location
		# COMPUTE HERE THE EIGENVALUES AT EACH GIVEN POINT

		# Returning the values
		return(lbd)

	#### Right eigen vectors ####
	def RightEigenVectors(self, Var, FluidIndex, n, x, *args):
		"""Computes the right eigenvectors associated to the eigenvalues.

		Args:
			Var         (2D numpy array):         the variables of the problem (NbVars x NbGivenPoints)
			FluidIndex  (integer numpy array):    the fluid present at each given point (NbGivenPoints)
			n           (2D numpy array):         the x-y values of the normal at each given point  (NbGivenPoints x 2)
			x           (2D numpy array):         (generally optional, required for this problem) the x-y locations at which the variables are given  (NbGivenPoints x 2)

		Returns:
			reg (numpy array): (NbVars x MbVars x NbGivenPoints) the matrix of eigenvectors for each given point

		.. note::

			- | *args is there only for compatibility reason at call time
		"""

		# ---------------- Right eigenvalues -------------------------------------
		# Creating right eigenvalues
		REV = np.zeros((Var.shape[0], Var.shape[0], Var.shape[1]))
		# COMPUTE HERE THE RIGHT EIGENVECTORS AT EACH GIVEN POINT

		# Returning the values
		return(REV)

	#### Right eigen vectors ####
	def LeftEigenVectors(self, Var, FluidIndex, n, x, *args):
		"""Computes the left eigenvectors associated to the eigenvalues.

		Args:
			Var         (2D numpy array):         the variables of the problem (NbVars x NbGivenPoints)
			FluidIndex  (integer numpy array):    the fluid present at each given point (NbGivenPoints)
			n           (2D numpy array):         the x-y values of the normal at each given point  (NbGivenPoints x 2)
			x           (2D numpy array):         (generally optional, required for this problem) the x-y locations at which the variables are given  (NbGivenPoints x 2)

		Returns:
			reg (numpy array): (NbVars x MbVars x NbGivenPoints) the matrix of eigenvectors for each given point

		.. note::

			- | *args is there only for compatibility reason at call time
		"""

		# ---------------- Creating left eigenvalues ---------------------------
		# Creating left eigenvalues
		LEV = np.zeros((Var.shape[0], Var.shape[0], Var.shape[1]))
		# COMPUTE HERE THE LEFT EIGENVECTORS AT EACH GIVEN POINT

		# Returning the values
		return(LEV)


# ==============================================================================
#   Class containing all the methods for defining various Equation of states
# ==============================================================================
#### Class containing the methods giving back the pressure ####
class EOS():
	"""Class furnishing the methods giving the Equation of State that can be
	used in combination with the :code:`Chosen Fluid Model` definition.
	Upon creation, fills the field :code:`EOS` with the function callback
	corresponding to the wished initialisation function. The implemented EoS
	are linked as follows. See their respective documentation for more information.

		1. | EOS definition Template

	"""

	#### Automatic initialisation routine (mapper) ####
	def __init__(self, Id):
		"""
		Args:
			Id (integer):   the index corresponding to the equation of fluid the user wants when considering the governing equations given in this module and the associated fluid.

		|

		.. rubric:: Methods
		"""

		# ADAPT HERE THE MAPPING TO THE EOS
		if   Id==1:
			self.EOS     = self.EOSTemplate
			self.EoSName = "EOSTemplate"

		else: raise NotImplementedError("Error in the selection of the Equation of State\n\
		                                 Unknown index. Check your Problem definition. Abortion.")

	#### EOS for stiff fluids #####
	def EOSTemplate(self, Var, FluidProp, Type):
		"""Equation of State for stiff fluids, giving back either the internal energy or pressure.

		Args:
			Type       (string):             desired output, e.g.: "e": internal energy, "p": pressure
			Var        (float numpy array):  the value of the variables (NbVariables x NbGivenPoints). If type "e", the variables should be [rho, rhoU, rhoV, p].T. If type "p", the variables should be [rho, rhoU, rhoV, E].
			FluidProp  (FluidModel list):    the list of the fluid properties associated to each given Point

		Returns:
			ThewishedQuantity    (float numpy array):  the pressure values at the given points (NbGivenPoints)
		"""

		# Computing the pressure
		if Type == "THE WISHED RETURNED QUANTITY":
			# COMPUTE HERE THE WISHED QUANTITY FROM THE GIVEN STATE
			val = 0

		# Returning the asked value
		return(val)

# ==============================================================================
#	Class containing all the predefined suitable initial conditions
# ==============================================================================
class InitialConditions():
	"""Class furnishing the solution initialisation routines that are suitable
	to study the the Euler Equations, defined for working on a subdomain.
	Access the routine computing the initial conditions through the field :code:`IC`, filled upon creation
	with the function callback corresponding to the index of the wished initialisation function.
	The implemented initialisation method are linked as follows.
	See their respective documentation for more information.

		0. | ICTemplate

	"""

	#### Automatic initialisation routine (mapper) ####
	def __init__(self, Id, *parameters):
		"""Args:
			Id (integer):   the index corresponding to the initialisation method the user wants when considering the governing equations given in this module.
			params (list of arguments):  the (fixed) arguments to pass to the selected function

		|

		.. rubric:: Methods
		"""

		# Registering the parameters
		self.params = params

		# Mapping the initialisation routines
		if   Id == 0: self.IC = self.ICTemplate

		else: raise NotImplementedError("Error in the selection of the Initial conditions\
		                                 Unknown index. Check your Problem definition. Abortion.")


	#### SOD test case ####
	def ICTemplate(self, PointsID, Mesh, EOS=[], FluidProp=[], subdomain = [], *argv):
		"""Initialising the solution to a constant state that matches the SOD-inner value.
		(The given points should all belonging to a same subdomain).

		Args:
			PointsID   (integer array-like):     the index of the points to consider
			Mesh       (MeshStructure):          the considered mesh
			EOS        (function callback):      (optional), the equation of state given by the model of the fluid that is present at the given points
			FluidProp  (FluidSpecs):             (optional, not used here) the the properties of the fluid present where the given points are
			Subdomain  (shapely multipolyon):    (optional, not used here) the shapely polygon to which the given points belong

		Returns:
			Init       (float numpy array):      The initialised values at the considered points (NbVariables x NbGivenPoints)

		|

		.. note::

			- *args is here only for compatibility on call
			- There is usually one initialisation routine per subdomain, and the PointsID are not necessarily contigous, be careful when assigning the values back in the regular solution.
			- For running smothly the inner circle diameter of the given subdomain should be at least 2
		"""

		# CREATE THE INIT VECTOR ACCORDING TO THE NUMBER OF VARIABLES
		NbVars = 0
		Init = np.zeros((NbVars, len(PointsID)))

		# DEFINE HERE THE INITIALISATION VALUES

		# Returning the values
		return(Init)
