#***************************************************************************
# Copyright (C) 2020 Élise Le Mélédo
# This file is part of LESARD.
#
# LESARD is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LESARD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LESARD.  If not, see <https://www.gnu.org/licenses/>.
#****************************************************************************

# ==============================================================================
#                  Simple fluids definitions
# ==============================================================================

class CustomModel():
	""" Custom model that can be used to test the any simple problems or debug 
	problem that does not rely on any Fluid's properties. 
	
	|
	
	Class to copy within the file "FluidModels.py" located in the "SourceCode/ProblemDefinition" folder.
	
	|
	
	.. note::
		
		- This is only a product-type class: no method is available
		- At instance creation it takes as many input (whose values will be trashed) as wished for call time compatibility
		
	|
	
	"""

	def __init__(self, color, *args):
		"""
		Args:
			color (string or matplotlib color scheme): the color of the fluid for plotting
			others: other arguments compatible with the properties given through the Mappers.py file when defining a Fluid's type (oil, water, etc)
		"""
		
		self.color = color
		self.other = args[0]
