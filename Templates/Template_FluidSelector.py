#***************************************************************************
# Copyright (C) 2020 Élise Le Mélédo
# This file is part of LESARD.
#
# LESARD is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LESARD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LESARD.  If not, see <https://www.gnu.org/licenses/>.
#****************************************************************************

# Template to copy in _Solver/FluidSelectors/
# Add the fluid slector from its module name under a new index in the file Mappers.py


# ==============================================================================
# Importing python libraries
# ==============================================================================
from shapely.geometry import MultiLineString as ShapelyMultiLineString
from shapely.geometry import MultiPolygon    as ShapelyMultiPolygon
from shapely.geometry import Polygon         as ShapelyPolygon
from shapely.geometry import Point           as ShapelyPoint
from shapely.ops      import unary_union
import matplotlib.pyplot   as plt
import numpy               as np

# Import custom modules
import BarycentricBasisFunctions         as BF
import BarycentricTools                  as BT

# ==============================================================================
#    Defining the actuall class gathering the externally accessible functions
# ==============================================================================
class FS():
	""".. |bs|   unicode:: U+00A0 .. NO-BREAK SPACE

	Classical level set approach, initialised according to a signed distance and iterated
	with a continuous galerkin - lfx scheme, without redistancing.

	.. rubric:: Fields

	All the fields given as arguments at the instance's creation are repeated within
	the structure with the same names (see the parameters below). It also contains the
	further field

	- |bs| **FluidSelectorName** *(string)* -- the name of the level set

	|

	"""

	#### Automatic initialisation routine ####
	def __init__(self, Problem, Mesh, QuadratureRules, *Params):
		"""
		Args:
			Problem (Problem):       the considered problem
			Mesh    (MeshStructure): the considered mesh instance
			QuadratureRules  (Qudratures):            instance of quadrature class to be used when evaluating integral within the scheme
			Params           (optional, strings):     the level set's parameters as wished by the user, the first element being the order (integer)

		|

		.. rubric:: Methods
		"""


		# Setting the name of the class to be accessible from outside
		self.FluidSelectorName = "Template fluid selector"

		# Setting the mesh and parameters for an internal access
		self.Problem = Problem
		self.Mesh    = Mesh
		self.Params  = Params
		self.QuadratureRules = QuadratureRules

		# Extracting the parameters from the user input list
		if len(Params)==1: self.Order = int(Params[0])
		else: raise ValueError("Wrong number of user input argument when definining the fluid selector. Check you problem definition.")

		# Precomputing the basis function values at quadrature points for fastening the run time
		self.PreEvaluateQuadratureQuantities()	# To be removed in no basis function are needed in the scheme


	# **********************************************************
	# *  Definition of the Fluid spotter's type and dynamic    *
	# **********************************************************

	#### External initialisation routine ####
	def Initialiser(self, Solution):
		""" External initialisation routine.

		Args:
			Solution  (Solution structure): the solution instance the scheme is working on

		Returns:
			LSValues  (float numpy array):   the level set values at the MeshVertex and Dofs, to be copied to the Solution structure (NbSubdomains x (NbMeshPoints+NbDofs))
		"""

		# --------------- Shortcutting the required values ----------------------------------------------
		# Getting the subdomains list
		Subdomains = Solution.Subdomains

		# Recompute the number of dofs to drop the dependency on the mesh
		NbFluids = len(Subdomains)									# Valid because straight after initialisation
		NbDofs   = len(Solution.FluidFlag)							# Only dofs are there

		# Initialising the fluid selector values
		LSValues = np.zeros((len(Subdomains), len(Solution.FluidFlag)))

		# INITIALISE YOUR FLUID SELECTOR HERE

		# Returning the fresh LS values
		return(LSValues)

	#### Gives the dynamic of the Fluid's spotter through its flux ####
	def Flux(self, Var, Motion, *args):
		""" Flux function of the (conservative) EulerEquations

		Args:
			Var        (float numpy array):   the value of the Level Set values (NbFluids x NbGivenPoints)
			Motion     (float numpy array):   the velocity array (2xNbPoints)

		Returns:
			Flux       (3D numpy array):      the obtained flux (spatialdim x NbVars x NbPoints)

		.. note::

			*args is there for compatibility reason when e.g. a location xy is given when called

		--------------------------------------------------------------------------------------------------------------------------------"""

		# DEFINE THE EQUATION DRIVING YOUR FLUID SELECTOR HERE

		# Returning the values
		return(Flux)

	#### Jacobian of the equations set ####
	def Jacobian(self, Var, Motion, *args):
		""" .. |bs|   unicode:: U+00A0 .. NO-BREAK SPACE

		Computes the Jacobian of the flux for the advection of the LevelSet

		Args:
			Var         (float numpy array):    the value of the Level Set values (NbFluids x NbGivenPoints)
			Motion      (float numpy array):    the velocity array (2xNbPoints)

		Returns:
			J    (3D numpy array):   J[:,:,i] gives the jacobian of the flux taking care of the dynamic of the ith spatial coordinate.

		|

		.. note::

			- |bs| For each flux fi = (fi1,..., fin), the returned Jacobian reads:

			  | |bs| J[:,:] = [dfi1/dx1, ....., dfi1/dxn
			  | |bs| |bs| |bs|	|bs| |bs| |bs|	 ....
			  | |bs| |bs| |bs|	|bs| |bs| |bs|	df in/dx1, ....., dfin/dxn]

			- |bs| *args is there for compatibility reason when e.g. a location xy is given when called the x-y locations at which the variables are given  (NbGivenPoints x 2)

		---------------------------------------------------------------------"""

		# DEFINE THE JACOBIAN ASSOCIATED TO THE ABOVE FLUX HERE

		# Returning the Jacobian
		return(J)

	#### Spectral radius of the equations set ####
	def SpectralRadius(self, Var, Motion, n, *args):
		""" Computes the spectral radius associated to the motion flux.

		Args:
			Var         (2D numpy array):         the variables of the problem (NbVars x NbGivenPoints)
			Motion      (float numpy array):      the velocity array (2xNbPoints)
			n           (2D numpy array):         the x-y values of the normal at each given point  (NbGivenPoints x 2)

		Returns:
			Lmb         (numpy array):            the array containing the spectral radius at each given point

		.. note::

			*args is there for compatibility reason when e.g. a the xy locations at which the variables are given  (NbGivenPoints x 2)

		---------------------------------------------------------------------"""

		# Retrieving the Jacobian
		J = self.Jacobian(Var, Motion)

		# Computing the spectral radius at each given point, shortcuted since here we are scalar
		Lmb = np.abs([np.sum(J[i,i,:,:]*n.T, axis = 0) for i in range(self.Problem.NbFluids)])

		# Returning the obtained values
		return(Lmb)

	# ************************************************************************
	# *  Definition of the Fluid spotter's spatial impact and rectification  *
	# ************************************************************************

	#### Determining the fluid flags depending on the various values of the level set #####
	def FlagDofs(self, Solution):
		""" Flags the degrees of freedom according to the different values of the
			associated level set.

			Args:
				Solution  (Solution structure):  the solution instance the scheme is working on

			Returns:
				None: updating directly the value of FluidFlag in the solution instance
 		"""


		# FLAG THE DEGREES OF FREEDOM ACCORDING TO THE FLUID SELECTOR VALUES HERE


		# ---------------- Updating the flags at Dofs ------------------------------
		Solution.FluidFlag = 0

	#### Determining the fluid flags at a specific points locations given a cell (primal) and the considered barycentric coordinates #####
	def FlagPoints(self, LSValues, CellId=-1, x=[], FaceId=-1):
		""" Flags the degrees of freedom according to the different values of the
			associated level set.

			Args:
				LSValues  (float numpy multidarray):  the vector within which the values of the level set at dofs are stored (full vector in the same format as stored in Solution.LSValues)
				CellId    (integer):                  the id of the primal cell the points under consideration lie on (important esp. for points on the cell's boundary)
				x         (2D numpy array):           (optional) barycentric coordinates of the point under consideration, ordered as the physical vertices of the element (NbPoints x NbBarycoor)
				                                      If not given the FlagDofs will be simply computed at the Dofs of the CellId.
				FaceId    (integer):                  When CellId and x are given, the face where the given point lie (important for code speedup only)

			Returns:
				Flags (integer list):  If CellId and x are given, the list of indices corresponding to the fluid located at the positions given in x.
				                       If CellId is given but not x, the list of indices corresponding to the fluid located at the Dofs (according to their FV representation)
				                       If no CellId neither x is given, the vector within which the values of the level set at dofs are stored (full vector in the same format as stored in LSValues)
		"""

		# FLAG THE GIVEN POINTS ACCORDING TO THE FLUID SELECTOR VALUES HERE, POSSIBLY RECONSTRUCTED

		return(Flags)

	#### Routine that maps the values from the Dofs to the Physical vertices of the mesh ####
	def ReconstructFSAtVertices(self, Solution):
		""" Routine that maps the values from the Dofs to the Physical vertices of the mesh
		of the FluidSpotter and flags.

		Args:
			Solution  (Solution):  the currently being computed solution

		Returns:
			None: fills direclty the RSol and RFluidFlag values in the data structure

		.. Note::

			This is only for later plotting purposes.
		"""

		# PROCESS HERE THE RECONSTRUCTION OF THE FLUID SELECTOR VALUES
		# IN ORDER TO UPDATE THE VARIABLES
		Solution.RLSValues
		Solution.RFluidFlag

	#### Recomputing the subdomains according to the new values of the level set #####
	def UpdateSubdomains(self, Solution):
		""" Method modifying the subdomains associated to the Solution upon the values of
		LSValues.

		Args:
			Solution  (Solution):  the solution instance the scheme is working on

		Returns:
			None: updating directly the subdomains in the solution instance

		.. warning:: Not safe yet if the first path given out is internal
		"""

		# Update each of the fluid's locations
		for i in range(self.Problem.NbFluids):

			# UPDATE THE FLUID'S SUBDOMAINS HERE

			Solution.Subdomains[i] = polygon

	#### Redistancing the LSvalues according to the freshly computed level set values ####
	def Redistancing(self, Solution):
		""" Redistances the Level set values upon the computed subdomains.

		Args:
			Solution  (Solution):  the solution instance the scheme is working on

		Returns:
			None:  updating directly the LSValues in the solution instance
		"""

		for i in range(self.Problem.NbFluids):

			# REDISTANCE THE FLUID SELECTOR VALUE IF NEEDED THERE

			# Computing the signed distance
			Solution.LSValues[i,:] = np.prod(Value, axis=0)


	# ****************************************************************************
	# *  Definition of the Fluid spotter's evolution's process (e.g. advection)  *
	# ****************************************************************************

	#### Emulates a flux computation as done in the iteration routine ####
	def ComputeFlux(self, U, LSValues):
		""" Emulates a flux computation as done in the iteration routine in order
			to be used in the DeC subtimestep evaluation.

		Args:
			U           (numpy float array):  buffer containing the current state values at each dof (NbVars   x NbDofs)
			LS          (numpy float array):  buffer containing the current FluidSpotters values at each dof(NbFluids x NbDofs)

		Returns:
			fluxes      (numpy float (multiD)array):   fluxes in the format requires by the UpdateFSValues routine
		"""

		# DEFINE HERE THE FLUXES INVOLVED IN THE FLUIDS'S SELECTOR SPATIAL ITERATION SCHEME

		# Returning the full array
		return(fluxes)

	#### (Crucial routine) Spatial scheme to update the level set ####
	def Iteration(self, Solution, fluxes, i, du=0, dt=1):
		""" Main iteration of the scheme, implementing the continuous galerkin scheme.

		Args:
			Solution   (Solution):             structure containing the current solution's values to iterate
			fluxes     (numpy (multiD)array):  pre-computed fluxes at the points of interest. For this scheme, access with fluxes[element, face, coordinate(fx or fy), variable, pointindex]
			i          (integer):              the index of the considered element within which the partial residuals will be computed
			du         (float numpy array):    (optional) when using DeC, the difference in the time iteration
			dt         (float):                (optional) when using DeC, the full time step

		Returns:
			Resu       (float numpy array):    the computed residuals (NbVars x NbDofs)
		"""

		# DEFINE HERE THE SPATIAL SCHEME TO EVOLVE THE FLUID SELECTOR VALUES
		# USE THE EXTERNAL FLUXES VALUES FOR COMPATIBILITY WITH DEC

		# Returning the partial residuals
		return(Resu)

	#### Wrapper routine to update the quantitites that depend on the UpdateFSValues output ####
	def UpdateFSVAttributes(self, Solution):
		""" Wrapper routine to update the quantitites that depend on the UpdateFSValues fresh output

		Args:
			Solution   (Solution):  the solution instance the scheme is working on

		Returns:
			None: updating directly the subvalues in the solution instance
		"""

		# DEFINE HERE THE SEQUENCE OF ACTION POST-FLUID SPOTTER RESIDUAL COMPUTATION
		# OF ONE (SUB)TIME-STEP

		return(0)


	# **************************************************************
	# *  Definition of the Fluid spotter's scheme basis function   *
	# **************************************************************

	# Precomputing and storing the values of the basis functions at the quadrature points of each element
	def PreEvaluateQuadratureQuantities(self):
		"""
		Precomputing and storing the values of the basis functions at the quadrature points of each element

		Input:
			None:  Considers the given parameters of the class directly

		Returns:
			None: Creates the fields InnerBFValues and FaceWiseBFValues in the scheme structure, containing
			      the values of each basis function at each quadrature point for each element and face.
		.. note::

			Depending on your scheme, this may not be required. In this case, remove the line 70.
		"""

		# COMPUTE HERE THE VALUES OF THE QUADRATURE POINTS AND BASIS FUNCTIONS THERE THAT WILL NOT BE TIME-DEPENDENT
		# FILL THE FIELDS:
		self.InnerQBFValues    = [[]]*self.Mesh.NbInnerElements
		self.InnerQWeigths     = [[]]*self.Mesh.NbInnerElements
		self.InnerQGFValues    = [[]]*self.Mesh.NbInnerElements
		self.InnerQPoints      = [[]]*self.Mesh.NbInnerElements
		self.FaceWiseQWeigths  = [[]]*self.Mesh.NbInnerElements
		self.FaceWiseQPoints   = [[]]*self.Mesh.NbInnerElements
		self.FaceWiseQBFValues = [[]]*self.Mesh.NbInnerElements
