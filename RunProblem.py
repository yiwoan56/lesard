#***************************************************************************
# Copyright (C) 2020 Élise Le Mélédo
# This file is part of LESARD.
#
# LESARD is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LESARD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LESARD.  If not, see <https://www.gnu.org/licenses/>.
#****************************************************************************


"""
|

The file :code:`RunProblem.py` provides a simple interface to run the desired solver on
a target problem for a given mesh resolution. When targetting a problem to solve, make
sure that:

    - the problem of interest is already defined according to the given template (see the problem creation part of the :ref:`UserManual`) and is stored in the :code:`PredefinedTests/PredefinedProblems` folder (or corresponding if the Pathes got edited in the file `Pathes.py`)
    - the mesh associated to the problem is stored in the :code:`PredifinedTests/PredefinedMeshes` folder (or corresponding if the Pathes got edited in the file `Pathes.py`) and the wished resolution is available
    - the wished solver settings to consider for computing the numerical solution is valid (see the compatibility table given on the documentation homepage and the settings' file format in the :ref:`UserManual`), and is stored in the  folder :code:`PredefinedTests/PredefinedSettings` (or corresponding if the Pathes got edited in the file `Pathes.py`) and the wished resolution is available
|

The numerical solution can be computed by editing the fields of the file accordingly. By example, for solving an advection-translation problem
on two fluids, using a coarse mesh and a first order solver one can set the fields of the file :code:`RunProblem.py` to

.. code::

    ProblemDefinition = "Advection_Translation_2Fluids"
    SettingsChoice    = "FirstOrderGalerkin"
    MeshResolution    = 20

|

Solving the problem is then done as simply by the two lines

.. code::

    import SourceCode.Solve  as Solve
    Solve.Solve(ProblemDefinition, SettingsChoice, MeshResolution)

|

There is then just left to run the script by

.. code::

    $ python RunProblem.py

|

The solution will be periodically exported as binary pickle and vtk file in the folder :code:`Solutions/$ProblemDefinition_$SettingsChoice/Res$MeshResolution/RawResults`, where the $ indicates that
the associated variables names are used.

|

.. warning::

    Computing a solution to a problem will automatically erase any previous computation performed over the same tripplet (problem, resolution, solver).

|

.. note::

    - Make sure that you are using python3, otherwise some module-depency may fail (in particular)
    - The solution is only computed and periodically exported. No post-processing task is performed through this script.

"""

# ====================================================
#  Preliminary imports                               #
# ====================================================
import SourceCode.Solve  as Solve

# ====================================================
#  PostProcessing wrapper                            #
# ====================================================
if __name__ == '__main__':
    # Point to the problem you would like to solve, and define how you would like to tackle it
    ProblemDefinition = "Euler_KarniR22"
    SettingsChoice    = "SecondOrderGalerkin"
    MeshResolution    = 20

    # Solve the problem
    Solve.Solve(ProblemDefinition, SettingsChoice, MeshResolution)
