This code furnishes a user-friendly script that attemps to solve MultiPhase problems with LevelSets under the Residual Distribution framework. To have a full description of the features, please refer to the homepage of the documentation: https://www.math.uzh.ch/pages/lesard/

**_How to cite:_**
```
@misc{lesard,
	author       = {Le M{\'e}l{\'e}do, {\'E}lise},
	title        = "{LESARD: LEvel Set algorithm for Residual Distribution}",
	year         = {2020},
	howpublished = {GitLab repository: \url{https://git.math.uzh.ch/elemel/lesard}},
	note         = {Documentation: \url{https://www.math.uzh.ch/pages/lesard/}},
}
```


**_Note:_**
Only coarse meshes are proposed when forking the master branch, a more complete set of pretermine meshes is available in the branch ExampleMeshes, and you can generate your own with the asset code "GeneralMeshing".

