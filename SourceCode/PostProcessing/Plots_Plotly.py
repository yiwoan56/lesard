#***************************************************************************
# Copyright (C) 2020 Élise Le Mélédo
# This file is part of LESARD.
#
# LESARD is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LESARD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LESARD.  If not, see <https://www.gnu.org/licenses/>.
#****************************************************************************

"""

The module :code:`Plots_Plotly` located in the :code:`SourceCode/Postprocessing` file  proposes
visualisation routines for solution of a multi
phase problem which is stored in a Solution structure, in Plotly. It contains plotting 2D solutions, plotting 3D solutions
and animation routines.

|

.. note::

    The module is made to be used on-line, that is either while computing the solution (except videos) or after an exported solution has been loaded

|

.. warning::

    When the level set is constant (i.e. 1 fluid), the 3D plot of the level set may fail

|
|

"""

# ==============================================================================
#	Preliminary imports
# ==============================================================================
# Plots requirements
from matplotlib.path  import Path  as mpltPath

# Html plot requirements
from   plotly.subplots       import make_subplots
import plotly.graph_objects  as go
import plotly.figure_factory as ff

# Math packages
import scipy.interpolate
import numpy  as np

# Pythonic packages
import os, sys, glob
import copy, re

# Import local pathes and modules
import PostProcessing.Exports as EX
import Pathes

# ==============================================================================
# Fixed customisation properties
# ==============================================================================
# Defining usable color schemes in the order that makes them distinguishable when used next to each other
ColorMapsPlotly = ['aggrnyl', 'agsunset', 'algae', 'amp', 'armyrose', 'balance',
                   'blackbody', 'bluered', 'blues', 'blugrn', 'bluyl', 'brbg',
                   'brwnyl', 'bugn', 'bupu', 'burg', 'burgyl', 'cividis', 'curl',
                   'darkmint', 'deep', 'delta', 'dense', 'earth', 'edge', 'electric',
                   'emrld', 'fall', 'geyser', 'gnbu', 'gray', 'greens', 'greys',
                   'haline', 'hot', 'hsv', 'ice', 'icefire', 'inferno', 'jet',
                   'magenta', 'magma', 'matter', 'mint', 'mrybm', 'mygbm', 'oranges',
                   'orrd', 'oryel', 'peach', 'phase', 'picnic', 'pinkyl', 'piyg',
                   'plasma', 'plotly3', 'portland', 'prgn', 'pubu', 'pubugn', 'puor',
                   'purd', 'purp', 'purples', 'purpor', 'rainbow', 'rdbu', 'rdgy',
                   'rdpu', 'rdylbu', 'rdylgn', 'redor', 'reds', 'solar', 'spectral',
                   'speed', 'sunset', 'sunsetdark', 'teal', 'tealgrn', 'tealrose',
                   'tempo', 'temps', 'thermal', 'tropic', 'turbid', 'twilight',
                   'viridis', 'ylgn', 'ylgnbu', 'ylorbr', 'ylorrd']

# Defining color names that are acceptable in the CSCcolor table
CSSColor = [ 'greenyellow', 'aliceblue', 'lightgreen', 'antiquewhite', 'aqua', 'aquamarine', 'azure',
             'beige', 'bisque', 'black', 'blanchedalmond', 'blue',
             'blueviolet', 'brown', 'burlywood', 'cadetblue',
             'chartreuse', 'chocolate', 'coral', 'cornflowerblue',
             'cornsilk', 'crimson', 'cyan','darkblue',' darkcyan',
             'darkgoldenrod', 'darkgray', 'darkgrey', 'darkgreen',
             'darkkhaki', 'darkmagenta', 'darkolivegreen', 'darkorange',
             'darkorchid','darkred', 'darksalmon', 'darkseagreen',
             'darkslateblue', 'darkturquoise', 'dimgray', 'dimgrey',
             'floralwhite', 'ivory', 'khaki',
             'lemonchiffon', 'lightgoldenrodyellow', 'lightgreen', 'lightskyblue',
             'lightsteelblue', 'mediumseagreen', 'mediumturquoise', 'oldlace',
             'palevioletred', 'royalblue', 'skyblue', 'turquoise']

# ==============================================================================
#  Tools, solution and path readers
# ==============================================================================

# ----------------- Interpolators ----------------------------------------------

#### Converts three vectors to a surface, masking the xy coodinates when holes ####
def GetMeshgridFom3VectorsMaskX(Mesh, X, Y, Z):
    """ Interpolation routine in order to plot.

    .. |bs|   unicode:: U+00A0 .. NO-BREAK SPACE

    Args:
        Mesh       (MeshStructure):        the mesh in use
        X          (float array like):        the x coordinates of the considered points
        Y          (float array like):        the y coordinates of the considered points
        Z          (float array like):        the data's (scalar) value at the considered points

    .. rubric:: Returns

    - |bs| **xgrid** *(float 2D numpy array)*   --    x meshgrid of the points where the solution Z has been interpolated
    - |bs| **ygrid** *(float 2D numpy array)*   --    y meshgrid of the points where the solution Z has been interpolated
    - |bs| **zgrid** *(float 2D numpy array)*   --    values of the interpolated Z at the generated grid points

    |

    .. note::

        - When holes in the domains, the xy values are masked with None in both corresponding meshgrids
        - The x resolution of the interpolation grid is set to 5*(xmax-xmin)/np.min(Mesh.InnerElementsDiameter), and respectively for the y axis
        - The interpolation method is set to "linear", with a fill value of "-1e-15" in order to fit the 0 crossing at a physical boundary in case of interpolating a level set.
    """

    # ------------------------------- Initialisations -----------------------------------------
    # Generating points in a square range of the scatter vector
    xgrid = Mesh.CartesianPoints[:,0]
    ygrid = Mesh.CartesianPoints[:,1]

    gxmin, gxmax = np.min(xgrid), np.max(xgrid)
    gymin, gymax = np.min(ygrid), np.max(ygrid)

    gridx = np.linspace(gxmin, gxmax, int(5*(gxmax-gxmin)/np.min(Mesh.InnerElementsDiameter)))
    gridy = np.linspace(gymin, gymax, int(5*(gymax-gymin)/np.min(Mesh.InnerElementsDiameter)))

    # ------------------------ Interpolating on a new grid -----------------------------------------
    # Generating the corresponding grid points
    xgrid, ygrid = np.meshgrid(gridx, gridy)

    # Interpolating with a fill-value, which is not impacting as the concerned points will be left out in the patch construction
    zgrid = scipy.interpolate.griddata((X,Y),Z, (xgrid, ygrid),method='linear',fill_value="-1e-15")

    # -------- Selecting points of the previous rectangle that actually belongs to the polygon --------
    # Converting the grid to scatter points
    pointsX=np.array(np.ndarray.ravel(xgrid))
    pointsY=np.array(np.ndarray.ravel(ygrid))

    # Converting the rectangle points in a suitable format
    points2=np.column_stack((pointsX,pointsY))

    # Building the path corresponding to the polygon
    polygons = [np.array(Dom) for Dom in Mesh.Domain]
    pathes = [mpltPath(polygon) for polygon in polygons]

    # Extract the points that are inside or on the path contour
    inside2 = np.prod(np.array([pathes[0].contains_points(points2, radius=1e-15)]+\
                               [    ~path.contains_points(points2, radius=1e-15)\
                                for path in pathes[1:]]), axis=0)

    # Select the considered indices and reshape them to match the grid
    ind  = np.where(inside2==False)
    ind2 = np.unravel_index(ind, np.shape(xgrid))

    # --------- Final processing and giving out the data  ----------------------
    # Masking the points that fall out
    xgrid[ind2] = None
    ygrid[ind2] = None

    # Giving back the interpolated values
    return(xgrid,ygrid,zgrid)

#### Converts three vectors to a surface, masking the z coodinates when holes ####
def GetMeshgridFom3VectorsMaskZ(Mesh, X, Y, Z):
    """ Interpolation routine in order to plot.

    Args:
        Mesh       (MeshStructure):        the mesh in use
        X          (float array like):        the x coordinates of the considered points
        Y          (float array like):        the y coordinates of the considered points
        Z          (float array like):        the data's (scalar) value at the considered points

    .. rubric:: Returns

    - |bs| **xgrid** *(float 2D numpy array)*   --    x meshgrid of the points where the solution Z has been interpolated
    - |bs| **ygrid** *(float 2D numpy array)*   --    y meshgrid of the points where the solution Z has been interpolated
    - |bs| **zgrid** *(float 2D numpy array)*   --    values of the interpolated Z at the generated grid points

    |

    .. note::

        - When holes in the domains, the z values are masked with None in both corresponding meshgrids
        - The x resolution of the interpolation grid is set to 5*(xmax-xmin)/np.min(Mesh.InnerElementsDiameter), and respectively for the y axis
        - The interpolation method is set to "linear", with a fill value of "-1e-15" in order to fit the 0 crossing at a physical boundary in case of interpolating a level set.
    """

    # ------------------------------- Initialisations -----------------------------------------
    # Generating points in a square range of the scatter vector
    xgrid = Mesh.CartesianPoints[:,0]
    ygrid = Mesh.CartesianPoints[:,1]

    gxmin, gxmax = np.min(xgrid), np.max(xgrid)
    gymin, gymax = np.min(ygrid), np.max(ygrid)

    gridx = np.linspace(gxmin, gxmax, int(5*(gxmax-gxmin)/np.min(Mesh.InnerElementsDiameter)))
    gridy = np.linspace(gymin, gymax, int(5*(gymax-gymin)/np.min(Mesh.InnerElementsDiameter)))
    #gridx = np.linspace(gxmin, gxmax, len(xgrid))
    #gridy = np.linspace(gymin, gymax, len(ygrid))

    # ------------------------ Interpolating on a new grid -----------------------------------------
    # Generating the corresponding grid points
    xgrid, ygrid = np.meshgrid(gridx, gridy)

    # Interpolating with a fill-value, which is not impacting as the concerned points will be left out in the patch construction
    zgrid = scipy.interpolate.griddata((X,Y), Z, (xgrid, ygrid), method='cubic', fill_value="-1e-15")         # somehow linear will fail if the solution is constant

    # -------- Selecting points of the previous rectangle that actually belongs to the polygon --------
    # Converting the grid to scatter points
    pointsX = np.array(np.ndarray.ravel(xgrid))
    pointsY = np.array(np.ndarray.ravel(ygrid))

    # Converting the rectangle points in a suitable format
    points2=np.column_stack((pointsX,pointsY))

    # Building the path corresponding to the polygon
    polygons = [np.array(Dom)     for Dom     in Mesh.Domain]
    pathes   = [mpltPath(polygon) for polygon in polygons]

    # Extract the points that are inside or on the path contour
    inside2 = np.prod(np.array([pathes[0].contains_points(points2, radius=1e-15)]+\
                               [    ~path.contains_points(points2, radius=1e-15)\
                                for path in pathes[1:]]), axis=0)

    # Select the considered indices and reshape them to match the grid
    ind  = np.where(inside2 == False)
    ind2 = np.unravel_index(ind, np.shape(zgrid))

    # --------- Final processing and giving out the data  ----------------------
    # Masking the points that fall out
    zgrid[ind2] = None

    # Giving back the interpolated values
    return(xgrid,ygrid,zgrid)

# ----------------- General plotters -------------------------------------------

#### Generic plotting routine from plotly that considers any 3D frame ####
def GenerateFigurePlotly3D(Frame, bounds, Title = "Plot Title", Zaxis = "Zaxis"):
    """ A generic 3D plotly routine that encapsulates a plotted object in a figure.

    Args:
        Frame      (Plotly object):           any plotly content that can be incorporated in a figure
        bounds     (float array-like):        the xmin-xmax bounds of the plot (the same will be used for the y axis, guaranteeing the equal option: always use the larger data to not cut-off the picture unwinlingly)
        title      (string):                  (optional) the title of the figure
        Zaxis      (string):                  the zaxis label to be set (default = "Zaxis")

    Returns:
        fig        (plotly figure instance):  the figure in Plotly format

    .. note::

        The figure is only generated, not shown neither exported
    """

    # Creating a figure instance
    fig = go.Figure(data=Frame)

    # Setting the layout w.r.t. plotly syntax
    fig.update_layout(autosize   = False,
                      showlegend = True,
                      width  = 800,
                      height = 600,
                      title  = Title,
                      scene  = dict(xaxis = dict(range=[bounds[0],bounds[1]]),
                                    yaxis = dict(range=[bounds[0],bounds[1]]),
                                    xaxis_title = 'x',
                                    yaxis_title = 'y',
                                    zaxis_title = Zaxis),
                      font   = dict(family = "Arial",
                                    size   = 14,
                                    color  = "#7f7f7f"))

    # Returning the dictionnary defining the figure
    return(fig)

#### Generic plotting routine from plotly ####
def GenerateFigurePlotly2D(Frame, bounds, Title="Plot Title"):
    """ A generic 2D plotly routine that encapsulates a plotted object in a figure.

    Args:
        Frame      (Plotly object):           any plotly content that can be incorporated in a figure
        bounds     (float array-like):        the xmin-xmax bounds of the plot (the same will be used for the y axis, guaranteeing the equal option: always use the larger data to not cut-off the picture unwinlingly)
        title      (string):                  (optional) the title of the figure

    Returns:
        fig        (plotly figure instance):  the figure in Plotly format

    .. note::

        The figure is only generated, not shown neither exported
    """

    # Creating a figure instance
    fig = go.Figure(data=Frame)

    # Defining the figure attributes in a plotly syntax
    fig.update_layout(autosize   = False,
                      width      = 800,
                      height     = 600,
                      title      = Title,
                      showlegend = True,
                      scene = dict( xaxis = dict(range=[bounds[0],bounds[1]],),
                                    yaxis = dict(range=[bounds[0],bounds[1]]),
                                    xaxis_title = 'x',
                                    yaxis_title = 'y'),
                      font = dict(family = "Courier New, monospace",
                                  size   = 14,
                                  color  = "#7f7f7f"))

    # Returning the figure instance
    return(fig)

#### Generic routine that creates a plotly 3D animation #####
def PlotAnimation3D(LightExport, TestName, Resolution, FrameFunc, cmin, cmax, args, Title = "Plot Title", Zaxis = "Zaxis"):
    """ Routine that creates a 3D plotly hmtl animation given the Frame function
        framefunc and the solutions that are stored in the TestName folder.

        Args:
            LightExport (integer):            integer telling if the results have been exported with a light (1, without mesh) or full export (0)
            TestName    (string):             the name of the folder where the results are stored
            Resolution  (integer):            the resolution of the associated mesh
            FrameFunc   (function callback):  the plotting function furnishing each frame, whose three first arguments should be Problem, Mesh, Solution, cbounds
            cmin        (float):              the minimum value of the plotted quantities
            cmax        (float):              the maximum value of the plotted quantities
            args        (array-like):         containing the further arguments to be passed to FrameFunc
            Title       (string):             (optional) the title of the animation
            Zaxis       (string):             (optional) the label of the z axis

        Returns:
            fig         (plotly animation):   a plotly animation
    """

    # ----------- Collecting the solution figures from the given data -------------------
    # Selecting all the available files w.r.t. to the exported time points
    lst = glob.glob(os.path.join(Pathes.SolutionPath, TestName, "Res"+str(Resolution).replace(".","_"), "RawResults","*.sol"))
    Times = np.sort([float(FileName[-12:-4].replace("_",".")) for FileName in lst])

    # Creating one frame per time-point
    Frames = []
    for t in range(len(Times)):
        if LightExport == 0: Mesh, ProblemData, Problem, Solution = EX.LoadSolution(TestName, Resolution, Times[t])
        else:                Mesh, ProblemData, Problem, Solution = EX.LoadSolutionMeshFree(TestName, Resolution, Times[t])
        FrameContent = FrameFunc(Problem, Mesh, Solution, [cmin, cmax], *args)
        Frames       = Frames + [go.Frame(data=FrameContent, name=str(t))]

    # ------------- Creating the animation ---------------------------------------------
    # Recomputing the initial snapshot
    if LightExport == 0: Mesh, ProblemData, Problem, Solution = EX.LoadSolution(TestName, Resolution, Times[0])
    else:                Mesh, ProblemData, Problem, Solution = EX.LoadSolutionMeshFree(TestName, Resolution, Times[0])
    FrameContent = FrameFunc(Problem, Mesh, Solution, [cmin, cmax], *args)

    # Setting the xy bounds upon it (assuming non-growing domain)
    xmin, xmax = np.min(Mesh.CartesianPoints[:,0]), np.max(Mesh.CartesianPoints[:,0])
    ymin, ymax = np.min(Mesh.CartesianPoints[:,1]), np.max(Mesh.CartesianPoints[:,1])

    # Initialising figure and setting the first snapshot  up
    fig = go.Figure(frames=Frames)
    for Frame in FrameContent: fig.add_trace(Frame)

    # Defining the frame transition
    FrameChange = {"frame": {"duration": 0},
                   "mode":  "immediate",
                   "fromcurrent": True,
                   "transition": {"duration": 0, "easing": "linear"}}
    FrameEnd = {"frame": {"duration": len(Times)},
                   "mode":  "immediate",
                   "fromcurrent": True,
                   "transition": {"duration":  len(Times), "easing": "linear"}}

    # Defining the slider properties
    sliders = [{"pad": {"b": 1, "t": 1}, "len": 0.9,
                "x": 0.1, "y": 0,
                "steps": [{"args": [[f.name], FrameChange],
                           "label": str(k),
                           "method": "animate"}
                           for k, f in enumerate(fig.frames)]}]

    # Updating the figure's layout
    fig.update_layout(title      = Title,
                      autosize   = False,
                      showlegend = True,
                      width      = 800,
                      height     = 600,
                      scene  = dict(xaxis = dict(range=[xmin, xmax]),
                                    yaxis = dict(range=[ymin, ymax]),
                                    zaxis = dict(range=[cmin, cmax]),
                                    xaxis_title ='x',
                                    yaxis_title ='y',
                                    zaxis_title = Zaxis),
                      font   = dict(family = "Arial", size   = 14, color  = "#7f7f7f"),
                      updatemenus = [{"buttons": [{"args": [None, FrameEnd],
                                                   "label": "&#9654;",
                                                   "method": "animate"},
                                                  {"args": [[None], FrameChange],
                                                   "label": "&#9724;",
                                                   "method": "animate"}],
                                         "direction": "left",
                                         "pad": {"r": 1, "t": 1},
                                         "type": "buttons",
                                         "x": 0.1,
                                         "y": 0}],
                      sliders=sliders)

    # Returning thre created animation
    return(fig)

#### Generic routine that creates a plotly 2D animation #####
def PlotAnimation2D(LightExport, TestName, Resolution, FrameFunc, cmin, cmax, args, Title = "Plot Title"):
    """ Routine that creates a 2D plotly hmtl animation given the Frame function
        framefunc and the solutions that are stored in the TestName folder.

        Args:
            LightExport (integer):            integer telling if the results have been exported with a light (1, without mesh) or full export (0)
            TestName    (string):             the name of the folder where the results are stored
            Resolution  (integer):            the resolution of the associated mesh
            FrameFunc   (function callback):  the plotting function furnishing each frame, whose three first arguments should be Problem, Mesh, Solution, cbounds
            cmin        (float):              the minimum value of the plotted quantities
            cmax        (float):              the maximum value of the plotted quantities
            args        (array-like):         containing the further arguments to be passed to FrameFunc
            Title       (string):             (optional) the title of the animation

        Returns:
            fig         (plotly animation):   a plotly animation
    """

    # ----------- Collecting the solution figures from the given data -------------------
    # Selecting all the available files w.r.t. to the exported time points
    lst = glob.glob(os.path.join(Pathes.SolutionPath, TestName, "Res"+str(Resolution).replace(".","_"), "RawResults","*.sol"))
    Times = np.sort([float(FileName[-12:-4].replace("_",".")) for FileName in lst])

    # Creating one frame per time-point
    Frames = []
    for t in range(len(Times)):
        if LightExport == 0: Mesh, ProblemData, Problem, Solution = EX.LoadSolution(TestName, Resolution, Times[t])
        else:                Mesh, ProblemData, Problem, Solution = EX.LoadSolutionMeshFree(TestName, Resolution, Times[t])
        FrameContent = FrameFunc(Problem, Mesh, Solution, [cmin, cmax], *args)
        Frames       = Frames + [go.Frame(data=FrameContent, name=str(t))]

    # ------------- Creating the animation ---------------------------------------------
    # Recomputing the initial snapshot
    if LightExport == 0: Mesh, ProblemData, Problem, Solution = EX.LoadSolution(TestName, Resolution, Times[0])
    else:                Mesh, ProblemData, Problem, Solution = EX.LoadSolutionMeshFree(TestName, Resolution, Times[0])
    FrameContent = FrameFunc(Problem, Mesh, Solution, [cmin, cmax], *args)

    # Setting the xy bounds upon it (assuming non-growing domain)
    xmin, xmax = np.min(Mesh.CartesianPoints[:,0]), np.max(Mesh.CartesianPoints[:,0])
    ymin, ymax = np.min(Mesh.CartesianPoints[:,1]), np.min(Mesh.CartesianPoints[:,1])

    # Initialising figure and setting the first snapshot  up
    fig = go.Figure(frames=Frames)
    for Frame in FrameContent: fig.add_trace(Frame)

    # Defining the frame transition
    FrameChange = {"frame": {"duration": 0},
                   "mode":  "immediate",
                   "fromcurrent": True,
                   "transition": {"duration": 0, "easing": "linear"}}
    FrameEnd = {"frame": {"duration": len(Times)},
                   "mode":  "immediate",
                   "fromcurrent": True,
                   "transition": {"duration":  len(Times), "easing": "linear"}}

    # Defining the slider properties
    sliders = [{"pad": {"b": 1, "t": 1}, "len": 0.9,
                "x": 0.1, "y": 0,
                "steps": [{"args": [[f.name], FrameChange],
                           "label": str(k),
                           "method": "animate"}
                           for k, f in enumerate(fig.frames)]}]

    # Updating the figure's layout
    fig.update_layout(title      = Title,
                      autosize   = False,
                      showlegend = True,
                      width      = 800,
                      height     = 600,
                      scene  = dict(xaxis = dict(range=[xmin, xmax]),
                                    yaxis = dict(range=[ymin, ymax]),
                                    xaxis_title ='x',
                                    yaxis_title ='y'),
                      font   = dict(family = "Arial", size   = 14, color  = "#7f7f7f"),
                      updatemenus = [{"buttons": [{"args": [None, FrameEnd],
                                                   "label": "&#9654;",
                                                   "method": "animate"},
                                                  {"args": [[None], FrameChange],
                                                   "label": "&#9724;",
                                                   "method": "animate"}],
                                         "direction": "left",
                                         "pad": {"r": 1, "t": 1},
                                         "type": "buttons",
                                         "x": 0.1,
                                         "y": 0}],
                      sliders=sliders)

    # Returning thre created animation
    return(fig)


# ==============================================================================
#  Plotting 3D Solutions
# ==============================================================================

# ---------------------------- Level sets --------------------------------------
#### Plot all the level sets on one single 3D plot #####
def PlotAllLevelSets(Mesh, Solution, ParametersId):
    """ Plots all the level sets on the computational domain.

    Args:
        Mesh           (MeshStructure):       the considered mesh
        Solution       (Solution):            the solution of the considered problem
        ParametersId   (string):              the string forming the "ProblemName_ParameterName", mapping to the right solution export folder

    Returns:
        None: The 3D plot of all the levels set on the considered domain are exported in the problem-dependent solution export folder.

    .. note::

        The figure is only exported, not shown
    """

    # ----------------------- Retrieving the solution's values ---------------------------
    # Select the data to plot
    X = Mesh.CartesianPoints[:,0]
    Y = Mesh.CartesianPoints[:,1]
    Z = Solution.RLSValues

    # -------------------- Customisation properties --------------------------------------
    Name   = "Levels sets of all fluids"
    bounds = [np.min([*X,*Y]),np.max([*X,*Y])]

    # -------------------- Generating the figure -----------------------------------------
    # Initialising the frames to be plotted
    FrameToT = [[]]*np.shape(Z)[0]

    # Plotting the LS both one by one and on the same figure
    for Id in range(np.shape(Z)[0]):
        # Getting a surface from the vectors
        xx,yy,zz = GetMeshgridFom3VectorsMaskZ(Mesh,X,Y,Z[Id,:])

        # Generating the surface and their 0 level
        Frame = go.Surface(x = copy.deepcopy(xx),
                           y = copy.deepcopy(yy),
                           z = copy.deepcopy(zz),
                           contours   = { "z": {"show": True, "start": 0, "end": 0.1, "size": 0.1}},
                           colorscale = ColorMapsPlotly[Id],
                           name       = Name,
                           opacity    = 0.8,
                           showscale  = False)

        # Extending the frames to plot
        FrameToT[Id] = Frame

    # Creating the figure instance upon all the above generated surfaces
    fig = GenerateFigurePlotly3D(FrameToT, bounds, Name, "LS value")

    # --------------- Export the figure at the right location -------------------------
    # Creating the export folder if not already existing
    FolderName = os.path.join(Pathes.SolutionPath, ParametersId, "Res"+str(Mesh.MeshResolution).replace(".","_"), "DomainPlots")
    try:    os.makedirs(FolderName)
    except: pass

    # Creating the file name and exporting it in the right folder
    try:
        FileName = ("Plty_LSValues3D_All_t{0:8.6f}".format(Solution.t)).replace(".","_")
        fig.write_html(os.path.join(FolderName, FileName)+".html")
    except:
        print("WARNING: Error in the plot generation. Impossible export.")

#### Generates the frame corresponding to a level set ####
def GenerateFrameEachLevelSet(Problem, Mesh, Solution, cbounds, VariableId, ParametersId):
    """ Generates a figure's frame of the slevel set variableId on the full computational domain.

    Args:
        Problem        (Problem):             the considered problem
        Mesh           (MeshStructure):       the considered mesh
        Solution       (Solution):            the solution of the considered problem
        cbounds        (float array-like):    values determining the bounds of the color scale to use
        VariableId,    (integer):             the index of the desired variable to be plot
        ParametersId   (string):              the string forming the "ProblemName_ParameterName", mapping to the right solution export folder

    Returns:
        None: The frame representing the solution on the considered domain with a subdomain-dependent color scale.

    .. note::

        The routine has been split from PlotSolutionOnEachSubdomain to ease the construction of a video frame later
    """

    # ----------------------- Retrieving the solution's values ---------------------------
    # Select the data to plot
    X = Mesh.CartesianPoints[:,0]
    Y = Mesh.CartesianPoints[:,1]
    Z = Solution.RLSValues

    # -------------------- Customisation properties --------------------------------------
    # XY bounds of the figure
    bounds = [np.min([*X,*Y]),np.max([*X,*Y])]

    # --------------  Preparing the export at the right location -------------------------
    FolderName = os.path.join(Pathes.SolutionPath, ParametersId, "Res"+str(Mesh.MeshResolution).replace(".","_"), "DomainPlots")
    try:    os.makedirs(FolderName)
    except: pass

    # --------------- Creating the figure ------------------------------------------------
    # Generating the name of the surface
    Name = "LS n°{0:d}".format(VariableId+1)

    # Getting a surface from the vectors
    xx,yy,zz = GetMeshgridFom3VectorsMaskZ(Mesh,X,Y,Z[VariableId,:])

    # Generating the surface and their 0 level
    Frame = go.Surface(x = copy.deepcopy(xx),
                       y = copy.deepcopy(yy),
                       z = copy.deepcopy(zz),
                       contours   = {"z": {"show": True, "start": 0, "end": 0.1, "size": 0.1}},
                       colorscale = "haline",
                       name       = Name)

    # Returning the constructed frame
    return([Frame])

#### Plot each level set on different 3D plots ####
def PlotEachLevelSet(Mesh, Solution, ParametersId):
    """ Plots the level sets in independent figures, each on the full computational domain.

    Args:
        Mesh           (MeshStructure):       the considered mesh
        Solution       (Solution):            the solution of the considered problem
        ParametersId   (string):              the string forming the "ProblemName_ParameterName", mapping to the right solution export folder

    Returns:
        None: The 3D plot of all the levels set on the considered domain, exported in the problem-dependent solution export folder.

    .. note::

        The figure is only exported, not shown
    """

    # ----------------------- Retrieving the solution's values ---------------------------
    # Select the data to plot
    X = Mesh.CartesianPoints[:,0]
    Y = Mesh.CartesianPoints[:,1]
    Z = Solution.RLSValues

    # -------------------- Customisation properties --------------------------------------
    # XY bounds of the figure
    bounds = [np.min([*X,*Y]),np.max([*X,*Y])]

    # --------------  Preparing the export at the right location -------------------------
    FolderName = os.path.join(Pathes.SolutionPath, ParametersId, "Res"+str(Mesh.MeshResolution).replace(".","_"), "DomainPlots")
    try:    os.makedirs(FolderName)
    except: pass

    # --------------- Creating the figure ------------------------------------------------
    # Plotting the LS both one by one and on the same figure
    for Id in range(np.shape(Z)[0]):
        # Generating the name of the surface
        Name = "LS n°{0:d}".format(Id+1)

        # Getting a surface from the vectors
        xx,yy,zz = GetMeshgridFom3VectorsMaskZ(Mesh,X,Y,Z[Id,:])
        # Generating the surface and their 0 level
        Frame = go.Surface(x = copy.deepcopy(xx),
                           y = copy.deepcopy(yy),
                           z = copy.deepcopy(zz),
                           contours   = {"z": {"show": True, "start": 0, "end": 0.1, "size": 0.1}},
                           colorscale = "haline",
                           name       = Name)

        # Creating the figure instance upon all the above generated surfaces
        fig = GenerateFigurePlotly3D([Frame], bounds, Name)

        # --------------- Export the figure at the right location -------------------------
        # Creating the file name and exporting it in the right folder
        try:
            FileName = ("Plty_LSValues3D_Fluid{0:02d}_t{1:8.6f}".format(Id, Solution.t)).replace(".","_")
            fig.write_html(os.path.join(FolderName, FileName)+".html")
        except:
            print("WARNING: Error in the plot generation. Impossible export.")

#### Plot each level set on different 3D plots for triangular meshes ####
def PlotEachLevelSetTri(Mesh, Solution, ParametersId):
    """ Plots the level sets in independent figures, each on the full computational domain.

    Args:
        Mesh           (MeshStructure):       the considered mesh
        Solution       (Solution):            the solution of the considered problem
        ParametersId   (string):              the string forming the "ProblemName_ParameterName", mapping to the right solution export folder

    Returns:

        None: The 3D plot of all the levels set on the considered domain, exported in the problem-dependent solution export folder.

    .. note::

        The figure is only exported, not shown
    """

    # ----------------------- Retrieving the solution's values ---------------------------
    # Select the data to plot
    X = Mesh.CartesianPoints[:,0]
    Y = Mesh.CartesianPoints[:,1]
    Z = Solution.RLSValues

    # -------------------- Customisation properties --------------------------------------
    # XY bounds of the figure
    bounds = [np.min([*X,*Y]),np.max([*X,*Y])]

    # --------------  Preparing the export at the right location -------------------------
    FolderName = os.path.join(Pathes.SolutionPath, ParametersId, "Res"+str(Mesh.MeshResolution).replace(".","_"), "DomainPlots")
    try:    os.makedirs(FolderName)
    except: pass

    # --------------- Creating the figure ------------------------------------------------

    # Plotting the LS both one by one and on the same figure
    for Id in range(np.shape(Z)[0]):
        # Generating the name of the surface
        Name = "LS n°{0:d}".format(Id+1)


        # Generating the surface and their 0 level
        fig = ff.create_trisurf(x=X, y=Y, z=Z,
                         simplices = Mesh.InnerElements,
                         title = "", colormap='Viridis', plot_edges= False)

        # Setting the layout w.r.t. plotly syntax
        fig.update_layout(autosize   = False,
                      showlegend = True,
                      width  = 800,
                      height = 600,
                      title  = "Level Set function #{0:2d}".format(Id+1),
                      scene  = dict(xaxis = dict(range=[bounds[0],bounds[1]]),
                                    yaxis = dict(range=[bounds[0],bounds[1]]),
                                    xaxis_title = 'x',
                                    yaxis_title = 'y',
                                    zaxis_title = 'LS Values'),
                      font   = dict(family = "Arial",
                                    size   = 14,
                                    color  = "#7f7f7f"))

        # --------------- Export the figure at the right location -------------------------
        # Creating the file name and exporting it in the right folder
        try:
            FileName = ("Plty_LSValues3D_Tri_Fluid{0:02d}_t{1:8.6f}".format(Id, Solution.t)).replace(".","_")
            fig.write_html(os.path.join(FolderName, FileName)+".html")
        except:
            print("WARNING: Error in the plot generation. Impossible export.")



# ---------------------------- Solutions --------------------------------------

#### Plot a variable of the solution regardless the subdomain ####
def PlotSolution(Problem, Mesh, Solution, VariableId, ParametersId):
    """ Plots the solution's variableId on the full computational domain.

    Args:
        Problem        (Problem):             the considered problem
        Mesh           (MeshStructure):       the considered mesh
        Solution       (Solution):            the solution of the considered problem
        VariableId,     (integer):             the index of the desired variable to be plot
        ParametersID   (string):              the string forming the "ProblemName_ParameterName", mapping to the right solution export folder

    Returns:
        The 3D plot of all the given solution set on the considered domain, exported in the problem-dependent solution export folder.

    .. note::

        The figure is only exported, not shown
    """

    # ----------------------- Retrieving the solution's values ---------------------------
    # Select the data to plot
    X = Mesh.CartesianPoints[:,0]
    Y = Mesh.CartesianPoints[:,1]
    Z = Solution.RSol[VariableId,:]

    # -------------------- Customisation properties --------------------------------------
    # Retrieve the variables names to create the labels
    variableName      = Problem.GoverningEquations.VariablesNames[VariableId]
    variableLatexName = Problem.GoverningEquations.VariablesLatexNames[VariableId]
    variableLatexUnit = Problem.GoverningEquations.VariablesLatexUnits[VariableId]

    # Defining the plotting limits
    bounds = [np.min([*X,*Y]),np.max([*X,*Y])]

    # ------------- Preparing the export at the right location --------------------------
    FolderName = os.path.join(Pathes.SolutionPath, ParametersId, "Res"+str(Mesh.MeshResolution).replace(".","_"), "3DPlots")
    try:    os.makedirs(FolderName)
    except: pass

    # ------------- Creating the actual plot --------------------------------------------
    # Getting a surface from the vectors
    xx,yy,zz = GetMeshgridFom3VectorsMaskZ(Mesh,X,Y,Z)

    # Generating the surface
    Frame = go.Surface(x = copy.deepcopy(xx),
                       y = copy.deepcopy(yy),
                       z = copy.deepcopy(zz),
                       colorscale = ColorMapsPlotly[0],
                       name       = variableName)

    # Exporting the surface
    fig = GenerateFigurePlotly3D([Frame], bounds, variableName)

    # --------------- Export the figure at the right location -------------------------
    # Creating the file name and exporting it in the right folder
    try:
        FileName = ("Plty_3DSol_OnWholeDomain_{0!s}_t{1:8.6f}".format(variableName, Solution.t)).replace(".","_")
        fig.write_html(os.path.join(FolderName, FileName)+".html")
    except:
        print("WARNING: Error in the plot generation. Impossible export.")

#### Generate frame of the solution with a colorscale per subdomain ####
def GenerateFrameSolutionOnEachSubdomain(Problem, Mesh, Solution, cbounds, VariableId, ParametersId):
    """ Generates a figure's frame of the solution's variableId on the full computational domain with a color scale per subdomain.

    Args:
        Problem        (Problem):             the considered problem
        Mesh           (MeshStructure):       the considered mesh
        Solution       (Solution):            the solution of the considered problem
        cbounds        (float array-like):    values determining the bounds of the color scale to use
        VariableId,    (integer):             the index of the desired variable to be plot
        ParametersId   (string):              the string forming the "ProblemName_ParameterName", mapping to the right solution export folder

    Returns:
        None: The frame representing the solution on the considered domain with a subdomain-dependent color scale.

    .. note::

        The routine has been split from PlotSolutionOnEachSubdomain to ease the construction of a video frame later
    """

    # ----------------------- Retrieving the solution's values ---------------------------
    # Select the data to plot
    X  = Mesh.CartesianPoints[:,0]
    Y  = Mesh.CartesianPoints[:,1]
    Z  = Solution.RSol[VariableId,:]
    LS = Solution.RLSValues[:,:]

    # -------------------- Customisation properties --------------------------------------
    # Retrieve the variables names
    variableName      = Problem.GoverningEquations.VariablesNames[VariableId]
    variableLatexName = Problem.GoverningEquations.VariablesLatexNames[VariableId]
    variableLatexUnit = Problem.GoverningEquations.VariablesLatexUnits[VariableId]

    # Plotting bounds
    bounds = [np.min([*X,*Y]),np.max([*X,*Y])]

    # ------------- Creating the actual plot --------------------------------------------
    # Getting a surface from the vectors
    xx,yy,zz = GetMeshgridFom3VectorsMaskZ(Mesh,X,Y,Z)

    # Generating one surface per subfluid
    FrameTot = []

    for i in set(map(int,Solution.FluidFlag)):
        # Selecting the part of the solution that lie in the fluid's subdomain
        _,_,lzz = GetMeshgridFom3VectorsMaskX(Mesh,X,Y,LS[i,:])
        ind = np.where(lzz < 0)

       # Masking the disregarded fluid in the span
        lz  = copy.deepcopy(zz)
        lz[ind] = None

       # Generating the surface
        Frame = go.Surface(x = copy.deepcopy(xx),
                           y = copy.deepcopy(yy),
                           z = copy.deepcopy(lz),
                           cmin = cbounds[0],
                           cmax = cbounds[1],
                           colorscale = ColorMapsPlotly[i],
                           name         = variableName)
        FrameTot += [Frame]

    # Returning the collection of surfaces
    return(FrameTot)

#### Plot a variable of the solution with a color scale per subdomain ####
def PlotSolutionOnEachSubdomain(Problem, Mesh, Solution, VariableId, ParametersId, cbounds):
    """ Generates a figure's frame of the solution's variableId on the full computational domain with a color scale per subdomain.

    Args:
        Problem        (Problem):             the considered problem
        Mesh           (MeshStructure):       the considered mesh
        Solution       (Solution):            the solution of the considered problem
        VariableId,    (integer):             the index of the desired variable to be plot
        ParametersID   (string):              the string forming the "ProblemName_ParameterName", mapping to the right solution export folder
        cbounds        (float array-like):    values determining the bounds of the color scale to use

    Returns:
        None: The frame representing the solution on the considered domain with a subdomain-dependent color scale.

    .. note::

        The routine has been split from PlotSolutionOnEachSubdomain to ease the construction of a video frame later
    """

    # ----------------- Preparing the export at the right location -------------------------
    FolderName = os.path.join(Pathes.SolutionPath, ParametersId, "Res"+str(Mesh.MeshResolution).replace(".","_"), "DomainPlots")
    try:    os.makedirs(FolderName)
    except: pass

    # -------------------- Customisation properties --------------------------------------
    # Retrieve the variables names
    variableName      = Problem.GoverningEquations.VariablesNames[VariableId]
    variableLatexName = Problem.GoverningEquations.VariablesLatexNames[VariableId]
    variableLatexUnit = Problem.GoverningEquations.VariablesLatexUnits[VariableId]

    # ------------------------ Create the figure -------------------------------------------------------
    # Creating the collection of surfaces to plot in a same figure
    # Visibility bounds for the figure
    width      = abs(np.min([*Mesh.CartesianPoints[:,0], *Mesh.CartesianPoints[:,1]])-np.max([*Mesh.CartesianPoints[:,0], *Mesh.CartesianPoints[:,1]]))
    bounds     = [np.min([*Mesh.CartesianPoints[:,0], *Mesh.CartesianPoints[:,1]]),np.max([*Mesh.CartesianPoints[:,0], *Mesh.CartesianPoints[:,1]])]

    FrameTot = GenerateFrameSolutionOnEachSubdomain(Problem, Mesh, Solution, cbounds, VariableId, ParametersId)
    fig      = GenerateFigurePlotly3D(FrameTot, bounds, variableName)

    # --------------- Export the figure at the right location -------------------------
    # Creating the results folder if not already existing
    Folder = os.path.join(Pathes.SolutionPath, ParametersId, "Res"+str(Mesh.MeshResolution).replace(".","_"), "3DPlots")
    try:    os.makedirs(Folder)
    except: pass

    # Creating the file name and exporting it in the right folder
    try:
        FileName = ("Plty_3DSol_OnSubDomains_{0!s}_t{1:8.6f}".format(variableName, Solution.t)).replace(".","_")
        fig.write_html(os.path.join(FolderName, FileName)+".html")
    except:
        print("WARNING: Error in the plot generation. Impossible export.")


# ==============================================================================
#  Plotting 2D Solutions
# ==============================================================================

# ---------------------------- Level sets --------------------------------------
#### Plot the Level sets w.r.t. the subdomain ####
def PlotLevelSetOnEachSubDomain(Problem, Mesh, Solution, ParametersId):
    """ Plots the positive part of each level set on their corresponding subdomain.

    Args:
        Problem        (Problem):             the considered problem
        Mesh           (MeshStructure):       the considered mesh
        Solution       (Solution):            the solution of the considered problem
        ParametersId   (string):              the string forming the "ProblemName_ParameterName", mapping to the right solution export folder

    Returns:

        None: The 2D plot of all the levels set on their corresponding subdomains, exported in the problem-dependent solution export folder.

    .. note::

        The figure is only exported, not shown
    """

    # ----------------------- Retrieving the solution's values ---------------------------
    # Select the data to plot
    X = Mesh.CartesianPoints[:,0]
    Y = Mesh.CartesianPoints[:,1]
    LS = Solution.RLSValues[:,:]

    # ------------------- Customisation properties ----------------------------------------
    # Visibility bounds for the figure
    bounds = [np.min([*X,*Y]),np.max([*X,*Y])]

    # ----------------- Preparing the export at the right location -------------------------
    FolderName = os.path.join(Pathes.SolutionPath, ParametersId, "Res"+str(Mesh.MeshResolution).replace(".","_"), "DomainPlots")
    try:    os.makedirs(FolderName)
    except: pass

    # ------------- Creating the actual plot --------------------------------------------
    # Initialising the collection of the surfaces that will fill the frames
    FrameTot = []

    # Spanning each fluid to retrieve each level set's values
    for i in set(map(int,Solution.FluidFlag)):
        # Getting the interpolated values with both masks as we need all xyz bounds without None later
        xx,yy,zz = GetMeshgridFom3VectorsMaskZ(Mesh,X,Y,LS[i,:])
        _,_,lzz  = GetMeshgridFom3VectorsMaskX(Mesh,X,Y,LS[i,:])

        # ----- Generating a contour that actually separates the fluid's areas visually
        # (deepcopy is required otherwise blank surface is generated)
        Frame2 = go.Contour(x = copy.deepcopy(xx)[0,:],
                            y = copy.deepcopy(yy)[:,0],
                            z = copy.deepcopy(zz),
                            line = dict(width = 3),
                            showscale   = False,
                            autocontour = False,
                            colorscale  = ColorMapsPlotly[::-1][i],
                            contours    = dict(coloring  = "lines",start=0, end=0.1, size=0.2))

        # ----- Generating a color plot for each subdomain
        # Selecting the points of interest for the considered subdomain
        ind = np.where(lzz  < 0)
        rzz = copy.deepcopy(zz)
        rzz[ind] = None

        # Colorfill upon the level set values on each subdomain
        Frame = go.Heatmap(x = copy.deepcopy(xx)[0,:],
                   y = copy.deepcopy(yy)[:,0],
                   z = copy.deepcopy(rzz),
                   colorscale = ColorMapsPlotly[i])
        # Note: For smoother results one can also use the (slow) Countour instead of Heatmap, by adding the
        #       following options: line = dict(width = 0), contours = dict(start=np.min(Z), end = np.max(Z),
        #       size = 0.02, coloring  = "heatmap")

        # Enriching the patches of the surfaces to plot
        FrameTot = FrameTot + [copy.deepcopy(Frame), copy.deepcopy(Frame2)]

    # Generating the associated figure
    fig = GenerateFigurePlotly2D(FrameTot, bounds)

    # --------------- Export the figure at the right location -------------------------
    # Creating the file name and exporting it in the right folder
    try:
        FileName = ("Plty_LSValues2D_SubDomains_t{0:8.6f}".format(Solution.t)).replace(".","_")
        fig.write_html(os.path.join(FolderName, FileName)+".html")
    except:
        print("WARNING: Error in the plot generation. Impossible export.")

#### Generate a frame corresponding to a level set of the solution w.r.t. the subdomain ####
def GenerateFrameLevelSetOnEachSubDomain(Problem, Mesh, Solution, cbounds, VariableId, ParametersId):
    """ Plots the positive part of each level set on their corresponding subdomain.

    Args:
        Problem        (Problem):             the considered problem
        Mesh           (MeshStructure):       the considered mesh
        Solution       (Solution):            the solution of the considered problem
        cbounds        (float array-like):    values determining the bounds of the color scale to use
        VariableId,    (integer):             the index of the desired variable to be plot
        ParametersId   (string):              the string forming the "ProblemName_ParameterName", mapping to the right solution export folder

    Returns:
        None: The 2D plot of all the levels set on their corresponding subdomains, exported in the problem-dependent solution export folder.

    .. note::

        The figure is only exported, not shown
    """

    # ----------------------- Retrieving the solution's values ---------------------------
    # Select the data to plot
    X = Mesh.CartesianPoints[:,0]
    Y = Mesh.CartesianPoints[:,1]
    LS = Solution.RLSValues[:,:]

    # ------------------- Customisation properties ----------------------------------------
    # Visibility bounds for the figure
    bounds = [np.min([*X,*Y]),np.max([*X,*Y])]

    # ----------------- Preparing the export at the right location -------------------------
    FolderName = os.path.join(Pathes.SolutionPath, ParametersId, "Res"+str(Mesh.MeshResolution).replace(".","_"), "DomainPlots")
    try:    os.makedirs(FolderName)
    except: pass

    # ------------- Creating the actual plot --------------------------------------------
    # Initialising the collection of the surfaces that will fill the frames
    FrameTot = []

    # Spanning each fluid to retrieve each level set's values
    for i in set(map(int,Solution.FluidFlag)):
        # Getting the interpolated values with both masks as we need all xyz bounds without None later
        xx,yy,zz = GetMeshgridFom3VectorsMaskZ(Mesh,X,Y,LS[i,:])
        _,_,lzz  = GetMeshgridFom3VectorsMaskX(Mesh,X,Y,LS[i,:])

        # ----- Generating a contour that actually separates the fluid's areas visually
        # (deepcopy is required otherwise blank surface is generated)
        Frame2 = go.Contour(x = copy.deepcopy(xx)[0,:],
                            y = copy.deepcopy(yy)[:,0],
                            z = copy.deepcopy(zz),
                            line = dict(width = 3),
                            showscale   = False,
                            autocontour = False,
                            colorscale  = ColorMapsPlotly[::-1][i],
                            contours    = dict(coloring  = "lines",start=0, end=0.1, size=0.2))

        # ----- Generating a color plot for each subdomain
        # Selecting the points of interest for the considered subdomain
        ind = np.where(lzz  < 0)
        rzz = copy.deepcopy(zz)
        rzz[ind] = None

        # Colorfill upon the level set values on each subdomain
        Frame = go.Heatmap(x = copy.deepcopy(xx)[0,:],
                   y = copy.deepcopy(yy)[:,0],
                   z = copy.deepcopy(rzz),
                   colorscale = ColorMapsPlotly[i])
        # Note: For smoother results one can also use the (slow) Countour instead of Heatmap, by adding the
        #       following options: line = dict(width = 0), contours = dict(start=np.min(Z), end = np.max(Z),
        #       size = 0.02, coloring  = "heatmap")

        # Enriching the patches of the surfaces to plot
        FrameTot = FrameTot + [copy.deepcopy(Frame), copy.deepcopy(Frame2)]

    # Returning the created frame
    return(FrameTot)

# ---------------------------- Solution ----------------------------------------
#### Plot a variable of the solution regardless the subdomain ####
def PlotSolution2D(Problem, Mesh, Solution, VariableId, ParametersId):
    """ Plots the solution's variableId on the full computational domain in 2D.

    Args:
        Problem        (Problem):             the considered problem
        Mesh           (MeshStructure):       the considered mesh
        Solution       (Solution):            the solution of the considered problem
        VariableId,    (integer):             the index of the desired variable to be plot
        ParametersId   (string):              the string forming the "ProblemName_ParameterName", mapping to the right solution export folder

    Returns:
        None: The 2D plot of all the given solution set on the considered domain, exported in the problem-dependent solution export folder.

    .. note::

        The figure is only exported, not shown
    """

    # ----------------------- Retrieving the solution's values ---------------------------
    # Select the data to plot
    X  = Mesh.CartesianPoints[:,0]
    Y  = Mesh.CartesianPoints[:,1]
    Z  = Solution.RSol[VariableId,:]
    LS = Solution.RLSValues[:,:]

    # -------------------- Customisation properties --------------------------------------
    # Retrieve the variables names to create the labels
    variableName      = Problem.GoverningEquations.VariablesNames[VariableId]
    variableLatexName = Problem.GoverningEquations.VariablesLatexNames[VariableId]
    variableLatexUnit = Problem.GoverningEquations.VariablesLatexUnits[VariableId]

    # Defining the plotting limits
    bounds = [np.min([*X,*Y]),np.max([*X,*Y])]

    # ---------------- Preparing the export at the right location -------------------------
    FolderName = os.path.join(Pathes.SolutionPath, ParametersId, "Res"+str(Mesh.MeshResolution).replace(".","_"), "2DPlots")
    try:    os.makedirs(FolderName)
    except: pass

    # ------------- Creating the actual plot --------------------------------------------
    # Getting a surface from the vectors
    xx,yy,zz = GetMeshgridFom3VectorsMaskZ(Mesh,X,Y,Z)

    # Generating the surface
    FrameTot = [go.Heatmap(x = copy.deepcopy(xx)[0,:],
                           y = copy.deepcopy(yy)[:,0],
                           z = copy.deepcopy(zz),
                           colorscale = ColorMapsPlotly[0],
                           name       = variableName)]
    # Note: For smoother results one can also use the (slow) Countour instead of Heatmap, by adding the
    #       following options: line = dict(width = 0), contours = dict(start=np.min(Z), end = np.max(Z),
    #       size = 0.02, coloring  = "heatmap")


    # Plot the contour of each LS for reference
    for i in set(map(int, Solution.FluidFlag)):
        # Interpolating the LS values on the current grid
        _,_,lzz = GetMeshgridFom3VectorsMaskZ(Mesh,X,Y,LS[i,:])

        # Generating the contours
        Frame = go.Contour(x = copy.deepcopy(xx)[0,:],
                           y = copy.deepcopy(yy)[:,0],
                           z = copy.deepcopy(lzz),
                           line = dict(width = 3),
                           showscale   = False,
                           colorscale  = ColorMapsPlotly[::-1][i],
                           autocontour = False,
                           contours    = dict(coloring  = "lines", start=0, end=0.1, size=0.2),
                           name        = variableName)

        # Adding the patch to the surfaces to plot
        FrameTot = FrameTot + [Frame]

    # Generating the figure's frame
    fig = GenerateFigurePlotly2D(FrameTot, bounds, "Colorscale representation of the variable"+variableName)

    # --------------- Export the figure at the right location -------------------------
    # Creating the file name and exporting it in the right folder
    try:
        FileName = ("Plty_2DSol_OnWholeDomain_{0!s}_t{1:8.6f}".format(variableName, Solution.t)).replace(".","_")
        fig.write_html(os.path.join(FolderName, FileName)+".html")
    except:
        print("WARNING: Error in the plot generation. Impossible export.")

#### Plot a variable of the solution regardless the subdomain ####
def PlotSolution2DOnEachSubDomain(Problem, Mesh, Solution, VariableId, ParametersId):
    """ Plots the solution's variableId on the full computational domain in 2D, with a subdomain-dependent colorscale.

    Args:
        Problem        (Problem):             the considered problem
        Mesh           (MeshStructure):       the considered mesh
        Solution       (Solution):            the solution of the considered problem
        VariableId,    (integer):             the index of the desired variable to be plot
        ParametersId   (string):              the string forming the "ProblemName_ParameterName", mapping to the right solution export folder

    Returns:
        None: The 2D plot of all the given solution set on the considered domain, exported in the problem-dependent solution export folder.

    .. note::

        The figure is only exported, not shown
    """

    # ----------------------- Retrieving the solution's values ---------------------------
    # Select the data to plot
    X  = Mesh.CartesianPoints[:,0]
    Y  = Mesh.CartesianPoints[:,1]
    Z  = Solution.RSol[VariableId,:]
    LS = Solution.RLSValues[:,:]

    # -------------------- Customisation properties --------------------------------------
    # Retrieve the variables names to create the labels
    variableName      = Problem.GoverningEquations.VariablesNames[VariableId]
    variableLatexName = Problem.GoverningEquations.VariablesLatexNames[VariableId]
    variableLatexUnit = Problem.GoverningEquations.VariablesLatexUnits[VariableId]

    # Set the visibility figure's bounds
    bounds = [np.min([*X,*Y]),np.max([*X,*Y])]

    # --------------- Preparing the export at the right location -------------------------
    FolderName = os.path.join(Pathes.SolutionPath, ParametersId, "Res"+str(Mesh.MeshResolution).replace(".","_"), "2DPlots")
    try:    os.makedirs(FolderName)
    except: pass

    # --------------- Creating the actual plot -------------------------------------------
    # Getting a surface from the vectors
    xx,yy,zz = GetMeshgridFom3VectorsMaskZ(Mesh,X,Y,Z)

    # Initialising the collection of surfaces to plot on the same figure
    FrameTot = []

    # Spanning all the subdomains
    for i in set(map(int,Solution.FluidFlag)):
        # Selecting the solution's points lying on the spanned subdomain
        _,_,lzz = GetMeshgridFom3VectorsMaskX(Mesh,X,Y,LS[i,:])
        ind     = np.where(lzz  < 0)
        lz      = copy.deepcopy(zz)
        lz[ind] = None

        # Plotting the color scale value
        Frame = go.Heatmap(x = copy.deepcopy(xx)[0,:],
                   y = copy.deepcopy(yy)[:,0],
                   z = lz,
                   colorscale = ColorMapsPlotly[i],
                   name       = variableName)
        # Note: For smoother results one can also use the (slow) Countour instead of Heatmap, by adding the
        #       following options: line = dict(width = 0), contours = dict(start=np.min(Z), end = np.max(Z),
        #       size = 0.02, coloring  = "heatmap")


        # Recalling the level set's contours
        Frame2 = go.Contour(x = copy.deepcopy(xx)[0,:],
                            y = copy.deepcopy(yy)[:,0],
                            z = copy.deepcopy(lzz),
                            line = dict(width = 3),
                            showscale   = False,
                            autocontour = False,
                            colorscale  = ColorMapsPlotly[::-1][i],
                            contours    = dict(coloring  = "lines", start=0, end=0.1, size=0.2),
                            name        = variableName)

        # Registering the new surfaces to be plot in the figure
        FrameTot = FrameTot + [Frame, Frame2]

    # Actually plotting the figure
    fig = GenerateFigurePlotly2D(FrameTot, bounds, variableName)

    # --------------- Export the figure at the right location -------------------------
    # Creating the file name and exporting it in the right folder
    try:
        FileName = ("Plty_2DSol_OnSubDomains_{0!s}_t{1:8.6f}".format(variableName, Solution.t)).replace(".","_")
        fig.write_html(os.path.join(FolderName, FileName)+".html")
    except:
        print("WARNING: Error in the plot generation. Impossible export.")


# ==============================================================================
#  Controlled animations
# ==============================================================================

# ---------------------------- Level sets --------------------------------------

### Animate each Level set of an exported solution accross the time ####
def Animate3DLevelSet(TestName, Resolution):
    """ Animates the level set for each, on the full domain.

    Args:
        LightExport  (integer):  integer telling if the results have been exported with a light (1, without mesh) or full export (0)
        TestName     (string):   the string forming the "ProblemName_ParameterName", mapping to the right solution export folder
        Resolution   (integer):  resolution of the considered mesh

    Returns:
        None: The 3D animation is exported an html format

    .. note::

        The figure is only exported, not shown
    """

    # ---------------- Creating the color bounds for the global plot -------------------
    # Listing all the available files
    lst   = glob.glob(os.path.join(Pathes.SolutionPath, TestName, "Res"+str(Resolution).replace(".","_"), "RawResults","*.sol"))
    Times =  np.sort([float(FileName[-12:-4].replace("_",".")) for FileName in lst])

    # Safety check
    if Times.size==0: raise(ValueError("Error: No previous results exported. Abortion."))

    # Check the type of solution export
    Settings    = open(os.path.join(Pathes.SolutionPath, TestName, "Res"+str(Resolution).replace(".","_"), "RawResults","UsedSettings.txt"), "r")
    LightExport = int(re.split(" |\t", Settings.readlines()[20])[0])
    Settings.close()

    # Loading one sample to get the basic properties
    if LightExport == 0: Mesh, ProblemData, Problem, Solution = EX.LoadSolution(TestName, Resolution, Times[0])
    else:                Mesh, ProblemData, Problem, Solution = EX.LoadSolutionMeshFree(TestName, Resolution, Times[0])
    N = Problem.NbFluids

    # -------------- Defining the variables to plot -------------------------------------
    for i in range(N):
        # Searching for the min-max z range among them
        cmin = +np.inf
        cmax = -np.inf
        for t in range(len(Times)):
            # Loading the solution
            if LightExport == 0: Mesh, ProblemData, Problem, Solution = EX.LoadSolution(TestName, Resolution, Times[t])
            else:                Mesh, ProblemData, Problem, Solution = EX.LoadSolutionMeshFree(TestName, Resolution, Times[t])

            # Creating the plotting function's attributes
            args = [i, TestName]

            # Getting the bound range
            rr = (np.max([*Solution.RLSValues[i,:]])-np.min([*Solution.RLSValues[i,:]]))
            if np.isclose(rr,0): rr = 1

            cmax = np.max([cmax, *Solution.RLSValues[i,:]])+0.1*rr
            cmin = np.min([cmin, *Solution.RLSValues[i,:]])-0.1*rr

        # Getting the animation
        fig = PlotAnimation3D(LightExport, TestName, Resolution, GenerateFrameEachLevelSet, cmin, cmax, args)

        # --------------- Saving the animation ---------------------------------
        # Creating the saving environment
        FolderName = os.path.join(Pathes.SolutionPath, TestName, "Res"+str(Mesh.MeshResolution).replace(".","_"), "DomainPlots")
        try:    os.makedirs(FolderName)
        except: pass

        # Creating the file name and exporting it in the right folder
        try:
            FileName = ("Plty_LSValues3D_Fluid{0:02d}_all".format(i)).replace(".","_")
            fig.write_html(os.path.join(FolderName, FileName)+".html")
        except:
            print("WARNING: Error in the plot generation. Impossible export.")

### Animate all the levels sets on the domain accross the time ####
def Animate2DLevelSet(TestName, Resolution):
    """ Animates all the levels sets on the domain accross the time, on their respective positive parts.

    Args:
        LightExport  (integer):  integer telling if the results have been exported with a light (1, without mesh) or full export (0)
        TestName   (string):   the string forming the "ProblemName_ParameterName", mapping to the right solution export folder
        Resolution (integer):  resolution of the considered mesh

    Returns:

        None: Only exports the 2D animation in an html format

    .. note::

        The figure is only exported, not shown
    """

    # ---------------- Creating the color bounds for the global plot -------------------
    # Listing all the available files
    lst   = glob.glob(os.path.join(Pathes.SolutionPath, TestName, "Res"+str(Resolution).replace(".","_"), "RawResults","*.sol"))
    Times =  np.sort([float(FileName[-12:-4].replace("_",".")) for FileName in lst])

    # Safety check
    if Times.size==0: raise(ValueError("Error: No previous results exported. Abortion."))

    # Check the type of solution export
    Settings    = open(os.path.join(Pathes.SolutionPath, TestName, "Res"+str(Resolution).replace(".","_"), "RawResults","UsedSettings.txt"), "r")
    LightExport = int(re.split(" |\t", Settings.readlines()[20])[0])
    Settings.close()

    # Loading one sample to get the basic properties
    if LightExport == 0: Mesh, ProblemData, Problem, Solution = EX.LoadSolution(TestName, Resolution, Times[0])
    else:                Mesh, ProblemData, Problem, Solution = EX.LoadSolutionMeshFree(TestName, Resolution, Times[0])
    N = Problem.NbFluids

    # -------------- Defining the variables to plot -------------------------------------
    for i in range(N):
        # Searching for the min-max z range among them
        cmin = +np.inf
        cmax = -np.inf
        for t in range(len(Times)):
            # Loading the solution
            if LightExport == 0: Mesh, ProblemData, Problem, Solution = EX.LoadSolution(TestName, Resolution, Times[t])
            else:                Mesh, ProblemData, Problem, Solution = EX.LoadSolutionMeshFree(TestName, Resolution, Times[t])

            # Creating the plotting function's attributes
            args = [i, TestName]

            # Getting the bound range
            rr = (np.max([*Solution.RLSValues[i,:]])-np.min([*Solution.RLSValues[i,:]]))
            if np.isclose(rr,0): rr = 0.1

            cmax = np.max([cmax, *Solution.RLSValues[i,:]])+0.1*rr
            cmin = np.min([cmin, *Solution.RLSValues[i,:]])-0.1*rr


    # Getting the animation
    fig = PlotAnimation2D(LightExport, TestName, Resolution, GenerateFrameLevelSetOnEachSubDomain, cmin, cmax, args)

    # --------------- Saving the animation ---------------------------------
    # Preparing the export environment
    FolderName = os.path.join(Pathes.SolutionPath, TestName, "Res"+str(Mesh.MeshResolution).replace(".","_"), "DomainPlots")
    try:    os.makedirs(FolderName)
    except: pass

    # Creating the file name and exporting it in the right folder
    try:
        FileName = ("Plty_LSValues2D_SubDomains_all")
        fig.write_html(os.path.join(FolderName, FileName)+".html")
    except:
        print("WARNING: Error in the plot generation. Impossible export.")

# ---------------------------- Solution ----------------------------------------

### Animate each variable of an exported solution ####
def Animate3DSolution(TestName, Resolution):
    """ Animates the solution for each variable.

    Args:
        LightExport  (integer):  integer telling if the results have been exported with a light (1, without mesh) or full export (0)
        TestName   (string):   the string forming the "ProblemName_ParameterName", mapping to the right solution export folder
        Resolution (integer):  resolution of the considered mesh

    Returns:
        None: Only exports the 3D animation in an html format

    .. note::

        The figure is only exported, not shown
    """

    # ---------------- Creating the color bounds for the global plot -------------------
    # Listing all the available files
    lst   = glob.glob(os.path.join(Pathes.SolutionPath, TestName, "Res"+str(Resolution).replace(".","_"), "RawResults","*.sol"))
    Times =  np.sort([float(FileName[-12:-4].replace("_",".")) for FileName in lst])

    # Safety check
    if Times.size==0: raise(ValueError("Error: No previous results exported. Abortion."))

    # Check the type of solution export
    Settings    = open(os.path.join(Pathes.SolutionPath, TestName, "Res"+str(Resolution).replace(".","_"), "RawResults","UsedSettings.txt"), "r")
    LightExport = int(re.split(" |\t", Settings.readlines()[20])[0])
    Settings.close()

    # Loading one sample to get the basic properties
    if LightExport == 0: Mesh, ProblemData, Problem, Solution = EX.LoadSolution(TestName, Resolution, Times[0])
    else:                Mesh, ProblemData, Problem, Solution = EX.LoadSolutionMeshFree(TestName, Resolution, Times[0])
    N = Problem.GoverningEquations.NbVariables

    # -------------- Defining the variables to plot -------------------------------------
    for i in range(N):
        # Searching for the min-max z range among them
        cmin = +np.inf
        cmax = -np.inf
        for t in range(len(Times)):
            # Loading the solution
            if LightExport == 0: Mesh, ProblemData, Problem, Solution = EX.LoadSolution(TestName, Resolution, Times[t])
            else:                Mesh, ProblemData, Problem, Solution = EX.LoadSolutionMeshFree(TestName, Resolution, Times[t])

            # Getting the customisation attributes
            VariableName = Problem.GoverningEquations.VariablesNames[i]
            UnitName     = Problem.GoverningEquations.VariablesUnits[i]

            # Creating the plotting function's attributes
            args = [i, TestName]

            # Getting the bound range
            rr = (np.max([*Solution.RSol[i,:]])-np.min([*Solution.RSol[i,:]]))
            if np.isclose(rr,0): rr = 0.1

            cmax = np.max([cmax, *Solution.RSol[i,:]])+0.1*rr
            cmin = np.min([cmin, *Solution.RSol[i,:]])-0.1*rr

        # Getting the animation
        fig = PlotAnimation3D(LightExport, TestName, Resolution, GenerateFrameSolutionOnEachSubdomain, cmin, cmax, args)

        # --------------- Saving the animation ---------------------------------
        # Preparing the export environment
        FolderName = os.path.join(Pathes.SolutionPath, TestName, "Res"+str(Mesh.MeshResolution).replace(".","_"), "3DPlots")
        try:    os.makedirs(FolderName)
        except: pass

        # Creating the file name and exporting it in the right folder
        try:
            FileName = ("Plty_3DSol_OnSubDomain_{0!s}_all".format(VariableName)).replace(".","_")
            fig.write_html(os.path.join(FolderName, FileName)+".html")
        except:
            print("WARNING: Error in the plot generation. Impossible export.")
