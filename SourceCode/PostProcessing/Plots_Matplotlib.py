#***************************************************************************
# Copyright (C) 2020 Élise Le Mélédo
# This file is part of LESARD.
#
# LESARD is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LESARD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LESARD.  If not, see <https://www.gnu.org/licenses/>.
#****************************************************************************

"""

The module :code:`Plots_Matplotlib` located in the :code:`SourceCode/Postprocessing` file  proposes visualisation routines for solution of a multi
phase problem which is stored in a Solution structure, using matplotlib.
It contains plotting 2D solutions, plotting 3D solutions, animation and check-up routines.

|
.. note::

    The module is made to be used on-line, that is either while computing the solution (except videos) or after the solution has been exported

|
|

"""


# ==============================================================================
#	Preliminary imports
# ==============================================================================

# 2D plots requirements
from matplotlib.collections import PatchCollection
from matplotlib.patches     import PathPatch
from matplotlib.path        import Path             as mpltPath
import matplotlib.pyplot                            as plt

# 3D plots requirements
from mpl_toolkits.mplot3d import Axes3D

# Color management
from matplotlib.colors import hsv_to_rgb
from matplotlib.colors import rgb2hex
from matplotlib        import cm, rc

# Customisation
import matplotlib.ticker  as ticker
import matplotlib.text    as txt

# Math packages
import scipy.interpolate
import scipy.integrate
import numpy  as np

# Pythonic packages
import itertools
import os, sys
import copy

# Import local directories
import Pathes


# ==============================================================================
#  Tools, solution and path readers
# ==============================================================================
#### Module-wise global variables ####
ColorMapsMatplotlib = [ 'viridis',  'Pastel1', 'Pastel2', 'Paired', 'Accent', 'Dark2',
                        'Set1', 'Set2', 'Set3', 'tab10', 'tab20', 'tab20b', 'tab20c', 'hsv',
                        'plasma', 'inferno', 'magma', 'cividis','Greys', 'Purples', 'Blues',
                        'Greens', 'Oranges', 'Reds', 'YlOrBr', 'YlOrRd',
                        'OrRd', 'PuRd', 'RdPu', 'BuPu', 'GnBu', 'PuBu', 'YlGnBu', 'PuBuGn',
                        'BuGn', 'YlGn']

#### Creates the ring codes from a shapely object
def ring_coding(ob):
    """ Creates the matplotlib ring codes from a shapely object

    Args:
        ob (shapely object): the shapely object of interest

    Returns:
        codes (shapely codes): the matplotlib path-dependent associated code
    """

    # -------------- Computing the tools ---------------------------------------
    n = len(ob.coords)
    codes = np.ones(n, dtype=mpltPath.code_type) * mpltPath.LINETO
    codes[0] = mpltPath.MOVETO
    return(codes)

### Converts a shapely polygon to a python path ####
def pathify(polygon):
    """ Creates a matplotlib path out of a shapely polygon.

    Args:
        polygon  (shapely polygon or multipolygon):  the polygon of interest

    Returns:
        path (matplotlib path): the path created from the vertices and codes of the given polygon
    """

    # -------------------- Computing the tools ---------------------------------
    # Convert coordinates to path vertices.
    vertices = np.concatenate(
        [np.asarray(polygon.exterior)]
        + [np.asarray(r) for r in polygon.interiors])
    codes = np.concatenate(
        [ring_coding(polygon.exterior)]
        + [ring_coding(r) for r in polygon.interiors])
    return mpltPath(vertices, codes)

#### Converts three vectors to a surface ####
def GetMeshgridFom3VectorsMaskX(Mesh, X, Y, Z):
    """ Interpolation routine in order to plot.

    .. |bs|   unicode:: U+00A0 .. NO-BREAK SPACE

    Args:
        Mesh       (MeshStructure):        the mesh in use
        X          (float array like):        the x coordinates of the considered points
        Y          (float array like):        the y coordinates of the considered points
        Z          (float array like):        the data's (scalar) value at the considered points

    .. rubric:: Returns

    - |bs| **xgrid** *(float 2D numpy array)*   --    x meshgrid of the points where the solution Z has been interpolated
    - |bs| **ygrid** *(float 2D numpy array)*   --    y meshgrid of the points where the solution Z has been interpolated
    - |bs| **zgrid** *(float 2D numpy array)*   --    values of the interpolated Z at the generated grid points

    |

    .. note::

        - When holes in the domains, the xy values are masked with None in both corresponding meshgrids
        - The x resolution of the interpolation grid is set to 5*(xmax-xmin)/np.min(Mesh.InnerElementsDiameter), and respectively for the y axis
        - The interpolation method is set to "linear", with a fill value of "-1e-15" in order to fit the 0 crossing at a physical boundary in case of interpolating a level set.
    """

    # ----------------------- Initialisation of the grid ---------------------------------------
    # Generating points in a square range of the scatter vector
    xgrid = Mesh.CartesianPoints[:,0]
    ygrid = Mesh.CartesianPoints[:,1]
    gxmin, gxmax = np.min(xgrid), np.max(xgrid)
    gymin, gymax = np.min(ygrid), np.max(ygrid)
    gridx = np.linspace(gxmin, gxmax, int(5*(gxmax-gxmin)/np.min(Mesh.InnerElementsDiameter)))
    gridy = np.linspace(gymin, gymax, int(5*(gymax-gymin)/np.min(Mesh.InnerElementsDiameter)))

    # Generating the corresponding grid points
    xgrid, ygrid = np.meshgrid(gridx, gridy)
    #xgrid, ygrid = np.meshgrid(np.sort(xgrid), np.sort(ygrid))

    # Interpolating with a fill-value, which is not impacting as the concerned points will be left out in the patch construction
    zgrid = scipy.interpolate.griddata((X,Y),Z, (xgrid, ygrid),method='linear',fill_value="-1e-15")

    # -------- Selecting points of the previous rectangle that actually belongs to the polygon --------
    # Converting the grid to scatter points
    pointsX=np.array(np.ndarray.ravel(xgrid))
    pointsY=np.array(np.ndarray.ravel(ygrid))

    # Converting the rectangle points in a suitable format
    points2=np.column_stack((pointsX,pointsY))
    # Retrieving the shape of the polygon
    polygons = [np.array(Dom) for Dom in Mesh.Domain]
    # Building the path corresponding to the polygon
    pathes = [mpltPath(polygon) for polygon in polygons]
    # Extract the points that are inside or on the path contour
    inside2 = np.prod(np.array([pathes[0].contains_points(points2, radius=1e-15)]+\
                               [    ~path.contains_points(points2, radius=1e-15)\
                                for path in pathes[1:]]), axis=0)

    ind  = np.where(inside2==False)
    ind2 = np.unravel_index(ind, np.shape(xgrid))

    # --------- Interpolating the scattered data on the plotting points ----------
    # Masking the points that fall out
    xgrid[ind2] = None
    ygrid[ind2] = None

    # Giving back the interpolated values
    return(xgrid,ygrid,zgrid)

#### Converts three vectors to a surface ####
def GetMeshgridFom3VectorsMaskZ(Mesh,X,Y,Z):
    """ Interpolation routine in order to plot.

    Args:
        Mesh       (MeshStructure):        the mesh in use
        X          (float array like):        the x coordinates of the considered points
        Y          (float array like):        the y coordinates of the considered points
        Z          (float array like):        the data's (scalar) value at the considered points

    .. rubric:: Returns

    - |bs| **xgrid** *(float 2D numpy array)*   --    x meshgrid of the points where the solution Z has been interpolated
    - |bs| **ygrid** *(float 2D numpy array)*   --    y meshgrid of the points where the solution Z has been interpolated
    - |bs| **zgrid** *(float 2D numpy array)*   --    values of the interpolated Z at the generated grid points

    |

    .. note::

        - When holes in the domains, the z values are masked with None in both corresponding meshgrids
        - The x resolution of the interpolation grid is set to 5*(xmax-xmin)/np.min(Mesh.InnerElementsDiameter), and respectively for the y axis
        - The interpolation method is set to "linear", with a fill value of "-1e-15" in order to fit the 0 crossing at a physical boundary in case of interpolating a level set.
    """

    # ----------------------- Initialisation of the grid ---------------------------------------
    # Generating points in a square range of the scatter vector
    xgrid = Mesh.CartesianPoints[:,0]
    ygrid = Mesh.CartesianPoints[:,1]
    gxmin, gxmax = np.min(xgrid), np.max(xgrid)
    gymin, gymax = np.min(ygrid), np.max(ygrid)
    gridx = np.linspace(gxmin, gxmax, int(5*(gxmax-gxmin)/np.min(Mesh.InnerElementsDiameter)))
    gridy = np.linspace(gymin, gymax, int(5*(gymax-gymin)/np.min(Mesh.InnerElementsDiameter)))

    # Generating the corresponding grid points
    xgrid, ygrid = np.meshgrid(gridx, gridy)
    #xgrid, ygrid = np.meshgrid(np.sort(xgrid), np.sort(ygrid))

    # Interpolating with a fill-value, which is not impacting as the concerned points will be left out in the patch construction
    zgrid = scipy.interpolate.griddata((X,Y),Z, (xgrid, ygrid),method='linear',fill_value="-1e-15")

    # -------- Selecting points of the previous rectangle that actually belongs to the polygon --------
    # Converting the grid to scatter points
    pointsX=np.array(np.ndarray.ravel(xgrid))
    pointsY=np.array(np.ndarray.ravel(ygrid))

    # Converting the rectangle points in a suitable format
    points2=np.column_stack((pointsX,pointsY))
    # Retrieving the shape of the polygon
    polygons = [np.array(Dom) for Dom in Mesh.Domain]
    # Building the path corresponding to the polygon
    pathes = [mpltPath(polygon) for polygon in polygons]
    # Extract the points that are inside or on the path contour
    inside2 = np.prod(np.array([pathes[0].contains_points(points2, radius=1e-15)]+\
                               [    ~path.contains_points(points2, radius=1e-15)\
                                for path in pathes[1:]]), axis=0)

    ind  = np.where(inside2==False)
    ind2 = np.unravel_index(ind, np.shape(xgrid))

    # --------- Interpolating the scattered data on the plotting points ----------
    # Masking the points that fall out
    zgrid[ind2] = None

    # Giving back the interpolated values
    return(xgrid,ygrid,zgrid)

#### Plots a shapely polygon in a matplotlib patch format ####
def plotMultipoly(Poly, color,ax):
    """ Simple routine that plots a polygon being form of several ones in a shapely format

    Args:
        Poly     (shapely multipolygon):           the polygon to plot
        color    (matplotlib compatible color):    the wished color (alpha is set to 0.4)
        ax       (matplotlib axes instance):       ax attached to the figure the polygon should be plot on

    Returns:
        None: fills direclty the figure attached to ax
    """

    # ----------------------- Plot the multipolygon ---------------------------------------
    # Plot the boundaries, otherwise non connected MultiPolygon are not printed (unknown reason bug)
    #for polygon in Poly: plt.plot(*polygon.exterior.xy, color = color)

    # Creates the patches of the polygons
    for poly in Poly:
        patches = PathPatch(pathify(poly), linewidth=None, facecolor=color, edgecolor=None, alpha=0.4)
        ax.add_patch(patches)


# ==============================================================================
#  Level Sets tools usable by any element type
# ==============================================================================

#### Debug plotting routine that plots fluid's flag at each Dof ####
def PlotFlags(Problem, Mesh, Solution, ParametersId):
    """ Debug plotting routine that plots fluid's flag at each Dof

    Args:
        Problem        (Problem):             the considered problem and its properties
        Mesh           (MeshStructure):       the considered mesh
        Solution       (Solution):            the solution of the considered problem
        ParametersId   (string):              name of the export folder (TestCase_TestSettings)

    Returns:
        None: Only exporting the plot
    """

    # ------------ Preparing the figures and the plotting environment --------------------------
    # Initilising matplotlib figure
    plt.figure()
    fig,ax = plt.subplots()

    # --------- Plotting in the current figure of the class ----------------------
    # Spanning all the fluids
    for i in range(0, Problem.NbFluids):
        # Selecting all the Dofs falling in the subdomains
        Fluid = Problem.FluidIndex[i]
        Index = np.where(Solution.RFluidFlag == i)[0]

        # Plotting the markers
        plt.plot(Mesh.CartesianPoints[Index,0], Mesh.CartesianPoints[Index,1],\
                 "+", color = Problem.FluidProp[i].color)

    # Plotting the fluids's contours in case of several fluids
    if Problem.NbFluids>1:
        xgrid, ygrid, zgrid = GetMeshgridFom3VectorsMaskX(Mesh, Mesh.CartesianPoints[:,0],Mesh.CartesianPoints[:,1],Solution.RFluidFlag)
        plt.contourf(xgrid, ygrid, zgrid, levels=range(0,Problem.NbFluids), alpha=0.4, colors = [Fluid.color for Fluid in Problem.FluidProp])


    # --------------- Export the figure at the right location -------------------------
    # Creating the export folder if not already existing
    FolderName = os.path.join(Pathes.SolutionPath, ParametersId, "Res"+str(Mesh.MeshResolution).replace(".","_"), "DomainPlots")
    try:    os.makedirs(FolderName)
    except: pass

    # Creating the file name and exporting it in the right folder
    try:
        FileName = ("Mplt_FlagMarkers_t{0:5.3f}".format(Solution.t)).replace(".","_")
        plt.savefig(os.path.join(FolderName, FileName)+".png", transparent=True, dpi=300)
    except:
        print("WARNING: Error in the plot generation. Impossible export.")

    # Closing the figure
    plt.close()

#### Plots the registered subdomain patches ####
def PlotSubdomains(Problem, Mesh, Solution, ParametersId):
    """ Plots the registered subdomain patches

    Args:
        Problem        (Problem):             the considered problem and its properties
        Mesh           (MeshStructure):       the considered mesh
        Solution       (Solution):            the solution of the considered problem
        ParametersId   (string):              name of the export folder (TestCase_TestSettings)

    Returns:
        None: Only exporting the plot
    """

    # -------------- Initialising the figure ---------------------------------------
    fig = plt.figure()
    ax  = fig.add_subplot(1, 1, 1)

    # --------------- Creating the figure ------------------------------------------
    Polys = Solution.Subdomains
    for i in range(len(Polys)): plotMultipoly(Polys[i], Problem.FluidProp[i].color, ax)

    # ---------------    Customisation properties -----------------------------------
    # Customisation of labels and latex conversion
    rc('font',size = 12)
    rc('font',family='serif')
    rc('axes',labelsize=14)

    # Customise the positioning of the labels
    [t.set_va('center') for t in ax.get_yticklabels()]
    [t.set_ha('right')  for t in ax.get_yticklabels()]
    [t.set_va('center') for t in ax.get_xticklabels()]
    [t.set_ha('left')   for t in ax.get_xticklabels()]

    # Set the grid and the pane colors
    fig.suptitle(r'Solution for the variable at the time ${0:5.3f}$'.format(Solution.t))
    ax.set_xlabel(r'x [{0!s}]'.format(Problem.GoverningEquations.XYLatexUnits[0]))
    ax.set_ylabel(r'y [{0!s}]'.format(Problem.GoverningEquations.XYLatexUnits[1]))
    plt.axis("equal")

    # --------------- Export the figure at the right location -------------------------
    # Creating the export environment
    FolderName = os.path.join(Pathes.SolutionPath, ParametersId, "Res"+str(Mesh.MeshResolution).replace(".","_"), "DomainPlots")
    try:    os.makedirs(FolderName)
    except: pass

    # Set the figure properties and shows it
    FileName = "Mplt_SubDomains_t{0:5.3f}".format(Solution.t).replace(".","_")
    plt.savefig(os.path.join(FolderName, FileName)+".png", transparent=True, dpi=300)
    plt.close()

#### Plots the zero contour of the level sets and the registered subdomain patches ####
def PlotLSubDomains(Problem, Mesh, Solution, ParametersId):
    """ Plots the zero contour of the level sets and the registered subdomain patches

    Args:
        Problem        (Problem):             the considered problem and its properties
        Mesh           (MeshStructure):       the considered mesh
        Solution       (Solution):            the solution of the considered problem
        ParametersId   (string):              name of the export folder (TestCase_TestSettings)

    Returns:
        None: only exporting the plot
    """

    # -------------- Initialising the figure ---------------------------------------
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)

    # --------------- Creating the figure ------------------------------------------

    # Plot the bounds of the domain for reference
    for Poly in np.array(Mesh.Domain): plt.plot([*np.array(Poly)[:,0],np.array(Poly)[0,0]], [*np.array(Poly)[:,1],np.array(Poly)[0,1]], "--k")

    # Plotting each subdomain and the fluid's type color
    for i in range(len(Solution.Subdomains)): plotMultipoly(Solution.Subdomains[i], Problem.FluidProp[i].color, ax)

    # Plot the contour of each fluid that is still present in the domain (problems of undertermined fluid will then pop up)
    Fluids = set(map(int, Solution.RFluidFlag))
    Fluids.discard(-1)
    for i in Fluids:
        # Getting the unique values of the level set
        UniqueValues = np.array(list(set(Solution.RLSValues[i,:])))

        # Plot the global fluid-separation contour if relevant
        if np.any(UniqueValues<=0):
            ax.tricontour(Mesh.CartesianPoints[:,0], Mesh.CartesianPoints[:,1], Mesh.InnerElements, \
                          Solution.RLSValues[i,:], [0], colors=Problem.FluidProp[i].color, linewidths=len(Fluids)-i+2)

    # ---------------    Customisation properties -----------------------------------

    # Customisation of labels and latex conversion
    rc('font',size = 12)
    rc('font',family='serif')
    rc('axes',labelsize=14)

    # Customise the positioning of the labels
    [t.set_va('center') for t in ax.get_yticklabels()]
    [t.set_ha('right')  for t in ax.get_yticklabels()]
    [t.set_va('center') for t in ax.get_xticklabels()]
    [t.set_ha('left')   for t in ax.get_xticklabels()]

    # Set the grid and the pane colors
    fig.suptitle(r'Solution for the variableat the time ${0:5.3f}$'.format(Solution.t))
    ax.set_xlabel(r'x [{0!s}]'.format(Problem.GoverningEquations.XYLatexUnits[0]))
    ax.set_ylabel(r'y [{0!s}]'.format(Problem.GoverningEquations.XYLatexUnits[1]))
    plt.axis("equal")

    # --------------- Export the figure at the right location -------------------------
    FolderName = os.path.join(Pathes.SolutionPath, ParametersId, "Res"+str(Mesh.MeshResolution).replace(".","_"), "DomainPlots")
    try:    os.makedirs(FolderName)
    except: pass

    # Set the figure properties and shows it
    FileName = "Mplt_LSubDomains_t{0:5.3f}".format(Solution.t).replace(".","_")
    plt.savefig(os.path.join(FolderName, FileName)+".png", transparent=True, dpi=300)
    plt.close()

#### Plots the zero contour of the level sets and the registered subdomain patches, triangle version ####
def PlotLSValuesTri(Problem, Mesh, Solution, ParametersId):
    """ Plots the zero contour of the level sets and the registered subdomain patches, given a triangulation.

    Args:
        Problem        (Problem):             the considered problem and its properties
        Mesh           (MeshStructure):       the considered mesh
        Solution       (Solution):            the solution of the considered problem
        ParametersId   (string):              name of the export folder (TestCase_TestSettings)

    Returns:
        None: only exporting the plot
    """

    # -------------- Initialising the figure ---------------------------------------
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)

    # --------------- Creating the figure ------------------------------------------
    # Plot the bounds of the domain for reference
    for Poly in np.array(Mesh.Domain): plt.plot([*np.array(Poly)[:,0],np.array(Poly)[0,0]], [*np.array(Poly)[:,1],np.array(Poly)[0,1]], "--k")

    # Plot the contour of each fluid that is still present in the domain (problems of undertermined fluid will then pop up)
    Fluids = set(map(int, Solution.RFluidFlag))
    Fluids.discard(-1)

    # Span each fluid
    for i in Fluids:
        # Get the unique values of the fluid
        UniqueValues = np.array(list(set(Solution.RLSValues[i,:])))

        # Plot the global fluid-separation contour if relevant
        if np.any(UniqueValues<=0):
            ax.tricontour(Mesh.CartesianPoints[:,0], Mesh.CartesianPoints[:,1], Mesh.InnerElements, \
                      Solution.RLSValues[i,:], [0],
                      colors="k",  linewidths=2)

        # Select where the values are non-negative and specify the contours to take into account
        ind  = np.where(Solution.RLSValues[i,:]>=0)[0]
        mm   = np.max(Solution.RLSValues[i,ind])
        lvls = np.linspace(0,mm,400)

        # In the case of several fluids, getting a surface from the vectors (hack from tricontourf to avoid patch tricontour and interpolation)
        if len(UniqueValues)>1:
            Ct = ax.tricontourf(Mesh.CartesianPoints[:,0], Mesh.CartesianPoints[:,1], Mesh.InnerElements,\
                                Solution.RLSValues[i,:],lvls,\
                                cmap = ColorMapsMatplotlib[i])

        # Else, just patch with a single color
        else:
            ax.tripcolor(Mesh.CartesianPoints[:,0], Mesh.CartesianPoints[:,1], Mesh.InnerElements,\
                                0*Solution.RLSValues[i,:], cmap = ColorMapsMatplotlib[i])

    # ---------------    Customisation properties -----------------------------------
    # Customisation of labels and latex conversion
    rc('font',size = 12)
    rc('font',family='serif')
    rc('axes',labelsize=14)

    # Customise the positioning of the labels
    [t.set_va('center')   for t in ax.get_yticklabels()]
    [t.set_ha('right')    for t in ax.get_yticklabels()]
    [t.set_va('center')   for t in ax.get_xticklabels()]
    [t.set_ha('left')     for t in ax.get_xticklabels()]

    # Set the grid and the pane colors
    fig.suptitle(r'Solution for the variable at the time ${0:5.3f}$'.format(Solution.t))
    ax.set_xlabel(r'x [{0!s}]'.format(Problem.GoverningEquations.XYLatexUnits[0]))
    ax.set_ylabel(r'y [{0!s}]'.format(Problem.GoverningEquations.XYLatexUnits[1]))
    plt.axis("equal")

    # --------------- Export the figure at the right location -------------------------
    FolderName = os.path.join(Pathes.SolutionPath, ParametersId, "Res"+str(Mesh.MeshResolution).replace(".","_"), "DomainPlots")
    try:    os.makedirs(FolderName)
    except: pass

    # Set the figure properties and shows it
    FileName = "Mplt_LSValues2DTri_t{0:5.3f}".format(Solution.t).replace(".","_")
    plt.savefig(os.path.join(FolderName, FileName)+".png", transparent=True, dpi=300)
    plt.close()


# ==============================================================================
#  Level Set tools only usable for triangles
# ==============================================================================
#### Plot each level set independently of the subdomain ####
def PlotLSValuesOnWholeDomainTri(Problem, Mesh, Solution,  ParametersId):
    """ Plots the fluid selectors of the subdomain, given a triangulation

    Args:
        Problem        (Problem):             the considered problem and its properties
        Mesh           (MeshStructure):       the considered mesh
        Solution       (Solution):            the solution of the considered problem
        ParametersId   (string):              name of the export folder (TestCase_TestSettings)

    Returns:
        None: only exporting the plot
    """

    # Batching over the level set functions
    for LevelSetId in set(map(int, Solution.RFluidFlag)):

        # --------------- Preparing the data to plot -----------------------------------
        # Extracting the data of interest
        X = Mesh.CartesianPoints[:,0]
        Y = Mesh.CartesianPoints[:,1]
        Z = Solution.RLSValues[LevelSetId,:]

        # --------------- Creating the figure ------------------------------------------
        fig = plt.figure()
        ax  = fig.gca(projection='3d')
        ax.plot_trisurf(X, Y, Z, triangles=Mesh.InnerElements, cmap=ColorMapsMatplotlib[0],\
                              edgecolor=None ,linewidth=0, antialiased=False)
        #ax.tricontour(X,Y,Mesh.InnerElements, Z , [0], colors="r", offset=0.5  )

        # ---------------    Customisation properties -----------------------------------
        # Retrieve the variables names
        variableName      =  "LevelSet_{0!s}".format(LevelSetId)
        variableLatexName =  "Level Set #{0!s}".format(LevelSetId)
        variableLatexUnit =  ""

        # Customisation of labels and latex conversion
        rc('font',size      = 12)
        rc('font',family    = 'serif')
        rc('axes',labelsize = 14)

        # Customise the positioning of the labels
        [t.set_va('center') for t in ax.get_yticklabels()]
        [t.set_ha('right')  for t in ax.get_yticklabels()]
        [t.set_va('center') for t in ax.get_xticklabels()]
        [t.set_ha('left')   for t in ax.get_xticklabels()]
        [t.set_va('center') for t in ax.get_zticklabels()]
        [t.set_ha('right')  for t in ax.get_zticklabels()]

        # Set the grid and the pane colors
        ax.grid(False)
        ax.w_xaxis.set_pane_color((1.0, 1.0, 1.0, 0.2))
        ax.w_yaxis.set_pane_color((1.0, 1.0, 1.0, 0.2))
        ax.w_zaxis.set_pane_color((1.0, 1.0, 1.0, 0.2))

        fig.suptitle(r'Values of the Level Set #{0!s} at the time ${1:5.3f}$'.format(variableLatexName, Solution.t))
        ax.set_xlabel(r'x [{0!s}]'.format(Problem.GoverningEquations.XYLatexUnits[0]))
        ax.set_ylabel(r'y [{0!s}]'.format(Problem.GoverningEquations.XYLatexUnits[1]))
        ax.set_zlabel(r'{0!s} [{1!s}]'.format(variableLatexName, variableLatexUnit))


        # --------------- Export the figure at the right location -------------------------
        FolderName = os.path.join(Pathes.SolutionPath, ParametersId, "Res"+str(Mesh.MeshResolution).replace(".","_"), "DomainPlots")
        try:    os.makedirs(FolderName)
        except: pass

        FileName = "Mplt_3DLS_OnWholeDomainTri_{0!s}_t{1:5.3f}".format(variableName, Solution.t).replace(".","_")
        plt.savefig(os.path.join(FolderName, FileName)+".png", transparent=True, dpi=300)
        plt.close()


# ==============================================================================
#  Soltuion tools only usable for triangles
# ==============================================================================
#### Plot one variable independently of the subdomain, given a triangulation ####
def PlotVariableOnWholeDomainTri2D(Problem, Mesh, Solution, VariableId, ParametersId):
    """ Plot one variable independently of the subdomain, given a triangulation

    Args:
        Problem        (Problem):             the considered problem and its properties
        Mesh           (MeshStructure):       the considered mesh
        Solution       (Solution):            the solution of the considered problem
        VariableId     (integer):             the index of the desired variable to be plot
        ParametersId   (string):              name of the export folder (TestCase_TestSettings)

    Returns:
        None: only exporting the plot
    """

    # --------------- Preparing the data to plot -----------------------------------
    # Extracting the data of interest
    X = Mesh.CartesianPoints[:,0]
    Y = Mesh.CartesianPoints[:,1]
    Z = Solution.RSol[VariableId,:]

    # --------------- Creating the figure ------------------------------------------
    # Matplolib initialisations
    fig = plt.figure()
    ax  = fig.subplots()

    # Still plotting the contours of the LS in case of several fluids
    for i in set(map(int, Solution.RFluidFlag)):
        UniqueValues = np.array(list(set(Solution.RLSValues[i,:])))
        if np.any(UniqueValues<=0):
            ax.tricontour(X, Y, Mesh.InnerElements, Solution.RLSValues[i,:], \
                          levels = [0], colors = "k", linewidths=2)

    # Plotting the solution homogeneously on the full subdomain
    ax.tripcolor(X, Y, Z, triangles=Mesh.InnerElements, cmap=ColorMapsMatplotlib[0],\
                          edgecolor=None ,linewidth=0, antialiased=False, shading = 'gouraud')

    # ---------------    Customisation properties -----------------------------------
    # Retrieve the variables names
    variableName       = Problem.GoverningEquations.VariablesNames[VariableId]
    variableLatexName  = Problem.GoverningEquations.VariablesLatexNames[VariableId]
    variableLatexUnit  = Problem.GoverningEquations.VariablesLatexUnits[VariableId]

    # Customisation of labels and latex conversion
    rc('font',size = 12)
    rc('font',family='serif')
    rc('axes',labelsize=14)

    # Customise the positioning of the labels
    [t.set_va('center') for t in ax.get_yticklabels()]
    [t.set_ha('right')  for t in ax.get_yticklabels()]
    [t.set_va('center') for t in ax.get_xticklabels()]
    [t.set_ha('left')   for t in ax.get_xticklabels()]

    # Set the grid and the pane colors
    fig.suptitle(r'Solution for the variable {0!s} at the time ${1:5.3f}$'.format(variableLatexName, Solution.t))
    ax.set_xlabel(r'x [{0!s}]'.format(Problem.GoverningEquations.XYLatexUnits[0]))
    ax.set_ylabel(r'y [{0!s}]'.format(Problem.GoverningEquations.XYLatexUnits[1]))

    # --------------- Export the figure at the right location -------------------------
    FolderName = os.path.join(Pathes.SolutionPath, ParametersId, "Res"+str(Mesh.MeshResolution).replace(".","_"), "2DPlots")
    try:         os.makedirs(FolderName)
    except: pass

    FileName = "Mplt_2DSol_OnWholeDomainTri_{0!s}_t{1:5.3f}".format(variableName, Solution.t).replace(".","_")
    plt.savefig(os.path.join(FolderName, FileName)+".png", transparent=True, dpi=300)
    plt.close()

#### Plot one variable with a color scale depending of the subdomain, given a triangulation ####
def PlotVariableOnEachSubDomainTri2D(Problem, Mesh, Solution, VariableId, ParametersId):
    """ Plot one variable with a color scale depending of the subdomain, given a triangulation

    Args:
        Problem        (Problem):             the considered problem and its properties
        Mesh           (MeshStructure):       the considered mesh
        Solution       (Solution):            the solution of the considered problem
        VariableId     (integer):             the index of the desired variable to be plot
        ParametersId   (string):              name of the export folder (TestCase_TestSettings)

    Returns:
        None: only exporting the plot
    """

    # --------------- Preparing the data to plot -----------------------------------
    # Extracting the data of interest
    X = Mesh.CartesianPoints[:,0]
    Y = Mesh.CartesianPoints[:,1]
    Z = Solution.RSol[VariableId,:]
    LS = Solution.RLSValues[:,:]

    # --------------- Creating the figure ------------------------------------------
    fig = plt.figure()
    ax  = fig.subplots()

    # Spanning the fluids
    for i in set(map(int, Solution.RFluidFlag)):

        # Still plotting the contours of the LS in case of several fluids
        UniqueValues = np.array(list(set(Solution.RLSValues[i,:])))
        if np.any(UniqueValues<=0): ax.tricontour(X, Y, Mesh.InnerElements, LS[i,:], colors = "k", levels = [0], linewidths=2)

        # Plotting the masked values for each fluid
        ind = np.where(LS[i,:]<-1e-16)[0]
        Ele = np.array(Mesh.InnerElements)
        Mask = [bool(set(ele).intersection(ind)==set(ele)) for ele in Ele]

        ax.tripcolor(X, Y, Z, triangles = Mesh.InnerElements, cmap=ColorMapsMatplotlib[i],\
                                 mask = Mask, edgecolor=None ,linewidth=0, antialiased=False, alpha = 0.7,
                                 shading = 'gouraud')


    # ---------------    Customisation properties -----------------------------------
    # Retrieve the variables names
    variableName      = Problem.GoverningEquations.VariablesNames[VariableId]
    variableLatexName = Problem.GoverningEquations.VariablesLatexNames[VariableId]
    variableLatexUnit = Problem.GoverningEquations.VariablesLatexUnits[VariableId]

    # Customisation of labels and latex conversion
    rc('font',size      = 12)
    rc('font',family    = 'serif')
    rc('axes',labelsize = 14)

    # Customise the positioning of the labels
    [t.set_va('center') for t in ax.get_yticklabels()]
    [t.set_ha('right')  for t in ax.get_yticklabels()]
    [t.set_va('center') for t in ax.get_xticklabels()]
    [t.set_ha('left')   for t in ax.get_xticklabels()]

    # Set the grid and the pane colors
    fig.suptitle(r'Solution for the variable {0!s} at the time ${1:5.3f}$'.format(variableLatexName, Solution.t))
    ax.set_xlabel(r'x [{0!s}]'.format(Problem.GoverningEquations.XYLatexUnits[0]))
    ax.set_ylabel(r'y [{0!s}]'.format(Problem.GoverningEquations.XYLatexUnits[1]))


    # --------------- Export the figure at the right location -------------------------
    FolderName = os.path.join(Pathes.SolutionPath, ParametersId, "Res"+str(Mesh.MeshResolution).replace(".","_"), "2DPlots")
    try:    os.makedirs(FolderName)
    except: pass

    FileName = "Mplt_2DSol_OnSubDomainsTri_{0!s}_t{1:5.3f}".format(variableName, Solution.t).replace(".","_")
    plt.savefig(os.path.join(FolderName, FileName)+".png", transparent=True, dpi=300)
    plt.close()

#### Plot one variable independently of the subdomain ####
def PlotVariableOnWholeDomainTri(Problem, Mesh, Solution, VariableId, ParametersId):
    """ Plot one variable independently of the subdomain, given a triangulation

    Args:
        Problem        (Problem):             the considered problem and its properties
        Mesh           (MeshStructure):       the considered mesh
        Solution       (Solution):            the solution of the considered problem
        VariableId     (integer):             the index of the desired variable to be plot
        ParametersId   (string):              name of the export folder (TestCase_TestSettings)

    Returns:
        None: only exporting the plot
    """

    # --------------- Preparing the data to plot -----------------------------------
    # Extracting the data of interest
    X = Mesh.CartesianPoints[:,0]
    Y = Mesh.CartesianPoints[:,1]
    Z = Solution.RSol[VariableId,:]

    # --------------- Creating the figure ------------------------------------------
    fig = plt.figure()
    ax  = fig.gca(projection='3d')
    ax.plot_trisurf(X, Y, Z, triangles=Mesh.InnerElements, cmap=ColorMapsMatplotlib[0],\
                             edgecolor=None ,linewidth=0, antialiased=False)

    # ---------------    Customisation properties -----------------------------------
    # Retrieve the variables names
    variableName      = Problem.GoverningEquations.VariablesNames[VariableId]
    variableLatexName = Problem.GoverningEquations.VariablesLatexNames[VariableId]
    variableLatexUnit = Problem.GoverningEquations.VariablesLatexUnits[VariableId]

    # Customisation of labels and latex conversion
    rc('font',size      = 12)
    rc('font',family    = 'serif')
    rc('axes',labelsize = 14)

    # Customise the positioning of the labels
    [t.set_va('center') for t in ax.get_yticklabels()]
    [t.set_ha('right')  for t in ax.get_yticklabels()]
    [t.set_va('center') for t in ax.get_xticklabels()]
    [t.set_ha('left')   for t in ax.get_xticklabels()]
    [t.set_va('center') for t in ax.get_zticklabels()]
    [t.set_ha('right')  for t in ax.get_zticklabels()]

    # Set the grid and the pane colors
    ax.grid(False)
    ax.w_xaxis.set_pane_color((1.0, 1.0, 1.0, 0.2))
    ax.w_yaxis.set_pane_color((1.0, 1.0, 1.0, 0.2))
    ax.w_zaxis.set_pane_color((1.0, 1.0, 1.0, 0.2))

    fig.suptitle(r'Solution for the variable {0!s} at the time ${1:5.3f}$'.format(variableLatexName, Solution.t))
    ax.set_xlabel(r'x [{0!s}]'.format(Problem.GoverningEquations.XYLatexUnits[0]))
    ax.set_ylabel(r'y [{0!s}]'.format(Problem.GoverningEquations.XYLatexUnits[1]))
    ax.set_zlabel(r'{0!s} [{1!s}]'.format(variableLatexName, variableLatexUnit))


    # --------------- Export the figure at the right location -------------------------
    FolderName = os.path.join(Pathes.SolutionPath, ParametersId, "Res"+str(Mesh.MeshResolution).replace(".","_"), "3DPlots")
    try:    os.makedirs(FolderName)
    except: pass

    FileName = "Mplt_3DSol_OnWholeDomainTri_{0!s}_t{1:5.3f}".format(variableName, Solution.t).replace(".","_")
    plt.savefig(os.path.join(FolderName, FileName)+".png", transparent=True, dpi=300)
    plt.close()

#### Plot one variable with a color scale depending of the subdomain ####
def PlotVariableOneEachSubDomainTri(Problem, Mesh, Solution, VariableId, ParametersId):
    """ Plot one variable with a color scale depending of the subdomain

    Args:
        Problem        (Problem):             the considered problem and its properties
        Mesh           (MeshStructure):       the considered mesh
        Solution       (Solution):            the solution of the considered problem
        VariableId     (integer):             the index of the desired variable to be plot
        ParametersId   (string):              name of the export folder (TestCase_TestSettings)

    Returns:
        None: only exporting the plot
    """

    # --------------- Preparing the data to plot -----------------------------------
    # Extracting the data of interest
    X  = Mesh.CartesianPoints[:,0]
    Y  = Mesh.CartesianPoints[:,1]
    Z  = Solution.RSol[VariableId,:]
    LS = Solution.RLSValues[:,:]

    # --------------- Creating the figure ------------------------------------------
    fig = plt.figure()
    ax  = fig.gca(projection='3d')

    # Span each fluid present in the domain and plot one variable surface per area and the countour of each area
    # according to the interpolated value of the FluidSelector
    Fluids = set(map(int, Solution.RFluidFlag))
    for i in Fluids:
        # Masking the points that lie outisde the fluid's area
        ind = np.where(LS[i,:]<-1e-16)[0]
        Ele = np.array(Mesh.InnerElements)
        Mask = [set(ele).intersection(ind)==set(ele) for ele in Ele]

        ax.plot_trisurf(X, Y, Z, triangles = Mesh.InnerElements, cmap=ColorMapsMatplotlib[i],\
                                 mask = Mask, edgecolor=None ,linewidth=0, antialiased=False, alpha = 0.7)

    # ---------------    Customisation properties -----------------------------------
    # Retrieve the variables names
    variableName      = Problem.GoverningEquations.VariablesNames[VariableId]
    variableLatexName = Problem.GoverningEquations.VariablesLatexNames[VariableId]
    variableLatexUnit = Problem.GoverningEquations.VariablesLatexUnits[VariableId]

    # Customisation of labels and latex conversion
    rc('font',size      = 12)
    rc('font',family    = 'serif')
    rc('axes',labelsize =14)

    # Customise the positioning of the labels
    [t.set_va('center') for t in ax.get_yticklabels()]
    [t.set_ha('right')  for t in ax.get_yticklabels()]
    [t.set_va('center') for t in ax.get_xticklabels()]
    [t.set_ha('left')   for t in ax.get_xticklabels()]
    [t.set_va('center') for t in ax.get_zticklabels()]
    [t.set_ha('right')  for t in ax.get_zticklabels()]

    # Set the grid and the pane colors
    ax.grid(False)
    ax.w_xaxis.set_pane_color((1.0, 1.0, 1.0, 0.2))
    ax.w_yaxis.set_pane_color((1.0, 1.0, 1.0, 0.2))
    ax.w_zaxis.set_pane_color((1.0, 1.0, 1.0, 0.2))

    fig.suptitle(r'Solution for the variable {0!s} at the time ${1:5.3f}$'.format(variableLatexName, Solution.t))
    ax.set_xlabel(r'x [{0!s}]'.format(Problem.GoverningEquations.XYLatexUnits[0]))
    ax.set_ylabel(r'y [{0!s}]'.format(Problem.GoverningEquations.XYLatexUnits[1]))
    ax.set_zlabel(r'{0!s} [{1!s}]'.format(variableLatexName, variableLatexUnit))

    # --------------- Export the figure at the right location -------------------------
    FolderName = os.path.join(Pathes.SolutionPath, ParametersId, "Res"+str(Mesh.MeshResolution).replace(".","_"), "3DPlots")
    try:    os.makedirs(FolderName)
    except: pass

    FileName = "Mplt_3DSol_OnSubDomainsTri_{0!s}_t{1:5.3f}".format(variableName, Solution.t).replace(".","_")
    plt.savefig(os.path.join(FolderName, FileName)+".png", transparent=True, dpi=300)
    plt.close()

#### Plot the variable retrieved from the EoS independently of the subdomain ####
def PlotPrimaryVariableOnWholeDomainTri(Problem, Mesh, Solution, Field, ParametersId):
    """ Plot one variable independently of the subdomain, given a triangulation

    Args:
        Problem        (Problem):             the considered problem and its properties
        Mesh           (MeshStructure):       the considered mesh
        Solution       (Solution):            the solution of the considered problem
        Field          (integer):             the index of the primary variable to plot the solution for
        ParametersId   (string):              name of the export folder (TestCase_TestSettings)

    Returns:
        None: only exporting the plot
    """

    # --------------- Preparing the data to plot -----------------------------------
    # Extracting the data of interest
    X = Mesh.CartesianPoints[:,0]
    Y = Mesh.CartesianPoints[:,1]
    Z = Solution.RSol[:,:]

    # Computing the value from the conservative variables
    Zbuf = Problem.GoverningEquations.ConservativeToPrimary(Z, Solution.RFluidFlag)[Field,:]


    # --------------- Creating the figure ------------------------------------------
    fig = plt.figure()
    ax  = fig.gca(projection='3d')
    ax.plot_trisurf(X, Y, Zbuf, triangles=Mesh.InnerElements, cmap=ColorMapsMatplotlib[0], edgecolor=None ,linewidth=0, antialiased=False)

    # ---------------    Customisation properties -----------------------------------
    # Retrieve the variables names
    variableName      = Problem.GoverningEquations.PrimaryVariablesNames[Field]
    variableLatexName = Problem.GoverningEquations.PrimaryVariablesLatexNames[Field]
    variableLatexUnit = Problem.GoverningEquations.PrimaryVariablesLatexUnits[Field]

    # Customisation of labels and latex conversion
    rc('font',size      = 12)
    rc('font',family    = 'serif')
    rc('axes',labelsize = 14)

    # Customise the positioning of the labels
    [t.set_va('center') for t in ax.get_yticklabels()]
    [t.set_ha('right')  for t in ax.get_yticklabels()]
    [t.set_va('center') for t in ax.get_xticklabels()]
    [t.set_ha('left')   for t in ax.get_xticklabels()]
    [t.set_va('center') for t in ax.get_zticklabels()]
    [t.set_ha('right')  for t in ax.get_zticklabels()]

    # Set the grid and the pane colors
    ax.grid(False)
    ax.w_xaxis.set_pane_color((1.0, 1.0, 1.0, 0.2))
    ax.w_yaxis.set_pane_color((1.0, 1.0, 1.0, 0.2))
    ax.w_zaxis.set_pane_color((1.0, 1.0, 1.0, 0.2))

    fig.suptitle(r'Solution for the variable {0!s} at the time ${1:5.3f}$'.format(variableLatexName, Solution.t))
    ax.set_xlabel(r'x [{0!s}]'.format(Problem.GoverningEquations.XYLatexUnits[0]))
    ax.set_ylabel(r'y [{0!s}]'.format(Problem.GoverningEquations.XYLatexUnits[1]))
    ax.set_zlabel(r'{0!s} [{1!s}]'.format(variableLatexName, variableLatexUnit))


    # --------------- Export the figure at the right location -------------------------
    FolderName = os.path.join(Pathes.SolutionPath, ParametersId, "Res"+str(Mesh.MeshResolution).replace(".","_"), "3DPlots")
    try:    os.makedirs(FolderName)
    except: pass

    FileName = "Mplt_3DSol_OnWholeDomainTri_{0!s}_t{1:5.3f}".format(variableName, Solution.t).replace(".","_")
    plt.savefig(os.path.join(FolderName, FileName)+".png", transparent=True, dpi=300)
    plt.close()

#### Plot the variable coming from the EoS with a color scale depending of the subdomain ####
def PlotPrimaryVariableOneEachSubDomainTri(Problem, Mesh, Solution, Field, ParametersId):
    """ Plot one variable with a color scale depending of the subdomain

    Args:
        Problem        (Problem):             the considered problem and its properties
        Mesh           (MeshStructure):       the considered mesh
        Solution       (Solution):            the solution of the considered problem
        Field          (integer):             the index of the primary variable one wants the plot for
        ParametersId   (string):              name of the export folder (TestCase_TestSettings)

    Returns:
        None: only exporting the plot
    """

    # --------------- Preparing the data to plot -----------------------------------
    # Extracting the data of interest
    X  = Mesh.CartesianPoints[:,0]
    Y  = Mesh.CartesianPoints[:,1]
    Z  = Solution.RSol[:,:]
    LS = Solution.RLSValues[:,:]

    # Computing the value from the conservative variables
    Zbuf = Problem.GoverningEquations.ConservativeToPrimary(Z, Solution.RFluidFlag)[Field,:]


    # --------------- Creating the figure ------------------------------------------
    fig = plt.figure()
    ax  = fig.gca(projection='3d')

    # Span each fluid present in the domain and plot one variable surface per area and the countour of each area
    # according to the interpolated value of the FluidSelector
    Fluids = set(map(int, Solution.RFluidFlag))
    for i in Fluids:
        # Masking the points that lie outisde the fluid's area
        ind = np.where(LS[i,:]<-1e-16)[0]
        Ele = np.array(Mesh.InnerElements)
        Mask = [set(ele).intersection(ind)==set(ele) for ele in Ele]

        ax.plot_trisurf(X, Y, Zbuf, triangles = Mesh.InnerElements, cmap=ColorMapsMatplotlib[i],\
                                 mask = Mask, edgecolor=None ,linewidth=0, antialiased=False, alpha = 0.7)

    # ---------------    Customisation properties -----------------------------------
    # Retrieve the variables names
    variableName      = Problem.GoverningEquations.PrimaryVariablesNames[Field]
    variableLatexName = Problem.GoverningEquations.PrimaryVariablesLatexNames[Field]
    variableLatexUnit = Problem.GoverningEquations.PrimaryVariablesLatexUnits[Field]

    # Customisation of labels and latex conversion
    rc('font',size      = 12)
    rc('font',family    = 'serif')
    rc('axes',labelsize =14)

    # Customise the positioning of the labels
    [t.set_va('center') for t in ax.get_yticklabels()]
    [t.set_ha('right')  for t in ax.get_yticklabels()]
    [t.set_va('center') for t in ax.get_xticklabels()]
    [t.set_ha('left')   for t in ax.get_xticklabels()]
    [t.set_va('center') for t in ax.get_zticklabels()]
    [t.set_ha('right')  for t in ax.get_zticklabels()]

    # Set the grid and the pane colors
    ax.grid(False)
    ax.w_xaxis.set_pane_color((1.0, 1.0, 1.0, 0.2))
    ax.w_yaxis.set_pane_color((1.0, 1.0, 1.0, 0.2))
    ax.w_zaxis.set_pane_color((1.0, 1.0, 1.0, 0.2))

    fig.suptitle(r'Solution for the variable {0!s} at the time ${1:5.3f}$'.format(variableLatexName, Solution.t))
    ax.set_xlabel(r'x [{0!s}]'.format(Problem.GoverningEquations.XYLatexUnits[0]))
    ax.set_ylabel(r'y [{0!s}]'.format(Problem.GoverningEquations.XYLatexUnits[1]))
    ax.set_zlabel(r'{0!s} [{1!s}]'.format(variableLatexName, variableLatexUnit))

    # --------------- Export the figure at the right location -------------------------
    FolderName = os.path.join(Pathes.SolutionPath, ParametersId, "Res"+str(Mesh.MeshResolution).replace(".","_"), "3DPlots")
    try:    os.makedirs(FolderName)
    except: pass

    FileName = "Mplt_3DSol_OnSubDomainsTri_{0!s}_t{1:5.3f}".format(variableName, Solution.t).replace(".","_")
    plt.savefig(os.path.join(FolderName, FileName)+".png", transparent=True, dpi=300)
    plt.close()


# ==============================================================================
#  Soltuion tools usable for any element type (less precise for contour plots)
# ==============================================================================
#### Plot one variable independently of the subdomain ####
def PlotVariableOnWholeDomain2D(Problem, Mesh, Solution, VariableId, ParametersId):
    """ Plot one variable on the whole domain

    Args:
        Problem        (Problem):             the considered problem and its properties
        Mesh           (MeshStructure):       the considered mesh
        Solution       (Solution):            the solution of the considered problem
        VariableId    (integer):             the index of the desired variable to be plot
        ParametersId   (string):              name of the export folder (TestCase_TestSettings)

    Returns:
        None: only exporting the plot
    """

    # --------------- Preparing the data to plot -----------------------------------
    # Extracting the data of interest
    X  = Mesh.CartesianPoints[:,0]
    Y  = Mesh.CartesianPoints[:,1]
    Z  = Solution.RSol[VariableId,:]
    LS = Solution.RLSValues[:,:]

    # --------------- Creating the figure ------------------------------------------
    fig = plt.figure()
    ax  = fig.subplots()

    # Interpolating the data on the visualisation grid
    xx,yy,zz = GetMeshgridFom3VectorsMaskZ(Mesh, X, Y, Z)

    # --------------- Creating the figure ------------------------------------------
    fig = plt.figure()
    ax  = fig.subplots()

    # Plotting the colormap
    ax.pcolormesh(xx, yy, zz, shading="gouraud", alpha=0.7, cmap=ColorMapsMatplotlib[0], vmin=np.min(Z)-0.2, vmax=np.max(Z)+0.2)

    # Still plotting the contours of the LS
    Fluids = set(map(int, Solution.RFluidFlag))
    for i in Fluids:
        # Get the unique values of the fluid
        UniqueValues = np.array(list(set(Solution.RLSValues[i,:])))

        # Plot the global fluid-separation contour if relevant
        if np.any(UniqueValues<=0):
            # Interpolating the fluid selector on the visualisation grid
            xx, yy, lzz = GetMeshgridFom3VectorsMaskX(Mesh, X, Y, LS[i,:])
            ax.contour(xx, yy,  lzz, levels = [0], colors = "k", linewidths=2)

    # ---------------    Customisation properties -----------------------------------
    # Retrieve the variables names
    variableName      = Problem.GoverningEquations.VariablesNames[VariableId]
    variableLatexName = Problem.GoverningEquations.VariablesLatexNames[VariableId]
    variableLatexUnit = Problem.GoverningEquations.VariablesLatexUnits[VariableId]

    # Customisation of labels and latex conversion
    rc('font',size      = 12)
    rc('font',family    = 'serif')
    rc('axes',labelsize = 14)

    # Customise the positioning of the labels
    [t.set_va('center') for t in ax.get_yticklabels()]
    [t.set_ha('right')  for t in ax.get_yticklabels()]
    [t.set_va('center') for t in ax.get_xticklabels()]
    [t.set_ha('left')   for t in ax.get_xticklabels()]

    # Set the grid and the pane colors
    fig.suptitle(r'Solution for the variable {0!s} at the time ${1:5.3f}$'.format(variableLatexName, Solution.t))
    ax.set_xlabel(r'x [{0!s}]'.format(Problem.GoverningEquations.XYLatexUnits[0]))
    ax.set_ylabel(r'y [{0!s}]'.format(Problem.GoverningEquations.XYLatexUnits[1]))


    # --------------- Export the figure at the right location -------------------------
    FolderName = os.path.join(Pathes.SolutionPath, ParametersId, "Res"+str(Mesh.MeshResolution).replace(".","_"), "2DPlots")
    try:    os.makedirs(FolderName)
    except: pass

    FileName = "Mplt_2DSol_OnWholeDomain_{0!s}_t{1:5.3f}".format(variableName, Solution.t).replace(".","_")
    plt.savefig(os.path.join(FolderName, FileName)+".png", transparent=True, dpi=300)
    plt.close()

#### Plot one variable with a colorscale depending of the subdomain ####
def PlotVariableOnEachSubDomain2D(Problem, Mesh, Solution, VariableId, ParametersId):
    """ Plot one variable with a color scale depending of the subdomain

    Args:
        Problem        (Problem):             the considered problem and its properties
        Mesh           (MeshStructure):       the considered mesh
        Solution       (Solution):            the solution of the considered problem
        VariableId     (integer):             the index of the desired variable to be plot
        ParametersId   (string):              name of the export folder (TestCase_TestSettings)

    Returns:
        None: only exporting the plot
    """

    # --------------- Preparing the data to plot -----------------------------------
    # Extracting the data of interest
    X = Mesh.CartesianPoints[:,0]
    Y = Mesh.CartesianPoints[:,1]
    Z = Solution.RSol[VariableId,:]
    LS = Solution.RLSValues[:,:]

    # --------------- Creating the figure ------------------------------------------
    fig = plt.figure()
    ax  = fig.subplots()

    # Interpolating the data on the visualisation grid
    xx1, yy1, zz = GetMeshgridFom3VectorsMaskZ(Mesh, X, Y, Z)

    # Span each fluid present in the domain and plot one variable surface per area and the countour of each area
    # according to the interpolated value of the FluidSelector
    Fluids = set(map(int, Solution.RFluidFlag))
    for i in Fluids:
        # Interpolating the fluid selector on the visualisation grid
        xx, yy, lzz = GetMeshgridFom3VectorsMaskX(Mesh, X, Y, LS[i,:])

        # Masking the points that lie outisde the fluid's area
        ind = np.where(lzz<0)
        zz1 = copy.deepcopy(zz[:,:])
        zz1[ind] = None

        # Plotting the surface
        ax.pcolormesh(xx1, yy1, zz1, shading="gouraud", alpha=0.7, cmap=ColorMapsMatplotlib[i], \
                      vmin=np.min(Z)-0.2, vmax=np.max(Z)+0.2)

        # Get the unique values of the fluid
        UniqueValues = np.array(list(set(Solution.RLSValues[i,:])))
        # Plot the global fluid-separation contour if relevant
        if np.any(UniqueValues<=0): ax.contour(xx, yy, lzz, levels = [0], colors = "k", linewidths=2)

    # ---------------    Customisation properties -----------------------------------
    # Retrieve the variables names
    variableName      = Problem.GoverningEquations.VariablesNames[VariableId]
    variableLatexName = Problem.GoverningEquations.VariablesLatexNames[VariableId]
    variableLatexUnit = Problem.GoverningEquations.VariablesLatexUnits[VariableId]

    # Customisation of labels and latex conversion
    rc('font',size      = 12)
    rc('font',family    = 'serif')
    rc('axes',labelsize = 14)

    # Customise the positioning of the labels
    [t.set_va('center') for t in ax.get_yticklabels()]
    [t.set_ha('right')  for t in ax.get_yticklabels()]
    [t.set_va('center') for t in ax.get_xticklabels()]
    [t.set_ha('left')   for t in ax.get_xticklabels()]

    # Set the grid and the pane colors
    fig.suptitle(r'Solution for the variable {0!s} at the time ${1:5.3f}$'.format(variableLatexName, Solution.t))
    ax.set_xlabel(r'x [{0!s}]'.format(Problem.GoverningEquations.XYLatexUnits[0]))
    ax.set_ylabel(r'y [{0!s}]'.format(Problem.GoverningEquations.XYLatexUnits[1]))

    # --------------- Export the figure at the right location -------------------------
    FolderName = os.path.join(Pathes.SolutionPath, ParametersId, "Res"+str(Mesh.MeshResolution).replace(".","_"), "2DPlots")
    try:    os.makedirs(FolderName)
    except: pass

    FileName = "Mplt_2DSol_OnSubDomains_{0!s}_t{1:5.3f}".format(variableName, Solution.t).replace(".","_")
    plt.savefig(os.path.join(FolderName, FileName)+".png", transparent=True, dpi=300)
    plt.close()

#### Plot one variable independently of the subdomain ####
def PlotVariableOnWholeDomain(Problem, Mesh, Solution, VariableId, ParametersId):
    """ Plot one variable on the whole subdomain

    Args:
        Problem        (Problem):             the considered problem and its properties
        Mesh           (MeshStructure):       the considered mesh
        Solution       (Solution):            the solution of the considered problem
        VariableId     (integer):             the index of the desired variable to be plot
        ParametersId   (string):              name of the export folder (TestCase_TestSettings)

    Returns:
        None: only exporting the plot
    """

    # --------------- Preparing the data to plot -----------------------------------
    # Extracting the data of interest
    X = Mesh.CartesianPoints[:,0]
    Y = Mesh.CartesianPoints[:,1]
    Z = Solution.RSol[VariableId,:]

    # Interpolating the data on the visualisation grid
    xx,yy,zz = GetMeshgridFom3VectorsMaskX(Mesh, X, Y, Z)

    # --------------- Creating the figure ------------------------------------------
    fig = plt.figure()
    ax  = fig.gca(projection='3d')
    ax.plot_surface(xx, yy, zz, alpha=0.7, cmap=ColorMapsMatplotlib[0])

    # ---------------    Customisation properties -----------------------------------
    # Retrieve the variables names
    variableName      = Problem.GoverningEquations.VariablesNames[VariableId]
    variableLatexName = Problem.GoverningEquations.VariablesLatexNames[VariableId]
    variableLatexUnit = Problem.GoverningEquations.VariablesLatexUnits[VariableId]

    # Customisation of labels and latex conversion
    rc('font',size      = 12)
    rc('font',family    = 'serif')
    rc('axes',labelsize = 14)

    # Customise the positioning of the labels
    [t.set_va('center') for t in ax.get_yticklabels()]
    [t.set_ha('right')  for t in ax.get_yticklabels()]
    [t.set_va('center') for t in ax.get_xticklabels()]
    [t.set_ha('left')   for t in ax.get_xticklabels()]
    [t.set_va('center') for t in ax.get_zticklabels()]
    [t.set_ha('right')  for t in ax.get_zticklabels()]

    # Set the grid and the pane colors
    ax.grid(False)
    ax.w_xaxis.set_pane_color((1.0, 1.0, 1.0, 0.2))
    ax.w_yaxis.set_pane_color((1.0, 1.0, 1.0, 0.2))
    ax.w_zaxis.set_pane_color((1.0, 1.0, 1.0, 0.2))

    fig.suptitle(r'Solution for the variable {0!s} at the time ${1:5.3f}$'.format(variableLatexName, Solution.t))
    ax.set_xlabel(r'x [{0!s}]'.format(Problem.GoverningEquations.XYLatexUnits[0]))
    ax.set_ylabel(r'y [{0!s}]'.format(Problem.GoverningEquations.XYLatexUnits[1]))
    ax.set_zlabel(r'{0!s} [{1!s}]'.format(variableLatexName, variableLatexUnit))


    # --------------- Export the figure at the right location -------------------------
    FolderName = os.path.join(Pathes.SolutionPath, ParametersId, "Res"+str(Mesh.MeshResolution).replace(".","_"), "3DPlots")
    try:    os.makedirs(FolderName)
    except: pass

    FileName = "Mplt_3DSol_OnWholeDomain_{0!s}_t{1:5.3f}".format(variableName, Solution.t).replace(".","_")
    plt.savefig(os.path.join(FolderName, FileName)+".png", transparent=True, dpi=300)
    plt.close()

#### Plot one variable with a color scale depending on the subdomain ####
def PlotVariableOneEachSubDomain(Problem, Mesh, Solution, VariableId, ParametersId):
    """ Plot one variable with a color scale depending of the subdomain

    Args:
        Problem        (Problem):             the considered problem and its properties
        Mesh           (MeshStructure):       the considered mesh
        Solution       (Solution):            the solution of the considered problem
        VariableId     (integer):             the index of the desired variable to be plot
        ParametersId   (string):              name of the export folder (TestCase_TestSettings)

    Returns:
        None: only exporting the plot
    """

    # --------------- Preparing the data to plot -----------------------------------
    # Extracting the data of interest
    X  = Mesh.CartesianPoints[:,0]
    Y  = Mesh.CartesianPoints[:,1]
    Z  = Solution.RSol[VariableId,:]
    LS = Solution.RLSValues[:,:]

    # --------------- Creating the figure ------------------------------------------
    fig = plt.figure()
    ax  = fig.gca(projection='3d')

    # Interpolating the data on the visualisation grid
    _, _, zz = GetMeshgridFom3VectorsMaskX(Mesh, X, Y, Z)

    # Span each fluid present in the domain and plot one variable surface per area and the countour of each area
    # according to the interpolated value of the FluidSelector
    Fluids = set(map(int, Solution.RFluidFlag))
    for i in Fluids:
        # Interpolating the fluid selector on the visualisation grid
        xx, yy, lzz = GetMeshgridFom3VectorsMaskX(Mesh, X, Y, LS[i,:])

        # Masking the points that lie outisde the fluid's area
        ind = np.where(lzz<-1e-16)
        xx[ind] = None
        yy[ind] = None

        # Plotting the surface
        ax.plot_surface(xx, yy, zz, alpha=0.7, cmap=ColorMapsMatplotlib[i])

    # ---------------    Customisation properties -----------------------------------
    # Retrieve the variables names
    variableName      = Problem.GoverningEquations.VariablesNames[VariableId]
    variableLatexName = Problem.GoverningEquations.VariablesLatexNames[VariableId]
    variableLatexUnit = Problem.GoverningEquations.VariablesLatexUnits[VariableId]

    # Customisation of labels and latex conversion
    rc('font',size      = 12)
    rc('font',family    ='serif')
    rc('axes',labelsize = 14)

    # Customise the positioning of the labels
    [t.set_va('center') for t in ax.get_yticklabels()]
    [t.set_ha('right')  for t in ax.get_yticklabels()]
    [t.set_va('center') for t in ax.get_xticklabels()]
    [t.set_ha('left')   for t in ax.get_xticklabels()]
    [t.set_va('center') for t in ax.get_zticklabels()]
    [t.set_ha('right')  for t in ax.get_zticklabels()]

    # Set the grid and the pane colors
    ax.grid(False)
    ax.w_xaxis.set_pane_color((1.0, 1.0, 1.0, 0.2))
    ax.w_yaxis.set_pane_color((1.0, 1.0, 1.0, 0.2))
    ax.w_zaxis.set_pane_color((1.0, 1.0, 1.0, 0.2))

    fig.suptitle(r'Solution for the variable {0!s} at the time ${1:5.3f}$'.format(variableLatexName, Solution.t))
    ax.set_xlabel(r'x [{0!s}]'.format(Problem.GoverningEquations.XYLatexUnits[0]))
    ax.set_ylabel(r'y [{0!s}]'.format(Problem.GoverningEquations.XYLatexUnits[1]))
    ax.set_zlabel(r'{0!s} [{1!s}]'.format(variableLatexName, variableLatexUnit))

    # --------------- Export the figure at the right location -------------------------
    FolderName = os.path.join(Pathes.SolutionPath, ParametersId, "Res"+str(Mesh.MeshResolution).replace(".","_"), "3DPlots")
    try:    os.makedirs(FolderName)
    except: pass

    FileName = "Mplt_3DSol_OnSubDomains_{0!s}_t{1:5.3f}".format(variableName, Solution.t).replace(".","_")
    plt.savefig(os.path.join(FolderName, FileName)+".png", transparent=True, dpi=300)
    plt.close()

#### Plot the variable coming from the EoS independently of the subdomain ####
def PlotPrimaryVariableOnWholeDomain(Problem, Mesh, Solution, Field, ParametersId):
    """ Plot one variable on the whole subdomain

    Args:
        Problem        (Problem):             the considered problem and its properties
        Mesh           (MeshStructure):       the considered mesh
        Solution       (Solution):            the solution of the considered problem
        Field          (integer):             the index of the primary variable to plot
        ParametersId   (string):              name of the export folder (TestCase_TestSettings)

    Returns:
        None: only exporting the plot
    """

    # --------------- Preparing the data to plot -----------------------------------
    # Extracting the data of interest
    X = Mesh.CartesianPoints[:,0]
    Y = Mesh.CartesianPoints[:,1]
    Z = Solution.RSol[:,:]

    # Computing the value from the conservative variables
    Zbuf = Problem.GoverningEquations.ConservativeToPrimary(Z, Solution.RFluidFlag)[Field,:]

    # --------------- Creating the figure ------------------------------------------
    fig = plt.figure()
    ax  = fig.gca(projection='3d')

    # Interpolating the data on the visualisation grid
    xx, yy, zz = GetMeshgridFom3VectorsMaskX(Mesh, X, Y, Zbuf)

    # --------------- Creating the figure ------------------------------------------
    fig = plt.figure()
    ax  = fig.gca(projection='3d')
    ax.plot_surface(xx, yy, zz, alpha=0.7, cmap=ColorMapsMatplotlib[0])

    # ---------------    Customisation properties -----------------------------------
    # Retrieve the variables names
    variableName      = Problem.GoverningEquations.PrimaryVariablesNames[Field]
    variableLatexName = Problem.GoverningEquations.PrimaryVariablesLatexNames[Field]
    variableLatexUnit = Problem.GoverningEquations.PrimaryVariablesLatexUnits[Field]

    # Customisation of labels and latex conversion
    rc('font',size      = 12)
    rc('font',family    = 'serif')
    rc('axes',labelsize = 14)

    # Customise the positioning of the labels
    [t.set_va('center') for t in ax.get_yticklabels()]
    [t.set_ha('right')  for t in ax.get_yticklabels()]
    [t.set_va('center') for t in ax.get_xticklabels()]
    [t.set_ha('left')   for t in ax.get_xticklabels()]
    [t.set_va('center') for t in ax.get_zticklabels()]
    [t.set_ha('right')  for t in ax.get_zticklabels()]

    # Set the grid and the pane colors
    ax.grid(False)
    ax.w_xaxis.set_pane_color((1.0, 1.0, 1.0, 0.2))
    ax.w_yaxis.set_pane_color((1.0, 1.0, 1.0, 0.2))
    ax.w_zaxis.set_pane_color((1.0, 1.0, 1.0, 0.2))

    fig.suptitle(r'Solution for the variable {0!s} at the time ${1:5.3f}$'.format(variableLatexName, Solution.t))
    ax.set_xlabel(r'x [{0!s}]'.format(Problem.GoverningEquations.XYLatexUnits[0]))
    ax.set_ylabel(r'y [{0!s}]'.format(Problem.GoverningEquations.XYLatexUnits[1]))
    ax.set_zlabel(r'{0!s} [{1!s}]'.format(variableLatexName, variableLatexUnit))


    # --------------- Export the figure at the right location -------------------------
    FolderName = os.path.join(Pathes.SolutionPath, ParametersId, "Res"+str(Mesh.MeshResolution).replace(".","_"), "3DPlots")
    try:    os.makedirs(FolderName)
    except: pass

    FileName = "Mplt_3DSol_OnWholeDomain_{0!s}_t{1:5.3f}".format(variableName, Solution.t).replace(".","_")
    plt.savefig(os.path.join(FolderName, FileName)+".png", transparent=True, dpi=300)
    plt.close()

#### Plot one variable coming from the EoS with a color scale depending on the subdomain ####
def PlotPrimaryVariableOneEachSubDomain(Problem, Mesh, Solution, Field, ParametersId):
    """ Plot one variable with a color scale depending of the subdomain

    Args:
        Problem        (Problem):             the considered problem and its properties
        Mesh           (MeshStructure):       the considered mesh
        Solution       (Solution):            the solution of the considered problem
        Field          (integer):             the index of the primary variable to plot
        ParametersId   (string):              name of the export folder (TestCase_TestSettings)

    Returns:
        None: only exporting the plot
    """

    # --------------- Preparing the data to plot -----------------------------------
    # Extracting the data of interest
    X  = Mesh.CartesianPoints[:,0]
    Y  = Mesh.CartesianPoints[:,1]
    Z  = Solution.RSol[:,:]
    LS = Solution.RLSValues[:,:]

    # Computing the value from the conservative variables
    Zbuf = Problem.GoverningEquations.ConservativeToPrimary(Z, Solution.RFluidFlag)[Field,:]


    # --------------- Creating the figure ------------------------------------------
    fig = plt.figure()
    ax  = fig.gca(projection='3d')

    # Interpolating the data on the visualisation grid
    _, _, zz = GetMeshgridFom3VectorsMaskX(Mesh, X, Y, Zbuf)

    # Span each fluid present in the domain and plot one variable surface per area and the countour of each area
    # according to the interpolated value of the FluidSelector
    Fluids = set(map(int, Solution.RFluidFlag))
    for i in Fluids:
        # Interpolating the fluid selector on the visualisation grid
        xx, yy, lzz = GetMeshgridFom3VectorsMaskX(Mesh, X, Y, LS[i,:])

        # Masking the points that lie outisde the fluid's area
        ind = np.where(lzz<-1e-16)
        xx[ind] = None
        yy[ind] = None

        # Plotting the surface
        ax.plot_surface(xx, yy, zz, alpha=0.7, cmap=ColorMapsMatplotlib[i])

    # ---------------    Customisation properties -----------------------------------
    # Retrieve the variables names
    variableName      = Problem.GoverningEquations.PrimaryVariablesNames[Field]
    variableLatexName = Problem.GoverningEquations.PrimaryVariablesLatexNames[Field]
    variableLatexUnit = Problem.GoverningEquations.PrimaryVariablesLatexUnits[Field]

    # Customisation of labels and latex conversion
    rc('font',size      = 12)
    rc('font',family    ='serif')
    rc('axes',labelsize = 14)

    # Customise the positioning of the labels
    [t.set_va('center') for t in ax.get_yticklabels()]
    [t.set_ha('right')  for t in ax.get_yticklabels()]
    [t.set_va('center') for t in ax.get_xticklabels()]
    [t.set_ha('left')   for t in ax.get_xticklabels()]
    [t.set_va('center') for t in ax.get_zticklabels()]
    [t.set_ha('right')  for t in ax.get_zticklabels()]

    # Set the grid and the pane colors
    ax.grid(False)
    ax.w_xaxis.set_pane_color((1.0, 1.0, 1.0, 0.2))
    ax.w_yaxis.set_pane_color((1.0, 1.0, 1.0, 0.2))
    ax.w_zaxis.set_pane_color((1.0, 1.0, 1.0, 0.2))

    fig.suptitle(r'Solution for the variable {0!s} at the time ${1:5.3f}$'.format(variableLatexName, Solution.t))
    ax.set_xlabel(r'x [{0!s}]'.format(Problem.GoverningEquations.XYLatexUnits[0]))
    ax.set_ylabel(r'y [{0!s}]'.format(Problem.GoverningEquations.XYLatexUnits[1]))
    ax.set_zlabel(r'{0!s} [{1!s}]'.format(variableLatexName, variableLatexUnit))

    # --------------- Export the figure at the right location -------------------------
    FolderName = os.path.join(Pathes.SolutionPath, ParametersId, "Res"+str(Mesh.MeshResolution).replace(".","_"), "3DPlots")
    try:    os.makedirs(FolderName)
    except: pass

    FileName = "Mplt_3DSol_OnSubDomains_{0!s}_t{1:5.3f}".format(variableName, Solution.t).replace(".","_")
    plt.savefig(os.path.join(FolderName, FileName)+".png", transparent=True, dpi=300)
    plt.close()


# ==============================================================================
#  Checkup tools
# ==============================================================================

#### Plot the normal and dual information of an element ###
def PlotNormalDualInfo(Mesh, idx):
    """ Plots the normals and dual information of an element (normals, dual normals, barycentric point)
    for the considered element.

    Args:
        Mesh           (MeshStructure):             the considered mesh

    Returns:
        None: Runtime-plotted plot
    """

    # Select the physical element to plot
    ele = self.Mesh.InnerElements[idx]

    # Retrieve its physical properties
    coords = self.Mesh.CartesianPoints[ele+[ele[0]],:]
    a = self.Mesh.DualCenters[idx]

    n = self.Mesh.ElementsBoundaryWiseNormals[idx]
    v = self.Mesh.DualNormals[idx]

    # Loop on each face to plot the associated properties
    for f in range(len(ele)):

        # Retrieve the mid-point of the face
        md = a[f]

        # Retrieve the mid-point of the associated dual segment
        midpoint = 0.5*(a[f]+a[-1])

        # Plot the associated dual segment in blue and the face segment in red
        plt.plot([a[f][0], a[-1][0]], [a[f][1], a[-1][1]], "-pb")
        plt.plot(coords[f:f+2,0], coords[f:f+2,1], "*r-")

        # Plot the normal information
        plt.quiver(midpoint[0], midpoint[1], v[f][0], v[f][1])
        plt.quiver(md[0], md[1], n[f][0], n[f][1])

    # Show the element
    plt.show()
