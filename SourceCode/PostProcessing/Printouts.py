#***************************************************************************
# Copyright (C) 2020 Élise Le Mélédo
# This file is part of LESARD.
#
# LESARD is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LESARD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LESARD.  If not, see <https://www.gnu.org/licenses/>.
#****************************************************************************

"""
The module :code:`Printouts` located in the :code:`SourceCode/Postprocessing` file gathers
routines defining reccurring runtime printouts for concision.

|

"""

# ==============================================================================
#	Preliminary imports
# ==============================================================================
import numpy as np


# ==============================================================================
# Implemented printouts
# ==============================================================================

#### Print a recap of the problem and solver at the launch of the problem ####
def PrintPbInfo(Mesh, Prolem, ProblemData, Solver, Solution):
	""" Print a recap of the problem and solver at the launch of the problem

		Args:
			Mesh        (MeshStructure):    the considered mesh
			Problem     (Problem):  		the considered problem
			ProblemData (Parameters):       the parameters of the solver
			Solver      (Solver):           the used solver
			Solution    (Solution):         the obtained solution

		Returns:
			None
		
		.. note::
		
			To be completed
	"""
	
	# Informing the user
	print("""\n\n**************************************\n*  Problem and solver information    *\n**************************************\n""")
	
	print("Considered problem:")
	print("------------------")
	print("Considered problem:\t ", ProblemData.ProblemID)
	print("Considered mesh:\t ",    Mesh.MeshName)
	
	print("\nConsidered solver:")
	print("------------------")
	#print("Considered time iteration scheme:\t ",    Solver.SpatialScheme.SchemeName)
	
	print("\nComputation starting point:")
	print("---------------------------")
	print("Mesh resolution: ", Mesh.MeshResolution)
	print("Starting time:\t ",   Solution.t)
	
	# Closing matters
	print("""**************************************\n""")

#### Printout the solution bounds ####
def Printout_MinMaxSolVal(Problem, Solution):
	""" Printouts the min and max values of the solution for each variable for the given solution

		Args:
			Problem   (Problem):   the considered problem
			Solution  (Solution):  the obtained solution

		Returns:
			None
	"""

	# Inform the user
	print("Computing for the time {0!s}".format(Solution.t))
	for i in range(Problem.GoverningEquations.NbVariables):
		print("  Variable "+str(i)+"({0!s})".format(Problem.GoverningEquations.VariablesNames[i])+\
			  ":\t    min:"+str(np.min(Solution.Sol[i,:]))+"  \tmax: "+str(np.max(Solution.Sol[i,:])))

#### Printout the primary solution bounds ####
def Printout_MinMaxSolVal_Primary(Problem, Solution):
	""" Printouts the min and max values of the solution for each primary variable for the given solution

		Args:
			Problem   (Problem):  		    the considered problem
			Solution  (Solution srucutre):  the obtained solution

		Returns:
			None
	"""

	# Compute the primary values
	Values = Problem.GoverningEquations.ConservativeToPrimary(Solution.Sol, Solution.FluidFlag)

	# Inform the user
	print("Computing for the time {0!s}".format(Solution.t))
	for i in range(Problem.GoverningEquations.NbVariables):
		print("  Variable "+str(i)+"({0!s})".format(Problem.GoverningEquations.PrimaryVariablesNames[i])+\
			  ":\t    min:"+str(np.min(Values[i,:]))+"  \t max: "+str(np.max(Values[i,:])))

#### Printout the values of the FluidSpotter per element
def Printout_DetailedLSValues(t, Problem, Mesh, Solution):
	""" Printouts the min and max values of the solution for each variable for the given solution

		Args:
			t         (float):              the considered time point
			Problem   (Problem):  		    the considered problem
			Mesh      (MeshStructure):      the considered mesh
			Solution  (Solution srucutre):  the obtained solution

		Returns:
			None
	"""

	# Inform the user
	for (i,ele) in enumerate(Mesh.InnerElements):
		print("==================================")
		print("Element #", i,"\t with Dofs:", ele, "with respective coordinates:")
		print(Mesh.CartesianCoordinates[ele,:])
		print(Solution.LSValues[0,Mesh.ElementsDofs[i]])
