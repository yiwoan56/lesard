#***************************************************************************
# Copyright (C) 2020 Élise Le Mélédo
# This file is part of LESARD.
#
# LESARD is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LESARD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LESARD.  If not, see <https://www.gnu.org/licenses/>.
#****************************************************************************

"""

|
|

All the predefined Fluids' models (classes) that are casting
the fluid's properties in order to be passed to a governing equations
are given in the file :code:`SourceCode/ProblemDefinition/FluidModels.py`

|

.. note::

    The fluid's properties to be passed to those models are practically selected in
    the file :code:`SourceCode/Mappers.py`

|
|

------------------------------------------

Implemented models
-------------------

|

"""


# ==============================================================================
#                  Simple fluids definitions
# ==============================================================================

#### Dummy model for compatibility ####
class Dummy():
	""" Dummy model that can be used to test the any simple problems or debug
	problem that does not rely on any Fluid's properties.
	This is a dummy model, it only contains a :code:`color` attribute for the visualisation process,
	filled upon instance creation.

	.. note::

		- This is only a product-type class: no method is available
		- At instance creation it takes as many input (whose values will be trashed) as wished for call time compatibility

	|

	"""

	def __init__(self, color, *args):
		"""
		Args:
			color (string or matplotlib color scheme): the color of the fluid for plotting
		"""

		self.color = color

#### Simple fluid model ####
class SimpleFluid():
	""" SimpleFluid model that can be used to test the EulerEquations or Advection problems,
	for either the stiff or idel EOS. It fills the fluid's model fields :code:`color`,
	:code:`gamma` and :code:`pinf` upon instance creation.

	.. note::

		- This is only a product-type class: no method is available
		- It takes as many input (whose values will be trashed) as wished for call time compatibility

	|

	"""

	def __init__(self, color, gamma, pinf, R, *args):
		"""
		Args:
			color (string or matplotlib color scheme): the color of the fluid for plotting
			gamma (float): model attribute
			pinf (float):  model attribute
		"""

		self.gamma = gamma
		self.pinf  = pinf
		self.R     = R
		self.color = color
