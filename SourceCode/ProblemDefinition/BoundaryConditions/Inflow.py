#***************************************************************************
# Copyright (C) 2020 Élise Le Mélédo
# This file is part of LESARD.
#
# LESARD is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LESARD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LESARD.  If not, see <https://www.gnu.org/licenses/>.
#****************************************************************************

#The module :code:`SourceCode/BoundaryConditions/Inflow.py` contains a class that gathers all the inflow
#functions that one may want to consider from a boundary.

# ==============================================================================
# Import modules
# ==============================================================================
# Import necessary python libraries
import LocalLib.ReversedFunctools as fcts


# ==============================================================================
# Inflow boundary conditions
# ==============================================================================
class Inflow():
	"""
	Class furnishing the methods giving the shape of the inflow on a given boundary.
	Upon creation,  stores in the field :code:`Inflow` the routine itself, whose fixed
	parameters have been shortcut.

		1. | Constant
	"""

	# ------------------------------------------------------------------------------------
	#          Interfacing initialisation
	# ------------------------------------------------------------------------------------
	def __init__(self, Id, *params):
		"""
		Args:
			Id       (integer):   the index corresponding to the equation of fluid the user wants when considering the governing equations given in this module and the associated fluid.
			Params   (list):      (optinal) the fixed arguments to pass to the function (see each of them for details)
		|

		.. rubric:: Methods
		"""

		# ----------------- Mapping to the implemented conditions
		if   Id==1: self.Inflow = fcts.partial(self.Constant(), *params)

		else: raise NotImplementedError("Error in the selection of the Boundary conditions \n\
		                                 Unknown index in the inflow function. Check your Problem definition. Abortion.")


	# ------------------------------------------------------------------------------------
	#           Implemented conditions
	# ------------------------------------------------------------------------------------

	#### Constant value as an inflow ####
	def Constant(self, Problem, Mesh, Solution, Residuals, FluidId, Bd, BdIndex, Constant, *args):
		"""
		Inflows a constant at the given points

		Args:
			Problem    (Problem):            the considered problem
			Mesh       (MeshStructure):      the considered mesh
			Solution   (Solution):           the considered solution (as of the previous subtimestep (as used in the temporal scheme))
			Residuals  (2D numpy array):     the value of the currently computed residuals(NbVars x NbDofs)
			FluidId    (integer):            the index of the considered fluid (warning: NOT its value in mappers)
			Bd         (integer):            the tag of the boundary the considered points are lying on
			BdIndex    (integer list):       the list of indices of the degrees of freedom to treat
			Constant   (floar):              the constant value to add on the degrees of freedom

		Returns:
			res        (2D float numpy array):  the updated residuals

		.. note::

			*args is here only for compatibility on call
		"""

		return(Constant+Residuals[:,BdIndex])
