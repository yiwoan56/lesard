#***************************************************************************
# Copyright (C) 2020 Élise Le Mélédo
# This file is part of LESARD.
#
# LESARD is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LESARD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LESARD.  If not, see <https://www.gnu.org/licenses/>.
#****************************************************************************

#The module :code:`SourceCode/BoundaryConditions/Wall.py` contains a single function that prescribes a wall boundary condition

# ==============================================================================
#                  Import the different modules
# ==============================================================================
import numpy as np


# ==============================================================================
#                  Definition of the routine
# ==============================================================================
def Wall(Problem, Mesh, Solution, Residuals, FluidId, Bd, BdIndex, *args):
	"""
	Routine that does nothing, just lets outflow (there for compatibility)

	Args:
		Problem    (Problem):            the considered problem
		Mesh       (MeshStructure):      the considered mesh
		Solution   (Solution):           the considered solution (as of the previous subtimestep (as used in the temporal scheme))
		Residuals  (2D numpy array):     the value of the currently computed residuals(NbVars x NbDofs)
		FluidId    (integer):            the index of the considered fluid (warning: NOT its value in mappers)
		Bd         (integer):            the tag of the boundary the considered points are lying on
		BdIndex    (integer list):       the list of indices of the degrees of freedom to treat

	Returns:
		res        (2D float numpy array):  the updated residuals

	|

	.. warning::

		Only accepts horizontal and vertical wall, is not computing the curvy location-dependent impact (yet)

	.. note::

		*args is here only for compatibility on call

	|
	"""

	# ------------------- Determine the wall conditions depending on the layout of the given Dofs -------------
	# Retrieve the cartesian coordinates of the given Dofs
	XY = Mesh.DofsXY[BdIndex,:]

	# Check if we are in a vertical wall
	if np.isclose(np.min(XY[:,0]), np.max(XY[:,0])) and not	np.isclose(np.min(XY[:,1]), np.max(XY[:,1])):
		Residuals[1, BdIndex] = 0

	# Check if we are in an horizontal wall
	elif (not np.isclose(np.min(XY[:,0]), np.max(XY[:,0])) and np.isclose(np.min(XY[:,1]), np.max(XY[:,1]))):
		Residuals[2, BdIndex] = 0

	# Else not yet implemented
	else:
		raise(NotImplementedError("""Error in the application of the boundary conditions. A wall condition\
		 cannot be yet applied to a non-straight boundary. Please adapt your wishes or code it."""))

	# ------------------- Return the values --------------------------------------------
	return(Residuals[:,BdIndex])
