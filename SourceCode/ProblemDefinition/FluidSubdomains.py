#***************************************************************************
# Copyright (C) 2020 Élise Le Mélédo
# This file is part of LESARD.
#
# LESARD is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LESARD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LESARD.  If not, see <https://www.gnu.org/licenses/>.
#****************************************************************************

"""
.. |bs|   unicode:: U+00A0 .. NO-BREAK SPACE

|

-----------------------------------------------------

Features
-----------------------------------------------------

|

The module furnishes the functions that initialise automatically
the initial fluid subdomains according to the parameters given
by the user in the problem's definition.
The available routines can be categorized into three main categories:

    - Generic subdomains definition routines (the only functions to consider when being a simple user)

    - Domain splitting routines (the technical unitary part of the generic subdomains definition routine,
      e.g. cut a plane through Bezier lines, define droplets)

    - Flagging degrees of freedom and vertices according to the freshly defined sudomains

|

.. note::

    - |bs| Any is non fully disconnected domain is supported, even non arc-connected ones. However, it may fail if the given domain includes degenerated boundaries (i.e. two overlapping boundaries, self-intersecting shapes etc)

    - |bs| Plotting the initial subdomains can be done with the routine :code:`PlotSubdomains` included in the :code:`LSRD.SourceCode.PostProcessing.Plots_Matplotlib` module.

|


.. rubric::  Set-up

The only set-up is to load the relevant module (if not working with the full code, adapt the path to the :code:`FluidSubdomains` module).

.. code::

    import ProblemDefinition.FluidSubdomains as SD

|
|
|

-----------------------------------------------------

Generic principle and example of use
-----------------------------------------------------

|

.. rubric:: Principle

Given a mesh and a list of :math:`n` shapes parametered in the format accepted by the routine :code:`LSRD.SourceCode.ProblemDefinition.FluidSubdomains.Mapper`,
the main routine of the module (:code:`FluidSubdomains.GetSubdomains`) defines :math:`n+1` subdomains.
The domain of the first fluid will be set as the difference from the computational domain by the union of all the patches, while the subdomain of each following
fluid is created by patching each given shape of the list on the top of each other.

|

By example, if one defines three ordered fluids over a square domain correspoding to ["water", "oil", "milk"] and furnishes two patches
corresponding to ["centred circle", "half left plane"], then the milk will be set on the half left plane, the oil on the half right part of
the centred circle and the water everywhere elsewhere (see more concrete examples below in the three fluids paragraph).

|

.. note::
    It is also possible to only define a simple fluid. The furnished vector :code:`patch` should then be empty.

|

.. warning::

    - When cutting the plane through a bezier or an arbitrary line, make sure that the line is cutting the domain completely (i.e that both extremities of the lines are beyond the domain's outer boundary)

    - If during the process a subdomain appears to be empty, the code will abort due to inconsistency of the problem's definition, for safety.

|
|

.. rubric:: Examples

Various examples emphasising the definition of subdomains depending on their possible types are provided below,
and can be tested from the furnished example meshes. For a complete description of the parameters and possibilities,
see the documentation of :code:`LSRD.SourceCode.ProblemDefinition.FluidSubdomains.Mapper`.

|

For the sake of the demonstration, we consider a L-shaped sample mesh having a hole in its middle and assume it has been preloaded in the variable
:code:`Mesh` by e.g. the command

.. code::

    import Meshing
    Mesh = Meshing.Meshing_DualMesh.DualMesh(MeshName, MeshOrder, MeshType, 20).Mesh

|
|

**Case of two fluids**

|

We first explain the principles of domain splitting and patching on a simple two fluids definition.
One first possibility is to create "bubbles" by specifying as a parameter (4,  BoxCenterX, BoxCenterY, BoxWidthX, BoxWidthY, BubblesDiameter, BubblesSpacingX, BubblesSpacingY)
where the box is the rectangular area within which the bubbles should be created. Note that (as in the right figure case), if the radius of the bubble is larger than the
given spacing, the bubbles will be merged accordingly.

    .. list-table::

        * - *Generate  disconnected bubbles*

            .. code::

                patch = [(4, 0.5, 1.0, 0.8, 0.8, 0.05, 0.15, 0.15)]
                SD.GetSubdomains(patch, Mesh)

            .. figure:: _images/Mplt_SubDomains_bubbles_1.png


          - *Generate partially merged bubbles*

            .. code::

                patch = [(4,0.5, 1.0, 1.8, 0.8, 0.05, 0.15, 0.15)]
                SD.GetSubdomains(patch, Mesh)

            .. figure:: _images/Mplt_SubDomains_bubbles_2.png


|

More simply, one can also split the domain by a single line.
Note that the line should cut the domain through, otherwise the code will automatically crash.
Defining a custom line as in the left figure, one has to give the first tuple value (integer)
depending on the part of the cutting domain he likes to consider (see the technical documentation
for more details). Automatic cutting lines are also predefined, as shown in the right figure.
Bezier cuts are defined in a similar spirit, though one has to pay attention to the definition of the
drag vector (see the technical documentation).

    .. list-table::

        * - *Cut through predefined lines*

            .. code::

                patch = [(2,0.6,1.5,0,0.5)]
                SD.GetSubdomains(patch, Mesh)

            .. figure:: _images/Mplt_SubDomains_cut_1.png


          - *Cut through custom lines*

            .. code::

                patch = [1]
                SD.GetSubdomains(patch, Mesh)

            .. figure:: _images/Mplt_SubDomains_cut_2.png

    .. list-table::

        * - *Cut through bezier lines (1)*

            .. code::

                patch = [(0, 0,2, 0.5, 0.2, -0.5, -0.2)]
                SD.GetSubdomains(patch, Mesh)

            .. figure:: _images/Mplt_SubDomains_cut3.png


          - *Cut through bezier lines (2)*

            .. code::

                patch = [(0, 0,1, 0.5, 0.2, -0.5, 0.2)]
                SD.GetSubdomains(patch, Mesh)

            .. figure:: _images/Mplt_SubDomains_cut4.png

|

Lastly, one can also define directly the patch as a polygonal shape given by its vertices.
There too, holes and boundaries are not imparing the patch's definition.

    .. list-table::

        * - *Patches may follow boundaries*

            .. code::

                patch = [[[0.5, 0],[0.5, 0.5],[0, 0.5]]]
                SD.GetSubdomains(patch, Mesh)

            .. figure:: _images/Mplt_SubDomains_polygon_1.png


          - *Patches may be defined over holes*

            .. code::

                patch = [[[0.2, 0.2],[0.8, 0.2],[0.5, 0.9],[0.3,0.7]]]
                SD.GetSubdomains(patch, Mesh)

            .. figure:: _images/Mplt_SubDomains_polygon2.png

|
|


**Case of several fluids**

|

Let us now emphasise the definition of more than three subdomains. As shortly explained above in the
"Generic principle" section, the subdomains have to be seen as a collection of superposing patches,
from the right to the left of the given list. Below shows the patching of the subdomains giving the
patches ordering. The color of the fluids according to their given order in the problem's definition
is ["blue", "red", "green"], and the patches are given accordingly to ["patch fluid red", "patch fluid green"],
the rightest given fluid beeing on the top of the superposition. The "patch fluid blue" is defined as the
remaining non-assigned area within the computational domain.

    .. list-table::

        * - Patches with the custom triangle on the top of the diagonal domain split

            .. code::

                patch = [0, [[0.2, 0.2],[0.8, 0.2],[0.5, 0.9]]]
                SD.GetSubdomains(patch, Mesh)

            .. figure:: _images/Mplt_SubDomains_3patch1.png


          - Patches with the diagonal domain split on the top of the custom triangle

            .. code::

                patch = [[[0.2, 0.2],[0.8, 0.2],[0.5, 0.9]], 0]
                SD.GetSubdomains(patch, Mesh)

            .. figure:: _images/Mplt_SubDomains_3patch2.png

|

"""


# ==============================================================================
#	Preliminary imports
# ==============================================================================
# Functional and math libraries
import numpy as np
import copy

# Geometrical libraries
from shapely.geometry import LineString, Polygon, LinearRing, MultiPolygon, Point
from shapely.ops      import linemerge, unary_union, polygonize


# ==============================================================================
#	Post-fluid subdomain definition tasks
# ==============================================================================
def ExportFluidFlags(SubDomains, Mesh):
    """

    Taggs the Dofs according to the fluid index active at its location.

    Args:
        SubDomains (shapely multipolygons list):  the list of all subdomains correspoding to each given fluid
        Mesh       (MeshStructure):               the mesh associated to the problem, where the subdomains have to be defined

    Returns:
        Flags      (numpy integer array):         list of the Flags selecting the proper fluid (NbDofs)

    .. note::

        - |bs| In case of an element on the interface, the first registered fluid prevails in the tag
        - |bs| The flags are initialised to -1: if there is a -1 in the output Flag list, the subdomains definition failed

    """

    # Initialising the Flags according to the number of Dofs + number of Vertices
    Flags = -1*np.ones(Mesh.NbDofs, dtype=int)

    # Spanning all the physical points associated to the Dofs and all the vertices
    for i in range(0,Mesh.NbDofs):
        Pt = Mesh.DofsXY[i,:]
        j=0
        while j<len(SubDomains):
            if SubDomains[j].intersects(Point(Pt[0], Pt[1]).buffer(1e-15)):
                Flags[i] = j
                break
            j += 1

    # Giving back the fresh flags
    return(Flags)

# ==============================================================================
#	Domain splitting and subdomain definition routines
# ==============================================================================
##### Patching all the partial subdomains together to define the final subdomains ####
def GetSubdomains(SubDomainList, Mesh):
    """
    Patching the partial subdomains in order to define the actual fluid's subdomains.


    Args:
        SubDomainList (shapely multipolygons list):  the list of all partial subdomains correspoding to the split given for each fluid (NbFluids-1)
        Mesh          (MeshStructure):               the mesh associated to the problem and subdomains

    Returns:
        Polys         (shapely multipolygons list):  the list of the proper fluid's subdomains

    .. rubric:: Principle

    The subdomains are defined as a patchwork. The domains given in the SubDomainList are superposed
    from left to right in the order of the list (the domain at the most right is on the top of all the
    others, the one on the most left is at the bottom of all the others).

    Assuming N fluids, the fluid N will be located on the whole patch given at the last position of the SubDomainList
    the fluid N-1 will be located on the whole patch given at the one to last position of the SubDomainList, minus the patch of the fluid N
    the fluid 1 will be located on the patch consisting of the Domain minus all the other patches.

    .. note:: This routine supports non arc-connected domains but not disconnected domains
    """

    # ------------- Converting the user information to the shapely format -------------------------------
    # Getting the outer domain and converting to multipoly
    OuterPoly = np.array(Mesh.Domain[0])
    OuterPoly = MultiPolygon([Polygon([(a[0], a[1]) for a in OuterPoly])])

    # Getting the inner holes, convert them to multipoly and generate the pierced domain
    InnerPoly = unary_union ([Polygon([(a[0], a[1]) for a in InnerDomain]) for InnerDomain in Mesh.Domain[1:]])
    Domain = OuterPoly.difference(InnerPoly)

    # ------------- Construct all the partial subdomains -------------------------------------------------------
    PolysBuff = [Mapper(i, Mesh) for i in SubDomainList]

    # ------------- Deduce all the domains of the respective fluids through the patchwork ----------------------
    # Constructing the patchwork from the last partial patch to the domain itself

    # Check if there are actually several fluids, register the domain as the only subdomain otherwise
    if not SubDomainList:
        Polys = [Domain]

    # If there exists at least one subdomain
    else:
        # Initialising the Buffers
        Union = PolysBuff[-1]                   # Area already covered by any patch
        Polys = [Union.intersection(Domain)]    # Initialising the list of all the subdomains

        if not (Polys[0].geom_type == 'MultiPolygon'): Polys = [MultiPolygon([Polys[0]])]

        # Looping on all the (domain+partial patches) from the second to last one
        for MultiPoly in ([Domain]+PolysBuff[:-1])[::-1]:
            # Defining the current subdomains as a substraction of the already covered ares from the current partial patch
            CurrentPatch = MultiPoly
            CurrentPatch = CurrentPatch.difference(Union).intersection(Domain)

            # Forcing the multipolygon shape here (for later shapely compatibility)
            if CurrentPatch.geom_type == 'MultiPolygon': Polys = Polys + [CurrentPatch]
            else: Polys = Polys + [MultiPolygon([CurrentPatch])]

            # Updated the undissociated covered area
            Union = unary_union(Polys)

    # ----------------- Subdomains's validity checks -------------------------------------------------------
    # Check that all fluids are present
    if Domain.geom_type == 'Polygon': Domain = MultiPolygon([Domain])

    for MultiPoly in Polys:
        if MultiPoly.is_empty: raise(RuntimeError("Error in the fluid's subdomains definition.\n\
                                      At least one given fluid is not present. Abortion."))

    # Return the list of subdomains, ordered as the given subdomains
    return(Domain, Polys[::-1])

#### Parameters interface: Mapping the partial subdomain spltting functions (patches definition) #####
def Mapper(Id, Mesh):
    """
    Mapper routine for the subdomain's partial patches definition that are defined in the below functions.


    .. rubric:: Parameters

    - |bs| **Mesh** *(MeshStructure)* -- the mesh associated to the problem and subdomains
    - |bs| **Id**   *(integer, tuple or list)* -- the index and properties corresponding to the type of simple patches one desires, considering the smaller rectangle containing the outer domain as the reference shape. The subdomain's definition reads then as follows upon the type of the given argument.


        - |bs| integer (predefined simple patch creation)
            0. | Domain split by diagonal bottom left  to upper right, right part
            1. | Domain split by diagonal bottom left  to upper right, left part
            2. |  Domain split by diagonal bottom right to upper left,  right part
            3. | Domain split by diagonal bottom right to upper left,  left part
            4. | Domain split by halfplane vertical line,  right part
            5. | Domain split by halfplane vertical line,  left part
            6. | Domain split by halfplane horizotal line, bottom part
            7. | Domain split by halfplane horizotal line, top part

        - |bs| tuple (index and information specifying the nature and properties of the whished partial patches):
            0.
                | Domain split through a bezier spline, right or bottom part
                | *Id = (0, VertexId1, VertexId2, DragVector1X, DragVector1Y, DragVector2X, DragVector2Y)*
            1.
                | Domain split through a bezier spline, left or top part
                | *Id = (1, VertexId1, VertexId2, DragVector1X, DragVector1Y, DragVector2X, DragVector2Y)*
            2.
                | Domain split through a line given by two vertices, right or bottom part
                | *Id = (2, Vertex1X, Vertex1Y, Vertex2X, Vertex2Y)*
            3.
                | Domain split through a line given by two vertices, left or top part
                | *Id = (3, Vertex1X, Vertex1Y, Vertex2X, Vertex2Y)*
            4.
                | Dropplets in a box
                | *Id = (4, BoxCenterX, BoxCenterY, BoxWidthX, BoxWidthY, BubblesDiameter, BubblesSpacingX, BubblesSpacingY)*

        - |bs| list:  the list of vertices forming the polygonal patch

    Returns:
        Patch (shapely multipolygon): the Patch forming the considered subdomain

    |

    .. note::

        The code aborts upon a wrong shape of Id; it does not check the validity of the input.
        Code failure without clear explanation may raise in case of misuse.
    """

    # --------------------- Simples cuts ------------------------------------------------
    if type(Id) is int:
        if Id == 0:
            # Diagonal Bottom left to upper Right, right part
            Line = lambda a,b,c,d: [(a,b),(c,d)]
            return(CutThroughRelativeLine(0, Line, Mesh))

        elif Id == 1:
            # Diagonal Bottom left to upper Right, left part
            Line = lambda a,b,c,d: [(a,b),(c,d)]
            return(CutThroughRelativeLine(1, Line, Mesh))

        elif Id == 2:
            # Diagonal bottom right to upper Left, right part
            Line = lambda a,b,c,d: [(c,b),(a,d)]
            return(CutThroughRelativeLine(0, Line, Mesh))

        elif Id == 3:
            # Diagonal bottom right to upper Left, left part
            Line = lambda a,b,c,d: [(c,b),(a,d)]
            return(CutThroughRelativeLine(1, Line, Mesh))

        elif Id == 4:
            # HalfPlane Vertical, Right part
            Line = lambda a,b,c,d: [(0.5*(a+c),b),(0.5*(a+c),d)]
            return(CutThroughRelativeLine(0, Line, Mesh))

        elif Id == 5:
            # HalfPlane Vertical, Left part
            Line = lambda a,b,c,d: [(0.5*(a+c),b),(0.5*(a+c),d)]
            return(CutThroughRelativeLine(1, Line, Mesh))

        elif Id == 6:
            # HalfPlane Horizotal, bottom part
            Line = lambda a,b,c,d: [(a,0.5*(b+d)),(c,0.5*(b+d))]
            return(CutThroughRelativeLine(0, Line, Mesh))

        elif Id == 7:
            # HalfPlane Horizotal, top part
            Line = lambda a,b,c,d: [(a,0.5*(b+d)),(c,0.5*(b+d))]
            return(CutThroughRelativeLine(1, Line, Mesh))

    # ----------------------- Involved cuts ----------------------------------------
    elif type(Id) is tuple:
        if Id[0] == 0:
            # Cut through custom spline, right-bottom part
            return(CutThroughBezierLine(0, Id[1:], Mesh))

        elif Id[0] == 1:
            # Cut through custom spline, top-left part
            return(CutThroughBezierLine(1, Id[1:], Mesh))

        elif Id[0] == 2:
            # Cut through a line given by two vertices, right-bottom part
            Line = np.reshape(Id[1:],(2,2))
            return(CutThroughAbsoluteLine(0, Line, Mesh))

        elif Id[0] == 3:
            # Cut through a line given by two vertices, top-left part
            Line = np.reshape(Id[1:],(2,2))
            return(CutThroughAbsoluteLine(1, Line, Mesh))

        elif Id[0] == 4:
            return(DroppletsInBox(Mesh, Id[1:3], Id[3:5], Id[5:8]))

    # ---------------------- Given polygon -------------------------------------
    elif type(Id) is list:
        # Given a polygon by its ordered vertices
        return(Polygon([(a[0], a[1]) for a in Id]))

    # --------------------- Error message --------------------------------------
    else:
        raise ValueError("Error in the definition of the fluid's subdomains:\n\
                          The definition of a patch does not match any of the accepted format.\n\
                          Abortion.")

# ==============================================================================
#	Technical routines actually splitting the given plane
# ==============================================================================
#### Defining a collection of dropplets as a patch ####
def DroppletsInBox(Mesh, BoxCenter, BoxWidth, BubblesProp):
    """

    Creates a patch consisting of bubbles in a box.

    Args:
        Mesh         (MeshStructure):        the mesh associated to the problem and subdomains
        BoxCenter    (float list-alike):     the x and y coordinates of the box center
        BoxWidth     (float list-alike):     the x and y width of the box
        BubblesProp  (float list-alike):     the dropplets diameter, their x spacing and their y spacing

    Returns:
        polygon  (shapely mutlipolygon):    the corresponding partial patch


    .. Note::

        If there the bubbles are too large w.r.t. their spacing, they will
        be merged and form "8"-like shapes.
    """

    # Getting the domain and converting it into a shapely format
    OuterPoly = np.array(Mesh.Domain[0])
    OuterPoly = MultiPolygon([Polygon([(a[0], a[1]) for a in OuterPoly])])

    # Computing the number of bubbles according to the box and the wished spacing
    NbPointsX = int(np.ceil(BoxWidth[0]/BubblesProp[1]))
    NbPointsY = int(np.ceil(BoxWidth[1]/BubblesProp[2]))

    # Generating the droplet centers
    x = BoxCenter[0]-0.5*BoxWidth[0] + np.linspace(0.5*BubblesProp[0],BoxWidth[0], NbPointsX)
    y = BoxCenter[1]-0.5*BoxWidth[1] + np.linspace(0.5*BubblesProp[0],BoxWidth[1], NbPointsX)
    CentersList = np.reshape(np.meshgrid(x,y),(2,len(x)*len(y))).T

    # Generating the shapely mutlipolygon being the union of the droplets
    polygon = unary_union([Point(a[0], a[1]).buffer(0.5*BubblesProp[0]) for a in CentersList]).intersection(OuterPoly)
    if polygon.geom_type == 'Polygon': polygon = MultiPolygon([polygon])

    # Giving it back
    return(polygon)

#### Defining a patch as a part of a plane after being cut through a Bezier line #####
def CutThroughBezierLine(Part, Id, Mesh):
    """

    Creates a patch consisting of the desired part of a plane after being cut through a Bezier line.

    .. rubric:: Parameters

    - **Mesh** *(MeshStructure)*    -- the mesh associated to the problem and subdomains
    - **Part** *(integer)*          -- the part of the plane to conserve (0 for right or bottom, 1 for top or left)
    - **Id**   *(float list-alike)* -- list giving the parameters of the bezier curve as follows

        -
           | Id[0:2]: vertex Id of the outer polygon on which will be stitched the bezier line
           | |bs| |bs| |bs| |bs| |bs| |bs| (that can be converted to integers, corresponding to 4 (number of boxing corners) + IndexOfOuterPolygonVertex ).
           | |bs| |bs| |bs| |bs| |bs| |bs| It can also be the boundig box corners (WishedCornerId),
           | |bs| |bs| |bs| |bs| |bs| |bs| there,  WishedCornerId has to be given according to the order: [BottomLeft, BottomRight, TopRight, TopLeft])

        -
           | Id[2:4]: Vector giving the direction and strenght for which to drag the curve from the 1st given vertex
           | |bs| |bs| |bs| |bs| |bs| |bs| (will be pondered by the X-Y lengths of the polygon's bounding box)

        -
            | Id[4:6]: Vector giving the direction and strenght for which to drag the curve from the 2nd given vertex
            | |bs| |bs| |bs| |bs| |bs| |bs| (will be pondered by the X-Y lengths of the polygon's bounding box)

    Returns:
        polygon (shapely mutlipolygon): the corresponding partial patch

    .. note::

        The resolution of the spline is set to be 20times the mesh size
    """

    # ----------------- Initialisation of the areas to cut ---------------------
    # Getting the domain and converting it into a shapely format
    OuterPoly = np.array(Mesh.Domain[0])
    polygon   = Polygon([(p[0], p[1]) for p in OuterPoly])

    # Retrieving the vertex index giving the line's origin and the dragging info
    Idp = list(map(int, Id[:2]))
    Id  = np.array(Id[2:])

    # Constructiong the bounding box
    a = min(OuterPoly[:,0])
    b = min(OuterPoly[:,1])
    c = max(OuterPoly[:,0])
    d = max(OuterPoly[:,1])

    # Enriching the outerpolygon by the bounding box
    P = np.array([[a,b],[c,b],[c,d],[a,d]]+OuterPoly.tolist())

    # Construction of the bezier line end points
    P1 = P[Idp[0]]          # First extremity
    P4 = P[Idp[1]]          # Second extremity

    # Construction of the bezier line dragging points
    L = [abs(c-a), abs(d-b)]
    P2 = P4 + Id[2:4]*np.array(L)
    P3 = P1 + Id[0:2]*np.array(L)

    # Forcing the numpy format of the bezier points
    P1, P2, P3, P4 = P1.reshape(2,1), P2.reshape(2,1), P3.reshape(2,1), P4.reshape(2,1)

    # ---------------- Create 4-points Bezier curve ----------------------------
    # Case lines are generated over y
    if np.isclose(P1[0], P4[0]):
        f = lambda t: (1-t)**3*P1+t**3*P4+3*(1-t)**2*t*P2+3*(1-t)*t**2*P3
        t = np.linspace(0,1,int(np.ceil(20/np.min(Mesh.InnerElementsDiameter))))
        Points = (f(t)+np.array([a,b]).reshape((2,1))).T

    # Case lines are generated over x
    else:
        f = lambda t: (1-t)**3*P1+(t)**3*P4+3*(1-t)**2*t*P2+3*(1-t)*t**2*P3
        t = np.linspace(0,1,int(np.ceil(20/np.min(Mesh.InnerElementsDiameter))))
        Points = (f(t)+np.array([a,b]).reshape((2,1))).T

    # ------------- Cut the outerpolygon by the bezier line --------------------
    # Define the cutting line
    LinePoints = [(p[0], p[1]) for p in Points]
    line       = LineString(Points)

    # Splitting the polygon
    merged  = linemerge([polygon.boundary, line])
    borders = unary_union(merged)

    # Extracting the sub polygons vertices, and omitting the ones not initially included in the original polygon
    # (typically when the cutting line creates a spurious area around an inward corner)
    polygons = np.array([poly for poly in list(polygonize(borders)) if poly.representative_point().within(polygon)])
    polygons_innerpoint = [poly.representative_point().coords[0]   for poly in polygons]

    # Dispatching them depending on their location from the cutting line
    DirectOrientation = np.array([LinearRing([polygons_innerpoint[i]] + LinePoints).is_ccw for i in range(len(polygons))])
    LeftPolys  = polygons[np.where(DirectOrientation)[0]]
    RightPolys = polygons[np.where(~DirectOrientation)[0]]

    # Mapping the part of the plane to send back.
    # False -> Right/Bottom part, True -> Left/Top part
    if Part == 1:
        poly = unary_union(LeftPolys.tolist()).intersection(polygon)
        if poly.geom_type == 'Polygon': poly = MultiPolygon([poly])
    elif Part == 0:
        poly = unary_union(RightPolys.tolist()).intersection(polygon)
        if poly.geom_type == 'Polygon': poly = MultiPolygon([poly])
    else:
        raise ValueError("Error in the creation of the patch through a Bezier line cut.\n\
                          Check the given parameters. Abortion.")

    # Returning the whished patch
    return(poly)

#### Simple recuperation of a part of the plane cut by a custom line ####
def CutThroughAbsoluteLine(Part, Line, Mesh):
    """
    Creates a patch consisting of the desired part of a plane after being cut through an absolute line.

    Args:
        Mesh (MeshStructure):     the mesh associated to the problem and subdomains
        Part (integer):           the part of the plane to conserve (0 for right or bottom, 1 for top or left)
        Line (float numpy array): list of the x-y coordinates of points defining the line (nbpoints x 2) the
                                  extremities of the line should be outside or on the boundary of the outer polygon

    Returns:
        polygon (shapely mutlipolygon): the corresponding partial patch

    """

    # Getting the domain and converting it into a shapely format
    OuterPoly = np.array(Mesh.Domain[0])
    polygon   = Polygon([(a[0], a[1]) for a in OuterPoly])

    # Define the cutting line
    LinePoints = [(a[0], a[1]) for a in Line]
    line       = LineString(LinePoints)

    # Splitting the polygon
    merged  = linemerge([polygon.boundary, line])
    borders = unary_union(merged)

    # Extracting the sub polygons vertices, and omitting the ones not initially included in the original polygon
    # (typically when the cutting line creates a spurious area around an inward corner)
    polygons = np.array([poly for poly in list(polygonize(borders)) if poly.representative_point().within(polygon)])
    polygons_innerpoint = [poly.representative_point().coords[0]    for poly in polygons]

    # Dispatching them depending on their situation from the cutting line
    DirectOrientation = np.array([LinearRing([polygons_innerpoint[i]] + LinePoints).is_ccw for i in range(len(polygons))])
    LeftPolys  = polygons[np.where(DirectOrientation)[0]]
    RightPolys = polygons[np.where(~DirectOrientation)[0]]

    # Mapping the part of the plane to send back.
    # False -> Right/Bottom part, True -> Left/Top part
    if Part == 1:
        poly = unary_union(LeftPolys.tolist()).intersection(polygon)
        if poly.geom_type == 'Polygon': poly = MultiPolygon([poly])
    elif Part == 0:
        poly = unary_union(RightPolys.tolist()).intersection(polygon)
        if poly.geom_type == 'Polygon': poly = MultiPolygon([poly])
    else:
        raise ValueError("Error in the creation of the patch through an absolute line cut.\n\
                          Check the given parameters. Abortion.")

    # Giving back the desired patch
    return(poly)

#### Simple recuperation of a part of the plane cut by a relative line ####
def CutThroughRelativeLine(Part, Line, Mesh):
    """
    Creates a patch consisting of the desired part of a plane after being cut through an absolute line.

    Args:
        Mesh (MeshStructure):     the mesh associated to the problem and subdomains
        Part (integer):           the part of the plane to conserve (0 for right or bottom, 1 for top or left)
        Line (lambda function):   given the bounding box points, creates the list of shapely points to consider

    Returns:
        polygon (shapely mutlipolygon): the corresponding partial patch

    .. note::

        - If there the bubbles are too large w.r.t. their spacing, they will be merged and form "8"-like shapes.
        - The resolution of the spline is set to be 20times the mesh size

    """

    # Getting the domain and converting it into a shapely format
    OuterPoly = np.array(Mesh.Domain[0])
    polygon   = Polygon([(a[0], a[1]) for a in OuterPoly])

    # Define the cutting line
    LinePoints = Line(min(OuterPoly[:,0]), min(OuterPoly[:,1]), max(OuterPoly[:,0]), max(OuterPoly[:,1]))
    line       = LineString(LinePoints)

    # Splitting the polygon
    merged  = linemerge([polygon.boundary, line])
    borders = unary_union(merged)

    # Extracting the sub polygons vertices, and omitting the ones not initially included in the original polygon
    # (typically when the cutting line creates a spurious area around an inward corner)
    polygons = np.array([poly for poly in list(polygonize(borders)) if poly.representative_point().within(polygon)])
    polygons_innerpoint = [poly.representative_point().coords[0]    for poly in polygons]
    
    # Dispatching them depending on their situation from the cutting line
    DirectOrientation = np.array([LinearRing([polygons_innerpoint[i]] + LinePoints).is_ccw for i in range(len(polygons))])
    LeftPolys  = polygons[np.where(DirectOrientation)[0]]
    RightPolys = polygons[np.where(~DirectOrientation)[0]]

    # Mapping the part of the plane to send back.
    # False -> Right/Bottom part, True -> Left/Top part
    if Part == 1:
        poly = unary_union(LeftPolys.tolist()).intersection(polygon)
        if poly.geom_type == 'Polygon': poly = MultiPolygon([poly])
    elif Part == 0:
        poly = unary_union(RightPolys.tolist()).intersection(polygon)
        if poly.geom_type == 'Polygon': poly = MultiPolygon([poly])
    else:
        raise ValueError("Error in the creation of the patch through an relative line cut.\n\
                          Check the given parameters. Abortion.")

    # Return the considered patch
    return(poly)
