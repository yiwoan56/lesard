#***************************************************************************
# Copyright (C) 2020 Élise Le Mélédo
# This file is part of LESARD.
#
# LESARD is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LESARD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LESARD.  If not, see <https://www.gnu.org/licenses/>.
#****************************************************************************

"""
All the necessary routines that implement
the 2D Euler Equations (given in conservative variables) are defined in
:code:`GoverningEquations/EulerEquations.py`
"""

# ==============================================================================
#	Preliminary imports
# ==============================================================================
# Import necessary python libraries
import numpy as np

# Import custom libraries
import LocalLib.ReversedFunctools as fcts

# ==============================================================================
#	Parameter that should be accessible from outside the class
# ==============================================================================
FluidModel   = "SimpleFluid"
IntegratedLS = False

# ==============================================================================
#	Equation class giving the flux, jacobian and all tools required by schemes
# ==============================================================================
class Equations():
	""" Class furnishing all the methods that are required to define numerical
	schemes and evolve the solution according to the Euler Equation

	|

	.. rubric:: Fields

	.. |bs|   unicode:: U+00A0 .. NO-BREAK SPACE

	It contains  the following initialisation parameters (later available as well as attributes)

		- |bs| **FluidProp**   *(list of FluidModel)*  --  the list of fluid property instances (NbFluids)
		- |bs| **EOS**         *(list of callbacks)*   --  the list of Equation of state functions (NbFluids)

	It further contains the following attributes, filled upon initialisation

		- |bs| **NbVariables**          *(integer)*          --   number of variables of the model
		- |bs| **VariablesNames**       *(list of strings)*  --   ordered name of the variables
		- |bs| **VariablesUnits**       *(list of strings)*  --   symbols of the units of the variables
		- |bs| **VariablesLatexNames**  *(list of strings)*  --   ordered name of the variables, latex encoding
		- |bs| **VariablesLatexUnits**  *(list of strings)*  --   symbols of the units of the variables, latex encoding
		- |bs| **XYLatexUnits**         *(list of strings)*  --   units of the x-y coordinates in latex encoding
    """

	#### Automatic initialisation routine ####
	def __init__(self, FluidProp, EOs):
		"""
		Args:
			FluidProp   (list of FluidModel):   the list of fluid property instances (NbFluids)
			EOs         (list of EOS):          the list of Equation of state instances (NbFluids)

		|

		.. rubric:: Methods
		"""

		# Defines the (conservative) variables number and names, in a latex typography
		self.NbVariables    = 4
		self.VariablesNames = ["rho", "rhoUx", "rhoUy", "E"]
		self.VariablesUnits = ["kg/m3", "m2/s", "m2/s", "J"]

		self.VariablesLatexNames = [r'$\rho$', r'$\rho U_x$', r'$\rho U_y$', r'$E$']
		self.VariablesLatexUnits = [r'$kg/m^3$', r'$kg/(m^2s)$', r'$kg/(m^2s)$', r'$J$']
		self.XYLatexUnits = [r'$m$', r'$m$']

		self.PrimaryVariablesLatexUnits = [r'$kg/m^3$', r'$m/s$', r'$m/s$', r'$Pa$']
		self.PrimaryVariablesLatexNames = [r'$\rho$', r'$U_x$', r'$U_y$', r'$p$']
		self.PrimaryVariablesNames      = ["rho", "Ux", "Uy", "p"]

		# Register the shortcut of the fluid's properties
		self.FluidProp = FluidProp
		self.EOs       = EOs

	#### Retrieve the primary variables from the conservative ones ####
	def ConservativeToPrimary(self, Var, FluidIndex, *args):
		"""Converts the conservative variables to the primary ones

		Args:
			Var         (2D numpy array):         the variables of the problem (NbVars x NbGivenPoints)
			FluidIndex  (integer numpy array):    the fluid present at each given point (NbGivenPoints)

		Returns:
			Var (numpy array): (NbVars x NbGivenPoints) the corresponding primary variables

		.. note::

			- | *args is there only for compatibility reason at call time
		"""

		# -------- Initialisations -----------------------------------------------------------------------
		# Getting the number of furnished points
		NbPoints = np.shape(Var)[1]
		PrimVars = np.zeros(np.shape(Var))

		# --------- Locating and extracting the right fluid's properties for each location ---------------
		# Initialising the pressure and gamma variables
		p = np.zeros(len(Var[0,:]))
		# List of fluids present in the list to be computed
		Fluids = map(int, list(set(FluidIndex)))

		# Construct the fluidproperties depending o the right fluid selector's value and evaluating the
		# EOS accordingly
		for Fluid in Fluids:
			Index = np.where(Fluid==np.array(FluidIndex))[0]
			p[Index]   = self.EOs[Fluid].EoS(Var[:,Index], np.array([self.FluidProp[Fluid]]*len(Index)), "p")

		# ------- Constructing the primary variables ----------------------------------------------------
		# Computing the primary variables
		PrimVars[0,:] = Var[0,:]
		PrimVars[1,:] = Var[1,:]/Var[0,:]
		PrimVars[2,:] = Var[2,:]/Var[0,:]
		PrimVars[3,:] = p[:]

		# Returning the Result
		return(PrimVars)

	#### Retrieve the conservative variables from the primary ones ####
	def PrimaryToConservative(self, PrimVar, FluidIndex, *args):
		"""Converts the primary variables to the conservative ones

		Args:
			Var         (2D numpy array):         the primary variables of the problem (NbVars x NbGivenPoints)
			FluidIndex  (integer numpy array):    the fluid present at each given point (NbGivenPoints)

		Returns:
			Var (numpy array): (NbVars x NbGivenPoints) the corresponding conservative variables

		.. note::

			- | *args is there only for compatibility reason at call time
		"""

		# -------- Initialisations -----------------------------------------------------------------------
		# Getting the number of furnished points
		NbPoints = np.shape(PrimVar)[1]
		Vars = np.zeros(np.shape(PrimVar))

		# Semiconverting the variables to pass to the EOS
		PrimVar[1,:] = PrimVar[0,:]*PrimVar[1,:]
		PrimVar[2,:] = PrimVar[0,:]*PrimVar[2,:]

		# --------- Locating and extracting the right fluid's properties for each location ---------------
		# Initialising the pressure and gamma variables
		e = np.zeros(len(PrimVar[0,:]))
		# List of fluids present in the list to be computed
		Fluids = list(set(FluidIndex))

		# Construct the fluidproperties depending o the right fluid selector's value and evaluating the
		# EOS accordingly
		for Fluid in Fluids:
			Index = np.where(Fluid==np.array(FluidIndex))[0]
			e[Index]   = self.EOs[Fluid].EoS(PrimVar[:,Index], np.array([self.FluidProp[Fluid]]*len(Index)), "e")

		# ------- Constructing the conservative variables ----------------------------------------------------
		# Computing the primary variables
		Vars[0,:] = PrimVar[0,:]
		Vars[1,:] = PrimVar[1,:]	# Directly from the previous semiconversion
		Vars[2,:] = PrimVar[2,:]	# Directly from the previous semiconversion
		Vars[3,:] = PrimVar[0,:]*e[:]+0.5*PrimVar[0,:]*(PrimVar[1,:]**2+PrimVar[2,:]**2)

		# Returning the Result
		return(Vars)

	#### Flux function of the equations set ####
	def Flux(self, Var, *args):
		"""Flux function of the  (conservative) EulerEquations

		.. |bs|   unicode:: U+00A0 .. NO-BREAK SPACE

		Args:
			Var  (float numpy array):   the value of the variables (NbVariables x NbGivenPoints)

		.. rubric:: Optional argument

		- |bs| **x**           *(float numpy array)*,   --  (optional, not impacting here) x-y coordinates of the given points (NbGivenPoints x 2)
		- |bs| **FluidIndex**  *(integer numpy array)*  --  the fluid present at each given point (NbGivenPoints)

		Returns:
			None: fills direclty the Parameters data structure

		|

		.. note::

			- | This function is Vectorised
			- | Fluid index is the fluid index in the initially given list, not the fluid type
			- | FluidProp is the list of all the fluid properties for each fluid (given the list, not the type)
			- | *args is there only for compatibility reason at call time
		"""

		# --------- Locating and extracting the right fluid's properties for each location ---------------
		# Gettings the arguments
		FluidIndex = args[1]

		# Initialising the pressure variable
		p = np.zeros(len(Var[0,:]))
		# List of fluids present in the list to be computed
		Fluids = list(set(FluidIndex))

		# Construct the fluidproperties depending o the right fluid selector's value and evaluating the
		# EOS accordingly
		for Fluid in Fluids:
			Index = np.where(Fluid==np.array(FluidIndex))[0]
			p[Index] = self.EOs[Fluid].EoS(Var[:,Index], np.array([self.FluidProp[Fluid]]*len(Index)), "p")

		# --------- Computing the Euler flux directly in conservative variables  ------------------------
		Fx = np.array([Var[1,:], 						# RhoU
					   Var[1,:]**2/Var[0,:]+p,			# RhoU**2+p
					   Var[1,:]*Var[2,:]/Var[0,:],  	# RhoUV
					   Var[1,:]/Var[0,:]*(Var[3,:]+p)])	# U*(E+p)

		Fy = np.array([Var[2,:], 						# RhoV
					   Var[1,:]*Var[2,:]/Var[0,:],  	# RhoUV
					   Var[2,:]**2/Var[0,:]+p,			# RhoV**2+p
					   Var[2,:]/Var[0,:]*(Var[3,:]+p)]) # V*(E+p)

		# Returning the flux
		Flux = np.array([Fx, Fy])

		return(Flux)

	#### Function that extracts the propagation speed from the variables to interface for the LevelSet ####
	def GetUV(self, Var, *args):
		""" Function giving back the x-y velocity at the considered point, given the variational
		values given for the the conservative Euler equation's variables.

		.. |bs|   unicode:: U+00A0 .. NO-BREAK SPACE

		Args:
			Var  (float numpy array):   the value of the variables (NbVariables x NbGivenPoints)
			x    (float numpy array):   (generally optional, required for this problem), x-y coordinates of the given points (NbGivenPoints x 2)

		.. rubric:: Optional argument

		- |bs| **FluidIndex**  *(integer numpy array)*  --  the fluid present at each given point (NbGivenPoints)

		Returns:
			UV   (float numpy array) -- the velocities values at the points (2 x NbGivenPoints)

		.. note::

			- | This function is Vectorised
			- | *args is there only for compatibility reason at call time
		"""

		# Computing the velocity values from the conservative Euleur equations
		UV = Var[1:3, :]/Var[0,:]

		# Returning the value
		return(UV)

	#### Jacobian of the equations set ####
	def Jacobian(self, Var, x, FluidIndex, *args):
		"""Computes the Jacobian of the flux for the  Euler equations

		.. |bs|   unicode:: U+00A0 .. NO-BREAK SPACE

		Args:
			Var        (float numpy array):   the value of the variables (NbVariables x NbGivenPoints)
			x          (float numpy array):   (generally optional, required for this problem), x-y coordinates of the given points (NbGivenPoints x 2)
			FluidIndex (integer numpy array): the fluid present at each given point (NbGivenPoints)

		Returns:
			J    (3D numpy array):   J[:,:,i] gives the jacobian of the flux taking care of the dynamic of the ith spatial coordinate.

		|

		.. note::

			- | For each flux fi = (fi1,..., fin), the returned Jacobian reads:

			  | |bs| J[:,:] = [dfi1/dx1, ....., dfi1/dxn
			  | |bs| |bs| |bs|	|bs| |bs| |bs|	 ....
			  | |bs| |bs| |bs|	|bs| |bs| |bs|	df in/dx1, ....., dfin/dxn]
			- | *args is there only for compatibility reason at call time

		"""

		# ------ Initialising the Jacobian structure ---------------------------
		# Getting the number of furnished points
		NbPoints = np.shape(Var)[1]
		J = np.zeros((4, 4, 2, NbPoints))

        # --------- Locating and extracting the right fluid's properties for each location ---------------
		# Initialising the pressure and gamma variables
		p   = np.zeros(len(Var[0,:]))
		gmm = np.zeros(len(Var[0,:]))
		# List of fluids present in the list to be computed
		Fluids = map(int, list(set(FluidIndex)))

		# Construct the fluidproperties depending o the right fluid selector's value and evaluating the
		# EOS accordingly
		for Fluid in Fluids:
			Index = np.where(Fluid==np.array(FluidIndex))[0]
			p[Index]   = self.EOs[Fluid].EoS(Var[:,Index], np.array([self.FluidProp[Fluid]]*len(Index)), "p")
			gmm[Index] = [self.FluidProp[Fluid].gamma]*len(Index)

        # --------- Computing the Euler Jacobian directly from conservative variables  ------------------------
		# Getting back the values of speed from the conservative variables
		u = Var[1,:]/Var[0,:]		# get the x-speed
		v = Var[2,:]/Var[0,:]		# get the y-speed

		# Computing the relevant quantitites that are required to compute the Jacobian
		uv2 = u**2 + v**2
		H   = (Var[3,:] + p)/Var[0,:]

		# Computing the Jacobian
		J[0,:,0,:] = np.array([np.zeros(NbPoints),         np.ones(NbPoints),    np.zeros(NbPoints),   np.zeros(NbPoints)])
		J[1,:,0,:] = np.array([-u**2 + 0.5*(gmm-1.)*uv2,   u*(3.-gmm),           v*(1.-gmm),           gmm-1.            ])
		J[2,:,0,:] = np.array([-u*v,                       v,                    u,                    np.zeros(NbPoints)])
		J[3,:,0,:] = np.array([u*(0.5*(gmm-1.)*uv2 - H ),  H -(gmm-1.)*u**2,     u*v*(1.-gmm),            gmm*u          ])

		J[0,:,1,:] = np.array([np.zeros(NbPoints),        np.zeros(NbPoints),    np.ones(NbPoints),    np.zeros(NbPoints)])
		J[1,:,1,:] = np.array([-u*v,                      v,                     u,                    np.zeros(NbPoints)])
		J[2,:,1,:] = np.array([-v**2 + 0.5*(gmm-1.)*uv2,  u*(1.-gmm),            v*(3.-gmm),           gmm-1.            ])
		J[3,:,1,:] = np.array([v*(0.5*(gmm-1.)*uv2 - H),  u*v*(1.-gmm),          H-(gmm-1.)*v**2,      gmm*v             ])

		# Returning the Jacobian
		return(J)

	#### Eigen values ####
	def EigenValues(self, Var, FluidIndex, n, x, *args):
		"""Computes the eigenvalues associated to the flux.

		Args:
			Var         (2D numpy array):         the variables of the problem (NbVars x NbGivenPoints)
			FluidIndex  (integer numpy array):    the fluid present at each given point (NbGivenPoints)
			n           (2D numpy array):         the x-y values of the normal at each given point  (NbGivenPoints x 2)
			x           (2D numpy array):         (generally optional, required for this problem) the x-y locations at which the variables are given  (NbGivenPoints x 2)

		Returns:
			lbd (numpy array): (NbGivenPoints x 4) the four eigenvalues at each given point

		.. note::

			- | *args is there only for compatibility reason at call time
		"""

		# ---------- Ugly always working way ------------------------------

		# --- Locating and extracting the right fluid's properties for each location
		# Initialising the pressure and gamma variables
		p   = np.zeros(len(Var[0,:]))
		c   = np.zeros(len(Var[0,:]))

		# List of fluids present in the list to be computed
		Fluids = map(int,list(set(FluidIndex)))

		# Getting the fluids properties depending on the fluid selector's value and evaluate the
		# EOS accordingly
		for Fluid in Fluids:
			# Extracting the points occupied by the selected fluid
			Index    = np.where(Fluid==np.array(FluidIndex))[0]

			# Getting the EoS dependent quantitites
			p[Index] = self.EOs[Fluid].EoS(Var[:,Index], np.array([self.FluidProp[Fluid]]*len(Index)), "p")

			# Getting the sound speed and velocity against normals
			c[Index] = np.sqrt(self.FluidProp[Fluid].gamma*np.abs(p[Index])/Var[0,Index])

		# --- Computing the spectral radius taking the sound speed into consideration
		# Getting the velocity against normals
		un    = np.sum(Var[1:3,:].T*n[:], axis=1)/Var[0,:]
		mod_n = np.sqrt(np.sum(n**2, axis=1))

		# Getting the actual eigenvalues
		lbd = np.zeros((len(Var[0,:]), 4))
		lbd[:,0] = un - c*mod_n
		lbd[:,1] = un
		lbd[:,2] = un + c*mod_n
		lbd[:,3] = un

		return(lbd)

	#### Right eigen vectors ####
	def RightEigenVectors(self, Var, FluidIndex, n, x, *args):
		"""Computes the right eigenvectors associated to the eigenvalues.

		Args:
			Var         (2D numpy array):         the variables of the problem (NbVars x NbGivenPoints)
			FluidIndex  (integer numpy array):    the fluid present at each given point (NbGivenPoints)
			n           (2D numpy array):         the x-y values of the normal at each given point  (NbGivenPoints x 2)
			x           (2D numpy array):         (generally optional, required for this problem) the x-y locations at which the variables are given  (NbGivenPoints x 2)

		Returns:
			reg (numpy array): (NbVars x MbVars x NbGivenPoints) the matrix of eigenvectors for each given point

		.. note::

			- | *args is there only for compatibility reason at call time
		"""

		# ---------------- Initialisations -------------------------------------
		p       = np.zeros(len(Var[0,:]))
		c       = np.zeros(len(Var[0,:]))
		dEoSRho = np.zeros(len(Var[0,:]))
		dEoSE   = np.zeros(len(Var[0,:]))

		# ---------------- Precomputations -------------------------------------
		# --- Variable-dependent quantities
		# Normalising the normals
		nn  = n/(np.sqrt(np.sum(n**2, axis=1)).T)
		# Retrieving the velocities
		uv  = Var[1:3]/Var[0]
		# Retrieving the normal component of the velocity
		un  = np.sum(uv*n[:].T, axis=0)
		# Retrieving the kinetic energy
		ke  = 0.5*np.sum(uv**2, axis=0)

		# --- EOS-dependent quantities
		# List of fluids present in the list to be computed
		Fluids = map(int, list(set(FluidIndex)))

		# Getting the fluids properties depending on the fluid selector's value and evaluate the EOS accordingly
		for Fluid in Fluids:
			# Extracting the points occupied by the selected fluid
			Index = np.where(Fluid==np.array(FluidIndex))[0]

			# Getting the EoS dependent quantitites
			p[Index]        = self.EOs[Fluid].EoS    (Var[:,Index], np.array([self.FluidProp[Fluid]]*len(Index)), "p")
			dEoSRho[Index]  = self.EOs[Fluid].dEoSRho(Var[:,Index], np.array([self.FluidProp[Fluid]]*len(Index)))
			dEoSE[Index]    = self.EOs[Fluid].dEoSE  (Var[:,Index], np.array([self.FluidProp[Fluid]]*len(Index)))

			# Getting the sound speed and velocity against normals
			c[Index] = np.sqrt(self.FluidProp[Fluid].gamma*np.abs(p[Index])/Var[0,Index])

		# Computing the total Enthalpy
		H = (Var[3,:]+p)/Var[0,:]

		# -------------------- Computing the eigenvectors ----------------------
		# Computing the tangents at each point
		nt = np.zeros(nn.T.shape)
		nt[0,:] =  nn[:,1]
		nt[1,:] = -nn[:,0]

		# Computing the velocities against the tangent at each point and the quantities involved in the
		uvnp = np.sum(uv*nt, axis=0)
		xi   = 2*ke - dEoSRho/dEoSE

		# Creating right eigenvalues
		REV = np.zeros((Var.shape[0], Var.shape[0], Var.shape[1]))
		#print(c*nn.T)
		REV[0,   0, :] = 1
		REV[1:3, 0, :] = uv - c*nn.T
		REV[3,   0, :] = H  - c*un

		REV[0,   1, :] = 1
		REV[1:3, 1, :] = uv
		REV[3,   1, :] = xi

		REV[0,   2, :] = 1
		REV[1:3, 2, :] = uv + c*nn.T
		REV[3,   2, :] = H  + c*un

		REV[0,   3, :] = 0
		REV[1:3, 3, :] = nt
		REV[3,   3, :] = uvnp

		# Returning the values
		return(REV)

	#### Right eigen vectors ####
	def LeftEigenVectors(self, Var, FluidIndex, n, x, *args):
		"""Computes the left eigenvectors associated to the eigenvalues.

		Args:
			Var         (2D numpy array):         the variables of the problem (NbVars x NbGivenPoints)
			FluidIndex  (integer numpy array):    the fluid present at each given point (NbGivenPoints)
			n           (2D numpy array):         the x-y values of the normal at each given point  (NbGivenPoints x 2)
			x           (2D numpy array):         (generally optional, required for this problem) the x-y locations at which the variables are given  (NbGivenPoints x 2)

		Returns:
			reg (numpy array): (NbVars x MbVars x NbGivenPoints) the matrix of eigenvectors for each given point

		.. note::

			- | *args is there only for compatibility reason at call time
		"""

		# ---------------- Initialisations -------------------------------------
		p       = np.zeros(len(Var[0,:]))
		c       = np.zeros(len(Var[0,:]))
		dEoSRho = np.zeros(len(Var[0,:]))
		dEoSE   = np.zeros(len(Var[0,:]))

		# ---------------- Precomputations -------------------------------------
		# --- Variable-dependent quantities
		# Normalising the normals
		nn  = n/(np.sqrt(np.sum(n**2, axis=1)).T)
		# Retrieving the velocities
		uv  = Var[1:3]/Var[0]
		# Retrieving the normal component of the velocity
		un  = np.sum(uv*n.T, axis=0)
		# Retrieving the kinetic energy
		ke  = 0.5*np.sum(uv**2, axis=0)

		# --- EOS-dependent quantities
		# List of fluids present in the list to be computed
		Fluids = list(set(FluidIndex))

		# Getting the fluids properties depending on the fluid selector's value and evaluate the EOS accordingly
		for Fluid in Fluids:
			# Extracting the points occupied by the selected fluid
			Index = np.where(Fluid==np.array(FluidIndex))[0]

			# Getting the EoS dependent quantitites
			p[Index]        = self.EOs[Fluid].EoS    (Var[:,Index], np.array([self.FluidProp[Fluid]]*len(Index)), "p")
			dEoSRho[Index]  = self.EOs[Fluid].dEoSRho(Var[:,Index], np.array([self.FluidProp[Fluid]]*len(Index)))
			dEoSE[Index]    = self.EOs[Fluid].dEoSE  (Var[:,Index], np.array([self.FluidProp[Fluid]]*len(Index)))

			# Getting the sound speed and velocity against normals
			c[Index] = np.sqrt(self.FluidProp[Fluid].gamma*np.abs(p[Index])/Var[0,Index])

		# Computing the total Enthalpy
		H = (Var[3,:]+p)/Var[0,:]

		# -------------------- Computing the eigenvectors ----------------------
		# Computing the tangents at each point
		nt = np.zeros(nn.T.shape)
		nt[0,:] =  nn[:,1]
		nt[1,:] = -nn[:,0]

		# Computing the velocities against the tangent at each point and the quantities involved in the
		uvnp = np.sum(uv*nt, axis=0)
		xi   = 2*ke - dEoSRho/dEoSE

		# Creating left eigenvalues
		LEV = np.zeros((Var.shape[0], Var.shape[0], Var.shape[1]))

		LEV[0,   0, :] =  c*un   + dEoSRho
		LEV[1:3, 0, :] = -c*nn.T - dEoSE*uv
		LEV[3,   0, :] = dEoSE

		LEV[0,   1, :] = -2*(dEoSRho - c**2)
		LEV[1:3, 1, :] =  2*dEoSE*uv
		LEV[3,   1, :] = -2*dEoSE

		LEV[0,   2, :] = -c*un   + dEoSRho
		LEV[1:3, 2, :] =  c*nn.T - dEoSE*uv
		LEV[3,   2, :] =  dEoSE

		LEV[0,   3, :] = -2*uvnp*c**2
		LEV[1:3, 3, :] =  2*nt*c**2
		LEV[3,   3, :] =  0

		LEV = LEV/(2*(c**2))

		# Returning the values
		return(LEV)

	#### Spectral radius of the equations set ####
	def SpectralRadius(self, Var, FluidIndex, n, x, *args):
		"""Computes the spectral radius associated to the flux.

		Args:
			Var         (2D numpy array):         the variables of the problem (NbVars x NbGivenPoints)
			FluidIndex  (integer numpy array):    the fluid present at each given point (NbGivenPoints)
			n           (2D numpy array):         the x-y values of the normal at each given point  (NbGivenPoints x 2)
			x           (2D numpy array):         (generally optional, required for this problem) the x-y locations at which the variables are given  (NbGivenPoints x 2)

		Returns:
			Lmb (numpy array): the spectral radius computed at each given point

		.. note::

			- | *args is there only for compatibility reason at call time
		"""

		# ---------- Cheap way that may fail in case of zero speed ------------------------------
		"""
		# Retrieving the Jacobian
		J = self.Jacobian(Var, x, FluidIndex)

		# Computing the spectral radius at each given point, shortcuted since here we are scalar
		Lmb = np.max(np.abs([np.sum(J[i,i,:,:]*n.T, axis = 0) for i in range(4)]), axis=0)
		"""

		# ---------- Ugly always working way ------------------------------

		# --- Locating and extracting the right fluid's properties for each location
		# Initialising the pressure and gamma variables
		p   = np.zeros(len(Var[0,:]))
		gmm = np.zeros(len(Var[0,:]))

		# List of fluids present in the list to be computed
		Fluids = map(int, list(set(FluidIndex)))

		# Getting the fluids properties depending on the fluid selector's value and evaluate the
		# EOS accordingly
		for Fluid in Fluids:
			Index = np.where(Fluid==np.array(FluidIndex))[0]
			p[Index]   = self.EOs[Fluid].EoS(Var[:,Index], np.array([self.FluidProp[Fluid]]*len(Index)), "p")
			gmm[Index] = [self.FluidProp[Fluid].gamma]*len(Index)

			# --- Computing the spectral radius taking the sound speed into consideration
		# Getting the sound speed
		c   = np.sqrt(gmm*np.abs(p)/Var[0,:])
		vi  = np.sqrt(np.sum(Var[1:3,:]**2, axis=0)/(Var[0,:]**2))

		Lmb = np.sqrt(np.sum(n[:,:]**2, axis=1))*(vi+c)

		# Returning the spectral radius at each given point
		return(Lmb)

	#### Roe matrix associated to the Euler problem ####
	def RoeMatrix(self, Var, n, FluidIndex, *args):
		"""A Roe matrix corresponding to the Euler Equations, accross one given edge

		Args:
			n           (float numpy array):    the outward normal of the considered element (nx, ny)
			Var         (float numpy array):    the value of the variables (NbVariables x 2). Var[:,1] average value of the considered element, Var[:,2] average value of the neigborhing element.
			FluidIndex  (integer numpy array):  the fluid present at each given point (NbGivenPoints)
			x           (float numpy array):    (optional, not impacting here) x-y coordinates of the given points (NbGivenPoints x 2)

		Returns:
			RoeMatrix (float numpy array):  a Roe matrix of the system

		|

		.. note::
			- This function is NOT vectorised
			- Fluid index is the fluid index in the initially given list, not the fluid type
			- FluidProp is the list of all the fluid properties for each fluid (given the list, not the type)
		"""

		# --------- Locating and extracting the right fluid's properties for each location ---------------
		# Initialising the pressure variable
		p = np.zeros(len(Var[0,:]))
		# List of fluids present in the list to be computed
		Fluids = list(set(FluidIndex))

		# Construct the fluidproperties depending o the right fluid selector's value and evaluating the
		# EOS accordingly
		for Fluid in Fluids:
			Index = np.where(Fluid==np.array(FluidIndex))[0]
			p[Index] = self.EOs[Fluid].EoS(Var[:,Index], np.array([self.FluidProp[Fluid]]*len(Index)), "p")

		# ------- Selecting which fluid to use in the values of the matrix itself ---------------------------
		# Considering the possibly different gamma values in both sides of the edge
		Gamma = np.array([self.FluidProp[Id].gamma for Id in FluidIndex])

		#  Selecing the upwinding gamma: here is not doing that, #TODO
		UpwGmm = Gamma[0]

		# Computing the averaged and normalised quantities involved in the Roe Matrix
		e = (Var[3,:]-0.5*(Var[2,:]+Var[3,:])/Var[0,:])/Var[0,:]
		h = (e+p)/Var[0,:]

		U = np.sum(Var[1,:]/np.sqrt(Var[0,:]))/(np.sum(np.sqrt(Var[0,:])))
		V = np.sum(Var[2,:]/np.sqrt(Var[0,:]))/(np.sum(np.sqrt(Var[0,:])))
		H = np.sum(h/np.sqrt(Var[0,:]))/(np.sum(np.sqrt(Var[0,:])))

		W = np.array([U,V])
		c = np.sqrt((UpwGmm-1)*(H-0.5*(U**2+V**2)))

		# Computing the matrix entering in the RoeMatrix product
		R = np.array([[1, 0, 1, 1],
			 [U-c*n[0], -n[1], U, U+c*n[0]  ],
			 [V-c*n[1], n[0], V, V+c*n[1] ],
			 [H-np.dot(W, n)*c, -U*n[1]+V*n[0], 0.5*(U+V)/(UpwGmm-1), H+np.dot(W,n)+c ]])
		S = np.diag([np.dot(W, n)-c, np.dot(W, n),np.dot(W, n), np.dot(W, n)+c ])

		# Computing the Roe Matrix (in an ugly way)
		RoeMatrix = np.dot(R, np.dot(np.abs(S), np.linalg.inv(R)))

		# Returning the RoeMatrix
		return(RoeMatrix)


# ==============================================================================
#   Class containing all the methods for defining various Equation of states
# ==============================================================================
#### Class containing the methods giving back the pressure ####
class EOS():
	"""Class furnishing the methods giving the Equation of State that can be
	used in combination with the :code:`SimpleFluid` definition.
	Upon creation, fills the field :code:`EOS` with the function callback
	corresponding to the wished initialisation function and the field :code:`EOSName` with
	the associated label. The implemented EoS are linked as follows.
	See their respective documentation for more information.

		1. | Stiff fluid
		2. | Ideal

	"""

	#### Automatic initialisation routine (mapper) ####
	def __init__(self, Id):
		"""
		Args:
			Id (integer):   the index corresponding to the equation of fluid the user wants when considering the governing equations given in this module and the associated fluid.

		|

		.. rubric:: Methods
		"""

		# Mapping towards the right function
		if   Id==1:
			self.EoS     = self.Stiff
			self.dEoSRho = self.dEoSRhoStiff
			self.dEoSE   = self.dEoSEStiff
			self.EoSName = "Stiff"

		elif Id==2:
			self.EoS     = self.Ideal
			self.dEoSRho = self.dEoSRhoIdeal
			self.dEoSE   = self.dEoSEIdeal
			self.EoSName = "Ideal"

		else: raise NotImplementedError("Error in the selection of the Equation of State\n\
		                                 Unknown index. Check your Problem definition. Abortion.")

	#### EOS for stiff fluids #####
	def Stiff(self, Var, FluidProp, Type):
		"""Equation of State for stiff fluids, giving back either the internal energy or pressure.

		Args:
			Type       (string):             desired output: "e": internal energy, "p": pressure
			Var        (float numpy array):  the value of the variables (NbVariables x NbGivenPoints). If type "e", the variables should be [rho, rhoU, rhoV, p].T. If type "p", the variables should be [rho, rhoU, rhoV, E].
			FluidProp  (FluidModel list):    the list of the fluid properties associated to each given Point

		Returns:
			p          (float numpy array):  the pressure values at the given points (NbGivenPoints)
		"""

		# Computing the pressure
		if Type == "p":
			FluidSpecs = np.array([[Fluid.pinf, Fluid.gamma] for Fluid in FluidProp])
			e   = (Var[3,:]-0.5*(Var[1,:]**2+Var[2,:]**2)/Var[0,:])/Var[0,:]
			val = Var[0,:]*e*(FluidSpecs[:,1]-1)-np.prod(FluidSpecs, axis=1)

		# Computing the internal energy
		if Type == "e":
			FluidSpecs = np.array([[Fluid.pinf, Fluid.gamma] for Fluid in FluidProp])
			val = (Var[3,:]+np.prod(FluidSpecs, axis=1))/((FluidSpecs[:,1]-1)*Var[0,:])

		# Returning the asked value
		return(val)

	#### EOS derivative with respect to rho variable ####
	def dEoSRhoStiff(self, Var, FluidProp):
		"""Derivative of the Equation of State for stiff fluids along the density variable at the given points.

		Args:
			Type       (string):             desired output: "e": internal energy, "p": pressure
			Var        (float numpy array):  the value of the conservative variables (NbVariables x NbGivenPoints).
			FluidProp  (FluidModel list):    the list of the fluid properties associated to each given Point

		Returns:
			deos       (float numpy array):  derivative of the Equation of State for stiff fluids along the density variable (NbGivenPoints)
		"""

		# Extracting the fluid properties
		FluidSpecs = np.array([[Fluid.pinf, Fluid.gamma] for Fluid in FluidProp])

		# Computing the derivative
		deos = -0.5*(FluidSpecs[:,1].T-1)*np.sum(Var[1:3,:]**2, axis=0)/(Var[0,:]**2)	# check minus

		# Returning the value
		return(deos)

	#### EOS derivative with respect to rho variable ####
	def dEoSEStiff(self, Var, FluidProp):
		"""Derivative of the Equation of State for stiff fluids along the Energy variable at the given points.

		Args:
			Type       (string):             desired output: "e": internal energy, "p": pressure
			Var        (float numpy array):  the value of the conservative variables (NbVariables x NbGivenPoints).
			FluidProp  (FluidModel list):    the list of the fluid properties associated to each given Point

		Returns:
			deos       (float numpy array):  derivative of the Equation of State for stiff fluids along the Energy variable (NbGivenPoints)
		"""

		# Extracting the fluid properties
		FluidSpecs = np.array([[Fluid.pinf, Fluid.gamma] for Fluid in FluidProp])

		# Computing the derivative
		deos = FluidSpecs[:,1].T-1

		# Returning the value
		return(deos)

	#### EOS for ideal fluids ####
	def Ideal(self, Var, FluidProp, Type):
		"""Equation of State for ideal fluids, giving back either the internal energy or pressure.

		Args:
			Type       (string):             desired output: "e": internal energy, "p": pressure
			Var        (float numpy array):  the value of the variables (NbVariables x NbGivenPoints). If type "e", the variables should be [rho, rhoU, rhoV, p].T. If type "p", the variables should be [rho, rhoU, rhoV, E].
			FluidProp  (FluidModel list):    the list of the fluid properties associated to each given Point

		Returns:
			p          (float numpy array):  the pressure values at the given points (NbGivenPoints)
		"""

		# Computing the pressure
		if Type == "p":
			FluidSpecs = np.array([[Fluid.pinf, Fluid.gamma] for Fluid in FluidProp])
			e   = (Var[3,:]-0.5*(Var[1,:]**2+Var[2,:]**2)/Var[0,:])/Var[0,:]
			val = Var[0,:]*e*(FluidSpecs[:,1]-1)

		# Computing the internal energy
		if Type == "e":
			FluidSpecs = np.array([[Fluid.pinf, Fluid.gamma] for Fluid in FluidProp])
			val = Var[3,:]/((FluidSpecs[:,1]-1)*Var[0,:])

		# Returning the asked value
		return(val)

	#### EOS derivative with respect to rho variable ####
	def dEoSRhoIdeal(self, Var, FluidProp):
		"""Derivative of the Equation of State for ideal fluids along the density variable at the given points.

		Args:
			Type       (string):             desired output: "e": internal energy, "p": pressure
			Var        (float numpy array):  the value of the conservative variables (NbVariables x NbGivenPoints).
			FluidProp  (FluidModel list):    the list of the fluid properties associated to each given Point

		Returns:
			deos       (float numpy array):  derivative of the Equation of State for stiff fluids along the density variable (NbGivenPoints)
		"""

		# Extracting the fluid properties
		FluidSpecs = np.array([[Fluid.pinf, Fluid.gamma] for Fluid in FluidProp])

		# Computing the derivative
		deos = -0.5*(FluidSpecs[:,1].T-1)*np.sum(Var[1:3,:]**2, axis=0)/(Var[0,:]**2)	# check minus

		# Returning the value
		return(deos)

	#### EOS derivative with respect to rho variable ####
	def dEoSEIdeal(self, Var, FluidProp):
		"""Derivative of the Equation of State for ideal fluids along the Energy variable at the given points.

		Args:
			Type       (string):             desired output: "e": internal energy, "p": pressure
			Var        (float numpy array):  the value of the conservative variables (NbVariables x NbGivenPoints).
			FluidProp  (FluidModel list):    the list of the fluid properties associated to each given Point

		Returns:
			deos       (float numpy array):  derivative of the Equation of State for stiff fluids along the Energy variable (NbGivenPoints)
		"""

		# Extracting the fluid properties
		FluidSpecs = np.array([[Fluid.pinf, Fluid.gamma] for Fluid in FluidProp])

		# Computing the derivative
		deos = FluidSpecs[:,1].T-1

		# Returning the value
		return(deos)


# ==============================================================================
#	Class containing all the predefined suitable initial conditions
# ==============================================================================
class InitialConditions():
	"""Class furnishing the solution initialisation routines that are suitable
	to study the the Euler Equations, defined for working on a subdomain.
	Access the routine computing the initial conditions through the field :code:`IC`, filled upon creation
	with the function callback corresponding to the index of the wished initialisation function.
	The implemented initialisation method are linked as follows.
	See their respective documentation for more information.

		0. | ConstantState
		1. | ConstantState2
		2. | StationaryVortex
		3. | SOD
		4. | SOD_Fluid1
		5. | SOD_Fluid2
		6. | Karni_Fluid1(bublle)
		7. | Karni_Fluid2(complement to the bubble)
	"""

	#### Automatic initialisation routine (mapper) ####
	def __init__(self, Id, *params):
		"""Args:
			Id     (integer):            the index corresponding to the initialisation method the user wants when considering the governing equations given in this module.
			params (list of arguments):  the (fixed arguments to pass to the selected function)

		|

		.. rubric:: Methods"""

		# Registering locally the parameters to give to the functions
		self.Params = params

		# Mapping the initialisation routines
		if   Id == 0: self.IC = self.ConstantState
		if   Id == 1: self.IC = self.ConstantState2
		elif Id == 2: self.IC = self.StationaryVortex
		elif Id == 3: self.IC = self.SOD
		elif Id == 4: self.IC = self.SOD_Fluid1
		elif Id == 5: self.IC = self.SOD_Fluid2
		elif Id == 6: self.IC = self.Bubble
		elif Id == 7: self.IC = self.PlanarShock

		else: raise NotImplementedError("Error in the selection of the Initial conditions\
		                                 Unknown index. Check your Problem definition. Abortion.")

	#### SOD test case ####
	def SOD_Fluid1(self, PointsID, Mesh, EoS, FluidProp, *args):
		"""Initialising the solution to a constant state that matches the SOD-inner value.
		(The given points should all belonging to a same subdomain).

		Args:
			PointsID   (integer array-like):     the index of the points to consider
			Mesh       (MeshStructure):          the considered mesh
			EOS        (function callback):      (optional), the equation of state given by the model of the fluid that is present at the given points
			FluidProp  (FluidSpecs):             (optional, not used here) the the properties of the fluid present where the given points are

		Returns:
			Init       (float numpy array):      The initialised values at the considered points (NbVariables x NbGivenPoints)

		|

		.. note::

			- *args is here only for compatibility on call
			- There is usually one initialisation routine per subdomain, and the PointsID are not necessarily contigous, be careful when assigning the values back in the regular solution.
			- For running smothly the inner circle diameter of the given subdomain should be at least 2

		"""

		# Initialising the point-values
		Init = np.zeros((4, len(PointsID)))

		# Computing the values that will be taken with the SOD bump
		Init[:,:] = np.array([[1., 0., 0., 1]]*len(PointsID)).T
		e = EoS.EoS(Init[:,:], [FluidProp]*len(PointsID), "e")
		Init[3,:] = Init[0,:]*e

		# Returning the values
		return(Init)

	#### SOD test case ####
	def SOD_Fluid2(self, PointsID, Mesh, EoS, FluidProp, *args):
		"""Initialising the solution to a constant state that matches the SOD-outer value.
		(The given points should all belonging to a same subdomain).

		Args:
			PointsID   (integer array-like):     the index of the points to consider
			Mesh       (MeshStructure):          the considered mesh
			EOS        (function callback):      the equation of state given by the model of the fluid that is present at the given points
			FluidProp  (FluidSpecs):             the the properties of the fluid present where the given points are

		Returns:
			Init       (float numpy array):      The initialised values at the considered points (NbVariables x NbGivenPoints)

		|

		.. note::

			- *args is here only for compatibility on call
			- There is usually one initialisation routine per subdomain, and the PointsID are not necessarily contigous, be careful when assigning the values back in the regular solution.
			- For running smothly the inner circle diameter of the given subdomain should be at least 2
		"""

		# Initialising the point-values
		Init = np.zeros((4, len(PointsID)))

		# Computing the values that will be taken outside the SOD bump
		Init[:,:] = np.array([[0.125, 0., 0., 0.1]]*len(PointsID)).T
		e = EoS.EoS(Init[:,:], [FluidProp]*len(PointsID), "e")
		Init[3,:] = Init[0,:]*e

		# Returning the values
		return(Init)

	#### Initialise the solution to a constant state ####
	def ConstantState(self, PointsID, Mesh, EoS, FluidProp, *args):
		"""Initialising the solution to a constant state on the given points, all belonging to a same subdomain.

		Args:
			PointsID   (integer array-like):     the index of the points to consider
			Mesh       (MeshStructure):          the considered mesh
			EOS        (function callback):      the equation of state given by the model of the fluid that is present at the given points
			FluidProp  (FluidSpecs):             the the properties of the fluid present where the given points are

		Returns:
			Init       (float numpy array):      The initialised values at the considered points (NbVariables x NbGivenPoints)

		|

		.. note::

			- *args is here only for compatibility on call
			- There is usually one initialisation routine per subdomain, and the PointsID are not necessarily contigous, be careful when assigning the values back in the regular solution.

		"""

		# Initialising the point-values
		Init = np.ones((4, len(PointsID)))

		# Retrieving the xy values of the points
		xy = Mesh.DofsXY[PointsID,:]

		# Defining the raw initilisation variable
		Init[1:3,:] = 0
		Init[0,:]   = 0.5
		Init[3,:]   = 0.125

		# Getting the value of E through the EOS
		e = EoS.EoS(Init, [FluidProp]*len(PointsID), "e")
		Init[3,:] = Init[0]*e

		# Returning the computed values
		return(Init)

	#### Initialise the solution to a constant state ####
	def ConstantState2(self, PointsID, Mesh, EoS, FluidProp, *args):
		"""Initialising the solution to a constant state on the given points, all belonging to a same subdomain.

		Args:
			PointsID   (integer array-like):     the index of the points to consider
			Mesh       (MeshStructure):          the considered mesh
			EOS        (function callback):      (optional), the equation of state given by the model of the fluid that is present at the given points
			FluidProp  (FluidSpecs):             (optional, not used here) the the properties of the fluid present where the given points are

		Returns:
			Init       (float numpy array):      The initialised values at the considered points (NbVariables x NbGivenPoints)

		|

		.. note::

			- *args is here only for compatibility on call
			- There is usually one initialisation routine per subdomain, and the PointsID are not necessarily contigous, be careful when assigning the values back in the regular solution.

		"""

		# Initialising the point-values
		Init = np.ones((4, len(PointsID)))

		# Defining the raw initilisation variable
		Init[:,:] = np.array([[0.125, 0., 0., 0.1]]*len(PointsID)).T
		e = EoS.EoS(Init[:,:], [FluidProp]*len(PointsID), "e")
		Init[3,:] = Init[0,:]*e

		# Returning the computed values
		return(Init)

	#### Initialise the solution to a StationaryVortex according to the given subdomain ####
	def StationaryVortex(self, PointsID, Mesh, EoS, FluidProp, subdomain, *args):
		""" Initialising the solution to a StationaryVortex centred at the center of mass
		of the subdmain. (The given points should all belonging to a same subdomain).

		Args:
			PointsID   (integer array-like):     the index of the points to consider
			Mesh       (MeshStructure):          the considered mesh
			EOS        (function callback):      the equation of state given by the model of the fluid that is present at the given points
			FluidProp  (FluidSpecs):             the the properties of the fluid present where the given points are
			subdomain  (shapely multipolyon):    (optional, not used here) the shapely polygon to which the given points belong

		Returns:
			Init       (float numpy array):      The initialised values at the considered points (NbVariables x NbGivenPoints)

		|

		.. note::

			- *args is here only for compatibility on call
			- There is usually one initialisation routine per subdomain, and the PointsID are not necessarily contigous, be careful when assigning the values back in the regular solution.
			- For running smothly the inner circle diameter of the given subdomain should be at least 2
		"""

		# Initialising the point-values
		Init = np.ones((4, len(PointsID)))

		# Retrieving the xy values of the points and the geometrical informations from the subdomain
		xy = Mesh.DofsXY[PointsID,:]
		xc = subdomain.centroid.x
		yc = subdomain.centroid.y

		# Defining the predefined constants (copied from THE fortran code)
		uinf = 0
		vinf = 0
		Tinf = 1
		beta = 5
		Ma   = 0.5*beta/np.pi

		# Defining the space-dependent quantities
		r2 = ((xy[:,0]-xc)**2+(xy[:,1]-yc)**2)
		co = np.exp(0.5*(1-r2))

		# Retrieving the quantities that will furnish a stationary vortex
		du = Ma*co*(-xy[:,1])
		dv = Ma*co*( xy[:,0])
		u  = uinf + du
		v  = vinf + dv

		# Linking them with the fluid's properties
		Dt  = -(FluidProp.gamma-1)*0.5*Ma*Ma*co*co/FluidProp.gamma
		rho = (Tinf + Dt)**(1/(FluidProp.gamma-1))

		# Defining the raw initilisation variable
		Init[0,:] = rho
		Init[1,:] = rho*u
		Init[2,:] = rho*v
		Init[3,:] = rho**FluidProp.gamma   # p

		# Getting the value of E through the EOS
		e = EoS.EoS(Init, [FluidProp]*len(PointsID), "e")
		Init[3,:] = Init[0,:]*e + 0.5*Init[0,:]*(u**2+v**2)

		# Returning the computed values
		return(Init)

	#### SOD test case ####
	def SOD(self, PointsID, Mesh, EoS, FluidProp, subdomain):
		""" Initialising the solution to a SOD centred at the center of mass, of width 0.5
		of the subdmain. (The given points should all belonging to a same subdomain).

		Args:
			PointsID   (integer array-like):     the index of the points to consider
			Mesh       (MeshStructure):          the considered mesh
			EOS        (function callback):      the equation of state given by the model of the fluid that is present at the given points
			FluidProp  (FluidSpecs):             the the properties of the fluid present where the given points are
			subdomain  (shapely multipolyon):    (optional, not used here) the shapely polygon to which the given points belong

		Returns:
			Init       (float numpy array):      The initialised values at the considered points (NbVariables x NbGivenPoints)

		|

		.. note::

			- There is usually one initialisation routine per subdomain, and the PointsID are not necessarily contigous, be careful when assigning the values back in the regular solution.
			- For running smothly the inner circle diameter of the given subdomain should be at least 2
		"""

		# Initialising the point-values
		Init = np.zeros((4, len(PointsID)))
		# Retrieving the xy values of the points and the geometrical informations from the subdomain
		xy = Mesh.DofsXY[PointsID,:]
		xc = subdomain.centroid.x
		yc = subdomain.centroid.y

		# Computing the radius where the SOD-ready bump will be placed
		r = np.sqrt(((xy[:,0]-xc)**2+(xy[:,1]-yc)**2))

		# Computing the values that will be taken with the SOD bump
		Ind1 = np.where(r<=0.5)[0]
		Init[:,Ind1] = np.array([[1., 0., 0., 1]]*len(Ind1)).T
		e = EoS.EoS(Init[:,Ind1], [FluidProp]*len(Ind1), "e")
		Init[3,Ind1] = Init[0,Ind1]*e

		# Computing the values that will be taken outside the SOD bump
		Ind2 = np.setdiff1d(range(len(PointsID)), Ind1)
		Init[:,Ind2] = np.array([[0.125, 0., 0., 0.1]]*len(Ind2)).T
		e = EoS.EoS(Init[:,Ind2], [FluidProp]*len(Ind2), "e")
		Init[3,Ind2] = Init[0,Ind2]*e

		# Returning the values
		return(Init)

	#### Karni fluid 1 ####
	def Bubble(self, PointsID, Mesh, EoS, FluidProp, subdomain, *args):
		"""Initialising the solution to a constant state corresponding to the bubble in the Karni test, where the
		density is considered as Rconsidered/Rair fluid.

		Args:
			PointsID   (integer array-like):     the index of the points to consider
			Mesh       (MeshStructure):          the considered mesh
			EOS        (function callback):      the equation of state given by the model of the fluid that is present at the given points
			FluidProp  (FluidSpecs):             the properties of the fluid present where the given points are
			subdomain  (shapely multipolyon):    (optional, not used here) the shapely polygon to which the given points belong

		Returns:
			Init       (float numpy array):      The initialised values at the considered points (NbVariables x NbGivenPoints)

		|

		.. note::

			- *args is here only for compatibility on call
			- There is usually one initialisation routine per subdomain, and the PointsID are not necessarily contigous, be careful when assigning the values back in the regular solution.
		"""

		# Initialising the point-values
		Init = np.zeros((4, len(PointsID)))

		# Computing the density value to use
		Rair = 0.287
		rho = Rair/FluidProp.R

		# Computing the values that will be taken outside the SOD bump
		Init[:,:] = np.array([[rho, 0., 0., 1]]*len(PointsID)).T
		e = EoS.EoS(Init[:,:], [FluidProp]*len(PointsID), "e")
		Init[3,:] = Init[0,:]*e

		# Returning the values
		return(Init)

	#### Karni fluid 2 ####
	def PlanarShock(self, PointsID, Mesh, EoS, FluidProp, subdomain, MachNum=1.19, *args):
		"""Initialising the solution to two constante state distributed on either side of a shock for the Planar shock test.
		The shock is set at 25% of the total x width of the domain, starting from the right. The (left) rested part is set to
		be such that (p, u, v, p) = [1., 0., 0., 1]. The shock is set to move from right to left.

		Args:
			PointsID   (integer array-like):            the index of the points to consider
			Mesh       (MeshStructure):                 the considered mesh
			EOS        (function callback):             the equation of state given by the model of the fluid that is present at the given points
			FluidProp  (FluidSpecs):                    the properties of the fluid present where the given points are
			subdomain  (shapely multipolyon):           the shapely polygon to which the given points belong
			MachNum    (float):                         the desired Mach number (given on the still fluid)

		Returns:
			Init       (float numpy array):      The initialised values at the considered points (NbVariables x NbGivenPoints)

		|

		.. warning::

			The computation of the state assumes air as fluid component

		|

		.. note::

			- *args is here only for compatibility on call
			- There is usually one initialisation routine per subdomain, and the PointsID are not necessarily contigous, be careful when assigning the values back in the regular solution.
		"""

		# ------------------ Plan the routine interface ------------------------------------------------
		# Extract the parameters if called from the instance
		if not MachNum: MachNum = self.Params[0]


		# ------------------ Retrieving the spatial location of the shock ------------------------------
		# Retrieving the xy values of the points and the geometrical informations from the subdomain
		xy = Mesh.DofsXY[PointsID,:]
		coords = np.array([np.array(sub.exterior.coords)  for sub in subdomain])
		maxx  = np.max(coords[0])
		minx  = np.min(coords[0])
		shocklocation = maxx-0.25*abs(maxx-minx)


		# ----------- Computing the quantities to assign to the point values ---------------------------
		# Initialising the point-values
		Init = np.zeros((4, len(PointsID)))

		# Initialising the still part of the fluid
		StillState = [1., 0., 0., 1]

		# Computing the associated reckless part of the fluid according to the Mach Number, the fluid
		# properties and the direction of the shock's propagation.
		gmm = FluidProp.gamma
		Ms  = MachNum
		direct = -1

		# Computing according to the RH relations for a planar normal shock given on the conservation laws
		if EoS.EoSName == "Ideal":
			h  = (gmm-1)/(gmm+1)
			c0 = np.sqrt(gmm*StillState[3]/StillState[0])

			RecklessState = [0.]*4
			RecklessState[0] = StillState[0]*(Ms**2)/(1-h+h*Ms**2)
			RecklessState[1] = RecklessState[0]*(StillState[1]+direct*(1-h)*c0*(Ms-1/Ms))
			RecklessState[2] = RecklessState[0]*(StillState[2])
			RecklessState[3] = StillState[3]*((1+h)*Ms**2-h)

		else:
			raise(NotImplementedError("Error: the asked initial conditions (planar shock) are not yet implemented for \
				the used Equation of state. Please check your problem definition."))


		# ------------------- Assigning the right values to the right points ------------------------
		# Computing the values that will be taken on the resting part of the fluid
		Ind1 = np.where(xy[:,0]<=shocklocation)[0]
		Init[:,Ind1] = np.array([StillState]*len(Ind1)).T
		e = EoS.EoS(Init[:,Ind1], [FluidProp]*len(Ind1), "e")
		Init[3,Ind1] = Init[0,Ind1]*e

		# Computing the values that will be taken after the moving shock of the fluid and filling the right values
		Ind2 = np.setdiff1d(range(len(PointsID)), Ind1)
		Init[:,Ind2] = np.array([RecklessState]*len(Ind2)).T
		e = EoS.EoS(Init[:,Ind2], [FluidProp]*len(Ind2), "e")
		Init[3,Ind2] = Init[0,Ind2]*e+0.5*Init[1,Ind2]**2/Init[0,Ind2]+0.5*Init[2,Ind2]**2/Init[0,Ind2]

		# Returning the values
		return(Init)
