#***************************************************************************
# Copyright (C) 2020 Élise Le Mélédo
# This file is part of LESARD.
#
# LESARD is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LESARD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LESARD.  If not, see <https://www.gnu.org/licenses/>.
#****************************************************************************

"""
|

The file :code:`SourceCode/BoundaryConditions.py` contains a single routine that allows the user to apply
the set of boundary conditions on the residuals according to the user input (it should prealabily have
been recast in a list of boundary routines in the SolverInitialisations file).

|

"""

# ==============================================================================
#                  Import the different modules
# ==============================================================================
import numpy as np


# ==============================================================================
#                  Actual routine
# ==============================================================================

#### Applying each boundary condition wrt the boundary tags and the fluid ####
def ApplyBCSState(Problem, Mesh, Solution, Residuals):
	"""
	Applying each boundary condition wrt the boundary tags and the fluid
	(the Problem structure should be filled with the mapped boundary routines 
	for each boundary tag and fluid's type)

	Args:
		Problem          (Problem):                  The considered problem
		Mesh             (MeshStructure):            The considered mesh
		Solution         (Solution):                 The solution buffer containing the solution at the previous (and incremented in DeC) intermediary timestep (relates to the time scheme)
		Residuals        (2D numpy array):           The currently computed residuals (NbVars x NbDofs)

	Returns:
		Residuals        (2D numpy array):           The updated residuals according to the given boundary conditions (NbVars x NbDofs)
	"""
	

	# ----------------- Selecting the right boundary routine for each boundary tag --------------------------
	# Selecting the boundary tags that are considered by the Dofs
	Boundaries = set(Mesh.BoundaryDofsTag)
	
	# Spanning each of the boundary segment on which a treatment is needed (Outflow is seen as a void treatment here)
	for Bd in Boundaries:

		# Extracting the Dofs lying on this boundary
		Ids = np.where(Bd==np.array(Mesh.BoundaryDofsTag))[0]
		
		# Ensuring that there exist degrees of freedom to treat and that we are not in an untagged case (untagged=outflow)
		if Ids.size>0 and Bd !=0: 

			# Extracting the relevant dofs to take care about
			BdId = np.array(Mesh.BoundaryDofs)[Ids]
	
			# Quering the fluids that are present on those dofs and applying the boundary condition in consequence
			for Fluid in set(Solution.FluidFlag[BdId]):
				ind = BdId[np.where(Fluid==Solution.FluidFlag[BdId])[0]]
				Residuals[:, ind] = Problem.BoundaryConditions[Bd-1][Fluid](Problem, Mesh, Solution, Residuals, Fluid, Bd, ind)
		
	# Returning the freshly updated residuals
	return(Residuals)
	
