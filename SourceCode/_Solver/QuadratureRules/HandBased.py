#***************************************************************************
# Copyright (C) 2020 Élise Le Mélédo
# This file is part of LESARD.
#
# LESARD is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LESARD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LESARD.  If not, see <https://www.gnu.org/licenses/>.
#****************************************************************************

"""
The modules procures quadrature points and weights
for 1D and 2D quadrature on simplicial shape that are exact up to order 5.

|

"""

# ==============================================================================
#	Preliminary imports
# ==============================================================================
# import python libraries
import numpy as np


# ==============================================================================
#	Definition of quadrature points and weights
# ==============================================================================

#### Defining the inner quadrature points and weigths to use in every element ####
class InnerQuadrature():
	""" Class defining a hand-based quadrature with hard coded points and weights,
        so far applicable to triangles, and is exact up to order 5.
	"""

	# ------------------------------------------------------------------- #
	#			Initialisation and interfacing routine                    #
	# ------------------------------------------------------------------- #

	#### Initialisation ####
	def __init__(self, Mesh, *Params):
		"""
			Args:
				Mesh   (MeshStructure):  the considered mesh
				Params (optional):       here only for compatibility on call issues, no parameter is required
		"""

		# Initialise the dictionnary that will contain the routines of quadrature associated to each element type contained in the mesh
		self.QuadInnScheme = dict()

		# Spanning each element type to fill the dictionnary with the relevant routine
		for ele in set(Mesh.MeshType):
			if    ele==3: self.QuadInnScheme[3] = self.SimplicialQuadrature
			else: raise(ValueError("Error in the wished inner quadrature (handbased). No hand-based method has been defined for an element with" + str(ele) + "edges. Abortion."))

	#### Definition of inner quadrature points and weights (routine that will be called externally to compute the quadrature)###
	def PointsWeightsData(self, n):
		""" .. |bs|   unicode:: U+00A0 .. NO-BREAK SPACE

		Hardly coded definition of inner quadrature points and weights, given in barycentric coordinnates.
		Exact up to order 5.

		Args:
			n (integer): the number of edges of the considered 2d element

		.. rubric:: Returns

		|bs| - |bs| **points**  *(2D numpy float array)*   -- the 12 quadrature points in barycentric coordinates (12 x 3)

		|bs| - |bs| **weights** *(numpy float array)*      -- the associated quadrature weights
 		"""

		# ------------------- Computations ------------------------------------------------------
		# Extracting the points and weights from quadpy
		points, weights  = self.QuadInnScheme[n]()

		# Giving back the values
		return(points, weights)

	# ------------------------------------------------------------------- #
	#			Technical routines                                        #
	# ------------------------------------------------------------------- #

	#### Definition of inner quadrature points and weights for triangles ###
	def SimplicialQuadrature(self, *args):
		""" .. |bs|   unicode:: U+00A0 .. NO-BREAK SPACE

		Hardly coded definition of inner quadrature points and weights, given in barycentric coordinnates.
		Exact up to order 5.

		Args:
			None

		.. rubric:: Returns

		|bs| - |bs| **points**  *(2D numpy float array)*   -- the 12 quadrature points in barycentric coordinates (12 x 3)

		|bs| - |bs| **weights** *(numpy float array)*      -- the associated quadrature weights
		"""

		# ------------------- Computations ------------------------------------------------------
		# Initialising the export quantities
		weights = np.zeros(12)
		points  = np.zeros((12,3))

		# Defining the quadrature points and weights
		weights[0:3]  = 0.116786275726379
		weights[3:6]  = 0.050844906370207
		weights[6:12] = 0.082851075618374

		xo=0.501426509658179
		zo=0.5*(1.0-xo)
		points[0,:] = np.array([xo, zo, zo])
		points[1,:] = np.array([zo, xo, zo])
		points[2,:] = np.array([zo, zo, xo])

		xo=0.873821971016996
		zo=0.5*(1.0-xo)
		points[3,:] = np.array([xo, zo, zo])
		points[4,:] = np.array([zo, xo, zo])
		points[5,:] = np.array([zo, zo, xo])

		w  = 0.082851075618374
		xo = 0.053145049844816
		yo = 0.310352451033785
		zo = 1.0-xo-yo
		points[6,:] = np.array([xo, yo, zo])
		points[7,:] = np.array([xo, zo, yo])
		points[8,:] = np.array([yo, xo, zo])
		points[9,:] = np.array([yo, zo, xo])
		points[10,:] = np.array([zo, xo, yo])
		points[11,:] = np.array([zo, yo, xo])

		# Just to be sure, up to overriding
		points[:,2] = 1.- np.sum(points[:,0:2], axis = 1)
		weights    /= np.sum(weights)

		# Giving back the values
		return(points, weights)

#### Definition of boundary quadrature points and weights ###
class BoundaryQuadrature():
	""" Class defining a hand-based quadrature with hard coded points and weights,
        so far applicable to edges, and is exact up to order 5.
	"""

	# ------------------------------------------------------------------- #
	#			Initialisation and interfacing routine                    #
	# ------------------------------------------------------------------- #

	#### Initialisation ####
	def __init__(self, Mesh, *args):
		"""
			Args:
				Mesh  (MeshStructure):  the considered mesh
				args  (optional):       here only for compatibility on call issues, no parameter is required
		"""

		# Spanning each element type to fill the dictionnary with the relevant routine
		self.QuadBdScheme = self.LineQuadrature

	#### Definition of inner quadrature points and weights (routine that will be called externally to compute the quadrature)###
	def PointsWeightsData(self, n, f):
		""" .. |bs|   unicode:: U+00A0 .. NO-BREAK SPACE

		Function interfacing the quadrature with the technical routines of the class upon the user selection.

		Args:
			n  (integer): number of faces of the considered element
			f  (integer): considered triangle face

		.. rubric:: Returns

		|bs| - |bs| **points**  *(2D numpy float array)*   -- the 12 quadrature points in barycentric coordinates (12 x 3)

		|bs| - |bs| **weights** *(numpy float array)*      -- the associated quadrature weights
		"""

		# ------------------- Computations ------------------------------------------------------
		# Extracting the points and weights from quadpy
		points, weights  = self.QuadBdScheme(n,f)

		# Giving back the values
		return(points, weights)

	# ------------------------------------------------------------------- #
	#			Technical routines                                        #
	# ------------------------------------------------------------------- #

	def LineQuadrature(self, n, f, *args):
		""".. |bs|   unicode:: U+00A0 .. NO-BREAK SPACE

		Hardly coded definition of boundary quadrature points and weights, given in barycentric coordinnates.
		Exact up to order 5.

		Args:
			n  (integer): the number of edges of the considered element
			f  (integer): considered triangle face

		.. rubric:: Returns

		|bs| - |bs| **points**  *(2D numpy float array)*   -- the 3 quadrature points in barycentric coordinates (3 (NbPoints) x 3)

		|bs| - |bs| **weights** *(numpy float array)*      -- the associated quadrature weights
		"""

		# Initialising the export quantities
		weights = np.array([5., 8., 5.])/18.
		points  = np.zeros((3,n))

		# Selecting the considered face and prescribing the non-null coefficients
		# on the target vertices
		sb = [f, int(np.mod(f+1,n))]

		# Defining the quadrature points and weights
		s=np.sqrt(0.6)
		points[0,sb] = np.array([0.5*(1.0 - s), 0.5*(1.0 + s)])
		points[1,sb] = np.array([0.5, 0.5])
		points[2,sb] = np.array([0.5*(1.0 + s), 0.5*(1.0 - s)])

		# Returning the desired quantitites
		return(points, weights)
