#***************************************************************************
# Copyright (C) 2020 Élise Le Mélédo
# This file is part of LESARD.
#
# LESARD is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LESARD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LESARD.  If not, see <https://www.gnu.org/licenses/>.
#****************************************************************************

"""
The module procures quadrature points and weights
for 1D and 2D quadrature simplicial and quadrangular shapes according to the methods implemented in quadpy
and mapped by the indices given in each of the following Inner and Boundary classes.

|

"""

# ==============================================================================
#	Preliminary imports
# ==============================================================================
# import external python libraries
import numpy  as np
import quadpy as qp


# ==============================================================================
#	Definition of quadrature points and weights
# ==============================================================================

#### Class defining the inner quadrature rule to use one each element type ####
class InnerQuadrature():
	""" Class interfacing the user input to the quadpy routines defining the points and weights for the inner quadrature rules.

	.. note ::

		- In case of a mixed mesh, the type of quadrature will be applied to any element shape. If the selected quadrature is not available for at
		  least one element type of the mesh, the initialisation will abort.Thus, for generic meshes (esp. non convex polygonal), prefer the handbased
		  quadrature module.

		- When adding a new scheme for a specific element, do not use a number already in use for an other element (to keep the behaviour wished at the above point).

	|

	The schemes that are interfaced with quadpy are indexed as follows:

		0.  Quadpy selected scheme -- The best scheme according to quadpy. Takes for argment: the wished degree of exactness, integer
		1.  Albrecht Collatz       -- Taking no argument, scheme of degree 3
		2.  Dcutri                 -- Taking no argument, scheme of degree 13
		3.  Berntsen Espelid       -- Taking for argument: the wished BE-scheme, integer from 1 to 4.  All schemes are of degree 13.
		4.  Centroid               -- Taking no argument, scheme of degree 1
		5.  Vertex                 -- Taking no argument, scheme of degree 1
		6.  Seven Points           -- Taking no argument, scheme of degree 3
		7.  Cools Haegemans        -- Taking for argument: the wished CH-scheme, integer valuing 1. Scheme of degree 8.
		8.  Cubtri                 -- Taking no argument, scheme of degree 8
		9.  Dunavant               -- Taking for argument: the wished degree of exactness, integer between 4 and 20.
		10. Franke                 -- Taking for argument: the wished Franke-scheme, integer valuing either 9 or 10. Schemes of degree 7.
		11. Gatermann              -- Taking no argument, scheme of degree 7
		12. Griener Schmid         -- Taking for argument: the wished GS-scheme, integer valuing either 1 or 2. Schemes of degree 6.
		13. Xiao Gimbutas          -- Taking for argument: the wished degree of exactness, integer between 1 and 50.
		14. Witherden Vincent      -- Taking for argument: the wished degree of exactness, integer between 1 and 20, except 3.
		15. Williams Shunn Jameson -- Taking for argument: the WSJ scheme, integer between 1 and 8 yielding to schemes having for degree respectively 1,2,4,5,7,8,10,12.
		16. Hammer Marlowe Stroud  -- Taking for argument: the HMS scheme, integer between 1 and 5 yielding to schemes having for degree respectively 1,2,2,3,5.
		17. Hillion                -- Taking for argument: the Hillion scheme, integer between 1 and 10 yielding to schemes having for degree respectively 1,2,2,2,2,2,3,3,3,3.
		18. Wandzura Xiao          -- Taking for argument: the WX scheme, integer between 1 and 6 yielding to schemes having for degree respectively 5,10,15,20,25,30.
		19. Strang Fix Cowper      -- Taking for argument: the SFC scheme, integer being 1, or any between 3 and 10 yielding to schemes having for degree respectively 2,3,3,4,4,5,5,6,7.
		20. Vioreanu Rokhlin       -- Taking for argument: the VR scheme,  integer between 1 and 19 yielding to schemes having for degree respectively 2,4,5,7,8,10,12,14,15,17,19,20,22,24,25,27,28,30,32.
		21. Zhang Cui Liu          -- Taking for argument: the ZCL scheme, integer between 1 and 3 yielding to schemes having for degree respectively 8,14,20.
		22. Taylor Wingate Bos     -- Taking for argument: the TWB scheme, integer being either 1,2,3,5 or 8 yielding to schemes having for degree respectively 2,4,7,9,14.
		23. Stroud                 -- Taking no argument, scheme of degree 7.
		24. Lether                 -- Taking for argument: integer n, yielding to a non-symmetric scheme of degree 2n-2.
		25. Lyness Jespersen       -- Taking for argument: the LJ scheme, integer being either 1,2,3,5,6,7,8,9, or between 11 and 21 yielding to schemes having for degree respectively 2,2,4,4,4,5,5,6,6,7,7,8,8,8,9,9,11,11.
		26. Liu Vinokur            -- Taking for argument: the LV scheme, integer being from 3 to 13 except 7, yielding to schemes having for degree respectively 2,2,3,3,4,4,4,4,5,5.
		27. Papanicolopulos Rot    -- Taking for argument: the Papanicolopulos scheme, integer 8 to 17 , yielding to schemes having for degree respectively 17,18,18,19,20,21,22,23,24,25.
		28. Papanicolopulos Sym    -- Taking for argument: the Papanicolopulos scheme, integer 0 to 8, yielding to schemes having for degree respectively 15,17,17,17,17,17,17,17.
		29. Laursen Gellert        -- Taking for argument: the LG scheme, integer 6 to 15, yielding to schemes having for degree respectively 4,5,5,6,7,7,8,9,9.
		30. Grundmann Moeller      -- Taking for argument: the integer n such that the scheme is of degree 2*n+1.
		31: Silvester (open)       -- Taking for argument: the degree of the scheme, integer, arbitrary.
		32: Silvester (closed)     -- Taking for argument: the degree of the scheme, integer, arbitrary.
		
		"""

	# ------------------------------------------------------------------- #
	#			Initialisation routines                                   #
	# ------------------------------------------------------------------- #

	#### Initialisation routine ####
	def __init__(self, Mesh, SchemeType, *SchemeArgs):
		"""
			Args:
				SchemeType   (string or integer):          string containing an integer or integer used to map to the wished quadpy scheme
				Params       (list of strings, optional):  further paramters required by the selected scheme (see the above list for info)

			Returns:
				None: Fills the QuadInnScheme field within the structure, containing an instance of the quadpy scheme
		"""

		# ----------------------- Initialisations ------------------------------
		#Converting the strings of the user input to integers
		SchemeType    = int(SchemeType)
		SchemeArgs    = list(SchemeArgs)
		if len(SchemeArgs)>0: SchemeArgs[0] = int(SchemeArgs[0])

		# Initialising the dictionary that will contain the quadrature instance depending on the edge's number of the element
		self.QuadInnScheme = dict()

		# ------------------ Mapping to the quadratures of quadpy --------------
		# Spanning all the types of elements that one may encounter
		for ele in set(Mesh.MeshType):

			# Quadratures availables for simplicial shapes
			if   ele==3:
				if    SchemeType == 0:  self.QuadInnScheme[3] = qp.t2.get_good_scheme(SchemeArgs[0])
				elif  SchemeType == 1:  self.QuadInnScheme[3] = qp.t2.schemes["albrecht_collatz"]()
				elif  SchemeType == 2:  self.QuadInnScheme[3] = qp.t2.schemes["Dcutri"]()
				elif  SchemeType == 3:  self.QuadInnScheme[3] = qp.t2.schemes["berntsen_espelid_{0:d}".format(SchemeArgs[0])]()
				elif  SchemeType == 4:  self.QuadInnScheme[3] = qp.t2.schemes["centroid"]()
				elif  SchemeType == 5:  self.QuadInnScheme[3] = qp.t2.schemes["vertex"]()
				elif  SchemeType == 6:  self.QuadInnScheme[3] = qp.t2.schemes["seven_point"]()
				elif  SchemeType == 7:  self.QuadInnScheme[3] = qp.t2.schemes["cools_haegemans_{0:d}".format(SchemeArgs[0])]()
				elif  SchemeType == 8:  self.QuadInnScheme[3] = qp.t2.schemes["cubtri"]()
				elif  SchemeType == 9:  self.QuadInnScheme[3] = qp.t2.schemes["dunavant_{0:02d}".format(SchemeArgs[0])]()
				elif  SchemeType == 10: self.QuadInnScheme[3] = qp.t2.schemes["franke_{0:02d}".format(SchemeArgs[0])]()
				elif  SchemeType == 11: self.QuadInnScheme[3] = qp.t2.schemes["gatermann"]()
				elif  SchemeType == 12: self.QuadInnScheme[3] = qp.t2.schemes["griener_schmid_{0:0d}".format(SchemeArgs[0])]()
				elif  SchemeType == 13: self.QuadInnScheme[3] = qp.t2.schemes["xiao_gimbutas_{0:2d}".format(SchemeArgs[0])]()
				elif  SchemeType == 14: self.QuadInnScheme[3] = qp.t2.schemes["witherden_vincent_{0:2d}".format(SchemeArgs[0])]()
				elif  SchemeType == 15: self.QuadInnScheme[3] = qp.t2.schemes["williams_shunn_jameson_{0:d}".format(SchemeArgs[0])]()
				elif  SchemeType == 16: self.QuadInnScheme[3] = qp.t2.schemes["hammer_marlowe_stroud_{0:d}".format(SchemeArgs[0])]()
				elif  SchemeType == 17: self.QuadInnScheme[3] = qp.t2.schemes["hillion_{0:02d}".format(SchemeArgs[0])]()
				elif  SchemeType == 18: self.QuadInnScheme[3] = qp.t2.schemes["wandzura_xiao_{0:d}".format(SchemeArgs[0])]()
				elif  SchemeType == 19: self.QuadInnScheme[3] = qp.t2.schemes["strang_fix_cowper_{0:02d}".format(SchemeArgs[0])]()
				elif  SchemeType == 20: self.QuadInnScheme[3] = qp.t2.schemes["vioreanu_rokhlin_{0:02d}".format(SchemeArgs[0])]()
				elif  SchemeType == 21: self.QuadInnScheme[3] = qp.t2.schemes["zhang_cui_liu_{0:d}".format(SchemeArgs[0])]()
				elif  SchemeType == 22: self.QuadInnScheme[3] = qp.t2.schemes["taylor_wingate_bos_{0:d}".format(SchemeArgs[0])]()
				elif  SchemeType == 23: self.QuadInnScheme[3] = qp.t2.schemes["stroud_t2_7_1"]()
				elif  SchemeType == 24: self.QuadInnScheme[3] = qp.t2.schemes["lether"](SchemeArgs[0])
				elif  SchemeType == 25: self.QuadInnScheme[3] = qp.t2.schemes["lyness_jespersen_{0:02d}".format(SchemeArgs[0])]()
				elif  SchemeType == 26: self.QuadInnScheme[3] = qp.t2.schemes["liu_vinokur_{0:02d}".format(SchemeArgs[0])]()
				elif  SchemeType == 27: self.QuadInnScheme[3] = qp.t2.schemes["papanicolopulos_rot_{0:02d}".format(SchemeArgs[0])]()
				elif  SchemeType == 28: self.QuadInnScheme[3] = qp.t2.schemes["papanicolopulos_sym_{0:d}".format(SchemeArgs[0])]()
				elif  SchemeType == 29: self.QuadInnScheme[3] = qp.t2.schemes["laursen_gellert_{0:02d}".format(SchemeArgs[0])]()
				elif  SchemeType == 30: self.QuadInnScheme[3] = qp.tn.grundmann_moeller(3, SchemeArgs[0])
				elif  SchemeType == 31: self.QuadInnScheme[3] = qp.tn.silvester(3, "open", SchemeArgs[0])
				elif  SchemeType == 32: self.QuadInnScheme[3] = qp.tn.silvester(3, "closed", SchemeArgs[0])
				else: raise(ValueError("No matching index for the quadpy inner quadrature for simplicial elements. Please check your inputs. Abortion."))

			# Quadratures available on quadrangular shapes
			elif ele==4:
				if    SchemeType == 1:  self.QuadInnScheme[4] = qp.t2.schemes["albrecht_collatz"]()
				else: raise(ValueError("Error in the wished inner quadrature (quadpy). The quadrature type indexed by" + str(SchemeType) + " is not valid for all the quads elements. Abortion."))

			else: raise(ValueError("No quadrature is defined yet for the type of element contained in the mesh. Abortion."))

	# ------------------------------------------------------------------- #
	#			Interfacing routine                                       #
	# ------------------------------------------------------------------- #

	#### Definition of inner quadrature points and weights ###
	def PointsWeightsData(self, n):
		""" .. |bs|   unicode:: U+00A0 .. NO-BREAK SPACE

		Hardly coded definition of inner quadrature points and weights, given in barycentric coordinnates.
		Exact up to degree 5.

		Args:
			n (integer):  the number of faces of the element

		.. rubric:: Returns

		|bs| - |bs| **points**  *(2D numpy float array)*   -- the 12 quadrature points in barycentric coordinates (12 x 3)

		|bs| - |bs| **weights** *(numpy float array)*      -- the associated quadrature weights
		"""

		# ------------------- Computations ------------------------------------------------------
		# Extracting the points and weights from quadpy
		points  = self.QuadInnScheme[n].points.T
		weights = self.QuadInnScheme[n].weights

		# Giving back the values
		return(points, weights)

#### Class defining the boundary quadrature rule to use one each element's face ####
class BoundaryQuadrature():
	"""Class interfacing the user input to the quadpy routines defining the points and weights for the boundary quadrature rules.

	|

	The schemes are indexed as follows:

		1.  Gauss Patterson            -- Taking for argument: the wished degree of exactness, integer, arbitrary
		2.  Clenshaw Curtis            -- Taking for argument: the degree of exactness, integer, arbitrary
		3.  Fejer type 1               -- Taking for argument: the degree of exactness, integer, arbitrary
		4.  Fejer type 2               -- Taking for argument: the degree of exactness, integer, arbitrary
		5.  Newton Cotes closed        -- Taking for argument: n such that the degree of exactness is n+1 if n is even n otherwise, integer, arbitrary n
		6.  Newton Cotes open          -- Taking for argument: n such that the degree of exactness is n if n is even n-1 otherwise, integer, arbitrary n
		7.  Gauss Legendre             -- Taking for argument: n such that the degree of exactness is 2n-1, integer, arbitrary n>1
		8.  Gauss Kronrod              -- Taking for argument: n such that the degree of exactness is 2(int(ceil(3n/2)))+1, integer, arbitrary n>1
		9.  Gauss Jacobi               -- Taking for argument: (n,a,b), n such that the degree of exactness is 2n-1, integer arbitrary >1, a and b the weight powers, float.
		10. Gauss Lobatto              -- Taking for argument: n such that the degree of exactness is 2n-3, integer, arbitrary n>1
		11. Gauss Radau                -- Taking for argument: n such that the degree of exactness is 2n-1, integer, arbitrary n>1
	"""

	# ------------------------------------------------------------------- #
	#			Initialisation routines                                   #
	# ------------------------------------------------------------------- #

	#### Initialisation routine ####
	def __init__(self, Mesh, SchemeType, *SchemeArgs):
		"""
			Args:
				SchemeType   (string or integer):          string containing an integer or integer used to map to the wished quadpy scheme
				SchemeArgs   (list of strings, optional):  further paramters required by the selected scheme (see the above list for info)

			Returns:
				None: Fills the QuadBdScheme field within the structure, containing an instance of the quadpy scheme
		"""

		# ------------------- Interfacing ------------------------------------------------------
		# Retrieving the scheme type according to quadpy possibilities, along with their associated arguments
		SchemeType    = int(SchemeType)
		SchemeArgs    = list(SchemeArgs)
		if len(SchemeArgs)>0: SchemeArgs[0] = int(SchemeArgs[0])

		# Switch to the right quadrature scheme for the bounndary segment quadrature
		if    SchemeType == 1:  self.QuadBdScheme = qp.c1.gauss_patterson(SchemeArgs[0])
		elif  SchemeType == 2:  self.QuadBdScheme = qp.c1.clenshaw_curtis(SchemeArgs[0])
		elif  SchemeType == 3:  self.QuadBdScheme = qp.c1.fejer_1(SchemeArgs[0])
		elif  SchemeType == 4:  self.QuadBdScheme = qp.c1.fejer_2(SchemeArgs[0])
		elif  SchemeType == 5:  self.QuadBdScheme = qp.c1.newton_cotes_closed(SchemeArgs[0])
		elif  SchemeType == 6:  self.QuadBdScheme = qp.c1.newton_cotes_open(SchemeArgs[0])
		elif  SchemeType == 7:  self.QuadBdScheme = qp.c1.gauss_legendre(SchemeArgs[0])
		elif  SchemeType == 8:  self.QuadBdScheme = qp.c1.gauss_kronrod(SchemeArgs[0])
		elif  SchemeType == 9:  self.QuadBdScheme = qp.c1.gauss_jacobi(SchemeArgs[0], float(SchemeArgs[1]), float(SchemeArgs[2]))
		elif  SchemeType == 10: self.QuadBdScheme = qp.c1.gauss_lobatto(SchemeArgs[0])
		elif  SchemeType == 11: self.QuadBdScheme = qp.c1.gauss_radau(SchemeArgs[0])
		else: raise(ValueError("Error in the wished inner quadrature (quadpy). The quadrature type indexed by" + str(SchemeType) + " is not valid for all boundary quadrature. Abortion."))


	# ------------------------------------------------------------------- #
	#			Interfacing routine                                       #
	# ------------------------------------------------------------------- #

	#### Definition of boundary quadrature points and weights through quadpy ###
	def PointsWeightsData(self, n, f):
		""" .. |bs|   unicode:: U+00A0 .. NO-BREAK SPACE

		Hardly coded definition of boundary quadrature points and weights, given in barycentric coordinnates.
		Exact up to degree 5.

		Args:
			n  (integer): number of edges of the considered element
			f  (integer): considered triangle face

		.. rubric:: Returns

		|bs| - |bs| **points**  *(2D numpy float array)*   -- the 3 quadrature points in barycentric coordinates (3 (NbPoints) x 3)

		|bs| - |bs| **weights** *(numpy float array)*      -- the associated quadrature weights
		"""

		# ------------------- Computations ------------------------------------------------------
		# Initialising the export quantities
		pointts = 0.5*(self.QuadBdScheme.points+1)
		weights = self.QuadBdScheme.weights

		# Selecting the considered face and prescribing the non-null coefficients on the target vertices
		sb = [f, int(np.mod(f+1,n))]

		# Defining the quadrature points and weights
		points  = np.zeros((len(weights),n))
		points[:,sb] = np.array([pointts, 1-pointts]).T

		# Returning the desired quantitites
		return(points, weights)
