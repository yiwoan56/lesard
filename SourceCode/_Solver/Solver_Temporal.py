#***************************************************************************
# Copyright (C) 2020 Élise Le Mélédo
# This file is part of LESARD.
#
# LESARD is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LESARD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LESARD.  If not, see <https://www.gnu.org/licenses/>.
#****************************************************************************

#Module that furnishes the tools for a time stepping
#management (not containing the time scheme themselves)



# ==============================================================================
#	Preliminary imports
# ==============================================================================
import numpy as np


# ==============================================================================
#	Time management routines
# ==============================================================================

#### Computing the mass lumping coefficients ####
def GetLumpingCoefficients(Mesh, Solver):
    """ Computes the mass lumping coefficients.

    Args:
        Mesh (MeshStructure):       the instance containing the mesh information
        Solver (Solver):            instance containing all the solver's parameters

    Returns:
        None:                       fills directly the "Lumping" field of the mesh structure
    """

    # ------------ Initialisations ------------------------------------
    # Getting the type of the control volume
    typ = Solver.ControlVolumes
    # Initialising the
    Mesh.Lumping = np.zeros(Mesh.NbDofs)

    # ------------ Actual mass lumping computations -------------------
    # In the case of a dual mesh based control volume, compute the mass lumping coefficients
    # by agglomerating the pondered volume of each cell (which is the information contained in the DualAreasTot)
    if typ == "Dual":
        for i in range(Mesh.NbInnerElements):
            Mesh.Lumping[Mesh.ElementsDofs[i]] += Mesh.InnerElementsVolume[i]/len(Mesh.ElementsDofs[i])	#Mesh.Lumping[Mesh.ElementsBoundaryDofs[i]]
        Mesh.Lumping = 1/Mesh.Lumping

    # In the case of a primal mesh based control volume, (warning: only cell-centred FV in the RD setting)
    # register the elment's volume as the control one
    elif typ == "Primal":
        for i in range(Mesh.NbInnerElements):
            Mesh.Lumping[Mesh.ElementsInnerDofs[i]] += Mesh.InnerElementsVolume[i]/len(Mesh.ElementsDofs[i])
        Mesh.Lumping = 1/Mesh.Lumping

    # Safe exception case
    else:
        raise ValueError("Error: Unknown type of control volume. Check you scheme definition. Abortion.")

#### Computing the time step upon the solution and the CFL ####
def GetTimeStep(CFL, Tmax, Problem, Mesh, Solution):
    """ Computes the time step for each iteration upon the solution and the CFL.

    Args:
        CFL      (float):                the CFL number
        Tmax     (float):                simulation end-time
        Problem  (Problem structure):    the instance containing the problem information
        Mesh     (MeshStructure):        the instance containing the mesh information
        Solution (Solution):             the solution of the problem that is being evolved in time

    Returns:
        timestep (float):                the time step
    """

    # Preparing a huge increment, that is going to be reduced
    delta = 1

    # Looping on each element to find the smallest spectral radius
    for i in range(Mesh.NbInnerElements):
        # Extracting the physical element to get the number of edges
        ele  = Mesh.InnerElements[i][:]

        # Looping on each edge
        for f in range(len(ele)):
            # Getting the variational information
            dele  = Mesh.ElementsInnerDofs[i][:]+Mesh.ElementsBoundaryWiseDofs[i][f]
            Flags = Solution.FluidFlag[dele]
            Vars  = Solution.Sol[:,dele]

            # Getting the physical information
            Coords = Mesh.DofsXY[dele, :]
            nn     = np.array([Mesh.ElementsBoundaryWiseNormals[i][f]])

            # Computing the spectral radius and the maximum propagation quantity
            spr   = Problem.GoverningEquations.SpectralRadius(Vars, Flags, nn, Coords)
            delta = np.min([delta, *Mesh.InnerElementsVolume[i]/(np.abs(spr)+1e-6)])

    # Checking that we are not overdoing compared to the wished final time
    if Solution.t+delta*CFL>Tmax-1e-16: dt = Tmax-Solution.t
    else: dt = delta*CFL

    # Returning the time step
    return(dt)
