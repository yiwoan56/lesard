#***************************************************************************
# Copyright (C) 2020 Élise Le Mélédo
# This file is part of LESARD.
#
# LESARD is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LESARD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LESARD.  If not, see <https://www.gnu.org/licenses/>.
#****************************************************************************

#"""
#Module that defines a Vertex centred finite volume scheme
#"""

# ==============================================================================
#	Preliminary imports
# ==============================================================================
import numpy as np
import copy

# ==============================================================================
#	Parameter (and reader) class of scheme properties and runtime informations
# ==============================================================================
# Name of the module to be externally accessible
LimiterName = "Psi"

# ==============================================================================
#	Class furnishing all the limiter tools
# ==============================================================================
class Limiter():
	""" .. |bs|   unicode:: U+00A0 .. NO-BREAK SPACE

	Class furnishing all the schemes tools and registers the scheme's wished
	parameters. The main iteration is accessible with the method "Mollify".

	.. rubric:: Fields

	All the fields given as arguments at the instance's creation are repeated within
	the structure with the same names (see the parameters below).

	|

	"""

	#### Automatic initialisation routine ####
	def __init__(self, Problem, Mesh, *Params):
		""" .. |bs|   unicode:: U+00A0 .. NO-BREAK SPACE
		Args:
			Problem (Problem):       the considered problem
			Mesh    (MeshStructure): the considered mesh instance
			Params  (string list):   the limiters's parameters as wished by the user

		|

		.. rubric:: Methods

		"""

		# Short cutting the common problem instances
		self.Problem = Problem
		self.Mesh    = Mesh

		# Extracting the parameters from the user input list should they be needed (not the case here)
		self.Params  = Params

	#### Main routine, definining the limiting mollifier ####
	def Mollify(self, FlagPoints, SolBuff, resu, fluxes, i, du=0, dt=1):
		""" .. |bs|   unicode:: U+00A0 .. NO-BREAK SPACE

		Limting routine according to the Psi rule

		Args:
			FlagPoints  (function callback):   the handle of a function flagging the points to the relevant fluid (not used here)
			Solution   (solution structure):   structure containing the current solution's values to iterate
			resu       (float numpy array):    previoulsy computed residuals that have to be modified (NbVars x NbElementDofs)
			fluxes     (numpy (multiD)array):  pre-computed fluxes at the points of interest. For this scheme, access with fluxes[element, face, coordinate(fx or fy), variable, pointindex]
			i          (integer):              the index of the considered element within which the partial residuals will be computed
			du         (float numpy array):    (optional) when using DeC, the difference in the time iteration
			dt         (float):                (optional) when using DeC, the full time step

		Returns:
			Lresu      (float numpy array):    the limited residuals (NbVars x NbElementDofs)
		"""

		# -------- Initialisation ------------------------------------------------
		# Getting the number dofs contaned in the considered element
		NbVars, NbDofs = np.shape(resu)

		# Initialising the mollification vector (residuals)
		Lresu = np.zeros(np.shape(resu))

		# Setting the safety division trick
		safe = 1e-8

		# ------- Limiting the given residual within the given element ----------
		# Checking if it is actually relevant and possible to limit, and if yes, select
		# the variables on which to limit
		Phi    = np.sum(resu, axis = 1, keepdims=True)
		ModInd = np.where(Phi>safe)[0]
		NodInd = np.setdiff1d(range(NbVars), ModInd)

		# Modify the residuals for the variables corresponding to the indices of ModInd
		if ModInd.size>0:
			# Using the definition of the Psi limiter and retrieving the quantities from the
			# current residuals. Note that in mol, the abs is here to compensate
			# the negative zero python convention used in this case
			buff = resu[ModInd, :]/Phi[ModInd]
			mol  = np.abs(buff*(buff>0))
			den  = np.sum(mol, axis=1,keepdims=True)+safe

			# Getting the new distribution coefficients and the blending ratio
			bet = mol/den
			tet = np.abs(Phi[ModInd])/(np.sum(np.abs(resu[ModInd, :]), axis=1, keepdims=True)+safe)

			# Blending the scheme
			Lresu[ModInd,:] = (1-tet)*bet*Phi[ModInd] + tet*resu[ModInd, :]

		# Not touching the residuals for the variables corresponding to the indices of ModInd
		if NodInd.size>0: Lresu[NodInd, :] = resu[NodInd, :]

		# Returning the partial residuals
		return(Lresu)
