#***************************************************************************
# Copyright (C) 2020 Élise Le Mélédo
# This file is part of LESARD.
#
# LESARD is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LESARD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LESARD.  If not, see <https://www.gnu.org/licenses/>.
#****************************************************************************

#"""
#Module that defines a Vertex centred finite volume scheme
#"""

# ==============================================================================
#	Preliminary imports
# ==============================================================================
import numpy as np
import copy

# Import custom modules
import LocalLib.BarycentricBasisFunctions            as BF
import LocalLib.BarycentricTools                     as BT

# ==============================================================================
#	Parameter (and reader) class of scheme properties and runtime informations
# ==============================================================================
# Name of the module to be externally accessible
LimiterName = "Filtering Streamline"

# ==============================================================================
#	Class furnishing all the limiter tools
# ==============================================================================
class Filtering():
	""" .. |bs|   unicode:: U+00A0 .. NO-BREAK SPACE

	Class furnishing all the schemes tools and registers the scheme's wished
	parameters. The main iteration is accessible with the method "Mollify".

	.. rubric:: Fields

	All the fields given as arguments at the instance's creation are repeated within
	the structure with the same names (see the parameters below). It additionaly has
	upon instance creation the fields:

	- |bs| **Order**              *(integer)*           -- The order of the used basis functions (matching the order of the mesh)
	- |bs| **InnerQBFValues**     *(float numpy array)* -- Value of the basis function at the quadrature points of each element (NbInnerElementsx NbQuadraturePoints x NbBasisFunc)
	- |bs| **InnerQWeigths**      *(float numpy array)* -- Weights of the quadrature points of each element (NbInnerElements x NbQuadraturePoints)
	- |bs| **InnerQGFValues**     *(float numpy array)* -- Value of the gradient of the basis function at the quadrature points of each element (NbInnerElements x NbQuadraturePoints x NbBasisFunc x dim)
	- |bs| **InnerQPoints**       *(float numpy array)* -- Quadrature points of each element (NbInnerElements x NbQuadraturePoints)
	- |bs| **FaceWiseQWeigths**   *(float numpy array)* -- Weights of the quadrature points of each element's face (NbInnerElements x NbFace x NbQuadraturePoints)
	- |bs| **FaceWiseQPoints**    *(float numpy array)* -- Quadrature points of each element's face (NbInnerElements x NbFace x NbQuadraturePoints)
	- |bs| **FaceWiseQBFValues**  *(float numpy array)* -- Value of the basis function at the quadrature points of each element's face (NbInnerElements x NbFace x NbQuadraturePoints x NbBasisFunc)


	|

	.. warning::

		The streamline stabilisation only seems to work for advection so far, it crashes on euler

	|

	"""

	#### Automatic initialisation routine ####
	def __init__(self, Problem, Mesh, QuadratureRules, *Params):
		""" .. |bs|   unicode:: U+00A0 .. NO-BREAK SPACE
		Args:
			Problem          (Problem):       the considered problem
			Mesh             (MeshStructure): the considered mesh instance
			QuadratureRules  (Quadratures):   the quadrature instance with which the scheme will be performed (madatory paramter)
			Params           (string list):   the limiters's parameters as wished by the user (useless here)

		|

		.. rubric:: Methods
		"""

		# ------- Compatibility check with the user inputs and Amendements interface parsing ------------------------
		# Test the type of input that are required to work and inform the user accordingly
		if type(QuadratureRules) == str: raise (ValueError("The first argument of the streamline filtering should be a quadrature rule. Abortion."))
		else: self.QuadratureRules = QuadratureRules

		# ------- Setting up the parameters ------------------------------------------------------------------------
		# Setting the name of the class to be accessible from outside
		self.ModifierName = "Filtering Streamline"

		# Setting the mesh and parameters for an internal access
		self.Problem = Problem
		self.Mesh    = Mesh

		# Retrieving the mesh order, furnishing the variatianal order to set as streamline
		# according to the number of Dofs per element
		self.Order = int(self.Mesh.MeshOrder)

		# Precomputing the basis function values at quadrature points for fastening the run time
		self.PreEvaluateQuadratureQuantities()


	#### Main routine, definining the limiting mollifier ####
	def Mollify(self, FlagPoints, Solution, resu, fluxes, i, du=0, dt=1):
		""" .. |bs|   unicode:: U+00A0 .. NO-BREAK SPACE

		Streamline dissipation, that has to be ADDED to the residual. In the input line, do not
		simply write Filtering_1, but write 0+Filtering_1 so that it gets added to the initial residual

		Args:
			FlagPoints  (function callback):    the handle of a function flagging the points to the relevant fluid
			Solution   (solution structure):   structure containing the current solution's values to iterate
			resu       (float numpy array):    previoulsy computed residuals that have to be modified (NbVars x NbElementDofs)
			fluxes     (numpy (multiD)array):  pre-computed fluxes at the points of interest. For this scheme, access with fluxes[element, face, coordinate(fx or fy), variable, pointindex]
			i          (integer):              the index of the considered element within which the partial residuals will be computed
			du         (float numpy array):    (optional) when using DeC, the difference in the time iteration
			dt         (float):                (optional) when using DeC, the full time step

		Returns:
			Lresu      (float numpy array):    the limited residuals (NbVars x NbElementDofs)
		"""

		# -------- Initialisation ----------------------------------------------
		# Getting the number dofs contaned in the considered element
		NbVars, NbDofs = np.shape(resu)

		# Initialising the mollification vector (residuals)
		Lresu = np.zeros(np.shape(resu))

		# Setting the safety division trick
		safe = 1e-42

		# -------- Precomputations of the actual streamline --------------------
		# Getting the dofs contaned in the considered element
		dele   = np.array(self.Mesh.ElementsDofs[i])
		NbDofs = len(dele)

		# Initialising the mollification vector (residuals)
		Resu  = np.zeros([np.shape(Solution.Sol)[0], NbDofs])

		# Retrieving the fluxes at the Dofs
		Fld = fluxes[-1,:,:,:,:,0:len(dele)]

		# ---- Getting the element and state informations
		# Retrieving the physical information on the spanned element's vertices
		ele = self.Mesh.InnerElements[i]
		vol = self.Mesh.InnerElementsVolume[i]
		n   = self.Mesh.ElementsBoundaryWiseNormals[i]

		# Retrieving the information on the local degree of freedom
		U    = Solution.Sol[:, dele]
		dU   = du[:, dele]

		# Get the coordinates of the vertices in the physical order
		elevert = self.Mesh.CartesianPoints[ele,:]

		# ---- Computing the evolution information
		# Retrieving the quadrature points as barycentric coordinates and convert them to cartesian
		# in the Dof's vertex ordering referential
		bqp      = self.InnerQPoints[i]
		weights  = self.InnerQWeigths[i]
		qp       = BT.GetCartesianCoordinates(bqp, elevert)

		# Evaluate the basis function in the order of the Dofs
		EvaluatedBase      = self.InnerQBFValues[i]
		EvaluatedGradients = self.InnerQGFValues[i]

		# Reconstructing the solution and its gradient at the quadrature point
		Uloc  = np.dot(U,  EvaluatedBase.T)
		dUloc = np.dot(dU, EvaluatedBase.T)
		GUloc = np.matmul(EvaluatedGradients.T, U.T)

		# Reconstructing back the conservative variables at the wished points
		# (helps to preserve the pressure contacts, comment if E has to be preserved as well)
		FlagsLoc = FlagPoints(Solution.LSValues, i, bqp)

		# Compute the flux according to the fluid's flag
		Flux = Fld[i,0]

		# Compute the jacobian at the local points
		Jac = self.Problem.GoverningEquations.Jacobian(Uloc, qp, FlagsLoc)

		# --------------- Computing the stabilisation material --------------------
		# --- Extract the required quantities
		# Retrieving the Dofs that are associated to the vertices and averaging the solution value
		verts = np.array(self.Mesh.Vertex2Dofs)[ele].flatten()
		# Extracting the Dofs of the considered triangle (required in case of DG only)
		verts = [v for v in verts if v in dele]
		# Averaging the solution from the considered dofs
		unmat = np.array([np.mean(Solution.Sol[:,verts], axis=1)]).T
		# Considering the cell center as a point of evaluation
		xymat = np.array([self.Mesh.DualCenters[i][-1]])
		# Getting the fluid on which it is
		MeanValues = np.mean(Solution.LSValues[:,verts], axis=1)
		lsmat = np.array([FlagPoints(MeanValues)])

		# Computing a first order gradient per given submit
		if len(ele)==3: Grad = np.roll(self.Mesh.ElementsBoundaryWiseNormals[i], -1, axis=0)/self.Mesh.InnerElementsVolume[i]
		else:           raise NotImplementedError("No gradient has been implemented correctly for the filtering streamline yet.")

		# Initialising Nmat
		Nmat = np.zeros((self.Problem.GoverningEquations.NbVariables, self.Problem.GoverningEquations.NbVariables))

		# Computing Nmat
		for v in range(len(ele)):
			# Getting the eigenvalues at the center of mass of the element
			eig  = self.Problem.GoverningEquations.EigenValues(unmat, lsmat, Grad[v:v+1,:], xymat)[0,:]
			# Getting the safety net ratio
			beta = np.max(np.abs(eig))*0.1
			dd   = np.zeros(eig.shape)
			for j in range(len(eig)):
				if    np.abs(eig[j]) >= beta: dd[j] = np.abs(eig[j])
				else: dd[j] = ((eig[j])**2+beta**2)/(2*beta)

			# Constructing the matrix
			dia = np.diag(dd)
			R   = self.Problem.GoverningEquations.RightEigenVectors(unmat, lsmat, Grad[v:v+1,:], xymat)[:,:,0]
			L   = self.Problem.GoverningEquations.LeftEigenVectors (unmat, lsmat, Grad[v:v+1,:], xymat)[:,:,0]
			A   = np.dot(R, np.dot(dia, L))
			Nmat += A

		# Inverting the obtained term
		Nmat = np.linalg.inv(Nmat)

		# ---- Computing the local divergence flux
		# Getting the flux values at the
		divflux = np.sum(np.matmul(np.swapaxes(EvaluatedGradients, 0,2),np.swapaxes(Flux, 2, 1)), axis=0)
		divflux = np.matmul(Nmat, dUloc + dt*divflux.T)

		# ---- Computing the stabilisation term
		# Spanning the dofs to mollify
		for dof in range(NbDofs):
			# Performing the quadrature
			stream = np.sum([weights[q]*np.matmul(EvaluatedGradients[dof,q,0]*Jac[:,:,0,q]+EvaluatedGradients[dof,q,1]*Jac[:,:,1,q], divflux[:,q]) for q in range(len(weights))], axis=0)

			# Updating the residual at the spanned Dof by the local contribution
			Lresu[:,dof] += (stream)*vol

		# -------- Returning the additive ----------------------------------------
		# Returning the stream term that should be added to the original resuduals
		return(Lresu)


	# **************************************************************
	# *  Definition of the Fluid spotter's scheme basis function   *
	# **************************************************************

	# Precomputing and storing the values of the basis functions at the quadrature points of each element
	def PreEvaluateQuadratureQuantities(self):
		"""
		Precomputing and storing the values of the basis functions at the quadrature points of each element

		Args:
			None:  Considers the given parameters of the class directly

		Returns:
			None: Creates the fields InnerQBFValues, InnerQPoints, InnerQWeigths and InnerQGFValues in the modifier structure, containing the values of each basis function at each the pointsm, the wigths and the basis functions values there for each element.
		"""

		# Initialising the long-term storing vectors
		self.InnerQBFValues    = [[]]*self.Mesh.NbInnerElements
		self.InnerQWeigths     = [[]]*self.Mesh.NbInnerElements
		self.InnerQGFValues    = [[]]*self.Mesh.NbInnerElements
		self.InnerQPoints      = [[]]*self.Mesh.NbInnerElements

		# Spanning each element
		for i in range(self.Mesh.NbInnerElements):

			# Get the elements properties
			ele = self.Mesh.InnerElements[i]
			vol = self.Mesh.InnerElementsVolume[i]
			n   = self.Mesh.ElementsBoundaryWiseNormals[i]

			# Computing the basis functions values at the inner quadrature points
			bqp, weights = self.QuadratureRules.InnerQuadrature(len(ele))
			self.InnerQPoints[i]   = bqp
			self.InnerQWeigths[i]  = weights
			self.InnerQBFValues[i] = BF.BarycentricBasis    (len(ele), "B", self.Order, bqp)
			self.InnerQGFValues[i] = BF.BarycentricGradients(len(ele), "B", self.Order, bqp, n, vol)
