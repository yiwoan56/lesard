#***************************************************************************
# Copyright (C) 2020 Élise Le Mélédo
# This file is part of LESARD.
#
# LESARD is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LESARD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LESARD.  If not, see <https://www.gnu.org/licenses/>.
#****************************************************************************

"""
The module  :code:`Redistancer` located in the file :code:`_Solver/SpatialModifiers/Redistancer.py`furnishes a single class
:code:`Redistancing` that interfaces the user instructions for redistancing the level set given a wished scheme and its parameters.

Upon instance creation, it converts the string-user input defining the redistancing method to use, its application range (everywhere, if __name__ == '__main__':
an interface narrow band, etc) and defines an interface application routine that is to apply on any freshly computed Level set-residual.
The method :code:`Redistance` is therefore the generic interface to apply any LS residual to redistance.

|

.. rubric:: Usage

Upon instance creation, the redistanciation routine is selected and constructed upon the user parameters that
are specified in the following format

.. code::

	# Defining the parameters as RedistancingRoutineIndex_(ApplicationRangeIndex,ApplicationPeriod,ApplicationRangeParameter1,ApplicationRangeParameter2,...)<Parameters,of,selected,routine>
	Parameters = 1_(1,1,0.5)<0.2,1e-8,1e-5>
	# If no parameters is to be given or if you want to use the default ones (if specified in the selected RedistancingRoutine), then please use the syntax
	Parameters = 1_(1,1,0.5)<>

You can then define the redistanciation routine by creating the wrapper instance;

.. code::

	Redistancer = Redistancing(Parameters, Problem, Mesh)

and finally call the redistanciation routine by using the field :code:`Redistance`:, by

.. code::

	# Within the lsrd code, always call with the "ConstructionPoints" argument, being here the Dofs where Phi is available
	Phi = Solution.LSValues[0,:]
	ConstructionPoints = Mesh.DofsXY[:,:]

	NewValues = Redistancer.Redistance(Points, Phi, ConstructionPoints=ConstructionPoints)

|

.. note::

	If the parameters of the selected redistancing routine requires a quadrature rule, please use the same quadrature rule format as for the properties of the spatial modifiers.

|

.. note::

	The use of this class is triple: parse the user parameters to select the wished redistancer and its application range (spatially),
	allowing the use of a custom quadrature scheme in the redistancing function (if needed), and to selecting the points to redistance at.

|
|
"""

# ==============================================================================
#	Preliminary imports
# ==============================================================================
import numpy as np
import copy
import re

import Mappers
import _Solver.QuadratureRules.Quadratures  as QR
import LocalLib.ReversedFunctools           as fcts

# ==============================================================================
#	Class furnishing all the limiter tools
# ==============================================================================
class Redistancing():
	"""
	Class furnishing all the tools for a generic redistancing routine and registers the user's wished
	redistancing parameters, both for its application spatial range andfor the method itself.
	The main iteration is accessible through the method :code:`Redistance`, and is mapped in the method
	:code:`SelectRedistancing` (see its description to known the parameter value to give for selecting the desired method).

	|

	"""

	#### Automatic initialisation routine ####
	def __init__(self, Parameters, Problem, Mesh):
		"""Args:
				RedistancingParameters (string):        the redistancing parameters, containing the wished redistancing method, its spatial application range, frequency and the method's parameters.
				Problem                (Problem):       the considered problem
				Mesh                   (MeshStructure): the considered mesh instance

		|

		.. note::

			 See :code:`ParseRedistancingParams` for a description of the expected format.

		.. rubric:: Methods
		"""

		# ------------------ Initialisations -----------------------------------
		# Repeating the initialisation variable within the structure
		self.Params  = Parameters
		self.Problem = Problem
		self.Mesh    = Mesh

		# Mapping to the redistanciation algorithm if any
		if "0" != self.Params and self.Problem.NbFluids>1:
			# Parse the wished parameters
			Method, RedistanciationRange, RedistancingParameters = self.ParseRedistancingParams()
			# Map to the redistancing method and register the desired redistancing frequency
			self.RedistanciationPeriod = int(RedistanciationRange[1])
			self.RedistancingMethod    = Mappers.Redistancer(Method, *RedistancingParameters)

		# Otherwise store the bypass value to skip the redistancing in a soft way
		else:
			RedistanciationRange = [0]
			self.RedistanciationPeriod = np.inf

		# Initialise the class containing the redistancing routine upon the user wished parameters
		self.Redistance = self.SelectRedistancing(RedistanciationRange[0], *RedistanciationRange[2:])


	# ****************************************************************************
	#                   Parameters parser                                        #
	# ****************************************************************************
	##### Routine parsing the user parameters ####
	def ParseRedistancingParams(self):
		"""Converting the string user input into sets of parameters: the index of the
		wished redistanciation routine, the spatial application properties and the properties
		of the redistancing method itself.

		Args:
			None:  the self.Params field of the class should be filled and containing a string of the format
			       :code:`RedistancingRoutineIndex_(ApplicationRangeIndex, ApplicationPeriod, ApplicationRangeParameter1,ApplicationRangeParameter2,...)<Parameters,of,selected,routine>`
				   The minimal number of parameter to give is :code:`RedistancingRoutineIndex_(ApplicationRangeIndex,ApplicationPeriod)<>` or :code:`0` if no redistancing is wished.

		.. note ::

			- See the module :code:`Mappers` to get the Index corresponding to the desired redistancing routine method
			- See the method :code:`SelectRedistancing` to get the ApplicationRangeIndex
			- Refer to the mapped routine's documentation ( :code:`NarrowBandRedistancing`, :code:`GlobalRedistancing`, etc) for the knowing which ApplicationRangeParameters to use
			- See the selected RedistancingRoutine's documentation for knowing its parameters

		|

		.. warning::

			There should be NO space in the definition of the parameter string, even between the parameters specification

		Returns:
			Properties (tuple): Tuple contining in order:

								- the index of the wished redistancing routine (integer)
								- the spatial application range ([SpatialType (integer), ApplicationPeriod(integer), Paramter1 (string), Parameter2 (string), ...])
								- the parameters of the redistancing routine (list containing strings and/or a quadrature scheme instance)
		"""

		# Find the index of the wished redistancing routine
		Type = int(re.split("_", self.Params)[0])

		# Find the spatial application parameters
		SpatialType = list(filter(None, re.findall("\((.*?)\)", self.Params)[0].split(",")))
		if len(SpatialType)<2 or not SpatialType[0].isdigit() or not SpatialType[1].isdigit():
			raise(ValueError("Error parsing the level set redistancing parameters. Please check your input settings. Abortion."))
		else:
			SpatialType[0] = int(SpatialType[0])
			SpatialType[1] = int(SpatialType[1])


		# Find the redistancing algorithm parameters
		Parameters = list(filter(None, re.findall("\<(.*?)\>", self.Params)[0].split(",")))

		# In case of a quadrature parameter, retrieve the quadrature rules through the mappers to furnish it as a ready-to use argument
		for prop in range(len(Parameters)):
			quadrule = re.findall("\#(.*?)\#", Parameters[prop])
			if   len(quadrule)==2: Parameters[prop] = QR.Quadratures(self.Mesh, *[qr.replace("_", ",") for qr in quadrule])
			elif len(quadrule)==1: raise(ValueError("One input of the redistancer parameters has not been understood. Either the inner of boundary quadrature type has not been mentionned. Abortion."))
			elif len(quadrule)>2:  raise(ValueError("One input of the redistancer parameters has not been understood. Check the syntax of a arguments defining quadratures. Abortion."))

		# Return the parsed parameters
		return(Type, SpatialType, Parameters)

	# ****************************************************************************
	#                   Interfacing routines                                     #
	# ****************************************************************************
	def SelectRedistancing(self, RedistanciationRange, *RangeParameters):
		"""Mapping that maps to the wished redistancing method depending on the user wishes upon to the following indices to
		be given as a parameter.

		Args:
			RedistanciationRange (integer): The index corresponding to the wished spatial application range of the redistanciation routine, as follows

				0. | :code:`NoRedistancing`               --  Returns an identity function
				1. | :code:`NarrowBandRedistancing`       --  Performs a redistancing within a narrow band around the interface
				2. | :code:`GlobalRedistancing`           --  Performs a redistancing over the full computational domain
				3. | :code:`NarrowBandFixedRedistancing`  --  Performs a redistancing within a narrow band around the interface, keeping the level set values at the immediate vicinity of the interface untouched

			RangeParameters (Optional): If the selected redistanciation application metod requires arguments, its arguments (see the documentation of the selected routine to know them).

		|
		"""

		# Mapping to the desired routine
		if   RedistanciationRange == 0: return(self.NoRedistancing)
		elif RedistanciationRange == 1: return(fcts.partial(self.NarrowBandRedistancing, *RangeParameters))
		elif RedistanciationRange == 2: return(self.GlobalRedistancing)
		elif RedistanciationRange == 3: return(fcts.partial(self.NarrowBandFixedRedistancing, *RangeParameters))

		# Safe case for input mistakes
		else: raise(NotImplementedError("Error: The wished spatial application\
		domain for the level set redistancing is not available. Please check your input settings. Abortion."))

	# ****************************************************************************
	#           Spatial application routines                                     #
	# ****************************************************************************
	def NoRedistancing(self, Solution, *args):
		"""Dummy routine returning the identity over the LevelSet values of the solution,
		only defined for compatibility isses of the general code.

		Args:
			Solution (Solution): The solution structure of the investigated problem

		Returns:
			RedistancedSol (numpy array): The level set values numpy array contained within the solution structures, without any change

		.. note::

			*args is here for compatibility-on-call reasons
		"""

		# Returning the identity over the level set values
		return(Solution.LSValues[:,:])

	#### Redistancing the Level set over a narrow band around the interface ####
	def NarrowBandRedistancing(self, Solution, BandWidth, *args):
		"""Routines that applies the desired redistancing method over a narrow band around the interface.

		Args:
			Solution  (Solution):                         The solution structure of the investigated problem
			BandWidth (single float-containing string):   The width of the band to consider for redistancing (= maximal value of the level set under which it will be redistanced)

		Returns:
			RedistancedSol (numpy array): The redistanced level set values to substitute to LSValues in the solution structure
			                              (the substitution is not performed automatically)

		.. note::

			*args is here for compatibility-on-call reasons
		"""

		# Extracting the point-values defining the level set function over the computational domain
		Points = self.Mesh.DofsXY[:,:]
		Phi    = Solution.LSValues[:,:]

		# Initialise the redistancing solution
		RedistancedSol = np.zeros(Phi.shape)

		# Redistancing the level sets
		for i in range(self.Problem.NbFluids):
			# Selecting the points to redistance at
			ind  = np.where(np.abs(Phi[i,:])<0.5*np.float(BandWidth))[0]
			nind = np.setdiff1d(ind,range(self.Mesh.NbDofs))

			# Redistancing at the relevant point and leaving untouched the others
			RedistancedSol[i,ind]  = self.RedistancingMethod.Redistance(Points[ind,:], Phi[i,:], ConstructionPoints=Points)
			RedistancedSol[i,nind] = Phi[i,nind]

		# Returning the redistanced level set as values
		return(RedistancedSol)

	#### Redistancing the Level set over the full computational domain ####
	def GlobalRedistancing(self, Solution, *args):
		"""Routines that applies the desired redistancing method over the full computational domain.

		Args:
			Solution (Solution): The solution structure of the investigated problem

		Returns:
			RedistancedSol (numpy array): The redistanced level set values to substitute to LSValues in the solution structure
			                              (the substitution is not performed automatically)

		.. warning::

			Depending on the selected Redistanciation method, it may fail for points that are close to the boundary

		.. note::

			*args is here for compatibility-on-call reasons

		|
		"""

		# Extracting the point-values defining the level set function over the computational domain
		Points = self.Mesh.DofsXY[:,:]
		Phi    = Solution.LSValues

		# Initialise the redistancing solution
		RedistancedSol = np.zeros(Phi.shape)

		# Redistancing the level sets
		for i in range(self.Problem.NbFluids): RedistancedSol[i,:] = self.RedistancingMethod.Redistance(Points, Phi[i,:])

		# Returning the redistanced level set as values
		return(RedistancedSol)

	#### Redistancing the Level set over a narrown band within which the immediate vicinty points to the boundary are left untouched ####
	def NarrowBandFixedRedistancing(self, Solution, *args):
		raise(NotImplementedError("Redistancing spatial application range  narrow-fixed not yet implemented"))
