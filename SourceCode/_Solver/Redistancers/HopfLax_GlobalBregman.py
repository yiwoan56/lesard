#***************************************************************************
# Copyright (C) 2020 Élise Le Mélédo
# This file is part of LESARD.
#
# LESARD is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LESARD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LESARD.  If not, see <https://www.gnu.org/licenses/>.
#****************************************************************************

"""
.. |bs|   unicode:: U+00A0 .. NO-BREAK SPACE

|

Module that furnishes the tools to perform a redistancing through a Hopf-lax formula, solving the optimisation problem via a modified Bregman split approach
that is supposed to deal with local minima (not always in practice though).

Following the work of Byungjoon Lee, "Revisiting the redistaning problem using the Hopf-Lax formula, 10.1016/j.jcp.2016.11.005"
opf-Lax formula, 10.1016/j.jcp.2016.11.005", with a one-sided bisection method instead of the proposed secant method to allow saddle points safely,
and using an application of the work "Beyond Alternating Updates for Matrix Factorization with Inertial Bregman Proximal Gradient Algorithms" from
Mahesh Chandra Mukkamala and Peter Ochs for the Bregman split instead.

|

.. Note::

    - |bs| Due to the modification of the secant formula, the convergence is not strictly monotoneous in this implementation.
    - |bs| The local extrema are avoided but the saddle points stay problematic when trying to achieve global minima convergence.
    - |bs| This toolbox is only valid for scalar-valued functions (representing a level set)

|

.. Warning::

    - |bs| The code is not particularily robust and may fail on saddle points
    - |bs| On coarse grid if the gradient has to be approximated we may be stuck in local maxima and redistancing may fail

"""

# ==============================================================================
#	Preliminary imports
# ==============================================================================
# Pythonic import
import types
import copy

# Classical math librairies import
import numpy as np

# Import external libraries
import scipy.interpolate.interpnd as ind
import scipy.spatial.distance     as dist
import scipy.optimize

# Import custom libraries
import LocalLib.GlobalMinimisation as GM

# ==============================================================================================
#	Defining the class containing the properties to apply and the core reinitialisation routine
# ==============================================================================================
class Redistancing():
	"""
	Class furnishing the routines to define a redistanciation routine based on a HopfLax formula.

	.. note::

		The routine is updating the values of the level set at the degrees of freedom of the given mesh from
		the values that are stored there. Therefore, it is only suitable to be used on solution computed from
		a point-values dofs and not mixed techniques as the reconstruction techniques back and forth from generic
		dofs to point-values are not considered.
	"""

	### Initialisation of the instance by the parameters to consider in the reinitialisation process #####
	def __init__(self, InitTime=0.2, stol=1e-8, gtol=1e-5, smaxit=1000, gmaxit=50, eps=1e-8, verbose=False):
		"""
		Args:
			InitTime           (float):   Optional, default value = 0.2. Set the pseudo-time first iteration step in the bisection iteration process
			stol               (float):   Optional, default value = 1e-8.  The tolerance to stop the Secant iteration
			gtol               (float):   Optional, default value = 1e-5.  The tolerance to stop the Bregman iteration
			smaxit             (float):   Optional, default value = 1000.  The maximum iteration step for the secant method
			gmaxit             (float):   Optional, default value = 50.    The maximum iteration step for the bregman method
			eps                (float):   Optional, default value = 1e-8.  Safety net in the division of the weno weight and gradient division
			verbose            (boolean): Optional, default value = False. True if the user wants a printout each point of the obtained values or False if no wished printout

		.. note ::

			The arguments can be given as strings and will be converted accordingly if the string content is compatible with the exepected type.
		"""

		try:
			self.InitTime          = np.float(InitTime)
			self.eps               = np.float(eps)
			self.stol              = np.float(stol)
			self.gtol              = np.float(gtol)
			self.smaxit            = np.int(smaxit)
			self.gmaxit            = np.int(gmaxit)
			self.verbose           = np.bool(verbose)
		except:
			raise(ValueError("Wrong argument type or number given to the selected redistancing routine. Please check your input settings. Abortion."))



	# ********************************************************************************
	# *  Defining the functions giving the PDE to solve as a redistancing operator   *
	# ********************************************************************************
	# Defining the flux of the Eikonal equation
	def H (self, dx, dy): return(np.sqrt(dx**2+dy**2))

	# Defining its partial derivatives with respect to phix and phiy
	def Hx(self, dx, dy): return(dx*(1/np.sqrt(self.eps+dx**2+dy**2)))
	def Hy(self, dx, dy): return(dy*(1/np.sqrt(self.eps+dx**2+dy**2)))

	# Defining the primary cost functions entering in the optimisation problem and in the
	# Bregman splitting, depending on J
	def J(self, Phi, x):  return(np.abs(Phi(x)))


	# ********************************************************************************
	# *  Defining the helper routines for the gradient computations                  *
	# ********************************************************************************
	#### Definition of the weno weights ####
	def W(self, a,b,c,d, eps):
		"""Function defining the weno weigths.

		Args:
			a,b,c,d (float):   The parameters to pass in order to compute the weno weights
			eps    (float):    The spacing to consider when selecting the neighborhing points in the gradient approximation (same in both x and y direction)

		Returns:
			Weight  (float):   The Weno weight associated to the given coefficients
		"""

		# Computing the weno coefficients
		alph0 = 1/((eps+13*(a-b)**2+3*(a-3*b)**2)**2)
		alph1 = 6/((eps+13*(b-c)**2+3*(b+c)**2)**2)
		alph2 = 3/((eps+13*(c-d)**2+3*(3*c-d)**2)**2)

		# Computing and returning the weno weights
		w0 = alph0/(alph0+alph1+alph2)
		w2 = alph2/(alph0+alph1+alph2)
		return(1/3*w0*(a-2*b+c)+1/6*(w2-0.5)*(b-2*c+d))

	#### Function that compute the differences involved in the Weno's derivative computations ####
	def Deltas(self, Phi, Points, eps):
		"""Simple helper function that compute the differences involved in the Weno's derivative approximation

		Args:
			Phi    (Function callback): An interpolation of the function to get the derivatives for
			Points (float NbPoints x2): The list of points where to consider the function for its interpolation
			eps    (float):             The spacing to consider when selecting the neighborhing points in the gradient approximation (same in both x and y direction)

		Returns:
			Differences  (numpy arrays tuple): Returns a tuple containing in order:
			 							- DeltaxMPhi    (NbPoints float):  Backward difference in x
										- DeltayMPhi    (NbPoints float):  Backward difference in y
										- DeltaxPPhi    (NbPoints float):  Forward difference in x
										- DeltayPPhi    (NbPoints float):  Forward difference in y
										- DDeltaxMPPhi  (NbPoints float):  Backward Forward difference in x
										- DDeltayMPPhi  (NbPoints float):  Backward Forward difference in y
		"""

		# Initalising the problem data and defining the spacing used to compute the derivatives numerically
		NbPoints = np.shape(Points)[0]
		epsx, epsy = np.zeros(np.shape(Points)), np.zeros(np.shape(Points))
		epsx[:,0] = eps
		epsy[:,1] = eps

		# Computing the first order differences
		DeltaxPPhi = Phi(Points+epsx) - Phi(Points)
		DeltaxMPhi = Phi(Points) - Phi(Points-epsx)
		DeltayPPhi = Phi(Points+epsy) - Phi(Points)
		DeltayMPhi = Phi(Points) - Phi(Points-epsy)

		# Computing the second order differences
		DDeltaxMPPhi = np.zeros(NbPoints)
		DDeltayMPPhi = np.zeros(NbPoints)
		DDeltaxMPPhi = Phi(Points+epsx) - 2*Phi(Points-epsx) + Phi(Points-epsx)
		DDeltayMPPhi = Phi(Points+epsy) - 2*Phi(Points-epsy) + Phi(Points-epsy)

		# Returning the obtained differences
		return(DeltaxMPhi, DeltayMPhi, DeltaxPPhi, DeltayPPhi, DDeltaxMPPhi, DDeltayMPPhi)

	##### Routine approximating the gradient at the given points #####
	def GetGradient(self, Phi, Points, eps):
		""" Numerical gradient computation according to the WENO derivative computations

		Args:
			Points (float NbPoints x2): The list of points where to consider the function for its interpolation
			Phi    (Function callback): An interpolation of the function to get the derivatives for
			eps    (float):             The spacing to consider when selecting the neighborhing points in the gradient approximation (same in both x and y direction)

		Returns:
			Grad   (2d numpy array):   Returns np.array([Gradx, Grady]) where Gradx and Grady are as follows.
										- Gradx  (NbPoints float): gradient value in the x direction at all the given points
										- Grady  (NbPoints float): gradient value in the y direction at all the given points
		"""

		# Initalising the problem data and defining the spacing used to compute the derivatives numerically
		NbPoints = np.shape(Points)[0]
		epsx, epsy = np.zeros(np.shape(Points)), np.zeros(np.shape(Points))
		epsx[:,0] = eps
		epsy[:,1] = eps

		# Getting the weno differences at the weno evaluation points for the x derivative
		DeltaxMPhim2, _, DeltaxPPhim2, _, DDeltaxMPPhim2, _ =  self.Deltas(Phi, Points-2*epsx, eps)
		DeltaxMPhim1, _, DeltaxPPhim1, _, DDeltaxMPPhim1, _ =  self.Deltas(Phi, Points-1*epsx, eps)
		DeltaxMPhi0,  _, DeltaxPPhi0,  _, DDeltaxMPPhi0,  _ =  self.Deltas(Phi, Points       , eps)
		DeltaxMPhip1, _, DeltaxPPhip1, _, DDeltaxMPPhip1, _ =  self.Deltas(Phi, Points+1*epsx, eps)
		DeltaxMPhip2, _, DeltaxPPhip2, _, DDeltaxMPPhip2, _ =  self.Deltas(Phi, Points+2*epsx, eps)

		# Getting the weno differences at the weno evaluation points for the y derivative
		_, DeltayMPhim2, _, DeltayPPhim2, _, DDeltayMPPhim2 =  self.Deltas(Phi, Points-2*epsy, eps)
		_, DeltayMPhim1, _, DeltayPPhim1, _, DDeltayMPPhim1 =  self.Deltas(Phi, Points-1*epsy, eps)
		_, DeltayMPhi0,  _, DeltayPPhi0,  _, DDeltayMPPhi0  =  self.Deltas(Phi, Points,        eps)
		_, DeltayMPhip1, _, DeltayPPhip1, _, DDeltayMPPhip1 =  self.Deltas(Phi, Points+1*epsy, eps)
		_, DeltayMPhip2, _, DeltayPPhip2, _, DDeltayMPPhip2 =  self.Deltas(Phi, Points+2*epsy, eps)

		# Computing the gradient approximations
		dxPphi = 1/(12*eps)*(-DeltaxPPhim2+7*DeltaxPPhim1+7*DeltaxPPhi0-DeltaxPPhip1)\
		         +self.W(DDeltaxMPPhip2/eps, DDeltaxMPPhip1/eps, DDeltaxMPPhi0/eps, DDeltaxMPPhim1/eps, 1e-8)
		dxMphi = 1/(12*eps)*(-DeltaxPPhim2+7*DeltaxPPhim1+7*DeltaxPPhi0-DeltaxPPhip1)\
		         -self.W(DDeltaxMPPhim2/eps, DDeltaxMPPhim1/eps, DDeltaxMPPhi0/eps, DDeltaxMPPhip1/eps, 1e-8)
		dyPphi = 1/(12*eps)*(-DeltayPPhim2+7*DeltayPPhim1+7*DeltayPPhi0-DeltayPPhip1)\
		         +self.W(DDeltayMPPhip2/eps, DDeltayMPPhip1/eps, DDeltayMPPhi0/eps, DDeltayMPPhim1/eps, 1e-8)
		dyMphi = 1/(12*eps)*(-DeltayPPhim2+7*DeltayPPhim1+7*DeltayPPhi0-DeltayPPhip1)\
		         -self.W(DDeltayMPPhim2/eps, DDeltayMPPhim1/eps, DDeltayMPPhi0/eps, DDeltayMPPhip1/eps, 1e-8)

		# Returning the approximated gradient values
		Gradx = 0.5*(dxMphi+dxPphi)
		Grady = 0.5*(dyMphi+dyPphi)
		return(np.array([Gradx, Grady]))

	# ********************************************************************************
	# *  Defining the internal routines                                              *
	# ********************************************************************************
	#### Routine creating the interpolator based on the given Phi #####
	def InterpolatePhi(self, Points, Phi, *args):
		"""
		Interpolator that considers either a function Phi or a set of Points and point-values Phi and returns an interpolator function.
		It also returns the Gradient interpolator of the function contructed from a gradient approximation at the given Points.

		Args:
			Points (float NbPoints x2):                   The list of points where to consider the function for its interpolation
			Phi    (Function callback or NbPoints float): Level set to reinitialise, either given as a function or a list of point values associated to points
			args   (Two function callback, optional):     The callback function defining the partial derivatives in x and y, in that order

		Returns:
			InterpPhi   (function callback):   The interpolator of the function Phi
			InterpPhiDx (function callback):   The interpolator of the derivative in x of Phi
			InterpPhiDy (function callback):   The interpolator of the derivative in y of Phi

		.. note::

			If function callbacks are given, they should be 2d->IR, taking the xy-values as a column list (NbPoints x 2) and return a 1d numpy vector
		"""

		# ------------------------- Preinitialisations -----------------------------------------------------
		# Set the dx in order to go to the the surrounding points according to the mesh size
		hmin     = dist.pdist(Points).min()

		# ------------------------- Define the interpolator function ----------------------------------------
		# In case of sparse data, add the maximum as a fill value to force comming back to the considered domain
		if type(Phi)==types.FunctionType: InterpPhi = lambda x: Phi(copy.deepcopy(x))
		else:
			Interp = ind.LinearNDInterpolator((Points[:,0], Points[:,1]), Phi, fill_value=np.max(abs(Phi))+0.1)
			InterpPhi = lambda x: Interp(copy.deepcopy(x))

		# ------------------------- Define the interpolator function ----------------------------------------
		# Define the gradient interpolator functions if we have a direct access
		if len(args)==2 and type(args[0])==types.FunctionType and type(args[1])==types.FunctionType:
			InterpPhiDx = lambda x: args[0](copy.deepcopy(x))
			InterpPhiDy = lambda x: args[1](copy.deepcopy(x))

		# Approximate the gradient otherwise
		else:
			# Define a security box around the given points to compute the gradient within the Interpolator convex hull
			minx, miny = np.min(Points[:,0]), np.min(Points[:,1])
			maxx, maxy = np.max(Points[:,0]), np.max(Points[:,1])
			pp = np.array([[minx-3*self.gtol, miny-3*self.gtol],[minx-3*self.gtol, maxy+3*self.gtol],[maxx+3*self.gtol, miny-3*self.gtol],[maxx+3*self.gtol, maxy+3*self.gtol]])
			Points = np.append(Points, pp, axis=0)

			# Evaluate the derivatives from the interpolator
			grad = self.GetGradient(InterpPhi, Points, hmin)

			# Create the interpolator of the derivatives
			InterpDx   = ind.LinearNDInterpolator((Points[:,0], Points[:,1]), grad[0,:])
			InterpDy   = ind.LinearNDInterpolator((Points[:,0], Points[:,1]), grad[1,:])
			InterpPhiDx = lambda x: InterpDx(copy.deepcopy(x))
			InterpPhiDy = lambda x: InterpDy(copy.deepcopy(x))

		# Returning the quantities of interest
		return(InterpPhi, InterpPhiDx, InterpPhiDy)

	# ********************************************************************************
	# *  Defining the main redistancing routine aimed to be called externally        *
	# ********************************************************************************
	def Redistance(self, Points, Phi, *args, **kwargs):
		"""Implementation of the main iteration strategy of the Bregman split for the redistancing
		problem.

		Args:
			Points (float 2D numpy array):     The points on which reinitialise the level-set on (NbPoints x 2)
			Phi    (function callback):        The (initial) level set function to redistance (should return a scalar value),
			                                   given either as a float list associated to the points or as a function callback
			args   (Two function callback):    (optional) The callback function defining the partial derivatives in x and y, in that order
			kwargs (keyword argument):         The only keyword argument available is "ConstructionPoints", only active when Phi is given as
			                                   a float list. It corresponds to where the given point values Phi have been computed. If the
			                                   argument is not passed when Phi is a float list, the function assumes that Phi has been constructed
			                                   on Points. Note that the Points should be contained within the convex hull of ConstructionPoints.

		Returns:
			RPhi  (float numpy array):  The value of the redistanced level set function at the dofs (NbDofs)
		"""

		# -------------------- Initialise the problem -------------------------------------------------------
		# Getting the information of the problem
		NbPoints = np.shape(Points)[0]
		hmin     = dist.pdist(Points).min()

		# Initialising the user warning and the counter in case of the method's failure
		gfailed, sfailed = 0,0
		gkt,     skt     = 0,0

		# Scanning the keyword argument to see if Phi has been constructed on different points than those asked
		InterpPoints = Points
		for args,val in kwargs.items():
			if args == "ConstructionPoints": InterpPoints = val

		# Getting the linearly interpolated Level Set function and its gradient
		# (note: if Phi is a Lambda function, InterpPhi=Phi and the gradient is
		#        interpolated from the points where Phi will be redistanced)
		InterpPhi, InterpPhiDx, InterpPhiDy = self.InterpolatePhi(InterpPoints, Phi, *args)

		# Initialise the level set over the computational domain and the iterates
		Phi0 = InterpPhi(Points)		# Initial and iteration level set values
		Phi1 = np.zeros((NbPoints))		# Next iteration step level set values
		RPhi = np.zeros((NbPoints))		# Redistanced level set values

		# Set the initial values of the Bregman split
		b = np.zeros((NbPoints,2))
		v = np.zeros((NbPoints,2))
		d = np.zeros((NbPoints,2))

		# --------------- Redistance at each point independently --------------------------------------------------------
		# Loop over all the points to redistance (independent to each other)
		for i in range(NbPoints):

			# --- Initialisation of the time-iteration
			# Initialising the counter of the secant iterations
			skt = 0

			# Define the first time step to define the first ball on in the minimisation problem
			t  = 0
			Dt = self.InitTime*hmin
			tR, tL = None, 0

			# Do the specific flip treatment for the points where Phi<0
			if np.sign(Phi0[i])==-1:
				Interp    = lambda x:  -InterpPhi(np.array([np.squeeze(x)]))
				InterpDx  = lambda x:  -InterpPhiDx(np.array([np.squeeze(x)]))
				InterpDy  = lambda x:  -InterpPhiDy(np.array([np.squeeze(x)]))
				signt     = -1

			else:
				Interp    = lambda x:  InterpPhi(np.array([np.squeeze(x)]))
				InterpDx  = lambda x:  InterpPhiDx(np.array([np.squeeze(x)]))
				InterpDy  = lambda x:  InterpPhiDy(np.array([np.squeeze(x)]))
				signt     = 1

			# Prepare the optimiser function
			grad = lambda x:np.array([InterpDx(x), InterpDy(x)]).T
			Opti = GM.Optimiser()

			# --- Initialisation of PhiIt
			# Perform the first iteration by hand to initialise the secant iteration process
			PhiIt = np.squeeze(Interp(Points[[i],:]))

			# --- Perfom the iteration in pseudo-time with a modified bisection to cope with the unilateral problem
			# Selecting the starting point of the iteration
			x = Points[i,:]
			# Loop until we reach a negative point value as upper bound for t
			while PhiIt>0 and skt <self.smaxit:
				# Update the incremental values
				skt = skt+1
				t = t + Dt
				# Solve the optimisation problem
				x     = Opti.Optimise(Interp, grad, x, Points[i,:], t)
				PhiIt = Interp(x)
			tR = t

			# Loop until we reach our tolerance criterion
			while (np.abs(PhiIt)>self.stol) and skt<self.smaxit:
				# Incrementing the secant iteration counter
				skt = skt+1

				# Updating the current t value giving the redistancing
				if PhiIt>0: tL = t
				else:       tR = t
				t = 0.5*(tR+tL)

				# Solve the optimisation problem
				x = Opti.Optimise(Interp, grad, x, Points[i,:], t)

				# Registering a failure case if any
				PhiIt = Interp(x)

			# ----- When converged, update the redistanced level set
			# Safe check in case of failure in gradient evaluation or secant
			if not (np.isnan(t) or np.isinf(t)): RPhi[i] = signt*t
			else: RPhi[i] = InterpPhi(Points[i,:])

			# If not converged, register it as a failed
			if skt == self.gmaxit: sfailed =1

			# Informing the user if he wishes so
			if self.verbose: print("Computed for Dofs ", i,": ", Points[i,:],"\t Redistanced value", t, "\t Convergence of the iterate:",  PhiIt, skt)

		# Informing the user of potential failures
		if sfailed: print("WARNING: At least one point encountered a non-convergence of the secant method to the wished tolerance. Results may be innacurate.")
		if gfailed: print("WARNING: At least one point encountered a non-convergence of the gradient descent to the wished tolerance, most likely at the boundary if the given data is discrete. Results may be innacurate.")

		# Return the redistanced level set values at the Dofs
		return(RPhi)
