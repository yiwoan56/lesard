#***************************************************************************
# Copyright (C) 2020 Élise Le Mélédo
# This file is part of LESARD.
#
# LESARD is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LESARD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LESARD.  If not, see <https://www.gnu.org/licenses/>.
#****************************************************************************

#Module that procures interfaces from the scheme to the time stepper
#when used within a residual distribution framework.


# ==============================================================================
#	Preliminary imports
# ==============================================================================
# Pythonic import
import numpy as np
import copy

# Custom import (has to be precisely imported, BCs are not included in the __all__)
import _Solver.Solver_ApplyBCs as BCs


# ==============================================================================
#	Core routines
# ==============================================================================

#### Integrate the RD formulation in the Dec setting ####
def RD(Theta, m, M, dt, schemeSP, schemeFS, Problem, Mesh, Solution, up, lp):
    """ Integrates the RD formulation in the Dec setting:  interfaces from the scheme to the time stepper when used within a residual distribution framework

    Args:
        Theta (float numpy array):   coefficients of the DeC routine
        m     (integer):             current subtimestep of the DeC correction
        M     (integer):             total number of DeC corrections
        dt    (float):               the total current time step duration

        schemeSP (scheme module list):  handler of the spatial scheme module, followed by its amendements
        schemeFS (scheme module list):  handler of the FluidSpotter scheme module (the scheme should already contain all the wished amendments (limiter, jumps etc))
        Problem  (Problem):             considered problem
        Mesh     (MeshStructure):       considered mesh
        Solution (Solution):            snapshot of the solution at the previous full time step
        up       (float numpy array):   time-local increment in the solution coming from DeC
        lp       (float numpy array):   time-local increment in the FluidSpotter values coming from DeC

    Returns:
        Residuals (3D numpy array):     being of the form [upres (NbVarsxNbDofs), lsres(NbFluidsxNbDofs)], each term corresponds to the residuals of the state and the fluid's spotters, respectively.

    """

    # --------------------- Initialisations -----------------------------------------------
    # Intialising the value which will be considered at this time step
    SolBuff = copy.deepcopy(Solution)

    # Initialising the total residual vectors
    upres = np.zeros(np.shape(Solution.Sol))
    lsres = np.zeros(np.shape(Solution.LSValues))

    # ------------------ Incrementing according to the DeC approximation ------------------
    # Computing the temporal increment coming from dec
    dup = up[:,:,m]-up[:,:,0]
    dlp = lp[:,:,m]-lp[:,:,0]

    # Computing the temporally local flux and the infinitesimal update to the solution
    fincr = np.sum(np.array([Theta[r,m]*schemeSP[0].ComputeFlux(schemeFS[0].FlagPoints, up[:,:,r], lp[:,:,r]) for r in range(M+1)]), axis = 0)
    sincr = np.sum(np.array([Theta[r,m]*up[:,:,r] for r in range(M+1)]), axis = 0)

    # Computing the temporally local flux and the correction to the function
    lincr = np.sum(np.array([Theta[r,m]*schemeFS[0].ComputeFlux(up[:,:,r], lp[:,:,r]) for r in range(M+1)]), axis = 0)
    vincr = np.sum(np.array([Theta[r,m]*lp[:,:,r] for r in range(M+1)]), axis = 0)

    # ------------------ Applying the dynamic to the iterated values ------------------
    # Update fluid flag upon the new value of the level sets and filling a buffer structure
    SolBuff.LSValues = vincr
    SolBuff.Sol      = sincr
    schemeFS[0].FlagDofs(SolBuff)

    # Applying the LS and Dynamic schemes upon the time-locally iterated solution, element-wise
    # (the split on the elements already at this level is required by the possible mollification
    #  of the residual by limiters/streamline stabilisation)
    for i in range(Mesh.NbInnerElements):
        # Computing the initial residual with the given scheme
        resu = schemeSP[0].Iteration(schemeFS[0].FlagPoints, SolBuff, fincr, i, dup, dt)[:]
        resl = schemeFS[0].Iteration(SolBuff, lincr, i, dlp, dt)[:]

        # Mollifying the obtained partial residuals by a the wished techniques if applicable
        resu = schemeSP[1].Mollify(schemeFS[0].FlagPoints, SolBuff, resu, fincr, i, dup, dt)

        # Mapping the partial residuals to the total one
        dele = np.array(Mesh.ElementsDofs[i])
        upres[:, dele] += resu
        lsres[:, dele] += resl

    # ------------------ Applying the boundary conditions over the freshly computed residuals ------------------
    upres = BCs.ApplyBCSState(Problem, Mesh, SolBuff, upres)

    # ------------------ Applying the dynamic to the iterated values ------------------
    # Updating each Dof (here representing the average) by the residual
    # pondered (and lumped) by the control volume's area
    upres = upres*Mesh.Lumping
    lsres = lsres*Mesh.Lumping

    # Retuning the residuals'increments (not the new value of the solution)
    return([upres, lsres])
