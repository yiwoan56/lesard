#***************************************************************************
# Copyright (C) 2020 Élise Le Mélédo
# This file is part of LESARD.
#
# LESARD is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LESARD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LESARD.  If not, see <https://www.gnu.org/licenses/>.
#****************************************************************************

# ==============================================================================
#	Preliminary imports
# ==============================================================================
import numpy as np
import copy

# Import custom modules
import BarycentricBasisFunctions         as BF
import BarycentricTools                  as BT

# ==============================================================================
#	Parameter (and reader) class of scheme properties and runtime informations
# ==============================================================================
# Name of the module to be externally accessible
SchemeName = "Continuous Galerkin"

# Mapping to the mesh that should be loaded
class AssociatedMeshType():
	""" .. |bs|   unicode:: U+00A0 .. NO-BREAK SPACE

	Class that only gives out the mesh type that should be loaded when tackling the problem
	with this scheme, given the wished variational order. Access the associated mesh properties
	through the inner variables

		- |bs| **MeshType**  *(string)*   -- the mesh type according to fenics's classification
		- |bs| **MeshOrder** *(integer)*  -- the mesh order

	|

	.. Note::

		This is only a product-type class: no method is available

	|
	"""

	#### Automatic initialisation routine ####
	def __init__(self, SchemeParams = [1]):
		""" .. |bs|   unicode:: U+00A0 .. NO-BREAK SPACE
		Args:
			SchemeParams (string list, optional):   the parameters of the spatial scheme wished by the user

        """

		# Extracting the schemes properties that are required to determine the Mesh
		VariationalOrder = int(SchemeParams[0])

		# Selecting the corresponding mesh type
		self.MeshType  = "CG"
		self.MeshOrder = VariationalOrder
		self.ControlVolumes = "Dual"


# ==============================================================================
#	Class furnishing all the scheme tools
# ==============================================================================
class Scheme():
	""".. |bs|   unicode:: U+00A0 .. NO-BREAK SPACE


	Class furnishing all the schemes tools and registers the scheme's wished
	parameters. The main iteration is accessible with the method "Iteration".

	Repeats the instance's initialisation arguments as fields with identical names
	within the structure. Has for additional field upon instance creation the fields:

		- |bs| **Order**              *(integer)*           -- The order of the used basis functions
		- |bs| **InnerQBFValues**     *(float numpy array)* -- Value of the basis function at the quadrature points of each element (NbInnerElementsx NbQuadraturePoints x NbBasisFunc)
		- |bs| **InnerQWeigths**      *(float numpy array)* -- Weights of the quadrature points of each element (NbInnerElements x NbQuadraturePoints)
		- |bs| **InnerQGFValues**     *(float numpy array)* -- Value of the gradient of the basis function at the quadrature points of each element (NbInnerElements x NbQuadraturePoints x NbBasisFunc x dim)
		- |bs| **InnerQPoints**       *(float numpy array)* -- Quadrature points of each element (NbInnerElements x NbQuadraturePoints)
		- |bs| **FaceWiseQWeigths**   *(float numpy array)* -- Weights of the quadrature points of each element's face (NbInnerElements x NbFace x NbQuadraturePoints)
		- |bs| **FaceWiseQPoints**    *(float numpy array)* -- Quadrature points of each element's face (NbInnerElements x NbFace x NbQuadraturePoints)
		- |bs| **FaceWiseQBFValues**  *(float numpy array)* -- Value of the basis function at the quadrature points of each element's face (NbInnerElements x NbFace x NbQuadraturePoints x NbBasisFunc)

	|
	"""

	#### Automatic initialisation routine ####
	def __init__(self, Problem, Mesh, QuadratureRules, *SchemeParams):
		"""
		Args:
			Problem           (Problem):       the considered problem
			Mesh              (MeshStructure): the considered mesh instance
			QuadratureRules   (Quadratures):   the quadratures to use within the scheme.
			SchemeParams      (string list):   the scheme's parameters as wished by the user

		|

		.. rubric:: Methods

		"""

		# Storing internally the associated instances
		self.Problem = Problem
		self.Mesh    = Mesh
		self.QuadratureRules = QuadratureRules

		# Extracting the parameters from the user input list
		if len(SchemeParams)==2:
			self.Order            = int(SchemeParams[0])
			self.VariationalType  = SchemeParams[1]

		else: raise ValueError("Wrong number of user input argument when definining the spatial scheme. Check you problem definition.")

		# Precomputing the basis function values at quadrature points for fastening the run time
		self.PreEvaluateQuadratureQuantities()

	#### Emulates a flux computation as done in the iteration routine ####
	def ComputeFlux(self,  FlagPoints, U, LSValues):
		""" Emulates a flux computation as done in the iteration routine in order to be used in the DeC subtimestep evaluation.

		Args:
			FlagPoints   (function callback):  mapper to the routine of the FluidSpotter FlagPoints method to flag the Fluid on given points according the given LSValues at Dofs
			U            (numpy float array):  buffer containing the current state values at each dof (NbVars   x NbDofs)
			LSValues     (numpy float array):  buffer containing the current FluidSpotters values at each dof(NbFluids x NbDofs)

		Returns:
			fluxes     (numpy float (multiD)array): fluxes in the format required by the Iteration routine
		"""

		# --------------------------  Initialisations -------------------------------------------------
		# Computing the quantities for allocating the flux vector big enough (dirty hack for vectorialisation)
		MaxInnerQp = np.max([len(self.InnerQWeigths[i])       for i in range(0, self.Mesh.NbInnerElements)])													# Computing the space needed for the inner quadrature points
		MaxBdQp    = np.max([len(self.FaceWiseQWeigths[i][f]) for i in range(0, self.Mesh.NbInnerElements) for f in range(len(self.Mesh.InnerElements[i]))])	# Computing the space needed for the boundary quadrature points

		# Defining the output fluxes (inner and boundary ones)
		# The very first coordinate corresponds to the inner flux. Only Flux[0, :, 0, :, :, :] is filled
		# The second coordinate corresponds to the inner flux. Only Flux[1, :, :, :, :, 0:3] is filled
		Flux  = np.zeros((3, self.Mesh.NbInnerElements, 3, 2, self.Problem.GoverningEquations.NbVariables, np.max([MaxInnerQp, MaxBdQp, *[len(dele) for dele in self.Mesh.ElementsDofs]])))

		# ------------- Flux computation at the control's volume interface ----------------------------
		# Spanning on each element
		for i in range(self.Mesh.NbInnerElements):
			# ---- Getting the element and state informations
			# Retrieving the physical information on the spanned element's vertices
			ele = self.Mesh.InnerElements[i]
			vol = self.Mesh.InnerElementsVolume[i]
			n   = self.Mesh.ElementsBoundaryWiseNormals[i]

			# Retrieving the information on the local degree of freedom
			dele  = np.array(self.Mesh.ElementsDofs[i])
			UU    = U[:, dele]
			Flags = FlagPoints(LSValues, i)

			# Get the coordinates of the vertices in the physical order
			elevert  = self.Mesh.CartesianPoints[ele,:]
			delevert = self.Mesh.DofsXY[dele,:]

			# ---- Computing the flux at the Dofs directly
			Flux[2, i, 0, :, :, 0:len(dele)] = self.Problem.GoverningEquations.Flux(UU, delevert, Flags)

			# ---- Computing the plain integral
			# Retrieving the quadrature points as barycentric coordinates and convert them to cartesian
			# in the Dof's vertex ordering referential
			bqp      = self.InnerQPoints[i]
			weights  = self.InnerQWeigths[i]
			qp = BT.GetCartesianCoordinates(bqp, elevert)

			# Evaluate the basis function in the order of the Dofs
			EvaluatedBase  = self.InnerQBFValues[i]

			# Reconstructing the solution and its gradient at the quadrature point
			Uloc     = np.dot(UU,  EvaluatedBase.T)
			FlagsLoc = FlagPoints(LSValues, i, bqp)

			# Compute the flux according to the fluid's flag
			Flux[0, i, 0, :, 0:len(self.InnerQWeigths[i])] = self.Problem.GoverningEquations.Flux(Uloc, qp, Flags)

			for face in range(len(ele)):
				# Get the coordinates of the quadrature points on the edge, flipping the
				# points to match the Dof's order with the physical vertices
				qbbp    = self.FaceWiseQPoints[i][face]
				weights = self.FaceWiseQWeigths[i][face]
				bqp = BT.GetCartesianCoordinates(qbbp, elevert)

				# Compute each basis function value at those points
				EvaluatedBase = self.FaceWiseQBFValues[i][face]

				# Reconstruct the solution's value at this point
				Uloc  = np.dot(UU,  EvaluatedBase.T)

				# Evaluate the flux there
				FlagsLoc = FlagPoints(LSValues, i, qbbp)
				Flux[1,i,face,:,:,0:len(self.FaceWiseQWeigths[i][face])] = self.Problem.GoverningEquations.Flux(Uloc, bqp, FlagsLoc)

		# Returning the full array
		return(Flux)

	#### Main routine, definining the scheme ####
	def Iteration(self, FlagPoints, Solution, fluxes, i, du=0, dt=1):
		""" Main iteration of the scheme, implementing the most stupid scheme
		you can think of.

		Args:
			FlagPoints  (function callback):    the handle of a function flagging the points to the relevant fluid (not used here)
			Solution    (solution structure):   structure containing the current solution's values to iterate
			fluxes      (numpy (multiD)array):  pre-computed fluxes at the points of interest. For this scheme, access with fluxes[element, face, coordinate(fx or fy), variable, pointindex]
			i           (integer):              the index of the considered element within which the partial residuals will be computed
			du          (float numpy array):    (optional) when using DeC, the difference in the time iteration
			dt          (float):                (optional) when using DeC, the full time step

		Returns:
			Resu        (float numpy array):    the computed residuals (NbVars x NbDofs)
		"""


		# -------- Initialisation ------------------------------------------------
		# Getting the dofs contaned in the considered element
		dele   = np.array(self.Mesh.ElementsDofs[i])
		NbDofs = len(dele)

		# Getting the info about the number of quadrature points
		NbInnerQp = len(self.InnerQWeigths[i])
		NbBdQp    = [len(self.FaceWiseQWeigths[i][f]) for f in range(len(self.Mesh.InnerElements[i]))]

		# Initialising the mollification vector (residuals)
		Resu  = np.zeros([np.shape(Solution.Sol)[0], NbDofs])


		# ------- Getting the CG residuals for each element -------------------------

		# ---- Getting the element and state informations
		# Retrieving the physical information on the spanned element's vertices
		ele = self.Mesh.InnerElements[i]
		vol = self.Mesh.InnerElementsVolume[i]
		n   = self.Mesh.ElementsBoundaryWiseNormals[i]

		# Retrieving the information on the local degree of freedom
		LS   = Solution.LSValues[:, dele]
		U    = Solution.Sol[:, dele]
		dU   = du[:, dele]

		# ---- Computing the plain integral
		# Retrieving the quadrature points as barycentric coordinates and convert them to cartesian
		# in the Dof's vertex ordering referential
		bqp      = self.InnerQPoints[i]
		weights  = self.InnerQWeigths[i]

		# Evaluate the basis function in the order of the Dofs
		EvaluatedBase      = self.InnerQBFValues[i]
		EvaluatedGradients = self.InnerQGFValues[i]

		# Reconstructing the solution and its gradient at the quadrature point
		dUloc = np.dot(dU, EvaluatedBase.T)

		# Compute the flux according to the fluid's flag
		Flux = fluxes[0, i, 0, :, 0:NbInnerQp]

		# Complete the resodual at each Dof by the relevant quadrature value
		for dof in range(NbDofs):
			# Evaluating the flux at the quadrature point for the reconstructed flux variable
			qt  = np.sum([weights[q]*(EvaluatedGradients[dof,q,0]*Flux[0,:,q]+EvaluatedGradients[dof,q,1]*Flux[1,:,q]) for q in range(len(weights))], axis=0)
			DEC = np.sum( weights[:]*EvaluatedBase[:,dof]*dUloc, axis=1)

			# Updating the residual at the spanned Dof by the local contribution
			Resu[:,dof] += (DEC - dt*qt)*vol

		# ---- Computing the boundary integral
		# Spanning each edge in the physical element
		for face in range(len(ele)):
			# Get the coordinates of the quadrature points on the edge, flipping the
			# points to match the Dof's order with the physical vertices
			qbbp    = self.FaceWiseQPoints[i][face]
			weights = self.FaceWiseQWeigths[i][face]

			# Compute each basis function value at those points
			# (get the order of the basis in the order of the physical vertices:
			# the dele have to be ordered in the same output as the evaluated base:
			# check also for higher order the order of the dele given in the mesh and
			# the order of the basis function given here)
			EvaluatedBase = self.FaceWiseQBFValues[i][face]

			# Evaluate the flux there
			Flux = fluxes[1,i,face,:,:,0:NbBdQp[face]]

			# Compute the flux's normal contribution to each dof
			Qt = (Flux[0,:,:]*n[face,0]+Flux[1,:,:]*n[face,1])
			WeigthedBaseWiseQt = np.array([weights[pt]*np.array([Qt[:,pt]]).T*np.array([EvaluatedBase[pt,:]]) for pt in range(len(weights))])
			PonderedQt = np.sum(WeigthedBaseWiseQt, axis=0)

			# Adjust the residuals
			Resu[:,:] += dt*PonderedQt

		# Returning the partial residuals
		return(Resu)

	#### Routine that maps the values from the Dofs to the Physical vertices of the mesh ####
	def ReconstructSolutionAtVertices(self, Solution):
		""" Routine that maps the values from the Dofs to the Physical vertices of the mesh
		of the solution.

		Args:
			Solution  (Solution):  the currently being computed solution

		Returns:
			None: fills direclty the RSol and RFluidFlag values in the data structure

		.. Note::

			This is only for later plotting purposes.
		"""

		# -------------- Initialising the reconstructed values ----------------------------
		Solution.RSol = np.zeros((np.shape(Solution.Sol)[0], self.Mesh.NbMeshPoints))

		# ------------- Reconstructing the values -----------------------------------------
		# Extract the cartesian coordinates of the Dofs
		xyDofs = self.Mesh.DofsXY[:,:]

		# Spanning all the mesh points where to fill the values
		for i in range(self.Mesh.NbMeshPoints):

			# Averaging all the Dofs values located at the corresponding physical vertex
			index = self.Mesh.Vertex2Dofs[i]
			RSO = np.mean(Solution.Sol[:,index], axis=1)

			# Fill the reconstructed solution upon the found index
			Solution.RSol[:,i]      = RSO


	# **************************************************************
	# *  Definition of the Fluid spotter's scheme basis function   *
	# **************************************************************

	# Precomputing and storing the values of the basis functions at the quadrature points of each element
	def PreEvaluateQuadratureQuantities(self):
		"""
		Precomputing and storing the values of the basis functions at the quadrature points of each element

		Args:
			None:  Considers the given parameters of the class directly

		Returns:
			None: Creates the fields InnerBFValues and FaceWiseBFValues in the scheme structure, containing
			      the values of each basis function at each quadrature point for each element and face.
		"""

		# Initialising the long-term storing vectors
		self.InnerQBFValues    = [[]]*self.Mesh.NbInnerElements
		self.InnerQWeigths     = [[]]*self.Mesh.NbInnerElements
		self.InnerQGFValues    = [[]]*self.Mesh.NbInnerElements
		self.InnerQPoints      = [[]]*self.Mesh.NbInnerElements
		self.FaceWiseQWeigths  = [[]]*self.Mesh.NbInnerElements
		self.FaceWiseQPoints   = [[]]*self.Mesh.NbInnerElements
		self.FaceWiseQBFValues = [[]]*self.Mesh.NbInnerElements

		# Spanning each element
		for i in range(self.Mesh.NbInnerElements):

			# Get the elements properties
			ele = self.Mesh.InnerElements[i]
			vol = self.Mesh.InnerElementsVolume[i]
			n   = self.Mesh.ElementsBoundaryWiseNormals[i]

			# Get the type of the element through its vertices numbers
			self.FaceWiseQBFValues[i] = [[]]*len(ele)
			self.FaceWiseQWeigths[i]  = [[]]*len(ele)
			self.FaceWiseQPoints[i]   = [[]]*len(ele)

			# Computing the basis functions values at the inner quadrature points
			bqp, weights = self.QuadratureRules.InnerQuadrature(len(ele))
			self.InnerQPoints[i]   = bqp
			self.InnerQWeigths[i]  = weights
			self.InnerQBFValues[i] = BF.BarycentricBasis    (len(ele), self.VariationalType, self.Order, bqp)
			self.InnerQGFValues[i] = BF.BarycentricGradients(len(ele), self.VariationalType, self.Order, bqp, n, vol)

			# Computing per face the basis functions values at the edge's quadrature point
			for face in range(len(ele)):
				qbbp, weights = self.QuadratureRules.BoundaryQuadrature(len(ele), face)
				self.FaceWiseQPoints[i][face]   = qbbp
				self.FaceWiseQWeigths[i][face]  = weights
				self.FaceWiseQBFValues[i][face] = BF.BarycentricBasis(len(ele), self.VariationalType, self.Order, qbbp)
