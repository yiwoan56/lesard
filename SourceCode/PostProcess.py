#***************************************************************************
# Copyright (C) 2020 Élise Le Mélédo
# This file is part of LESARD.
#
# LESARD is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LESARD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LESARD.  If not, see <https://www.gnu.org/licenses/>.
#****************************************************************************

"""

The predefined set of exports plots are given in wrapper routines located in the file  :code:`PostProcess` under the
:code:`_Solver` folder. This is the main post-processing file that proposes predefined post-treatment tasks

|

"""

# ==============================================================================
#	Preliminary imports
# ==============================================================================
# Import Python os-mapping libraries and set the runtime error behavior
import sys, os, glob
import re

# Custom the system traceback and allow the Pathes to be imported when this file is ran
# from another location than its storage.
sys.path.append(os.path.dirname(__file__))
sys.tracebacklimit = None

# Import math tools
import numpy as np

# Add to python path the location of the modules and meshes
import Pathes

# Import local modules (everything for smooth pickles of binary objects)
import PostProcessing.Plots_Matplotlib  as PMP
import PostProcessing.Plots_Schlieren   as PSL
import PostProcessing.Plots_Plotly      as PLP
import PostProcessing.Exports           as EX
import PostProcessing.Errors            as EE


# ==============================================================================
# Post processing automatic error and convergence routines
# ==============================================================================

#### Computing the computational errors at a given time point for a given solution ####
def Compute_Errors(*argv):
	""" Exports the usually welcome plots after computation, in Matplotlib. The inputs should be given in the
	below given order.

	Args:
		ProblemName  (string):                      name of the problem module to investigate, stored in the "PredefinedProblems" folder (see the problem definition's instructions for more details)
		Settings     (string):                      name of the settings file specifying the scheme, time options etc to use for the study, stored in the "PredefinedSettings" folder
		Type         (integer):                     where to compute the errors (0: on veertices, 1: on degrees of freedom)

	Returns:
		None: The solution's matplotlib plots for each time step given in :code:`time`.

	|


	The automatically exported plots (called plotting routines) for the fluid's selector are:

		- | PlotConvergenceRate

	The automatically exported error values (called error routines) are

		- | ComputeErrors (automatically called through PlotConvergenceRate)
	"""

	# ---------------------- Checking the user input -------------------------------------
	# Checking the validity of the user input
	if len(argv) < 3:
		raise ValueError("""
					Not enough arguments have been given.
    	 				The problem cannot be initialised.
    					Check the given problem inputs. Abortion.""")
	elif not (type(argv[0])==str and type(argv[1])==str):
		raise ValueError("""
					Wrong type of input argument.
    					The problem cannot be initialised.
    					Check the given problem inputs. Abortion.
    					""")


	# -----------  Load the problem's definition and solver's attributes ----------------
	# Register the name of the test for export purposes
	ParametersId = argv[0]+"_"+argv[1]

	# Get the available resolutions by filtering out the folder names in the export results
	lst         = glob.glob(os.path.join(Pathes.SolutionPath, ParametersId, "Res*"))
	Resolutions = np.sort([int(FolderName.split("Res")[1]) for FolderName in lst])

	# Call the routine to export the errors and errors plots
	EE.PlotConvergenceRate(argv[0], ParametersId,  argv[2], Resolutions, argv[3])


# ==============================================================================
# Post processing solution format conversion routines
# ==============================================================================
#### Converting previous results to VTK format ####
def Results_ConvertToVTK(*argv):
	""" Converts a solution exported in .sol to a classical vtk output

	Args:
		ProblemName  (string):                      name of the problem module to investigate, stored in the "PredefinedProblems" folder (see the problem definition's instructions for more details)
		Settings     (string):                      name of the settings file specifying the scheme, time options etc to use for the study, stored in the "PredefinedSettings" folder
		time         (float array-like or string):  time points for which to process the plot, or "all"
		Resolution   (integer):                     mesh resolution

	Returns:
		None: Saves the converted solution for each time step given in :code:`time` in the same folder as targetted from the inputs.

	|
	"""

	# ---------------------- Checking the user input -------------------------------------
	# Checking the validity of the user input
	if len(argv) < 4:
		raise ValueError("""
					Not enough arguments have been given.
    	 				The problem cannot be initialised.
    					Check the given problem inputs. Abortion.""")
	elif not (type(argv[0])==str and type(argv[1])==str and type(argv[3])==int):
		raise ValueError("""
					Wrong type of input argument.
    					The problem cannot be initialised.
    					Check the given problem inputs. Abortion.
    					""")


	# -----------  Load the problem's definition and solver's attributes ----------------
	# Register the name of the test for export purposes
	ParametersId = argv[0]+"_"+argv[1]

	# Get the time stamps for which to generate the solutions' snapshots
	if argv[2]=="all":
		lst   = glob.glob(os.path.join(Pathes.SolutionPath, ParametersId, "Res"+str(argv[3]).replace(".","_"), "RawResults","*.sol"))
		times = np.sort([float(FileName[-12:-4].replace("_",".")) for FileName in lst])
		if times.size==0: print("Error: No previous results exported. Pass.")
	else:
		times = argv[2]
		lst   = np.array([len(glob.glob(os.path.join(Pathes.SolutionPath, ParametersId, "Res"+str(argv[3]).replace(".","_"), "RawResults","Solution_*t{0:8.6f}".format(tt).replace(".","_")+".sol"))) for tt in times])
		if np.any(lst==0): raise ValueError("Error: at least one of the wished time is not exported. Pass.")

	# Check the type of solution export
	Settings    = open(os.path.join(Pathes.SolutionPath, ParametersId, "Res"+str(argv[3]).replace(".","_"), "RawResults","UsedSettings.txt"), "r")
	LightExport = int(re.split(" |\t", Settings.readlines()[23])[0])
	Settings.close()

	# Load the mesh once for all
	if len(times)>0: Mesh, ProblemData, Problem, Solution = EX.LoadSolution(LightExport, ParametersId, argv[3], times[0], 1)

	# Loop on the wished times
	for t in times:
		# Load the solution
		ProblemData, Problem, Solution = EX.LoadSolution(LightExport, ParametersId, argv[3], t, 0)

		# Convert the solution
		for VariableId in range(Problem.GoverningEquations.NbVariables):
			EX.ExportSolutionVTK(ParametersId, Mesh, ProblemData, Problem, Solution)


# ==============================================================================
# Post processing automatic plotting routines
# ==============================================================================

#### Plotting with Matplotlib 2D schlieren images ####
def Plots_StaticSchlieren(*argv):
	""" Exports the usually welcome plots after computation, in Matplotlib. The inputs should be given in the
	below given order.

	Args:
		ProblemName  (string):                      name of the problem module to investigate, stored in the "PredefinedProblems" folder (see the problem definition's instructions for more details)
		Settings     (string):                      name of the settings file specifying the scheme, time options etc to use for the study, stored in the "PredefinedSettings" folder
		time         (float array-like or string):  time points for which to process the plot, or "all"
		Resolution   (integer):                     mesh resolution

	Returns:
		None: The solution's matplotlib plots for each time step given in :code:`time`.

	|

	The automatically exported plots (called plotting routines) for this function are for the selected variable:

		-	| Compute2DSchlierenImages

	.. note::

		For speedup, we assume that the mesh is the same for every time point. Change line 174 to be as 169 if wished otherwise
	"""

	# ---------------------- Checking the user input -------------------------------------
	# Checking the validity of the user input
	if len(argv) < 4:
		raise ValueError("""
					Not enough arguments have been given.
    	 				The problem cannot be initialised.
    					Check the given problem inputs. Abortion.""")
	elif not (type(argv[0])==str and type(argv[1])==str and type(argv[3])==int):
		raise ValueError("""
					Wrong type of input argument.
    					The problem cannot be initialised.
    					Check the given problem inputs. Abortion.
    					""")


	# -----------  Load the problem's definition and solver's attributes ----------------
	# Register the name of the test for export purposes
	ParametersId = argv[0]+"_"+argv[1]

	# Get the time stamps for which to generate the solutions' snapshots
	if argv[2]=="all":
		lst   = glob.glob(os.path.join(Pathes.SolutionPath, ParametersId, "Res"+str(argv[3]).replace(".","_"), "RawResults","*.sol"))
		times = np.sort([float(FileName[-12:-4].replace("_",".")) for FileName in lst])
		if times.size==0: print("Error: No previous results exported. Pass.")
	else:
		times = argv[2]
		lst   = np.array([len(glob.glob(os.path.join(Pathes.SolutionPath, ParametersId, "Res"+str(argv[3]).replace(".","_"), "RawResults","Solution_*t{0:8.6f}".format(tt).replace(".","_")+".sol"))) for tt in times])
		if np.any(lst==0): raise ValueError("Error: at least one of the wished time is not exported. Pass.")

	# Check the type of solution export
	Settings    = open(os.path.join(Pathes.SolutionPath, ParametersId, "Res"+str(argv[3]).replace(".","_"), "RawResults","UsedSettings.txt"), "r")
	LightExport = int(re.split(" |\t", Settings.readlines()[23])[0])
	Settings.close()

	# Load the mesh once for all
	if len(times)>0: Mesh, ProblemData, Problem, Solution = EX.LoadSolution(LightExport, ParametersId, argv[3], times[0], 1)

	# Loop on the wished times
	for t in times:
		# Load the solution
		ProblemData, Problem, Solution = EX.LoadSolution(LightExport, ParametersId, argv[3], t, 0)

		# Export the schlieren images
		for VariableId in range(Problem.GoverningEquations.NbVariables):
			PSL.Compute2DSchlierenImages(Problem, Mesh, Solution, VariableId, ParametersId)
			PSL.Compute2DSchlierenImagesPrimary(Problem, Mesh, Solution, VariableId, ParametersId)

#### Plotting with Matplotlib static visualisation tools ####
def Plots_StaticMatplotlib(*argv):
	""" Exports the usually welcome plots after computation, in Matplotlib. The inputs should be given in the
	below given order.

	Args:
		ProblemName  (string):                      name of the problem module to investigate, stored in the "PredefinedProblems" folder (see the problem definition's instructions for more details)
		Settings     (string):                      name of the settings file specifying the scheme, time options etc to use for the study, stored in the "PredefinedSettings" folder
		time         (float array-like or string):  time points for which to process the plot, or "all"
		Resolution   (integer):                     mesh resolution

	Returns:
		None: The solution's matplotlib plots for each time step given in :code:`time`.

	|

	The automatically exported plots (called plotting routines) for this function are for each variable:

		-	| PlotVariableOnWholeDomainTri2D
		-	| PlotVariableOnWholeDomainTri
		-	| PlotVariableOnEachSubDomainTri2D
		-	| PlotVariableOneEachSubDomainTri
		-	| PlotVariableOnWholeDomain2D
		-	| PlotVariableOnEachSubDomain2D
		-	| PlotVariableOnWholeDomain
		-	| PlotVariableOneEachSubDomain

	The automatically exported plots (called plotting routines) for the fluid's selector are:

		- | PlotLSValuesOnWholeDomainTri
		- | PlotLSValuesTri
		- | PlotLSValuesOnWholeDomainTri
	"""

	# ---------------------- Checking the user input -------------------------------------
	# Checking the validity of the user input
	if len(argv) < 4:
		raise ValueError("""
					Not enough arguments have been given.
    	 				The problem cannot be initialised.
    					Check the given problem inputs. Abortion.""")
	elif not (type(argv[0])==str and type(argv[1])==str and type(argv[3])==int):
		raise ValueError("""
					Wrong type of input argument.
    					The problem cannot be initialised.
    					Check the given problem inputs. Abortion.
    					""")


	# -----------  Load the problem's definition and solver's attributes ----------------
	# Register the name of the test for export purposes
	ParametersId = argv[0]+"_"+argv[1]

	# Get the time stamps for which to generate the solutions' snapshots
	if argv[2]=="all":
		lst   = glob.glob(os.path.join(Pathes.SolutionPath, ParametersId, "Res"+str(argv[3]).replace(".","_"), "RawResults","*.sol"))
		times = np.sort([float(FileName[-12:-4].replace("_",".")) for FileName in lst])
		if times.size==0: print("Error: No previous results exported. Pass.")
	else:
		times = argv[2]
		lst   = np.array([len(glob.glob(os.path.join(Pathes.SolutionPath, ParametersId, "Res"+str(argv[3]).replace(".","_"), "RawResults","Solution_*t{0:8.6f}".format(tt).replace(".","_")+".sol"))) for tt in times])
		if np.any(lst==0): raise ValueError("Error: at least one of the wished time is not exported. Pass.")

	# Check the type of solution export
	Settings    = open(os.path.join(Pathes.SolutionPath, ParametersId, "Res"+str(argv[3]).replace(".","_"), "RawResults","UsedSettings.txt"), "r")
	LightExport = int(re.split(" |\t", Settings.readlines()[23])[0])
	Settings.close()

	# Load the mesh once for all
	if len(times)>0: Mesh, ProblemData, Problem, Solution = EX.LoadSolution(LightExport, ParametersId, argv[3], times[0], 1)

	# Loop on the wished times
	for t in times:
		# Load the solution
		ProblemData, Problem, Solution = EX.LoadSolution(LightExport, ParametersId, argv[3], t, 0)

		# Batch the plotting routines
		for VariableId in range(Problem.GoverningEquations.NbVariables):
			#PMP.PlotVariableOnWholeDomainTri2D     (Problem, Mesh, Solution, VariableId, ParametersId)
			#PMP.PlotVariableOnEachSubDomainTri2D   (Problem, Mesh, Solution, VariableId, ParametersId)
			#PMP.PlotVariableOnWholeDomainTri       (Problem, Mesh, Solution, VariableId, ParametersId)
			#PMP.PlotVariableOneEachSubDomainTri    (Problem, Mesh, Solution, VariableId, ParametersId)
			#PMP.PlotVariableOnWholeDomain2D        (Problem, Mesh, Solution, VariableId, ParametersId)
			#PMP.PlotVariableOnEachSubDomain2D      (Problem, Mesh, Solution, VariableId, ParametersId)
			#PMP.PlotVariableOnWholeDomain          (Problem, Mesh, Solution, VariableId, ParametersId)
			#PMP.PlotVariableOneEachSubDomain       (Problem, Mesh, Solution, VariableId, ParametersId)
			#PMP.PlotPrimaryVariableOnWholeDomain   (Problem, Mesh, Solution, VariableId, ParametersId)
			PMP.PlotPrimaryVariableOnWholeDomainTri(Problem, Mesh, Solution, VariableId, ParametersId)
			#PMP.PlotPrimaryVariableOneEachSubDomain(Problem, Mesh, Solution, VariableId, ParametersId)
			PMP.PlotPrimaryVariableOneEachSubDomainTri(Problem, Mesh, Solution, VariableId, ParametersId)

		PMP.PlotLSValuesOnWholeDomainTri(Problem, Mesh, Solution,  ParametersId)
		PMP.PlotLSValuesTri(Problem, Mesh, Solution, ParametersId)

#### Plotting with matpotlib some debug tools ####
def Plot_DebugMatplotlib(*argv):
	"""
	Exports the debug tools for multiphase computation, in Matplotlib (linked to the :code:`Plots_Matplotlib` module).
	The inputs should be given in the below given order.

	Args:
		ProblemName  (string):                      name of the problem module to investigate, stored in the "PredefinedProblems" folder (see the problem definition's instructions for more details)
		Settings     (string):                      name of the settings file specifying the scheme, time options etc to use for the study, stored in the "PredefinedSettings" folder
		time         (float array-like or string):  time points for which to process the plot, or "all"
		Resolution   (integer):                     mesh resolution

	Returns:
		None: The solution's matplotlib plots for each time step given in :code:`time`.

	|

	The automatically debug (called routines) for this function are:

		- | PlotFlags
		- | PlotSubdomains
		- | PlotLSubDomains
		- | PlotLSValuesTri
	"""

	# ---------------------- Checking the user input -------------------------------------
	# Checking the validity of the user input
	if len(argv) < 4:
		raise ValueError("""
					Not enough arguments have been given.
						The problem cannot be initialised.
						Check the given problem inputs. Abortion.""")
	elif not (type(argv[0])==str and type(argv[1])==str and type(argv[3])==int):
		raise ValueError("""
					Wrong type of input argument.
						The problem cannot be initialised.
						Check the given problem inputs. Abortion.
						""")


	# -----------  Load the problem's definition and solver's attributes ----------------
	# Register the name of the test for export purposes
	ParametersId = argv[0]+"_"+argv[1]

	# Get the time stamps for which to generate the solutions' snapshots
	if argv[2]=="all":
		lst   = glob.glob(os.path.join(Pathes.SolutionPath, ParametersId, "Res"+str(argv[3]).replace(".","_"), "RawResults","*.sol"))
		times = np.sort([float(FileName[-12:-4].replace("_",".")) for FileName in lst])
		if times.size==0: print("Error: No previous results exported. Pass.")
	else:
		times = argv[2]
		lst   = np.array([len(glob.glob(os.path.join(Pathes.SolutionPath, ParametersId, "Res"+str(argv[3]).replace(".","_"), "RawResults","Solution_*t{0:8.6f}".format(tt).replace(".","_")+".sol"))) for tt in times])
		if np.any(lst==0): raise ValueError("Error: at least one of the wished time is not exported. Pass.")

	# Check the type of solution export
	Settings    = open(os.path.join(Pathes.SolutionPath, ParametersId, "Res"+str(argv[3]).replace(".","_"), "RawResults","UsedSettings.txt"), "r")
	LightExport = int(re.split(" |\t", Settings.readlines()[23])[0])
	Settings.close()

	# Load the mesh once for all
	if len(times)>0: Mesh, ProblemData, Problem, Solution = EX.LoadSolution(LightExport, ParametersId, argv[3], times[0], 1)

	# Loop on the wished times
	for t in times:
		# Load the solution
		ProblemData, Problem, Solution = EX.LoadSolution(LightExport, ParametersId, argv[3], t, 0)

		# Batch the plotting routines
		PMP.PlotFlags(Problem, Mesh, Solution,  ParametersId)
		PMP.PlotSubdomains(Problem, Mesh, Solution, ParametersId)
		PMP.PlotLSubDomains(Problem, Mesh, Solution, ParametersId)
		PMP.PlotLSValuesTri(Problem, Mesh, Solution, ParametersId)

#### Plotting with plotly static visualisation tools ####
def Plots_StaticPlotly(*argv):
	""" Exports the usually welcome plots after computation, in plotly (linked to the :code:`Plots_Plotly` module).
	The inputs should be given in the below given order.

	Args:
		ProblemName  (string):                      name of the problem module to investigate, stored in the "PredefinedProblems" folder (see the problem definition's instructions for more details)
		Settings     (string):                      name of the settings file specifying the scheme, time options etc to use for the study, stored in the "PredefinedSettings" folder
		time         (float array-like or string):  time points for which to process the plot, or "all"
		Resolution   (integer):                     mesh resolution

	Returns:
		None: The solution's matplotlib plots for each time step given in :code:`time`.

	|

	The automatically exported plots (called plotting routines) for this function are for each variable:

		- | PlotSolution2DOnEachSubDomain
		- | PlotSolution2D
		- | PlotLevelSetOnEachSubDomain
		- | PlotSolutionOnEachSubdomain
		- | PlotSolution


	The automatically exported plots (called plotting routines) for the fluid's selector are:

		- | PlotEachLevelSet
		- | PlotEachLevelSetTri
		- | PlotAllLevelSets
	"""

	# ---------------------- Checking the user input -------------------------------------
	# Checking the validity of the user input
	if len(argv) < 4:
		raise ValueError("""
						Not enough arguments have been given.
		 				The problem cannot be initialised.
						Check the given problem inputs. Abortion.""")
	elif not (type(argv[0])==str and type(argv[1])==str and type(argv[3])==int):
		raise ValueError("""
						Wrong type of input argument.
						The problem cannot be initialised.
						Check the given problem inputs. Abortion.
						""")


	# -----------  Load the problem's definition and solver's attributes ----------------
	# Register the name of the test for export purposes
	ParametersId = argv[0]+"_"+argv[1]

	# Get the time stamps for which to generate the solutions' snapshots
	if argv[2]=="all":
		lst   = glob.glob(os.path.join(Pathes.SolutionPath, ParametersId, "Res"+str(argv[3]).replace(".","_"), "RawResults","*.sol"))
		times = np.sort([float(FileName[-12:-4].replace("_",".")) for FileName in lst])
		if times.size==0: print("Error: No previous results exported. Pass.")
	else:
		times = argv[2]
		lst   = np.array([len(glob.glob(os.path.join(Pathes.SolutionPath, ParametersId, "Res"+str(argv[3]).replace(".","_"), "RawResults","Solution_*t{0:8.6f}".format(tt).replace(".","_")+".sol"))) for tt in times])
		if np.any(lst==0): raise ValueError("Error: at least one of the wished time is not exported. Pass.")

	# Check the type of solution export
	Settings    = open(os.path.join(Pathes.SolutionPath, ParametersId, "Res"+str(argv[3]).replace(".","_"), "RawResults","UsedSettings.txt"), "r")
	LightExport = int(re.split(" |\t", Settings.readlines()[23])[0])
	Settings.close()

	# Load the mesh once for all
	if len(times)>0: Mesh, ProblemData, Problem, Solution = EX.LoadSolution(LightExport, ParametersId, argv[3], times[0], 1)

	# Loop on the wished times
	for t in times:
		# Load the solution
		ProblemData, Problem, Solution = EX.LoadSolution(LightExport, ParametersId, argv[3], t, 0)

		# Generating the batch plots
		PLP.PlotEachLevelSet(Mesh, Solution, ParametersId)
		PLP.PlotEachLevelSetTri(Mesh, Solution, ParametersId)
		PLP.PlotAllLevelSets(Mesh, Solution, ParametersId)

		for VariableId in range(Problem.GoverningEquations.NbVariables):
			PLP.PlotSolution2DOnEachSubDomain(Problem, Mesh, Solution, VariableId, ParametersId)
			PLP.PlotSolution2D(Problem, Mesh, Solution, VariableId, ParametersId)
			PLP.PlotLevelSetOnEachSubDomain(Problem, Mesh, Solution, ParametersId)
			PLP.PlotSolutionOnEachSubdomain(Problem, Mesh, Solution, VariableId, ParametersId, [np.min(Solution.Sol[VariableId,:]), np.max(Solution.Sol[VariableId,:])])
			PLP.PlotSolution(Problem, Mesh, Solution, VariableId, ParametersId)

#### Plotting with plotly static visualisation tools ####
def Plots_AnimationsPlotly(*argv):
	"""
	Exports the usually welcome animation after computation, in Plotly. The inputs should be given in the
	below given order.

	Args:
		ProblemName  (string):                      name of the problem module to investigate, stored in the "PredefinedProblems" folder (see the problem definition's instructions for more details)
		Settings     (string):                      name of the settings file specifying the scheme, time options etc to use for the study, stored in the "PredefinedSettings" folder
		Resolution   (integer):                     mesh resolution

	Returns:
		None: The solution's plotly plot as html animation from all the raw files that have been exported in the results folder.

	|

	The automatically exported animations (called plotting routines) for this function are for each variable:

		-  | Animate3DSolution
		-  | Animate3DLevelSet
		-  | Animate2DLevelSet (very slow, visualisation time slow as well, commented hardly by default)
	"""

	# ---------------------- Checking the user input -------------------------------------
	# Checking the validity of the user input
	if len(argv) < 3:
		raise ValueError("""
						Not enough arguments have been given.
		 				The problem cannot be initialised.
						Check the given problem inputs. Abortion.""")
	elif not (type(argv[0])==str and type(argv[1])==str):
		raise ValueError("""
						Wrong type of input argument.
						The problem cannot be initialised.
						Check the given problem inputs. Abortion.
						""")

	# -----------  Load the problem's definition and solver's attributes ----------------
	# Register the name of the test for export purposes
	TestName = argv[0]+"_"+argv[1]

	# Produce the animation
	#PLP.Animate2DLevelSet(TestName, argv[2])  # (very slow, visualisation time slow as well)
	PLP.Animate3DSolution(TestName, argv[2])
	PLP.Animate3DLevelSet(TestName, argv[2])
