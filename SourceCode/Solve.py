#***************************************************************************
# Copyright (C) 2020 Élise Le Mélédo
# This file is part of LESARD.
#
# LESARD is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LESARD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LESARD.  If not, see <https://www.gnu.org/licenses/>.
#****************************************************************************

"""

Main file that solves a prefefined mutliphase problem


.. rubric:: Usage

- Create first a problem in a given module (follow the problem generation docs)
- Create or use a prefefined mesh for which this problem should be solved (follow the meshing docs)
- Solve your problem in a python environment by loading this module and simply running

	..code ::

		Main.Solve(ProblemDefinition, SettingsChoice, MeshResolution)

.. note::

	- If you have any "module not find" error, check the data given in the file "Pathes" at the root of the folders, that corresponds to all the necessary here-defined modules
	- The postprocessing is not automtic, you have to do it separately (follow the postprocessing docs)

----------------------------------------------------------------------------------------------------------"""


# ==============================================================================
#	Preliminary imports
# ==============================================================================
# Import Python os-mapping libraries and set the runtime error behavior
import sys, os, glob, re

# Custom the system traceback and allow the Pathes to be imported when this file is ran
# from another location than its storage.
sys.path.append(os.path.dirname(__file__))
sys.tracebacklimit = None

# Import mathy library
import numpy as np
import math

# Add to python path the location of the modules and meshes
import IncludePathes
import Pathes

# Import problem modules
import PredefinedTests.PredefinedProblems as Prob

# Import local modules
import PostProcessing.Printouts   as PO
import PostProcessing.Exports     as EX
import Meshing
import _Solver

# ==============================================================================
# Actual solving routine
# ==============================================================================
def Solve(*argv):
	"""
	Solving routine that considers a problem, a mesh resolution and a solver settings set.
	Inputs (in that order):

	Args:

		ProblemName  (string):  name of the problem module to investigate, stored in the "PredefinedProblems" folder (see the problem definition's instructions for more details)
		Settings     (string):  name of the settings file specifying the scheme, time options etc to use for the study, stored in the "PredefinedSettings" folder
		Resolution   (integer): the resolution of the mesh to be used (defined as in the Fenics' library. A corresponding list of max element's size can be found in the root folder of this code)

	Returns:
		None: The solution's file in both Numpy binary format and text format for each time step at the intervals given in the Settings' file. The post-processing of the solution is not automatically performed.
	"""


	# ---------------------- Checking the user input -------------------------------------
	print("\n -------------------------------------\
	      \n Checking the wished input parameters  \
		  \n -------------------------------------")
	
	print("Checking the validity of the type of given arguments")
	# Checking the validity of the user input
	if len(argv) < 3:
		raise ValueError("""
						Not enough arguments have been given.
		 				The problem cannot be initialised.
						Check the given problem inputs. Abortion.""")
	elif not (type(argv[0])==str and type(argv[1])==str and type(argv[2])==int):
		raise ValueError("""
						Wrong type of input argument.
						The problem cannot be initialised.
						Check the given problem inputs. Abortion.
						""")
	print("--> Done.")
	
	# Check if the user wishes a restart from a previously computed solution
	print("Checking the validity of the restart option if given.")
	restart = False
	if len(sys.argv)>1 and sys.argv[1]=="restart":
		# Get the available stored time stamps
		lst = glob.glob(os.path.join(Pathes.SolutionPath, argv[0]+"_"+argv[1], "Res"+str(argv[2]).replace(".","_"), "RawResults","*.sol"))
		if len(lst)==0: print("--> WARNING: No previous solution found despite a restart option. Starting from scratch.")
		else: restart = True 
	print("--> Done.")
	
	
	# -----------  Load the problem's definition and solver's attributes ----------------
	print("\n -------------------------------------\
	      \n Performing the pre-computations tasks\
		  \n -------------------------------------")
	
	# Register the name of the test for export purposes
	TestName = argv[0]+"_"+argv[1]

	# If no restart option or starting from scratch, define the solver specifications
	if restart == False:
		# Clean all previously computed solution within the destination folder
		print("Cleaning the export environment")
		EX.CleanExportEnvironment(TestName, argv[2])
		print("--> Done.")

		# Get the problem definition and the solver data from the use wishes (load info)
		print("Reading the problem specifications")
		ProblemData  = eval("Prob."+argv[0]+".ProblemData()")
		SolverParams = _Solver.Solver_Initialisations.Parameters(argv[1])
		print("--> Done.")

		# Loading the mesh depending on the Solver's requirements
		print("Loading the associated mesh")
		Mesh = Meshing.Meshing_DualMesh.DualMesh(ProblemData.MeshName,  SolverParams.MeshOrder,\
				SolverParams.MeshType, argv[2]).Mesh
		print("--> Done.")

		# Copy the new running settings in the results folder
		print("Export the used settings and mesh in the solution folder")
		EX.ExportSettings(TestName, argv[1], argv[2])
		EX.ExportUsedMesh(TestName, Mesh)
		print("--> Done.")

	# ----------- Perform the initialisation tasks ------------------------------------------------
	# If no restart option or starting from scratch, initialise everything
	if restart == False:
		# Initialise the variables to work with shortcut the mapped scheme routines
		print("Initialising the problem")
		print("--> Setting the problem up")
		Problem  = _Solver.Solver_Initialisations.Problem(ProblemData, Mesh)
		print("--> Setting the solver up")
		Solver   = _Solver.Solver_Initialisations.Solver(Problem, Mesh, SolverParams)
		print("--> Initialising the solution")
		Solution = _Solver.Solver_Initialisations.Solution(Problem, Mesh)
		print("--> Done.")

		# Generating the mass lumping coefficients
		print("Generating the mass lumping coefficients")
		_Solver.Solver_Temporal.GetLumpingCoefficients(Mesh, Solver)
		print("--> Done.")

		# Initialise the level set on the data at the Dof points
		print("Initialising the Fluid selector")
		Solution.LSValues = Solver.FluidSelector.Initialiser(Solution)
		print("--> Done.")
		
		# Initialising the simple running time and the export periodicity index
		Solution.t = 0
		lastexport = 0
	
	# If the restart option has been selected, casually load the previously computed solution and solver
	else:
		# Selecting the time point where to start from
		print("Selecting the last time point of the previously exported solution to start from.")
		times = np.sort([float(FileName[-12:-4].replace("_",".")) for FileName in lst])
		print("--> Done.")
		
		# Check the type of solution export to load the solution accodingly
		print("Loading the solver settings from which the previous solution has been computed.")
		SettingsFile = os.path.join(Pathes.SolutionPath, argv[0]+"_"+argv[1], "Res"+str(argv[2]).replace(".","_"), "RawResults","UsedSettings.txt")
		Settings     = open(SettingsFile, "r")
		LightExport  = int(re.split(" |\t", Settings.readlines()[23])[0]); Settings.close()
		print("--> Done.")
		
		print("Loading the previous solution to start from.")
		Mesh, ProblemData, Problem, Solution = EX.LoadSolution(LightExport, argv[0]+"_"+argv[1], argv[2], times[-1], 1)
		print("--> Done.")
		
		# Redefining the solver on the same set of wished parameters as the previous export's settings
		print("Redefining the solver according to the previous export's settings.")
		SolverParams = _Solver.Solver_Initialisations.Parameters(SettingsFile)
		Solver   = _Solver.Solver_Initialisations.Solver(Problem, Mesh, SolverParams)
		
		print("Generating the mass lumping coefficients")
		_Solver.Solver_Temporal.GetLumpingCoefficients(Mesh, Solver)
		print("--> Done.")

		# Initialising the simple running time and the export periodicity index
		lastexport = 0

	# Informing the user back on the registered problem parameters
	PO.PrintPbInfo(Mesh, Problem, ProblemData, Solver, Solution)


	# ----------- Iterate in time the solution -----------------------------
	print("\n -------------------------------------\
	       \n Starting time dependent computations \
	       \n -------------------------------------")

	# Initialising the export periodicity index
	k = 0

	# Export the initial considition after having reconstructed the initial values at the physical vertices
	for Reconstructor in Solver.Reconstructor: Reconstructor(Solution)
	EX.ExportSolution(SolverParams.LightExport, TestName, Mesh, ProblemData, Problem, Solution)

	# Inform the user
	if Solution.t < Solver.Tmax: PO.Printout_MinMaxSolVal_Primary(Problem, Solution)
	
	# Main time loop
	while Solution.t < Solver.Tmax:

		# ----- Perform a time step
		# Get the time step and increase the step counter
		dt = _Solver.Solver_Temporal.GetTimeStep(Solver.CFL, Solver.Tmax, Problem, Mesh, Solution)
		k = k+1

		# One time iteration taking as parameter the spatial schemes
		schemeSC = [Solver.SpatialScheme, Solver.SPAdmendements]
		schemeFS = [Solver.FluidSelector]
		Solution.Sol, Solution.LSValues = Solver.TimeStep(schemeSC, schemeFS, dt, Problem, Mesh, Solution)

		# ----- Perform the subsidiary updates
		# Increase the current time
		Solution.t += dt

		# Redistance the level set if the user wishes and the periodicity is matched
		if np.mod(k, Solver.Redistancer.RedistanciationPeriod) == 0: Solution.LSValues = Solver.Redistancer.Redistance(Solution)

		# Reconstruct the solution at the physical vertices of the mesh (just impacting the visualisation)
		for Reconstructor in Solver.Reconstructor: Reconstructor(Solution)

		# ----- Inform the user and export the solution when relevant
		# May yield to duplicate if the time step is less than 1e-11
		if (Solution.t-lastexport >= SolverParams.ExportInterval) or np.isclose(Solution.t, Solver.Tmax):
			lastexport = Solution.t
			PO.Printout_MinMaxSolVal_Primary(Problem, Solution)
			EX.ExportSolution(SolverParams.LightExport, TestName, Mesh, ProblemData, Problem, Solution)
		
		else: print("Computing for the time {0!s}".format(Solution.t))

	# ------------------ Closing tasks -----------------------------------------
	print("\n -------------------------------------\
		   \n Performing post-computations tasks   \
		   \n -------------------------------------")

	# User information
	print("Computation ended normally. Results exported in: Solutions/" + TestName)
	print("--> Done.")
