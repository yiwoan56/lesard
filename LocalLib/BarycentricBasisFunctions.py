#***************************************************************************
# Copyright (C) 2020 Élise Le Mélédo
# This file is part of LESARD.
#
# LESARD is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LESARD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LESARD.  If not, see <https://www.gnu.org/licenses/>.
#****************************************************************************

# Module that implements the basis functions given a reference shape
# (its number of edges basically), the order and the barycentric
# coordinates of the considered point with respect to the given triangle

# ==============================================================================
#	Pythonic interfacing constraints (safety barrier)
# ==============================================================================
__all__ = ["BarycentricBasis", "BarycentricGradients"]

# ==============================================================================
#	Preliminary imports
# ==============================================================================
import numpy as np
import itertools

# ==============================================================================
#	Interfacing the basis functions depending on the element's type
# ==============================================================================

#### Interfacing routine for the basis function computations ####
def BarycentricBasis(Nele, Type, Order, points):
    """
    Simple mapping to the implementation giving out the barycentic basis functions according to the elment's type (simplicial, quad, ...)

    Args:
        Nele   (integer):          the number of edges of the considered element to compute the basis functions for
        Type   (string):           type of wished basis functions
        Order  (integer):          order of the wished basis functions
        points (2D numpy array):   the points at which to evaluate the basis functons (NbPoints, 3)

    Returns:
        EvaluatedBase (2D numpy array):  the value of all the basis functions at each given point (NbBasisFunc x NbPoints)
                                         through the call of the relevant function.
    """

    # ---------------- Mapping to the right function -------------------------------------------------
    # Case of a simplicial shape
    if Nele==3: return(SimplicialBasis(Type, Order, points))
    else: raise(ValueError("Error: Barycentric basis functions are not yet implemented for elements having "+str(Nele)+"edges. Abortion."))

#### Interfacing routine for the gradient computations ####
def BarycentricGradients(Nele, Type, Order, points, n, vol):
    """
    Simple mapping to the implementation giving out the barycentic basis functions gradients according to the elment's type (simplicial, quad, ...)

    Args:
        Nele   (integer):          the number of edges of the considered element to compute the basis functions for
        Type   (string):           type of wished basis functions
        Order  (integer):          order of the wished basis functions
        points (2D numpy array):   the points at which to evaluate the basis functons (NbPoints, 3)
        n      (2D numpy array):   the normal vector to each face, in the order corresponding to the vertices furnishing the barycentric coordinates referential
        vol    (float):            the volume of the element furnishing the barycentric coordinates referential

    Returns:
        EvaluatedGradient  (3D numpy array):  the value of all the x and y gradient's coordinates at each given point, ordered accordingly to the
                                              vertices from which the barycentic coordinates have been generated (NbBasisFunc x NbPoints x 2),
                                              through the call of the relevant function.
    """

    # ---------------- Mapping to the right function -------------------------------------------------
    # Case of a simplicial shape
    if Nele==3: return(SimplicialGradients(Type, Order, points, n, vol))
    else: raise(ValueError("Error: Barycentric basis functions are not yet implemented for elements having "+str(Nele)+"edges. Abortion."))


# ==============================================================================
#	Technical routines for the basis functions computations
# ==============================================================================

#### Simplicial basis function from barycentric coordinates ####
def SimplicialBasis(Type, Order, points):
    """
    Determines the values of the basis functions according to barycentric coordinates.
    The order of the basis function is given by the order of the vertices upon which are defined the points in barycentric coordinates.

    Args:
        Type   (string):           type of wished basis functions
        Order  (integer):          order of the wished basis functions
        points (2D numpy array):   the points at which to evaluate the basis functons (NbPoints, 3)

    Returns:
        EvaluatedBase (2D numpy array):  the value of all the basis functions at each given point (NbBasisFunc x NbPoints).

    .. note::

        The order of the ouput basis function is given according to the permutation list of itertools
    """

    # ---------------- Bernstein basis function -----------------------------------------------------------------
    # Generate the basis functions according to the wished order (automatically)
    if Type == "B":

        # ------ Generate the permutations
        # Generate the indices of the vertices involved in the generation of the barycentric coordinates
        BaryIndex         = np.arange(3)
        # Generate the combinations corresponding to each barycentric coordinate to select in the monomial generation
        ConsideredIndices = list(itertools.combinations_with_replacement(BaryIndex, Order))
        # Initilalising the basis function according to the number of basis functions
        EvaluatedBase     = np.zeros((points.shape[0], len(ConsideredIndices)))

        # ------ Compute the basis functions
        # Compute the basis function in their general form
        for (i,Cind) in enumerate(ConsideredIndices):
             EvaluatedBase[:, i] = (np.math.factorial(Order)/np.prod([np.math.factorial(Cind.count(e)) for e in set(Cind)]))*np.prod(points[:,Cind], axis=1)

        # ------ Reorder the basis functions
        # Map the functions to match the Barycentric coordinates initiated by the MeshStructure (Laborious but working)

        # Find the order on each edge
        Reordering = np.zeros(len(ConsideredIndices), dtype=int); Reordering[0] = 1
        Tmp1 = np.zeros(Order+1); Tmp1[0] = 1;
        for j in range(1, Order+1): Tmp1[j] = Tmp1[j-1] + j
        Tmp2 = np.zeros(Order); Tmp2[0] = 1;
        for j in range(1, Order): Tmp2[j] = Tmp2[j-1] + j+1;

        # Assigning the reordering associated to the Meshstrcture
        Reordering[0:Order+1]         = Tmp1
        Reordering[Order+1:2*Order+1] = np.arange(Reordering[Order]+1, len(ConsideredIndices)+1)
        Reordering[2*Order+1:3*Order] = Tmp2[::-1][:-1]

        # Assigning the reordering associated to the internal basis functions
        Index = np.setdiff1d(np.arange(len(ConsideredIndices))+1,Reordering)
        Reordering[int(3*Order):] = Index
        Reordering = Reordering-1

        # Reorder the basis function accordingly
        EvaluatedBase = EvaluatedBase[:, Reordering]

    # Safety check
    else: raise NotImplementedError("The type of wished basis function is not implemented yet. Abortion.")

    # ------------------ Return the considered basis ----------------------------------
    return(EvaluatedBase)


# ==============================================================================
#	Technical routines for the basis functions gradient computations
# ==============================================================================

#### Simplicial gradient of basis function from barycentric coordinates ####
def SimplicialGradients(Type, Order, points, n, vol):
    """
    Determines the values of the basis functions gradient according to barycentric coordinates.
    The order of the basis function is given by the order of the vertices upon which are defined the points.

    Args:
        Type   (string):           type of wished basis functions
        Order  (integer):          order of the wished basis functions
        points (2D numpy array):   the points at which to evaluate the basis functons (NbPoints, 3)
        n      (2D numpy array):   the normal vector to each face, in the order corresponding to the vertices furnishing the barycentric coordinates referential
        vol    (float):            the volume of the element furnishing the barycentric coordinates referential

    Returns:
        EvaluatedGradient  (3D numpy array):  the value of all the x and y gradient's coordinates at each given point, ordered accordingly to the vertices from which the barycentic coordinates have been generated (NbBasisFunc x NbPoints x 2).

    .. note::

        The order of the ouput basis function is given according to the permutation list of itertools
    """

    # ---------------- Geometry tweak ---------------------------------------------------------------------------
    # Flippling the orientation of the normal as required in the gradient's formula
    n = -n

    # ---------------- Bernstein basis function -----------------------------------------------------------------
    # Generate the basis functions according to the wished order (automatically)
    if Type == "B":

        # ------ Generate the permutations
        # Generate the indices of the vertices involved in the generation of the barycentric coordinates
        BaryIndex         = np.arange(3)
        # Generate the combinations corresponding to each barycentric coordinate to select in the monomial generation
        ConsideredIndices = list(itertools.combinations_with_replacement(BaryIndex, Order))
        # Initilalising the gradient function and the pregradient help tool buffer according to the number of basis functions
        EvaluatedGradient = np.zeros((points.shape[0], len(ConsideredIndices), 3))
        Grad              = np.zeros((points.shape[0], 2, len(ConsideredIndices)))

        # ------ Compute the components involved in each gradient
        # Compute the derivatives of the basis function in their general form
        for (j,Cind) in enumerate(ConsideredIndices):
            for k in range(3):
                Ccind = list(Cind)[:]
                if k in Ccind:
                    Ccind.remove(k)
                    EvaluatedGradient[:, j, k] = np.array([(np.math.factorial(Order)/np.prod([np.math.factorial(Ccind.count(e)) for e in set(Ccind)]))*np.prod(points[:,Ccind], axis=1)])

        # ------ Reorder the gradients functions
        # Map the functions to match the Barycentric coordinates initiated by the MeshStructure (Laborious but working)

        # Find the order on each edge
        Reordering = np.zeros(len(ConsideredIndices), dtype=int); Reordering[0] = 1
        Tmp1 = np.zeros(Order+1); Tmp1[0] = 1;
        for j in range(1, Order+1): Tmp1[j] = Tmp1[j-1] + j
        Tmp2 = np.zeros(Order); Tmp2[0] = 1;
        for j in range(1, Order): Tmp2[j] = Tmp2[j-1] + j+1;

        # Assigning the reordering associated to the Meshstrcture
        Reordering[0:Order+1]         = Tmp1
        Reordering[Order+1:2*Order+1] = np.arange(Reordering[Order]+1, len(ConsideredIndices)+1)
        Reordering[2*Order+1:3*Order] = Tmp2[::-1][:-1]

        # Assigning the reordering associated to the internal basis functions
        Index = np.setdiff1d(np.arange(len(ConsideredIndices))+1,Reordering)
        Reordering[int(3*Order):] = Index
        Reordering = Reordering-1

        # Reorder the basis function accordingly
        EvaluatedGradient = EvaluatedGradient[:, Reordering,:]

        # ------ Adapt the gradients to the geometry
        # Scale the values to the geometry
        Grad = np.dot(np.swapaxes(np.array([EvaluatedGradient[:,:,0]-EvaluatedGradient[:,:,2],
                                            EvaluatedGradient[:,:,1]-EvaluatedGradient[:,:,2]]),0,2),\
                                            n[1:,:])/(2.*vol)

    # Safety check
    else: raise NotImplementedError("The type of wished basis function is not implemented yet. Abortion.")

    # ------------------ Return the considered basis ----------------------------------
    return(Grad)
