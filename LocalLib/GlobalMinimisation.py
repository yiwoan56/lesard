"""
Implementation of the CoCaIn-BPG algorithm (Convex-Concave Backtracking for Inertial Bregman Proximal Gradient Algorithms),
suitable for non-convex problem, without proximal mapping.

Code adapted from the example given by Mahesh Chandra Mukkamala, Peter Ochs, Thomas Pock and Shoham Sabach and related
to their paper that can be found on https://arxiv.org/abs/1904.03537

.. note::

    The global optimiser may not work if initial guess is saddle point of local extrema
    The algoritm will converge to a critical point (where Grad(f)=0)

"""


# ==================================================================================
#   Preliminary imports
# ==================================================================================
# Classical library imports
import numpy as np

# ==================================================================================
#   Solver definition
# ==================================================================================
class Optimiser():

    def __init__(self):
        self.del_val = 0.15
        self.eps_val = 0.00001
        self.uL_est = 1/(1-self.del_val) + 1e-3
        self.lL_est = 1e-4*self.uL_est
        self.max_iter = 100


    # -----------------------------------------------------------------
    #   Helper functions
    # -----------------------------------------------------------------
    def breg(self, U, U1):
        temp = 0.5*(np.sum(np.multiply(U-U1,U-U1)))
        if temp >=1e-15: return temp
        else: return 0

    def abs_fun(self, fun, grad, U, U1):
        G = grad(U1)
        x,y = U[0], U[1]
        x_1, y_1 = U1[0], U1[1]
        return fun(U1)+np.sum(np.multiply(G,U-U1))

    def make_update(self, y, evalgrad, uL_est):
        temp_p = y - (1/uL_est)*evalgrad
        return temp_p

    def find_gamma(self, U, prev_U, uL_est, lL_est):
        gamma = 1
        kappa = (self.del_val - self.eps_val)*(uL_est/(uL_est+lL_est))
        y_U = U + gamma*(U-prev_U)  # extrapolation
        while (kappa*self.breg(prev_U, U)< self.breg(U, y_U)):
            gamma = gamma*0.9
            y_U = U + gamma*(U-prev_U)
        return y_U, gamma

    def do_lb_search(self, fun, grad, U, U1, uL_est, lL_est):
        y_U, gamma = self.find_gamma(U, U1, uL_est, lL_est)
        while((self.abs_fun(fun, grad, U, y_U) - fun(U) - (lL_est*self.breg(U, y_U))) > 1e-7):
            lL_est = 2*lL_est
            y_U, gamma = self.find_gamma(U, U1, uL_est, lL_est)
        return lL_est, y_U, gamma

    def do_ub_search(self, fun, grad, y_U, uL_est):
        grad_u = np.squeeze(grad(y_U))
        x_U = self.make_update(y_U, grad_u, uL_est)
        while((self.abs_fun(fun, grad, x_U, y_U)- fun(x_U)+ (uL_est*self.breg(x_U, y_U))) < -1e-7):
            uL_est = (2)*uL_est
            x_U = self.make_update(y_U, grad_u, uL_est)
        return uL_est, x_U


    # -----------------------------------------------------------------
    #   Main optimisation routine
    # -----------------------------------------------------------------

    def Optimise(self, fun, grad, U0, Point, radius):
        uL_est = self.uL_est
        lL_est = self.lL_est
        U = U0
        init_U = U
        prev_U = U
        tmpU = U+1
        gamma_vals = [0]
        uL_est_vals = [uL_est]
        lL_est_vals = [uL_est]
        temp = fun(U)
        func_vals = [temp]
        lyapunov_vals = [temp]
        U_vals = [init_U]
        k=0
        while np.any(abs(U-tmpU)>1e-8) and k<10:
            k=k+1
            lL_est, y_U, gamma = self.do_lb_search(fun, grad, U, prev_U,  uL_est, lL_est)
            prev_U = U
            tmpU=U
            uL_est, U = self.do_ub_search(fun, grad, y_U, uL_est)
            temp = fun(U)
            uL_est_vals = uL_est_vals + [uL_est]
            lL_est_vals = lL_est_vals + [lL_est]
            gamma_vals = gamma_vals   + [gamma]
            U_vals = U_vals + [U]
            if np.isnan(temp): return(prev_U)
            InnerCircle = np.linalg.norm(U-Point)
            if    InnerCircle<=radius: U= U
            else: U = Point+radius*(U-Point)/InnerCircle
            func_vals = func_vals + [temp]
            lyapunov_vals = lyapunov_vals + [(1/uL_est)*temp + self.del_val*self.breg(U, prev_U)]
        return(U)
