#***************************************************************************
# Copyright (C) 2020 Élise Le Mélédo
# This file is part of LESARD.
#
# LESARD is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LESARD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LESARD.  If not, see <https://www.gnu.org/licenses/>.
#****************************************************************************

"""
.. |bs|   unicode:: U+00A0 .. NO-BREAK SPACE

|

Module that computes the solution of any equation of the type \frac{\partial \phi}{\partial t} + H(\phi, t) = 0 over
a given Cartesian domain.

It implements the WENO scheme for Cartesian grids for HJB equations, following the work of Peng and Jian,
"Weighted ENO schemes for HJB equations, PII: S106482759732455X", and uses a simple forward euler method in time.

"""

# ==============================================================================
#	Preliminary imports
# ==============================================================================
import copy
import numpy as np


import matplotlib.pyplot as plt
# ==============================================================================
#	WENO tools
# ==============================================================================
# --- Interate the equation (7) until the BregmanInitTime by the Weno 5th order scheme for the residuals, and Euler explicit in time (one iteration)
# Defining a consistant flux with H, here Global LFX
def Hh(dxP, dxM, dyP, dyM,  H, Hx, Hy):
	alph = np.max([np.max(Hx(dxP, dyP)), np.max(Hx(dxP, dyM)), np.max(Hx(dxM, dyP)), np.max(Hx(dxM, dyM))])
	bet  = np.max([np.max(Hx(dxP, dyP)), np.max(Hx(dxP, dyM)), np.max(Hx(dxM, dyP)), np.max(Hx(dxM, dyM))])
	Val  = H(0.5*(dxP+dxM), 0.5*(dyP+dyM)) - alph*(0.5*(dxP-dxM)) - bet*(0.5*(dyP-dyM))
	return(Val)

# Defining the Weno approximation function
def W(a,b,c,d, eps):

	alph0 = 1/((eps+13*(a-b)**2+3*(a-3*b)**2)**2)
	alph1 = 6/((eps+13*(b-c)**2+3*(b+c)**2)**2)
	alph2 = 3/((eps+13*(c-d)**2+3*(3*c-d)**2)**2)

	w0 = alph0/(alph0+alph1+alph2)
	w2 = alph2/(alph0+alph1+alph2)

	return(1/3*w0*(a-2*b+c)+1/6*(w2-0.5)*(b-2*c+d))

# ==============================================================================
#	Actual routine
# ==============================================================================
def Iterate(xx, yy, InitVal, Tmax, cfl, eps, H, Hx, Hy):
	# Retrieving the grid size
	DeltaX = np.abs(xx[1]-xx[0])
	DeltaY = np.abs(yy[1]-yy[0])

	# Initialising the iterated value
	Val  = copy.deepcopy(InitVal[:,:])
	t=0
	while t<Tmax:


		# Registering the buffer term to impose boundary conditions
		Buff  = copy.deepcopy(Val[:,:])

		# Computing the differences
		DeltaxPPhi = np.zeros(Val.shape)
		DeltaxMPhi = np.zeros(Val.shape)
		DeltayPPhi = np.zeros(Val.shape)
		DeltayMPhi = np.zeros(Val.shape)

		DeltaxPPhi[:,:-1] = Val[:, 1:] - Val[:, :-1]
		DeltaxMPhi[:, 1:] = Val[:, 1:] - Val[:, :-1]
		DeltayPPhi[:-1,:] = Val[1:,:]  - Val[:-1, :]
		DeltayMPhi[1:, :] = Val[1:,:]  - Val[:-1, :]

		DDeltaxMPPhi = np.zeros(InitVal.shape)
		DDeltayMPPhi = np.zeros(InitVal.shape)
		DDeltaxMPPhi[:,1:] = DeltaxPPhi[:,1:]-DeltaxPPhi[:,:-1]
		DDeltayMPPhi[1:,:] = DeltayPPhi[1:,:]-DeltayPPhi[:-1,:]

		# Approximating the derivatives
		dxPphi = np.zeros(InitVal.shape)
		dyPphi = np.zeros(InitVal.shape)
		dxMphi = np.zeros(InitVal.shape)
		dyMphi = np.zeros(InitVal.shape)

		dxPphi[:,2:-2] = 1/(12*DeltaX)*(-DeltaxPPhi[:,:-4]+7*DeltaxPPhi[:,1:-3]+7*DeltaxPPhi[:,2:-2]-DeltaxPPhi[:,3:-1])\
		                 +W(DDeltaxMPPhi[:,4:]/DeltaX, DDeltaxMPPhi[:,3:-1]/DeltaX, DDeltaxMPPhi[:,2:-2]/DeltaX, DDeltaxMPPhi[:,1:-3]/DeltaX, eps)
		dyPphi[2:-2,:] = 1/(12*DeltaY)*(-DeltayPPhi[:-4,:]+7*DeltayPPhi[1:-3,:]+7*DeltayPPhi[2:-2,:]-DeltayPPhi[3:-1,:])\
		                 +W(DDeltayMPPhi[4:,:]/DeltaY, DDeltayMPPhi[3:-1,:]/DeltaY, DDeltayMPPhi[2:-2,:]/DeltaY, DDeltayMPPhi[1:-3,:]/DeltaY, eps)
		dxMphi[:,2:-2] = 1/(12*DeltaX)*(-DeltaxPPhi[:,:-4]+7*DeltaxPPhi[:,1:-3]+7*DeltaxPPhi[:,2:-2]-DeltaxPPhi[:,3:-1])\
		                 -W(DDeltaxMPPhi[:,:-4]/DeltaX, DDeltaxMPPhi[:,1:-3]/DeltaX, DDeltaxMPPhi[:,2:-2]/DeltaX, DDeltaxMPPhi[:,3:-1]/DeltaX, eps)
		dyMphi[2:-2,:] = 1/(12*DeltaY)*(-DeltayPPhi[:-4,:]+7*DeltayPPhi[1:-3,:]+7*DeltayPPhi[2:-2,:]-DeltayPPhi[3:-1,:])\
		                 -W(DDeltayMPPhi[:-4,:]/DeltaY, DDeltayMPPhi[1:-3,:]/DeltaY, DDeltayMPPhi[2:-2,:]/DeltaY, DDeltayMPPhi[3:-1,:]/DeltaY, eps)

		# Compute the time step due to cfl
		lbd = np.max([np.max(Hx(dxPphi[2:-2,:] , dyPphi[2:-2,:] )), np.max(Hx(dxPphi[2:-2,:] , dyMphi[2:-2,:] )), np.max(Hx(dxMphi[2:-2,:] , dyPphi[2:-2,:] )), np.max(Hx(dxMphi[2:-2,:] , dyMphi[2:-2,:] ))])
		dt = cfl*np.max([DeltaX,DeltaY])/lbd
		if (Tmax<t+dt): dt = Tmax-t

		# Computing the residuals and updating with straighforward Euler
		dphi = -Hh(dxPphi[2:-2, 2:-2], dxMphi[2:-2, 2:-2], dyPphi[2:-2, 2:-2], dyMphi[2:-2, 2:-2], H, Hx, Hy)
		Val[2:-2,2:-2] = Val[2:-2,2:-2] + dt*dphi
		Val[0:2,:] = Buff[0:2,:]
		Val[-2:,:] = Buff[-2:,:]
		Val[:,0:2] = Buff[:,0:2]
		Val[:,-2:] = Buff[:,-2:]
		t = t+dt

	# Retrieve the gradients of the approximation at BregmanInitTime at the grid points
	Val[0:2,:] = 0
	Val[-2:,:] =0
	Val[:,0:2] = 0
	Val[:,-2:] = 0

	dxVal = 0.5*(dxPphi+dxMphi)
	dyVal = 0.5*(dyPphi+dyMphi)

	# Plot the redistanced level set
	return(Val, dxVal, dyVal)
